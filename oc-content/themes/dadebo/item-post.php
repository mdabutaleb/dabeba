<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','dadebo_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    dadebo_add_body_class('item item-post');
    $action = 'item_add_post';
    $edit = false;
    if(Params::getParam('action') == 'item_edit') {
        $action = 'item_edit_post';
        $edit = true;
    }

    ?>
<?php osc_current_web_theme_path('header.php') ; ?>
        <?php
    if (dadebo_default_location_show_as() == 'dropdown') {
        //require 'location.php';
    } else {
        //require 'location-new.php';
    }
    ?>
	<?php //ItemForm::location_javascript(); ?>
    <div class="form-container form-horizontal form-container-box item-post-width">
        <div class="resp-wrapper">
            <div class="header">
                <h1><?php _e('Publish a listing', 'dadebo'); ?></h1>
                <span class="cancel_link"><a href="<?php echo osc_base_url(); ?>">Cancel</a></span>
            </div>
            <ul id="error_list"></ul>
                <form name="item" action="<?php echo osc_base_url(true);?>" method="post" enctype="multipart/form-data" id="item-post">
                    <fieldset>
                    <input type="hidden" name="action" value="<?php echo $action; ?>" />
                        <input type="hidden" name="page" value="item" />
                    <?php if($edit){ ?>
                        <input type="hidden" name="id" value="<?php echo osc_item_id();?>" />
                        <input type="hidden" name="secret" value="<?php echo osc_item_secret();?>" />
                    <?php } ?>
                    <div class="general_info">
                        <h2><span class="section_number">1. </span><?php _e('General Information', 'dadebo'); ?></h2>
                        <div class="control-group">
                            <!--<label class="control-label" for="select_1"><?php _e('Category', 'dadebo'); ?>*</label>-->
                            <div class="controls">
                                <?php ItemForm::category_select(null, null, __('Select a category', 'dadebo')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <!--<label class="control-label" for="title[<?php echo osc_current_user_locale(); ?>]"><?php _e('Title', 'dadebo'); ?>*</label>-->
                            <div class="controls">
                                <?php // ItemForm::title_input('title',osc_current_user_locale(), osc_esc_html( dadebo_item_title() )); ?>
								<!--<input id="titleen_US" type="text" name="title[<?= osc_current_user_locale() ?>]" value="<?= $_POST['title'] ?>" placeholder="<?php _e('Add a title', 'dadebo'); ?>">-->
								<?php
									ItemForm::custom_input(
										'title',
										osc_current_user_locale(),
										osc_esc_html( dadebo_item_title()),
										__('Title', 'dadebo'),
										"text"
									);
								 ?>
                            </div>
                        </div>
                        <?php if( osc_price_enabled_at_items() ) { ?>
                        <div class="control-group control-group-price">
                            <!--<label class="control-label" for="price"><?php _e('Price', 'dadebo'); ?></label>-->
                            <div class="controls ">
                                <div class="price-control">
								    <?php ItemForm::custom_price(); ItemForm::currency_select(); ?>
                                    <span>$</span>
                                </div>
                                <?php //ItemForm::price_input_text(); ?>
                                <?php //ItemForm::currency_select(); ?>
                            </div>
                        </div>
                        <?php } ?>


						<div class="control-group">
                            <!--<label class="control-label" for="description[<?php echo osc_current_user_locale(); ?>]"><?php _e('Description', 'dadebo'); ?>*</label>-->
                            <div class="controls">
                                <?php //ItemForm::description_textarea('description',osc_current_user_locale(), osc_esc_html( dadebo_item_description() )); ?>
								<!--<textarea id="descriptionen_US" name="description[en_US]" rows="4" placeholder="<?php _e('Description', 'dadebo'); ?>"></textarea>-->
								<?php
									ItemForm::custom_textarea(
										'description',
										osc_current_user_locale(),
										osc_esc_html( dadebo_item_description()),
										__('Description', 'dadebo')
									); ?>
                            </div>
                        </div>

                        <?php if( osc_images_enabled_at_items() ) {
                            ItemForm::ajax_photos();
                         } ?>

						 <div class="show-file-uploaded"></div>

                     </div>
                        <div class="box location">
                            <h2><span class="section_number" >2. </span><?php _e('Listing Location', 'dadebo'); ?></h2>
                            <?php if(count(osc_get_countries()) > 1) { ?>
							<!--<input type="hidden" id="countryId" name="countryId" value="<?php echo osc_esc_html($aCountries[0]['pk_c_code']); ?>"/>-->
                            <div class="control-group">
                                <label class="control-label" for="location"><?php _e('Location', 'dadebo'); ?>*</label>
                                <div class="controls">
                                    <?php ItemForm::country_select(osc_get_countries(), osc_user()); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="regionId"><?php _e('Region', 'dadebo'); ?><span id='symbol-region-required'>*</span></label>
                                <div class="controls">
                                    <?php
                                    /*if (dadebo_default_location_show_as() == 'dropdown') {
                                        ItemForm::region_select(osc_get_regions(osc_user_field('fk_c_country_code')), osc_user());
                                    } else {
                                        ItemForm::region_text(osc_user());
                                    }*/
                                    ?>
									<!--<input id="region" type="text" name="region" value="" placeholder="<?php _e('Region', 'dadebo'); ?>">-->
									<?php ItemForm::custom_region_text(osc_user(), __("Region", "dadebo")) ?>
                                </div>
                            </div>
                            <?php
                            } else {
                                $aCountries = osc_get_countries();
                                $aRegions = osc_get_regions($aCountries[0]['pk_c_code']);
                                ?>

                            <?php } ?>

                            <div class="control-group">
                                <label class="control-label" for="city"><?php _e('City', 'dadebo'); ?><span id='symbol-city-required'>*</span></label>
                                <div class="controls">
                                    <?php
                                    /*if (dadebo_default_location_show_as() == 'dropdown') {
                                        if(Params::getParam('action') != 'item_edit') {
                                            ItemForm::city_select(null, osc_item());
                                        } else { // add new item
                                            ItemForm::city_select(osc_get_cities(osc_user_region_id()), osc_user());
                                        }
                                    } else {
                                        ItemForm::city_text(osc_user());
                                    }*/
                                    ?>
									<!--<input id="city" type="text" name="city" value="" placeholder="<?php _e('City', 'dadebo'); ?>">-->
									<?php ItemForm::custom_city_text(osc_user(),__('City', 'dadebo')) ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <!--<label class="control-label" for="address"><?php _e('Address', 'dadebo'); ?></label>-->
                                <div class="controls">
                                  <?php //ItemForm::address_text(osc_user()); ?>
								  <!--<input id="address" type="text" name="address" value="" placeholder="<?php _e('Address', 'dadebo'); ?>">-->
								  <?php ItemForm::custom_address_text(osc_user(), __("Address", "dadebo")) ?>
                                </div>
                            </div>
                        </div>
                        <!-- seller info -->
                        <?php if(!osc_is_web_user_logged_in() ) { ?>
                        <div class="box seller_info">
                            <h2><span class="section_number" >3. </span><?php _e("Publisher Info", 'dadebo'); ?></h2>
                            <div class="control-group">
                                <!--<label class="control-label" for="contactName"><?php _e('Name', 'dadebo'); ?>*</label>-->
                                <div class="controls">
									<input id="contactoName" type="text" name="contactName" value="" placeholder="* <?php _e('Name', 'dadebo'); ?>">
                                    <?php //ItemForm::contact_name_text(); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <!--<label class="control-label" for="contactEmail"><?php _e('E-mail', 'dadebo'); ?>*</label>-->
                                <div class="controls">
									<input id="contactEmail" type="text" name="contactEmail" value="" placeholder="* <?php _e('E-mail', 'dadebo'); ?>">
                                    <?php //ItemForm::contact_email_text(); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls checkbox">
                                    <?php ItemForm::show_email_checkbox(); ?> <label for="showEmail"><?php _e('Show e-mail on the listing page', 'dadebo'); ?></label>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        if($edit) {
                            ItemForm::plugin_edit_item();
                        } else {
                            ItemForm::plugin_post_item();
                        }
                        ?>
                        <div class="control-group ui-publish-button">
                            <div class="controls">
                                <?php

                                if (strpos(osc_active_plugins(), "nocaptcha_recaptcha/index.php")) {
                                    anr_captcha_form_field();
                                }

                                ?>
                                <button type="submit" class="ui-button ui-button-middle ui-button-main"><?php if($edit) { _e("Update", 'dadebo'); } else { _e("Publish", 'dadebo'); } ?></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $('#price').bind('hide-price', function(){
                $('.control-group-price').hide();
            });

            $('#price').bind('show-price', function(){
                $('.control-group-price').show();
            });

    <?php if(osc_locale_thousands_sep()!='' || osc_locale_dec_point() != '') { ?>
    $().ready(function(){
        $("#price").blur(function(event) {
            var price = $("#price").prop("value");
            <?php if(osc_locale_thousands_sep()!='') { ?>
            while(price.indexOf('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>')!=-1) {
                price = price.replace('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>', '');
            }
            <?php }; ?>
            <?php if(osc_locale_dec_point()!='') { ?>
            var tmp = price.split('<?php echo osc_esc_js(osc_locale_dec_point())?>');
            if(tmp.length>2) {
                price = tmp[0]+'<?php echo osc_esc_js(osc_locale_dec_point())?>'+tmp[1];
            }
            <?php }; ?>
            $("#price").prop("value", price);
        });
    });
    <?php }; ?>
</script>

<script type="text/javascript">

    function loadRegions(code, regionId) {
        if (!code) {
            return;
        }

        <?php if($path=="admin") { ?>
            var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + code;
        <?php } else { ?>
            var url = '<?php echo osc_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + code;
        <?php }; ?>

        var result = '';

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function(data){
                var length = data.length;
                var nonZero = 0;
                if(length > 0) {
                    console.log("region length > 0");
                    result += '<option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option>';
                    for(key in data) {
                        result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                    }
                    
                    if($("#regionId").length){
                        console.log($("regionId").parent().attr('class'));
                        if($("#regionId").is("select")) {
                            $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                            $("#regionId").closest('div').remove();
                        }
                        else {

                            console.log("hidden type");
                            $("#regionId").remove();
                            $("#cityId").remove();
                            hidden = 0;
                        }
                    }
                    $("#region").before('<select name="regionId" id="regionId" ></select>');
                    $("#region").before('<input type="hidden" id="regionRequired" name="regionRequired" value=true />');
                    $('#symbol-region-required').show();
                    $("#region").remove();
                    $("#regionId").val("");
                    
                    nonZero = 1;
                }
                else {
                    if($("#regionId").length) {
                        if($("#regionId").is("select")) {
                            console.log("select box not hidden");
                            $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                            $("#regionId").closest('div').remove();
                        }
                    }
                    
                    if($("#cityId").length) {
                        if($("#cityId").is("select")) {
                            console.log("select box");
                            $("#cityId").parent().before('<input type = "text" name = "city" id = "city" />');
                            $("#cityId").closest('div').remove();
                        }
                    }
                }

                $("#regionId").html(result);
                
                if (nonZero == 1){
                    if ($("#regionId").next("a").length == 0) {
                        selectUi($("#regionId"));
                    }

                    // Pre-select the region if regionId exist.
                    if (regionId) {
                        $("#regionId").val(regionId)
                        $("#regionId").trigger('change')
                    }
                }
            }
        });
    }

    function loadCities(regionId, cityId) {

        console.log('city id ' + cityId)
        if (!regionId){
            return ;
        }
        
        <?php if($path=="admin") { ?>
            var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + regionId;
        <?php } else { ?>
            var url = '<?php echo osc_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + regionId;
        <?php }; ?>

        var result = '';

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function(data){
                var length = data.length;
                var nonZero = 0;
                if(length > 0) {
                    console.log("city length > 0");
                    result += '<option selected value=""><?php echo osc_esc_js(__("Select a City / District...")); ?></option>';
                    for(key in data) {
                        result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                    }

                    $("#city").before('<select name="cityId" id="cityId" ></select>');
                    $("#city").before('<input type="hidden" id="cityRequired" name="cityRequired" value=true />')
                    $('#symbol-city-required').show();
                    $("#city").remove();
                    
                    nonZero = 1;

                    // If cityId has value, select the value.
                }
                else {
                    if($("#cityId").is("select")) {
                        $("#cityId").parent().before('<input type="text" name="city" id="city" />');
                        $("#cityId").closest('div').remove();
                    }
                }
                

                $("#cityId").html(result);

                if(nonZero == 1){
                    if ($("#cityId").next("a").length == 0) {
                        selectUi($("#cityId"));
                    }

                    // Pre-select the city if cityId exist.
                    if (cityId) {
                        $("#cityId").val(cityId)
                        $("#cityId").trigger('change')
                    }
                }

                if ($("#region").length > 0) {
                    console.log("hide city parent parent");
                }
            }
        });
    }

    $(document).ready(function() {

        // Edit mode
        var countryCode = $('#countryId').val(),
            regionId = $('#regionId').val(),
            cityId = $('#cityId').val();
        
        if (countryId) {
            loadRegions(countryCode, regionId)
        }

        $("#countryId").on("change",function() {
            
            $('#regionRequired, #cityRequired').remove();
            $('#symbol-region-required, #symbol-city-required').hide();

            var regionId = '<select name="regionId" id="regionId" ></select>';

            if ($("#regionId").next("a").length == 1 && $("#regionId").next('a').find('span').eq(0).html() != '<?php _e('Select a region...'); ?>') {
                $("#regionId").next('a').find('span').eq(0).html('<?php _e('Select a region...'); ?>');
            }

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a City / District...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a City / District...'); ?>');
            }

            var pk_c_code = $(this).val();

            <?php if($path=="admin") { ?>
                var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php } else { ?>
                var url = '<?php echo osc_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php }; ?>

            var result = '';

            if(pk_c_code != '') {

                $("#regionId").attr('disabled',false);
                $("#cityId").attr('disabled',true);

                loadRegions(pk_c_code);

             } else {
				 if($("#regionId").length) {
                    if($("#regionId").is("select")) {
                        $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                        $("#regionId").closest('div').remove();
                    }
				 }

                 if($("#cityId").length) {
                    if($("#cityId").is("select")) {
                        $("#cityId").parent().before('<input type = "text" name = "city" id = "city" />');
                        $("#cityId").closest('div').remove();
                    }
                 }

             }
        });

        $("body").on("change", 'select#regionId', function(){

            $('#cityRequired').remove();
            $('#symbol-city-required').hide();

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a City / District...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a City / District...'); ?>');
            }

            var data = "";

            var pk_c_code = $(this).val();
            if(pk_c_code != '') {

                $("#cityId").attr('disabled',false);

                loadCities(pk_c_code, cityId);

             } else {
                $("#cityId").attr('disabled',true);
             }
        });

        if( $("#regionId").attr('value') == "") {
			console.log("regionId attr empty");
            $("#cityId").attr('disabled',true);
        }

        if($("#countryId").length != 0) {
            if( $("#countryId").prop('type').match(/select-one/) ) {
				console.log("countryIdtype match");
                if( $("#countryId").attr('value') == "") {
                    $("#regionId").attr('disabled',true);
                }
            }
        }
	});
</script>

<?php osc_current_web_theme_path('footer.php'); ?>
