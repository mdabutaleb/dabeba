<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','dadebo_nofollow_construct');

    dadebo_add_body_class('login');
    osc_current_web_theme_path('header.php');
?>
<div class="form-container form-horizontal form-container-box login-box">
    <div class="header">
        <h1 style="font-size: 25px;"><?php _e('Access to your account', 'dadebo'); ?></h1>
    </div>
    <div class="resp-wrapper">
        <form action="<?php echo osc_base_url(true); ?>" method="post" >
            <input type="hidden" name="page" value="login" />
            <input type="hidden" name="action" value="login_post" />

            <div class="control-group">
                <!--<label class="control-label" for="email"><?php _e('E-mail', 'dadebo'); ?></label>-->
                <div class="controls noMarginInput">
                    <input type="email" name="email" value="" placeholder="<?php _e('E-mail', 'dadebo'); ?>" class="onlyBorderInput" />
                </div>
            </div>
            <div class="control-group">
                <!--<label class="control-label" for="password"><?php _e('Password', 'dadebo'); ?></label>-->
                <div class="controls noMarginInput">
                    <input id="password" type="password" name="password" value="" autocomplete="off" class="onlyBorderInput" placeholder="<?php _e('Password', 'dadebo'); ?>">
                </div>
            </div>
            <div class="control-group">
                <div class="controls checkbox" style="margin-left: 74px;">
                    <?php UserForm::rememberme_login_checkbox();?> <label for="remember"><?php _e('Remember me', 'dadebo'); ?></label>
                </div>
				<div class="controls" style="margin-left: 279px;text-align: center;">
                    <?php

                        if (strpos(osc_active_plugins(), "nocaptcha_recaptcha/index.php")) {
                            anr_captcha_form_field();
                        }

                    ?>
                    <button type="submit" class="ui-button ui-button-middle ui-button-main"><?php _e("Log in", 'dadebo');?></button>
                </div>
            </div>
            <div class="actions">
                <a href="<?php echo osc_register_account_url(); ?>"><?php _e("Register for a free account", 'dadebo'); ?></a><br /><a href="<?php echo osc_recover_user_password_url(); ?>"><?php _e("Forgot password?", 'dadebo'); ?></a>
            </div>
        </form>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>
