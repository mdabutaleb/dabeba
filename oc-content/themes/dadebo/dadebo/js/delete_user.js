$(document).ready(function(){
    $(".opt_delete_account a").click(function(){
        $("#dialog-delete-account").dialog('open');
    });

    $("#dialog-delete-account").dialog({
        autoOpen: false,
        modal: true,
        buttons: [
            {
                text: dadebo.langs.delete,
                click: function() {
                    window.location = dadebo.base_url + '?page=user&action=delete&id=' + dadebo.user.id  + '&secret=' + dadebo.user.secret;
                }
            },
            {
                text: dadebo.langs.cancel,
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
});