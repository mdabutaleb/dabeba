<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    $route= Params::getParam('route');
    if($route == "moments" || $route == "moment" || $route == "post-moment"){
        if($route == "post-moment"){
            dadebo_add_body_class('moment-listing moment-post-listing');
        }
        else{
            dadebo_add_body_class('moment-listing');
        }
        
    }
    elseif($route == "neighborhood-alerts" || $route == "neighborhood-alert" || $route == "post-neighborhood-alert"){
        if($route == "post-neighborhood-alert"){
            dadebo_add_body_class('neighborhood-alert-listing neighborhood-post-listing');
        }
        else{
            dadebo_add_body_class('neighborhood-alert-listing');
        }
        
    }
    else{
        dadebo_add_body_class('custom');
    }
    osc_current_web_theme_path('header.php') ;
?>
<?php osc_render_file(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>