<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // code to store selected location in session
    if(array_key_exists("s_reg",$_GET)){
                  $_SESSION['s_reg']= $_GET['s_reg'];
                  ob_start();
                  header("Location:/");
                  ob_end_flush();
                  die();
              }
    $url_c = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $parsed = parse_url($url_c);
    $exploded = explode('.', $parsed["host"]);
    if(count($exploded) > 2){}else{ $_SESSION['s_reg']=''; }

	$language = osc_current_user_locale();
	if($language=="zh_CN"){
		$next_language = "en_US";
	}
	else{
		$next_language = "zh_CN";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
    <head>
        <?php osc_current_web_theme_path('common/head.php') ; ?>
    </head>
<body <?php dadebo_body_class(); ?>>
<div id="header">
    <!-- header ad 728x60-->
    <div class="ads_header">
    <?php echo osc_get_preference('header-728x90', 'dadebo'); ?>
    <!-- /header ad 728x60-->
    </div>
    <div class="clear"></div>
    <div class="wrapper">
        <div class="mobile-header-left">
            <div id="logo">
				<?php if(isset($_SESSION['userLocale']) && $_SESSION['userLocale'] == "en_US"){
					echo logo_header();
				}else{
					if(isset($_SESSION['userLocale']) && $_SESSION['userLocale'] == 'zh_CN'){
						?>
						<a href="<?= osc_base_url()?>"><img src="<?= osc_base_url()?>oc-content/themes/dadebo/images/zh-logo.png" /></a>
						<?php
					}else{
						echo logo_header();
					}
				} ?>
				<span class="search_country"><?php echo osc_search_country(); ?></span>
                <span id="description"><?php /*echo osc_page_description();*/ ?><?php _e("A Chinese Community Resource", 'dadebo'); ?></span>
            </div>
        </div>
        <div class="mobile-header-right" style="display: none;">
            <a href="javascript:void(0);" style="font-size:15px;" onclick="myFunction()">
                <img src="<?php echo osc_base_url().'oc-content/themes/dadebo/images/menu.png'; ?>" width="25px" height="20px;">
            </a>
        </div>
        <ul class="nav desktop-nav">
            <?php if( osc_is_static_page() || osc_is_contact_page() ){ ?>
                <li class="search"><a class="ico-search icons" data-bclass-toggle="display-search"></a></li>
                <li class="cat"><a class="ico-menu icons" data-bclass-toggle="display-cat"></a></li>
            <?php } ?>
            <?php if( osc_users_enabled() ) { ?>
            <?php if( osc_is_web_user_logged_in() ) { ?>
                <li class="first logged">
                    <span><?php echo sprintf(__('Hi %s', 'dadebo'), osc_logged_user_name() . '!'); ?>  &middot;</span>
                    <strong><a href="<?php echo osc_user_dashboard_url(); ?>"><?php _e('My account', 'dadebo'); ?></a></strong> &middot;
                    <a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'dadebo'); ?></a>
                </li>
            <?php } else { ?>
                <li><a id="login_open" href="<?php echo osc_user_login_url(); ?>"><?php _e('Login', 'dadebo') ; ?></a></li>
                <?php if(osc_user_registration_enabled()) { ?>
                    <li><a href="<?php echo osc_register_account_url() ; ?>"><?php _e('Register for a free account', 'dadebo'); ?></a></li>
                <?php }; ?>
            <?php } ?>
            <?php } ?>
<li>  &nbsp; <a href="<?php echo osc_base_url().'index.php?page=language&locale='.$next_language; ?>" >
	<img src="<?php echo osc_base_url().'oc-content/themes/dadebo/images/'.$language.'-flags.png'; ?>">
</a></li>
            <?php
                  //code to detect if its moments page and change header accordingly
                  $route= Params::getParam('route');
                  if($route == "moments" || $route == "moment" || $route == "post-moment"){ if($route != "post-moment"){?>
                      <li class="publish"><a href="<?php echo osc_route_url('post-moment'); ?>"><?php _e("Publish your moment", 'dadebo');?></a></li>
                  <?php } }
		  elseif($route == "neighborhood-alerts" || $route == "neighborhood-alert" || $route == "post-neighborhood-alert"){ if($route != "post-neighborhood-alert"){?>
                      <li class="publish"><a href="<?php echo osc_route_url('post-neighborhood-alert'); ?>"><?php _e("Publish your Neighborhood Alerts", 'dadebo');?></a></li>
                  <?php } }
                  else{ ?>
                            <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
                            <li class="publish"><a href="<?php echo osc_item_post_url_in_category() ; ?>"><?php _e("Publish your ad", 'dadebo');?></a></li>
                            <?php } ?>
              <?php } ?>
        </ul>
    </div>
    <div class="clear"></div>
    <ul class="topnav" id="myTopnav" style="display: none;">
        <?php if( osc_users_enabled() ) { ?>
            <?php if( osc_is_web_user_logged_in() ) { ?>
                <li class="mobile-user">
                    <span class="user-login-pic"><?php profile_picture_show(osc_logged_user_id()); ?></span><span><a href="<?php echo osc_user_dashboard_url(); ?>"><?php echo sprintf(__('Hi %s', 'dadebo'), osc_logged_user_name() . '!'); ?> </a></span>
                </li>
            <?php } else { ?>
                <li class="login-register"><a id="mobile-login" href="<?php echo osc_user_login_url(); ?>"><?php _e('Login', 'dadebo') ; ?></a>
                <?php if(osc_user_registration_enabled()) { ?>
                    <a id="mobile-register" href="<?php echo osc_register_account_url() ; ?>"><?php _e('Register', 'dadebo'); ?></a>
                </li>
                <?php }; ?>
            <?php } ?>
        <?php } ?>
	<!--<li><a href="<?php echo osc_route_url('neighborhood-alert-disclaimer'); ?>"><?php _e("Neighborhood alert", 'dadebo');?></a></li>
	<li><a href="<?php echo osc_route_url('moments-disclaimer'); ?>"><?php _e("Moments", 'dadebo');?></a></li>
        <li><a href="#"><?php _e("Calendar", 'dadebo');?></a></li>-->
        <li><a href="http://faq.dadebo.com/"><?php _e("Help. FAQ, abuse, legal &fraud", 'dadebo');?></a></li>
        <li><a href="http://faq.dadebo.com/en/personal-safety/"><?php _e("Personal safety tips", 'dadebo');?></a></li>
        <li><a href="http://faq.dadebo.com/en/terms-of-use/"><?php _e("Terms of use", 'dadebo');?></a></li>
        <li><a href="http://faq.dadebo.com/en/privacy-policy/" <?php if( !osc_is_web_user_logged_in() ) { ?>class="last-menu"<?php } ?>><?php _e("Privacy Policy", 'dadebo');?></a></li>
	<?php if( osc_is_web_user_logged_in() ) { ?>
                <li class="mobile-logout last-menu">
                    <a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'dadebo'); ?></a>
                </li>
        <?php } ?>
    </ul>
    <div class="clear"></div>
    <!--mobile view publish button and search field-->
	<div id="mobile-publish">

		<div class="column-row">
			<div class="mobile-community-page">
	            <a href="http://faq.dadebo.com"><?php _e("Community Page", 'dadebo');?></a>
	        </div>

			<div class="mobile-community-page">
	            <a href="<?= osc_route_url('neighborhood-alert-disclaimer'); ?>"><?php _e("Neighborhood Alert", 'dadebo');?></a>
	        </div>
		</div>

		<div class="column-row">
			<div class="mobile-community-page">
	            <a href="<?= osc_route_url('moments-disclaimer'); ?>"><?php _e("Moments", 'dadebo');?></a>
	        </div>

			<div class="mobile-community-page">
	            <a href="#"><?php _e("Calendar", 'dadebo');?></a>
	        </div>
		</div>

	</div>
    <div id="mobile-publish" style="display:none;">
	<?php
                  //code to detect if its moments page and change header accordingly
                  $route= Params::getParam('route');
                  if($route == "moments" || $route == "moment" || $route == "post-moment"){ ?>
                      <div class="publish" style="display: none;"><a href="<?php echo osc_route_url('post-moment'); ?>"><?php _e("Publish your moment", 'dadebo');?></a></div>
                  <?php  }
		  elseif($route == "neighborhood-alerts" || $route == "neighborhood-alert" || $route == "post-neighborhood-alert"){ ?>
                      <div class="publish" style="display: none;"><a href="<?php echo osc_route_url('post-neighborhood-alert'); ?>"><?php _e("Publish Neighborhood Alerts", 'dadebo');?></a></div>
                  <?php  }
                  else{ ?>
                            <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
                            <div class="publish" style="display: none;"><a href="<?php echo osc_item_post_url_in_category() ; ?>"><?php _e("Publish your ad", 'dadebo');?></a></div>
                            <?php } ?>
              <?php } ?>
    <form action="<?php echo osc_base_url(true); ?>" method="get" class="search nocsrf" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
        <input type="hidden" name="page" value="search"/>
        <div class="main-search">
            <div class="cell">
	      <div class="search-input">
                <input type="text" name="sPattern" id="query" class="input-text" value="<?php if(osc_search_pattern()!=""){ echo osc_esc_html(osc_search_pattern()); } ?>" required="required" placeholder="<?php if(osc_search_pattern()==""){ echo osc_esc_html(__(osc_get_preference('keyword_placeholder', 'dadebo'), 'dadebo')); } ?>" oninvalid="this.setCustomValidity('<?php echo _e('Please fill out this field', 'dadebo'); ?>')"/>
              </div>
		<button style="display: none;" class="search-button-mobile js-submit"><img src="<?php echo osc_base_url().'oc-content/themes/dadebo/images/search_icon.png'; ?>"></button>
            </div>
            <?php  if ( osc_count_categories() ) { ?>
                <div class="cell selector">
                    <?php osc_categories_select('sCategory', null, __('Select a category', 'dadebo')) ; ?>
                </div>
                <div class="cell reset-padding search-button">
            <?php  } else { ?>
                <div class="cell">
            <?php  } ?>
                <button class="ui-button ui-button-big js-submit"><?php _e("Search", 'dadebo');?></button>
            </div>
        </div>
        <div id="message-seach"></div>
    </form>
    </div>
    <!--/mobile view publish button and search field-->

    <!--mobile view language and location selector-->
    <div class="mobile-language" style="display: none;">
        <!--<div class="mobile-community-page">
            <a href="http://faq.dadebo.com" target="_blank"><?php _e("Community Page", 'dadebo');?></a>
        </div>-->
		<div class="mobile-community-page"><a href="<?php echo osc_item_post_url_in_category() ; ?>"><?php _e("Publish your ad", 'dadebo');?></a></div>
        <div class="mobile-location-select">
            <?php
            $country_set = 0;
            while(osc_has_list_countries()) {
                           if($_SERVER['HTTP_HOST'] == osc_list_country_url()){
                            $country_set = 1;
                            }
             }
            ?>
            <a href="javascript:void(0);" onclick="LocationSelect()">
                <?php
                    if($country_set){
                        $location_image = 'placeholder_active.png';
                    }
                    else{
                        $location_image = 'placeholder_not_selected.png';
                    }
                ?>
                <img src="<?php echo osc_base_url().'oc-content/themes/dadebo/images/'.$location_image; ?>">
            </a>
        </div>
        <?php View::newInstance()->_exportVariableToView('list_countries', Search::newInstance()->listCountries('>=') ) ; ?>
        <?php if(osc_count_list_countries() > 0) {?>
                <ul class="ul_location" id="myLocation" style="display: none;">
                    <li class="default-text">Location</li>
                    <?php
                        $countriesHardcodedForTraditional = ['內陸帝國', '舊金山灣區', '洛杉磯', '薩克拉門托'];
                        $urlsHardcodedForTraditional = ['http://inland-empire.dadebo.com/', 'http://sf-bay-area.dadebo.com/',
                        'http://la.dadebo.com/', 'http://sacramento.dadebo.com/'];

                        $countriesHardcodedForSimplified = ['三藩市湾区', '萨克拉门托', '拉斯维加斯', '洛杉矶', '内陆地区'];
                        $urlsHardcodedForSimplified = ['http://sf-bay-area.dadebo.com/', 'http://sacramento.dadebo.com/', 'http://las-vegas.dadebo.com/','http://la.dadebo.com/', 'http://inland-empire.dadebo.com/'];
                    ?>

                    <?php if (osc_current_user_locale() == 'zh_TW') { ?>
                        <?php
                        $matches = array();

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php foreach($countriesHardcodedForTraditional as $key => $countryHardcoded) {
                            $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                            ?>
                            <li class="<?php if($current_location == $urlsHardcodedForTraditional[$key]){echo 'selected-location';} ?>">
                                <a href="<?php echo $urlsHardcodedForTraditional[$key] ?>">
                                    <?php if ( $urlsHardcodedForTraditional[$key] == osc_list_country_url()) { ?>
                                    <b><?php echo $countryHardcoded ?><b>
                                            <?php } else { ?>
                                                <?php echo $countryHardcoded ?>
                                            <?php } ?>
                                </a>
                            </li>
                        <?php } ?>

                    <?php } else if (osc_current_user_locale() == 'zh_CN') { ?>
                        <?php
                        $matches = array();

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php foreach($countriesHardcodedForSimplified as $key => $countryHardcoded) {
                            $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                            ?>
                            <li class="<?php if($current_location == $urlsHardcodedForSimplified[$key]){echo 'selected-location';} ?>">
                                <a href="<?php echo $urlsHardcodedForSimplified[$key] ?>">
                                    <?php if ( $urlsHardcodedForSimplified[$key] == osc_list_country_url()) { ?>
                                    <b><?php echo $countryHardcoded ?><b>
                                            <?php } else { ?>
                                                <?php echo $countryHardcoded ?>
                                            <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <?php
                        $matches = array();

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php while(osc_has_list_countries()) {
                            $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                            ?>
                            <li class="<?php if($current_location == osc_list_country_url()){echo 'selected-location';} ?>">
                                <a href="<?php echo osc_list_country_url();?>">
                                    <?php if ($url == osc_list_country_url()) { ?>
                                    <b><?php echo osc_list_country_name(); ?><b>
                                            <?php } else { ?>
                                                <?php echo osc_list_country_name(); ?>
                                            <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
        <?php } ?>



        <div class="mobile-language-select">
            <a href="<?php echo osc_base_url().'index.php?page=language&locale='.$next_language; ?>" >
                <img src="<?php echo osc_base_url().'oc-content/themes/dadebo/images/'.$language.'-flags.png'; ?>">
            </a>
        </div>
        <ul class="ul_language" id="myLanguage" style="display: none;">
            <li class="default-text">Language</li>
            <?php if ( osc_count_web_enabled_locales() > 1) { ?>
                <?php osc_goto_first_locale(); ?>
                <?php $i = 0;  ?>
                <?php while ( osc_has_web_enabled_locales() ) { ?>
                <li class="<?php if(osc_locale_code()==osc_current_user_locale()){echo 'selected-language';} ?>"><a id="<?php echo osc_locale_code(); ?>" href="<?php echo osc_change_language_url ( osc_locale_code() ); ?>"><?php echo osc_locale_name(); ?></a></li>
                    <?php $i++; ?>
                <?php } ?>
            <?php } ?>
        </ul>

    </div>
    <!--/mobile view language and location selector-->

</div>
<!--to show narrow location selector in header-->
<div class="narrow-location wrapper">
    <?php View::newInstance()->_exportVariableToView('list_countries', Search::newInstance()->listCountries('>=') ) ; ?>
        <?php if(osc_count_list_countries() > 0) {?>

                <ul>
                    <?php
                        $countriesHardcodedForTraditional = ['內陸帝國', '舊金山灣區', '洛杉磯', '薩克拉門托'];
                        $urlsHardcodedForTraditional = ['http://inland-empire.dadebo.com/', 'http://sf-bay-area.dadebo.com/',
                        'http://la.dadebo.com/', 'http://sacramento.dadebo.com/'];

                        $countriesHardcodedForSimplified = ['三藩市湾区', '萨克拉门托', '拉斯维加斯', '洛杉矶', '内陆地区'];
                        $urlsHardcodedForSimplified = ['http://sf-bay-area.dadebo.com/', 'http://sacramento.dadebo.com/', 'http://las-vegas.dadebo.com/','http://la.dadebo.com/', 'http://inland-empire.dadebo.com/'];
                    ?>

                    <?php if (osc_current_user_locale() == 'zh_TW') { ?>
                        <?php
                        $matches = array();
                        $current_area = "";

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php foreach($countriesHardcodedForTraditional as $key => $countryHardcoded) {
                                $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                                if($current_location == $urlsHardcodedForTraditional[$key]){
                                    echo '<li class="narrow-selected-location"><h5>';
                                    if ( $urlsHardcodedForTraditional[$key] == osc_list_country_url()) {
                                        echo $countryHardcoded;
                                    } else {
                                        echo $countryHardcoded;
                                    }
                                    echo '</h5></li>';
                                    $current_area = $current_location;
                                    if($current_area!=$_SESSION['current_area']){
                                      $_SESSION['s_reg'] = '';
                                    }
                                    $_SESSION['current_area'] = $current_area;
                                }
                        }
                        if($current_area!=""){
                            if($current_area=="http://inland-empire.dadebo.com/"){


                            }
                            if($current_area=="http://sf-bay-area.dadebo.com/"){
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='City of San Francisco')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=City of San Francisco">sfc</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='East bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=East bay area">eby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='North bay / marin')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=North bay / marin">nby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='Peninsula')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=Peninsula">pen</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='santa cruz co')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=santa cruz co">scz</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='South bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=South bay area">sby</a></li>';

                            }
                            if($current_area=="http://la.dadebo.com/"){
                               echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='westside-southbay-310')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=westside-southbay-310">wst</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san gabriel valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san gabriel valley">sgv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san fernando valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san fernando valley">sfv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='long beach / 562')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=long beach / 562">lgb</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='central LA 213/323')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=central LA 213/323">lac</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='antelope valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=antelope valley">ant</a></li>';
                            }
                            if($current_area=="http://sacramento.dadebo.com/"){

                            }

                        }
                        else{

                          foreach($countriesHardcodedForTraditional as $key => $countryHardcoded) {
                              ?>
                              <li>
                                  <a href="<?php echo $urlsHardcodedForTraditional[$key] ?>">
                                      <?php if ( $urlsHardcodedForTraditional[$key] == osc_list_country_url()) { ?>
                                      <b><?php echo $countryHardcoded ?><b>
                                              <?php } else { ?>
                                                  <?php echo $countryHardcoded ?>
                                              <?php } ?>
                                  </a>
                              </li>
                          <?php } }?>
                    <?php } else if (osc_current_user_locale() == 'zh_CN') { ?>
                          <?php
                        $matches = array();
                        $current_area = "";

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php foreach($countriesHardcodedForSimplified as $key => $countryHardcoded) {
                                $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                                if($current_location == $urlsHardcodedForSimplified[$key]){
                                    echo '<li class="narrow-selected-location"><h5>';
                                    if ( $urlsHardcodedForSimplified[$key] == osc_list_country_url()) {
                                        echo $countryHardcoded;
                                    } else {
                                        echo $countryHardcoded;
                                    }
                                    echo '</h5></li>';
                                    $current_area = $current_location;
                                    if($current_area!=$_SESSION['current_area']){
                                      $_SESSION['s_reg'] = '';
                                    }
                                    $_SESSION['current_area'] = $current_area;
                                }
                        }
                        if($current_area!=""){
                            if($current_area=="http://inland-empire.dadebo.com/"){
                            }
                            if($current_area=="http://sf-bay-area.dadebo.com/"){
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='City of San Francisco')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=City of San Francisco">sfc</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='East bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=East bay area">eby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='North bay / marin')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=North bay / marin">nby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='Peninsula')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=Peninsula">pen</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='santa cruz co')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=santa cruz co">scz</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='South bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=South bay area">sby</a></li>';

                            }
                            if($current_area=="http://la.dadebo.com/"){
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='westside-southbay-310')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=westside-southbay-310">wst</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san gabriel valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san gabriel valley">sgv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san fernando valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san fernando valley">sfv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='long beach / 562')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=long beach / 562">lgb</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='central LA 213/323')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=central LA 213/323">lac</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='antelope valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=antelope valley">ant</a></li>';
                            }
                            if($current_area=="http://sacramento.dadebo.com/"){

                            }

                        }
                        else{

                          foreach($countriesHardcodedForSimplified as $key => $countryHardcoded) {
                              ?>
                              <li>
                                  <a href="<?php echo $urlsHardcodedForSimplified[$key] ?>">
                                      <?php if ( $urlsHardcodedForSimplified[$key] == osc_list_country_url()) { ?>
                                      <b><?php echo $countryHardcoded ?><b>
                                              <?php } else { ?>
                                                  <?php echo $countryHardcoded ?>
                                              <?php } ?>
                                  </a>
                              </li>
                          <?php } }?>

                    <?php } else { ?>
                        <?php
                        $matches = array();

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php
                        $current_area = "";

                        while(osc_has_list_countries()) {
                            $current_location = "http://".$_SERVER['HTTP_HOST']."/";
                            if($current_location == osc_list_country_url()){
                                $current_area = osc_list_country_name();
                                if($current_area!=$_SESSION['current_area']){
                                  $_SESSION['s_reg'] = '';
                                }
                                $_SESSION['current_area'] = $current_area;
                                echo '<li class="narrow-selected-location"><h5>';
                                if($_SESSION['s_reg']!=""){
                                  echo $_SESSION['s_reg'];
                                }else{
                                  echo osc_list_country_name();
                                }
                                echo '</h5></li>';
                            }
                        }
                        if($current_area!=""){
                            if($current_area=="Inland Empire"){


                            }
                            if($current_area=="SF Bay Area"){
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='City of San Francisco')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=City of San Francisco">sfc</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='East bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=East bay area">eby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='North bay / marin')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=North bay / marin">nby</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='Peninsula')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=Peninsula">pen</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='santa cruz co')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=santa cruz co">scz</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='South bay area')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=South bay area">sby</a></li>';

                            }
                            if($current_area=="Los Angeles"){
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='westside-southbay-310')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=westside-southbay-310">wst</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san gabriel valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san gabriel valley">sgv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='san fernando valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=san fernando valley">sfv</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='long beach / 562')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=long beach / 562">lgb</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='central LA 213/323')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=central LA 213/323">lac</a></li>';
                                echo '<li style="font-weight: bold;" class="'.(($_SESSION['s_reg'] =='antelope valley')? 'selected-region':'').'"><a href="'.osc_base_url().'index.php?s_reg=antelope valley">ant</a></li>';

                            }
                            if($current_area=="Sacramento"){

                            }

                        }
                        else{
                            while(osc_has_list_countries()) {
                                ?>

                                <li>
                                    <a href="<?php echo osc_list_country_url();?>">
                                        <?php if ($url == osc_list_country_url()) { ?>
                                        <b><?php echo osc_list_country_name(); ?><b>
                                                <?php } else { ?>
                                                    <?php echo osc_list_country_name(); ?>
                                                <?php } ?>
                                    </a>
                                </li>
                            <?php }
                        }

                       } ?>
                </ul>
        <?php } ?>
</div>

<?php osc_show_widgets('header'); ?>
<div class="wrapper wrapper-flash">
    <?php
        $breadcrumb = osc_breadcrumb('&raquo;', false, get_breadcrumb_lang());
	$route= Params::getParam('route');
        if($route == "moment" || $route == "post-moment"){
              $breadcrumb = '
              <ul class="breadcrumb">
              <li class="first-child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.osc_base_url().'" itemprop="url"><span itemprop="title">DaDeBo</span></a></li>
              <li class="last-child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> &raquo; <a href="'.osc_route_url('moments').'" itemprop="url"><span itemprop="title">Moments</span></a></li>
              </ul>';
        }
	if($route == "neighborhood-alert" || $route == "post-neighborhood-alert"){
              $breadcrumb = '
              <ul class="breadcrumb">
              <li class="first-child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.osc_base_url().'" itemprop="url"><span itemprop="title">DaDeBo</span></a></li>
              <li class="last-child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> &raquo; <a href="'.osc_route_url('neighborhood-alerts').'" itemprop="url"><span itemprop="title">Neighborhood alerts</span></a></li>
              </ul>';
        }
        if( $breadcrumb !== '') { ?>
        <div class="breadcrumb">
            <?php echo $breadcrumb; ?>
            <div class="clear"></div>
        </div>
    <?php
        }
    ?>
    <?php osc_show_flash_message(); ?>
</div>
<?php osc_run_hook('before-content'); ?>
<div class="wrapper" id="content">
    <?php if(osc_is_home_page()) { ?>
        <div id="left-sidebar">
            <?php /*osc_show_widgets('leftsidebar');*/ ?>
	    <p class="left-sidebar-button"><a class="btn btn-crp btn-block" href="http://faq.dadebo.com" target="_blank"><?php _e("Community Resource", 'dadebo'); ?></a></p>
            <p class="left-sidebar-button"><a class="btn btn-crp btn-block" href="<?php echo osc_base_url().'/index.php?page=custom&amp;route=neighborhood-alert-disclaimer'; ?>"><?php _e("Neighborhood Alert", 'dadebo'); ?></a></p>
            <p class="left-sidebar-button"><a class="btn btn-crp btn-block" href="<?php echo osc_base_url().'/index.php?page=custom&amp;route=moments-disclaimer'; ?>"><?php _e("Moments", 'dadebo'); ?></a></p>
            <?php //osc_run_hook('add_to_left_sidebar_first'); ?>
            <?php echo custom_make_calendar(); ?>
            <?php osc_run_hook('add_to_left_sidebar_second'); ?>
        </div>
    <?php } ?>
    <?php osc_run_hook('before-main'); ?>
    <div id="main">
        <?php osc_run_hook('inside-main'); ?>
