<script type="text/javascript">
    $(document).ready(function() {

        $("#countryId").on("change",function() {
            var pk_c_code = $(this).val();
            var regionSelect = '<select name="regionId" id="regionId" ></select>';

            if ($("#regionId").next("a").length == 1 && $("#regionId").next('a').find('span').eq(0).html() != '<?php _e('Select a region...'); ?>') {
                $("#regionId").next('a').find('span').eq(0).html('<?php _e('Select a region...'); ?>');
            }

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a city...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a city...'); ?>');
            }

            <?php if($path=="admin") { ?>
                var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php } else { ?>
                var url = '<?php echo osc_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php }; ?>

            var result = '';

            if(pk_c_code != '') {

                $("#regionId").attr('disabled',false);
                $("#cityId").attr('disabled',true);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;

                        if(length > 0) {
                            result += '<option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }

                            $("#region").before('<select name="regionId" id="regionId" ></select>');
                            $("#region").remove();

//                            $("#city").before('<select name="cityId" id="cityId" ></select>');
//                            $("#city").remove();

                            $("#regionId").val("");

                        }

                        $("#regionId").html(result);

                        if ($("#regionId").next("a").length == 0) {
                            selectUi($("#regionId"));
                        }

                        // Hide or show the inputs.
                        if (length == 0) {

                            if ($("#region").length > 0) {
                                $("#region").parent().parent().hide();
                                $("#city").parent().parent().hide();
                            } else {
                                $("#regionId").parent().parent().parent().hide();
                                $("#regionId").parent().parent().hide();
                                $("#regionId").html('<option value="0">Default</option>');

                                $("#cityId").parent().parent().parent().hide();
                                $("#cityId").parent().parent().hide();
                                $("#cityId").html('<option value="0">Default</option>');
                            }
                        } else {

                            if ($("#region").length > 0) {
                                $("#region").parent().parent().show();
                                $("#city").parent().parent().show();
                            } else {
                                $("#regionId").parent().parent().parent().show();
                                $("#regionId").parent().parent().show();
                                $("#cityId").parent().parent().parent().hide();
                                $("#cityId").parent().parent().hide();
                                $("#cityId").html('<option value="0">Default</option>');
                            }
                        }
                    }
                 });

             } else {

                 // add empty select
                 $("#region").before('<select name="regionId" id="regionId" ><option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option></select>');
                 $("#region").remove();

                 $("#city").before('<select name="cityId" id="cityId" ><option value=""><?php echo osc_esc_js(__("Select a city...")); ?></option></select>');
                 $("#city").remove();

                 if( $("#regionId").length > 0 ){
                     $("#regionId").html('<option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option>');
                 } else {
                     $("#region").before('<select name="regionId" id="regionId" ><option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option></select>');
                     $("#region").remove();
                 }
                 if( $("#cityId").length > 0 ){
                     $("#cityId").html('<option value=""><?php echo osc_esc_js(__("Select a city...")); ?></option>');
                 } else {
                     $("#city").before('<select name="cityId" id="cityId" ><option value=""><?php echo osc_esc_js(__("Select a city...")); ?></option></select>');
                     $("#city").remove();
                 }
                 $("#regionId").attr('disabled',true);
                 $("#cityId").attr('disabled',true);
             }
        });

        $("body").on("change", 'select#regionId', function(){

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a city...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a city...'); ?>');
            }

            var data = "";

            var pk_c_code = $(this).val();
            <?php if($path=="admin") { ?>
                var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + pk_c_code;
            <?php } else { ?>
                var url = '<?php echo osc_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + pk_c_code;
            <?php }; ?>

            var result = '';

            if(pk_c_code != '') {

                $("#cityId").attr('disabled',false);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;
                        if(length > 0) {
                            result += '<option selected value=""><?php echo osc_esc_js(__("Select a city...")); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }

                            $("#city").before('<select name="cityId" id="cityId" ></select>');
                            $("#city").remove();
                        }

                        $("#cityId").html(result);

                        if ($("#cityId").next("a").length == 0) {
                            selectUi($("#cityId"));
                        }

                        if ($("#region").length > 0) {
                            $("#city").parent().parent().hide();
                        }

                        // Show or hide the inputs.
                        if (data.length > 0) {
                            $("#cityId").parent().parent().parent().show();
                            $("#cityId").parent().parent().show();
                        } else {
                            $("#cityId").parent().parent().parent().hide();
                            $("#cityId").parent().parent().hide();
                            $("#cityId").html('<option value="0">Default</option>');
                        }
                    }
                 });
             } else {
                $("#cityId").attr('disabled',true);
             }
        });

        if( $("#regionId").attr('value') == "") {
            $("#cityId").attr('disabled',true);
        }

        if($("#countryId").length != 0) {
            if( $("#countryId").prop('type').match(/select-one/) ) {
                if( $("#countryId").attr('value') == "") {
                    $("#regionId").attr('disabled',true);
                }
            }
        }

        /**
         * Validate form
         */

        // Validate description without HTML.
        $.validator.addMethod(
            "minstriptags",
            function(value, element) {
                altered_input = strip_tags(value);
                if (altered_input.length < 3) {
                    return false;
                } else {
                    return true;
                }
            },
            "<?php echo osc_esc_js(__("Description needs to be longer")); ?>."
        );

        // Code for form validation
        $("form[name=item]").validate({
            rules: {
                catId: {
                    required: true,
                    digits: true
                },
                <?php if(osc_price_enabled_at_items()) { ?>
                price: {
                    maxlength: 15
                },
                currency: "required",
                <?php } ?>
                <?php if(osc_images_enabled_at_items()) { ?>
                "photos[]": {
                    accept: "<?php echo osc_allowed_extension(); ?>"
                },
                <?php } ?>
                <?php if($path == 'front') { ?>
                contactName: {
                    minlength: 3,
                    maxlength: 35
                },
                contactEmail: {
                    required: true,
                    email: true
                },
                <?php } ?>
                regionId: {
                    required: true,
                    digits: true
                },
                cityId: {
                    required: true,
                    digits: true
                },
                cityArea: {
                    minlength: 3,
                    maxlength: 50
                },
                address: {
                    minlength: 3,
                    maxlength: 100
                }
            },
            messages: {
                catId: "<?php echo osc_esc_js(__('Choose one category')); ?>.",
                <?php if(osc_price_enabled_at_items()) { ?>
                price: {
                    maxlength: "<?php echo osc_esc_js(__("Price: no more than 50 characters")); ?>."
                },
                currency: "<?php echo osc_esc_js(__("Currency: make your selection")); ?>.",
                <?php } ?>
                <?php if(osc_images_enabled_at_items()) { ?>
                "photos[]": {
                    accept: "<?php echo osc_esc_js(sprintf(__("Photo: must be %s"), osc_allowed_extension())); ?>."
                },
                <?php } ?>
                <?php if($path == 'front') { ?>
                contactName: {
                    minlength: "<?php echo osc_esc_js(__("Name: enter at least 3 characters")); ?>.",
                    maxlength: "<?php echo osc_esc_js(__("Name: no more than 35 characters")); ?>."
                },
                contactEmail: {
                    required: "<?php echo osc_esc_js(__("Email: this field is required")); ?>.",
                    email: "<?php echo osc_esc_js(__("Invalid email address")); ?>."
                },
                <?php } ?>
                regionId: "<?php echo osc_esc_js(__("Select a region")); ?>.",
                cityId: "<?php echo osc_esc_js(__("Select a city")); ?>.",
                cityArea: {
                    minlength: "<?php echo osc_esc_js(__("City area: enter at least 3 characters")); ?>.",
                    maxlength: "<?php echo osc_esc_js(__("City area: no more than 50 characters")); ?>."
                },
                address: {
                    minlength: "<?php echo osc_esc_js(__("Address: enter at least 3 characters")); ?>.",
                    maxlength: "<?php echo osc_esc_js(__("Address: no more than 100 characters")); ?>."
                }
            },
            errorLabelContainer: "#error_list",
            wrapper: "li",
            invalidHandler: function(form, validator) {
                $('html,body').animate({ scrollTop: $('h1').offset().top }, { duration: 250, easing: 'swing'});
            },
            submitHandler: function(form){
                $('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
                setTimeout("$('button[type=submit], input[type=submit]').removeAttr('disabled')", 5000);
                form.submit();
            }
        });
    });

    /**
     * Strip HTML tags to count number of visible characters.
     */
    function strip_tags(html) {
        if (arguments.length < 3) {
            html=html.replace(/<\/?(?!\!)[^>]*>/gi, '');
        } else {
            var allowed = arguments[1];
            var specified = eval("["+arguments[2]+"]");
            if (allowed){
                var regex='</?(?!(' + specified.join('|') + '))\b[^>]*>';
                html=html.replace(new RegExp(regex, 'gi'), '');
            } else{
                var regex='</?(' + specified.join('|') + ')\b[^>]*>';
                html=html.replace(new RegExp(regex, 'gi'), '');
            }
        }
        return html;
    }

    function delete_image(id, item_id,name, secret) {
        //alert(id + " - "+ item_id + " - "+name+" - "+secret);
        var result = confirm('<?php echo osc_esc_js( __("This action can't be undone. Are you sure you want to continue?") ); ?>');
        if(result) {
            $.ajax({
                type: "POST",
                url: '<?php echo osc_base_url(true); ?>?page=ajax&action=delete_image&id='+id+'&item='+item_id+'&code='+name+'&secret='+secret,
                dataType: 'json',
                success: function(data){
                    var class_type = "error";
                    if(data.success) {
                        $("div[name="+name+"]").remove();
                        class_type = "ok";
                    }
                    var flash = $("#flash_js");
                    var message = $('<div>').addClass('pubMessages').addClass(class_type).attr('id', 'flashmessage').html(data.msg);
                    flash.html(message);
                    $("#flashmessage").slideDown('slow').delay(3000).slideUp('slow');
                }
            });
        }
    }


</script>