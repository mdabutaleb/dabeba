<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','dadebo_nofollow_construct');

    dadebo_add_body_class('user user-items');
    osc_add_hook('before-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('user-sidebar.php');
    }
    osc_current_web_theme_path('header.php') ;

    $listClass = '';
    $buttonClass = '';
    if(Params::getParam('ShowAs') == 'gallery'){
          $listClass = 'listing-grid';
          $buttonClass = 'active';
    }
    if(Params::getParam('ShowAs') == 'list'){
          $listClass = 'list-grid';
          $buttonClass = 'active';
    }
    if(Params::getParam('ShowAs') == 'map'){
          $listClass = 'map-grid';
          $buttonClass = 'active';
    }
    if(Params::getParam('ShowAs') == 'thumb'){
          $listClass = 'thumb-grid';
          $buttonClass = 'active';
    }
?>
<div class="list-header">
    <?php osc_run_hook('search_ads_listing_top'); ?>
    <h1><?php _e('My listings', 'dadebo'); ?></h1>
    <?php if(osc_count_items() == 0) { ?>
        <p class="empty" ><?php _e('No listings have been added yet', 'dadebo'); ?></p>
    <?php } else { ?>
        <div class="actions">
            <span class="doublebutton <?php echo $buttonClass; ?>">
                 <a href="<?php echo osc_esc_html(osc_user_list_items_url(array('sShowAs'=> 'list'))); ?>" class="list-button <?php if(rons_show_as() == 'list') { echo 'active'; } ?>" data-class-toggle="list-grid" data-destination="#listing-card-list"><span><?php _e('List','rons'); ?></span></a>
                 <a href="<?php echo osc_esc_html(osc_user_list_items_url(array('sShowAs'=> 'thumb'))); ?>" class="thumb-button <?php if(rons_show_as() == 'thumb') { echo 'active'; } ?>" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span><?php _e('Thumb','rons'); ?></span></a>
                 <a href="<?php echo osc_esc_html(osc_user_list_items_url(array('sShowAs'=> 'gallery'))); ?>" class="grid-button <?php if(rons_show_as() == 'gallery') { echo 'active'; } ?>" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span><?php _e('Grid','rons'); ?></span></a>
                 <a href="<?php echo osc_esc_html(osc_user_list_items_url(array('sShowAs'=> 'map'))); ?>" class="map-button <?php if(rons_show_as() == 'map') { echo 'active'; } ?>" data-class-toggle="map-grid" data-destination="#listing-card-list"><span><?php _e('Map','rons'); ?></span></a>
            </span>
        </div>
    </div>
    <?php if($listClass != 'map-grid') { ?>
        <?php echo $listClass ?>
        <?php
            View::newInstance()->_exportVariableToView("listClass",$listClass);
            View::newInstance()->_exportVariableToView("listAdmin", true);
            osc_current_web_theme_path('loop.php');
        ?>
        <div class="clear"></div>
        <?php
        if(osc_rewrite_enabled()){
            $footerLinks = osc_search_footer_links();
        ?>
            <ul class="footer-links">
                <?php foreach($footerLinks as $f) { View::newInstance()->_exportVariableToView('footer_link', $f); ?>
                    <?php if($f['total'] < 3) continue; ?>
                    <li><a href="<?php echo osc_footer_link_url(); ?>"><?php echo osc_footer_link_title(); ?></a></li>
                <?php } ?>
            </ul>
        <?php } ?>
        <div class="paginate" >
            <?php echo osc_pagination_items(); ?>
        </div>
        <?php } else {?>
            <?php radius_map_items(); ?> 
        <?php } ?>
    <?php } ?>
<?php osc_current_web_theme_path('footer.php') ; ?>
