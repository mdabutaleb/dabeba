<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>

<?php $size = explode('x', osc_thumbnail_dimensions()); ?>
<li class="<?php osc_run_hook("highlight_class"); ?>listing-card <?php echo $class; if(osc_item_is_premium()){ echo ' premium'; } ?>">
    <div class="thumb-wrapper">
    <div class="inner-thumb-wrapper">
    <?php if( osc_images_enabled_at_items() ) { ?>
        <?php if(osc_count_item_resources()) { ?>
        <?php for ( $i = 0; osc_has_item_resources() ; $i++ ) { ?>
                <a class="listing-thumb" href="<?php echo osc_resource_url(); ?>" rel="image_group" title="<?php echo osc_esc_html(osc_item_title()) ; ?>">
                        <img src="<?php echo osc_resource_thumbnail_url(); ?>" width="<?php echo $size[0]; ?>" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" title=""/>
                </a>
        <?php } ?>
        <?php } else { ?>
            <a class="listing-thumb" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>"></a>
        <?php } ?>
    <?php } ?>
    </div>
    </div>
    <div class="listing-detail">
        <div class="listing-cell">
            <div class="listing-data">
                <div class="listing-basicinfo">
                    <?php echo fi_make_favorite(); ?> <a href="<?php echo osc_item_url() ; ?>" class="title" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><?php echo osc_item_title() ; ?></a>
                    <div class="listing-attributes">
                        <span class="category-text" style="display: none;">Category: </span><span class="category"><?php echo osc_item_category() ; ?></span><span class="seperator"> -</span>
                        <span class="location"><?php echo osc_item_city(); ?> <?php if( osc_item_region()!='' ) { ?> (<?php echo osc_item_region(); ?>)<?php } ?></span> <span class="g-hide">-</span> <span class="listing-date"><?php echo osc_format_date(osc_item_pub_date()); ?></span>
                        <?php if( osc_price_enabled_at_items() ) { ?><span class="currency-value"><?php echo osc_format_price(osc_item_price()); ?></span><?php } ?>
                    </div>
                    <p><?php echo osc_highlight( osc_item_description() ,250) ; ?></p>
                </div>
                <div class="listing-user-info">
                    <h3>Posted By</h3>
                    <p>
                        <!--<img src="<?php echo osc_base_url(); ?>oc-content/themes/dadebo/images/profile_16.png">-->
                        <div class="listing-user-pic">
                        <?php profile_picture_show(osc_item_user_id()); ?>
                        </div>
                        <?php if( osc_item_user_id() != null ) { ?>
                            <a href="<?php echo osc_user_public_profile_url( osc_item_user_id() ); ?>" > <?php echo osc_item_contact_name(); ?></a>
                        <?php } else { ?>
                            <span class="name"><?php printf(__('%s', 'dadebo'), osc_item_contact_name()); ?></span>
                        <?php } ?>
                    </p>
                    <p style=" padding-top: 7px;">
                        <?php if( osc_item_user_id() != null ) {
                            $conn = getConnection();
                            $user_city = $conn->osc_dbFetchResults("SELECT s_city FROM %st_user WHERE pk_i_id='%d'", DB_TABLE_PREFIX, osc_item_user_id());
                            $user_city =$user_city[0]['s_city'];
                            if($user_city !="") {
                            ?>
                            <span><img src="<?php echo osc_base_url(); ?>oc-content/themes/dadebo/images/placeholder_16.png"> <?php echo $user_city; ?></span>
                        <?php } } ?>
                    </p>
                </div>
                <?php if($admin){ ?>
                    <span class="admin-options">
                        <a href="<?php echo osc_item_edit_url(); ?>" rel="nofollow"><?php _e('Edit item', 'dadebo'); ?></a>
                        <span>|</span>
                        <a class="delete" onclick="javascript:return confirm('<?php echo osc_esc_js(__('This action can not be undone. Are you sure you want to continue?', 'dadebo')); ?>')" href="<?php echo osc_item_delete_url();?>" ><?php _e('Delete', 'dadebo'); ?></a>
                        <?php if(osc_item_is_inactive()) {?>
                        <span>|</span>
                        <a href="<?php echo osc_item_activate_url();?>" ><?php _e('Activate', 'dadebo'); ?></a>
                        <?php } ?>
                        <?php if(function_exists("republish_pro_link")) { republish_pro_link(osc_item_id()); } ?>
                    </span>
                <?php } ?>
            </div>
        </div>
    </div>
</li>