<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','dadebo_nofollow_construct');

    dadebo_add_body_class('user user-profile');
    osc_add_hook('before-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('user-sidebar.php');
    }
    osc_add_filter('meta_title_filter','custom_meta_title');
    function custom_meta_title($data){
        return __('Update account', 'dadebo');
    }
    osc_current_web_theme_path('header.php') ;
    $osc_user = osc_user();
?>
<h1><?php _e('Edit Profile Account', 'dadebo'); ?></h1>
<?php //UserForm::location_javascript(); ?>
<?php UserForm::location_javascript_user(); ?>
<div class="form-container form-horizontal">
    <div class="resp-wrapper">
        <ul id="error_list"></ul>
        <div class="profile_upload">
            <?php profile_picture_upload(); ?>
        </div>
        <div style="clear:both;"></div>
        <form action="<?php echo osc_base_url(true); ?>" method="post">
            <input type="hidden" name="page" value="user" />
            <input type="hidden" name="action" value="profile_post" />
            <div class="control-group">
                <label class="control-label" for="name"><?php _e('Name / Alias *', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::name_text(osc_user()); ?>
                </div>
            </div>
            <!--<div class="control-group">
                <label class="control-label" for="user_type"><?php _e('User type', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::is_company_select(osc_user()); ?>
                </div>
            </div>-->
            <input type="hidden" name="b_company" value="0">

            <!--<div class="control-group">
                <label class="control-label" for="phoneMobile"><?php _e('Cell phone', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::mobile_text(osc_user()); ?>
                </div>
            </div>-->
            <!--<div class="control-group">
                <label class="control-label" for="phoneLand"><?php _e('Phone', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::phone_land_text(osc_user()); ?>
                </div>
            </div>-->
            <div class="control-group">
                <label class="control-label" for="country"><?php _e('Location', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::country_select(osc_get_countries(), osc_user()); ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="region"><?php _e('Region', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::region_select(osc_get_regions(), osc_user()); ?>
                </div>
            </div>
            <div class="control-group user-city">
                <label class="control-label" for="city"><?php _e('City', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::city_select(osc_get_cities(), osc_user()); ?>
                </div>
            </div>
            <div class="control-group user-zip">
                <label class="control-label" for="city_area"><?php _e('ZipCode', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::zip_text(osc_user()); ?>
                </div>
            </div>
            
            <!--<div class="control-group">
                <label class="control-label" for="city_area"><?php _e('City area', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::city_area_text(osc_user()); ?>
                </div>
            </div>-->
            <!--<div class="control-group">
                <label class="control-label"l for="address"><?php _e('Address', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::address_text(osc_user()); ?>
                </div>
            </div>-->
            <!--<div class="control-group">
                <label class="control-label" for="webSite"><?php _e('Website', 'dadebo'); ?></label>
                <div class="controls">
                    <?php UserForm::website_text(osc_user()); ?>
                </div>
            </div>-->
            <div class="control-group">
                <label class="control-label" for="s_info"><?php _e('Description', 'dadebo'); ?></label>
                <div class="controls">
                    <?php osc_goto_first_locale();  UserForm::info_textarea('s_info', osc_locale_code(), @$osc_user['locale'][osc_locale_code()]['s_info']); ?>
                </div>
            </div>
            <?php osc_run_hook('user_profile_form', osc_user()); ?>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="ui-button ui-button-middle ui-button-main"><?php _e("Update", 'dadebo');?></button>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <?php osc_run_hook('user_form', osc_user()); ?>
                </div>
            </div>
        </form>
    </div>
</div>
<!--
<script type="text/javascript">
    $(document).ready(function() {

        $("#countryId").on("change",function() {
            var pk_c_code = $(this).val();
            var regionId = '<select name="regionId" id="regionId" ></select>';

            if ($("#regionId").next("a").length == 1 && $("#regionId").next('a').find('span').eq(0).html() != '<?php _e('Select a region...'); ?>') {
                $("#regionId").next('a').find('span').eq(0).html('<?php _e('Select a region...'); ?>');
            }

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a city...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a city...'); ?>');
            }

            <?php if($path=="admin") { ?>
                var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php } else { ?>
                var url = '<?php echo osc_base_url(true)."?page=ajax&action=regions&countryId="; ?>' + pk_c_code;
            <?php }; ?>

            var result = '';

            if(pk_c_code != '') {

                $("#regionId").attr('disabled',false);
                $("#cityId").attr('disabled',true);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;
                        var nonZero = 0;

                        if(length > 0) {
                            console.log("region length > 0");
                            result += '<option value=""><?php echo osc_esc_js(__("Select a region...")); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }
                            
                            if($("#regionId").length){
                                $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                                $("#regionId").closest('div').remove();
                            }
                            $("#region").before('<select name="regionId" id="regionId" ></select>');
                            $("#region").remove();

//                            $("#city").before('<select name="cityId" id="cityId" ></select>');
//                            $("#city").remove();

                            $("#regionId").val("");
                            
                            nonZero = 1;

                        }
                        else {
                            if($("#regionId").length) {
                                console.log("select box");
                                $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                                $("#regionId").closest('div').remove();
                            }
                            
                            if($("#cityId").length) {
                                console.log("select box");
                                $("#cityId").parent().before('<input type = "text" name = "city" id = "city" />');
                                $("#cityId").closest('div').remove();
                            }
                        }

                        $("#regionId").html(result);
                        
                        if (nonZero == 1){
                            if ($("#regionId").next("a").length == 0) {
                                selectUi($("#regionId"));
                            }
                        }
                    }
                 });

             } else {
                 if($("#regionId").length) {
                    $("#regionId").parent().before('<input type = "text" name = "region" id = "region" />');
                    $("#regionId").closest('div').remove();
                 }
                            
                 if($("#cityId").length) {
                    $("#cityId").parent().before('<input type = "text" name = "city" id = "city" />');
                    $("#cityId").closest('div').remove();
                 }
                 
             }
        });

        $("body").on("change", 'select#regionId', function(){

            if ($("#cityId").next("a").length == 1 && $("#cityId").next('a').find('span').eq(0).html() != '<?php _e('Select a city...'); ?>') {
                $("#cityId").next('a').find('span').eq(0).html('<?php _e('Select a city...'); ?>');
            }

            var data = "";

            var pk_c_code = $(this).val();
            <?php if($path=="admin") { ?>
                var url = '<?php echo osc_admin_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + pk_c_code;
            <?php } else { ?>
                var url = '<?php echo osc_base_url(true)."?page=ajax&action=cities&regionId="; ?>' + pk_c_code;
            <?php }; ?>

            var result = '';

            if(pk_c_code != '') {

                $("#cityId").attr('disabled',false);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;
                        var nonZero = 0;
                        if(length > 0) {
                            console.log("city length > 0");
                            result += '<option selected value=""><?php echo osc_esc_js(__("Select a city...")); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }

                            $("#city").before('<select name="cityId" id="cityId" ></select>');
                            $("#city").remove();
                            
                            nonZero = 1;
                        }
                        else {
                            //result += '<option value=""><?php echo osc_esc_js(__('No results')); ?></option>';
                            $("#cityId").parent().before('<input type="text" name="city" id="city" />');
                            $("#cityId").closest('div').remove();
                        }
                        

                        $("#cityId").html(result);

                        if(nonZero == 1){
                            if ($("#cityId").next("a").length == 0) {
                                selectUi($("#cityId"));
                            }
                        }

                        if ($("#region").length > 0) {
                            console.log("hide city parent parent");
                            //$("#city").parent().parent().hide();
                            
                        }
                        /*
                        // Show or hide the inputs.
                        if (data.length > 0) {
                            $("#cityId").parent().parent().parent().show();
                            $("#cityId").parent().parent().show();
                        } else {
                            $("#cityId").parent().parent().parent().hide();
                            $("#cityId").parent().parent().hide();
                            $("#cityId").html('<option value="0">Default</option>');
                        }
                        */
                    }
                 });
             } else {
                $("#cityId").attr('disabled',true);
             }
        });

        if( $("#regionId").attr('value') == "") {
            console.log("regionId attr empty");
            $("#cityId").attr('disabled',true);
        }

        if($("#countryId").length != 0) {
            if( $("#countryId").prop('type').match(/select-one/) ) {
                console.log("countryIdtype match");
                if( $("#countryId").attr('value') == "") {
                    $("#regionId").attr('disabled',true);
                }
            }
        }
    });
</script>
-->
<?php osc_current_web_theme_path('footer.php') ; ?>