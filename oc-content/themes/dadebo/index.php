<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

/*
Theme Name: dadebo
Theme URI: http://jonajo.com/themes/dadebo
Description: Dadebo
Version: 1.0
Author: Jonajo
Author URI: http://jonajo.com/
Widgets:  header, footer, leftsidebar, rightsidebar
Theme update URI: Dadebo
*/

    function rons_theme_info() {
        return array(
             'name'        => 'Dadebo Theme'
            ,'version'     => '1.0'
            ,'description' => 'Dadebo theme'
            ,'author_name' => 'Jonajo'
            ,'author_url'  => 'http://www.jonajo.com/'
            ,'locations'   => array('header', 'footer', 'leftsidebar', 'rightsidebar')
        );
    }

?>