<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','dadebo_follow_construct');

    dadebo_add_body_class('home');
?>
<?php osc_current_web_theme_path('header.php') ; ?>
<div class="clear"></div>
</div><!-- main -->
<div id="right-sidebar">
    <?php if( osc_get_preference('sidebar-300x250', 'dadebo') != '') {?>
    <!-- sidebar ad 350x250 -->
    <div class="ads_300">
        <?php echo osc_get_preference('sidebar-300x250', 'dadebo'); ?>
    </div>
    <!-- /sidebar ad 350x250 -->
    <?php } ?>
    <?php if ( osc_count_web_enabled_locales() > 1) { ?>
            <?php osc_goto_first_locale(); ?>
            <?php $i = 0;  ?>
            <span id="select-language" class="see_by full_see_by">
                <label><?php echo _e('Language:', 'dadebo'); ?><b class="arrow-envelope"><b class="arrow-down"></b></b></label>
                <ul>
            <?php while ( osc_has_web_enabled_locales() ) { ?>
                    <li><a href="<?php echo osc_change_language_url ( osc_locale_code() ); ?>"><?php echo osc_locale_name(); ?></a></li>
            <?php } ?>
                </ul>
            </span>
        <?php } ?>
    <div class="widget-box">
        <?php View::newInstance()->_exportVariableToView('list_countries', Search::newInstance()->listCountries('>=') ) ; ?>
        <?php if(osc_count_list_countries() > 0) {?>
            <div class="box location">
                <h3><strong>Location</strong></h3>
                <ul>
                    <?php
                        $countriesHardcodedForTraditional = ['內陸帝國', '舊金山灣區', '洛杉磯', '薩克拉門托'];
                        $urlsHardcodedForTraditional = ['https://inland-empire.dadebo.com/', 'https://sf-bay-area.dadebo.com/',
                        'https://la.dadebo.com/', 'https://sacramento.dadebo.com/'];

                        $countriesHardcodedForSimplified = ['三藩市湾区', '萨克拉门托', '拉斯维加斯', '洛杉矶', '内陆地区'];  
                        $urlsHardcodedForSimplified = ['http://sf-bay-area.dadebo.com/', 'http://sacramento.dadebo.com/', 'http://las-vegas.dadebo.com/','http://la.dadebo.com/', 'http://inland-empire.dadebo.com/'];
                    ?>

                        <?php
                        $matches = array();

                        if (preg_match("@http:\/\/(.*?).dadebo.com\/@msi", osc_base_url(), $matches)) {
                            $url = $matches[0];
                        }
                        ?>
                        <?php while(osc_has_list_countries()) {
				$current_location = "https://".$_SERVER['HTTP_HOST']."/";
				?>
                            <li class="<?php if($current_location==osc_list_country_url()){ echo 'selected-location';} ?>" >
                                <a href="<?php echo osc_list_country_url();?>">
                                    <?php if ($url == osc_list_country_url()) { ?>
                                    <b><?php echo osc_list_country_name(); ?><b>
                                            <?php } else { ?>
                                                <?php echo osc_list_country_name(); ?>
                                            <?php } ?>
                                </a>
                            </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    </div>
	<?php osc_run_hook('add_to_right_sidebar_second'); ?>
</div><div class="clear"><!-- do not close, use main clossing tag for this case -->
<?php if( osc_get_preference('f', 'dadebo') != '') { ?>
<!-- homepage ad 728x60-->
<div class="ads_728">
    <?php echo osc_get_preference('homepage-728x90', 'dadebo'); ?>
</div>
<!-- /homepage ad 728x60-->
<?php } ?>
<?php osc_current_web_theme_path('footer.php') ; ?>
