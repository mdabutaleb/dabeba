<?php
    if(isset($_POST['submit'])) 
    {
        
        require_once 'ModelMoments.php';
        $user_id = osc_logged_user_id();
        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png'); // These will be the types of file that will pass the validation.
        $cout = 1;
        //validation of image file type
            while($cout<4){
                $pic = 'moment_pic_'.$cout;
                if($_FILES[$pic]['name'] !=""){
                    $filename = $_FILES[$pic]['name'];// Get the name of the file (including file extension).
                    $ext = strtolower(substr($filename, strpos($filename,'.'), strlen($filename)-1)); // Get the extension from the filename.
                    $image_name = 'moment_'.$moment_id.'_'.$cout.$ext;
                    // Check if the filetype is allowed, if not return error.
                    if(!in_array($ext,$allowed_filetypes)){
                        $_SESSION['moment_title'] = $_POST['moment_title'];
                        $_SESSION['moment_message'] = $_POST['moment_message'];
                        header('Location: '.osc_route_url('post-moment'));
                        osc_add_flash_error_message(__('Image is not valid', 'moments'));
                        die();
                    }
                }
                $cout++;
            }
        if($user_id!=""){
            $title = $_POST['moment_title'];
            $description = $_POST['moment_message'];
            $moment_id = ModelMoments::newInstance()->insertMoment($user_id, $title, $description);
            //die('**');
            //code to process the moment images
                $upload_path = osc_plugins_path().'moments/images/';
                $cout = 1;
                while($cout<4){
                    $pic = 'moment_pic_'.$cout;
                    if($_FILES[$pic]['name'] !=""){
                        $filename = $_FILES[$pic]['name'];// Get the name of the file (including file extension).
                        $ext = strtolower(substr($filename, strpos($filename,'.'), strlen($filename)-1)); // Get the extension from the filename.
                        $image_name = 'moment_'.$moment_id.'_'.$cout.$ext;
                        // Check if the filetype is allowed, if not return error.
                        if(!in_array($ext,$allowed_filetypes)){
                            die('The file you attempted to upload is not allowed.');
                        }
                        // Check if we can upload to the specified path, if not return error.
                        if(!is_writable($upload_path))
                        {
                            die('You cannot upload to the specified directory, please CHMOD it to 777.');
                        }
                        // Upload the file to your specified path.
                        if(move_uploaded_file($_FILES[$pic]['tmp_name'],$upload_path.$image_name)){
                            ModelMoments::newInstance()->insertMomentPic($moment_id, $image_name);
                        }
                    }
                    $cout++;
                }
            if(isset($_SESSION['moment_title'])){
                $_SESSION['moment_title'] = "";
                $_SESSION['moment_message'] = "";
            }
            header('Location: '.osc_route_url('moments'));
            osc_add_flash_ok_message(__('Moment posted successfully', 'moments'));
            
        }
        
        
    }
        
?>
<h2 class="render-title"><?php _e('Post your Moment', 'moments'); ?></h2>
<div class="moment-form form-container">
    <form action="" method="post" enctype="multipart/form-data" >
        <fieldset>
            <div class="form-horizontal">
                <div class="form-row controls">
                    <div class="form-label"><?php _e('Title', 'moments') ?></div>
                    <div class="form-controls"><input type="text" name="moment_title" value="<?php echo $_SESSION['moment_title']; ?>" maxlength="100" required oninvalid="this.setCustomValidity('<?php echo _e('Please fill out this field', 'dadebo'); ?>')"></div>
                </div>
                <div class="form-row controls">
                    <div class="form-label"><?php _e('Message', 'moments') ?></div>
                    <div class="form-controls"><textarea name="moment_message" rows="10" required><?php echo $_SESSION['moment_message']; ?></textarea></div>
                </div>
                <div class="form-row">
                    <h5><?php _e('Upload Moment Pics', 'moments'); ?></h5>
                </div>
                <div class="form-row">
                    <input type="file" name="moment_pic_1" id="moment_pic_1" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'moments'); ?>" data-buttonText="<?php _e('Choose file', 'moments'); ?>">
                </div>
               <div class="form-row">
                    <input type="file" name="moment_pic_2" id="moment_pic_2" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'moments'); ?>" data-buttonText="<?php _e('Choose file', 'moments'); ?>">
                </div>
                <div class="form-row">
                    <input type="file" name="moment_pic_3" id="moment_pic_3" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'moments'); ?>" data-buttonText="<?php _e('Choose file', 'moments'); ?>">
                </div>
                
                <div class="form-actions">
                    <input type="submit" value="<?php _e('Send', 'moments'); ?>" name="submit" class="btn btn-submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>

