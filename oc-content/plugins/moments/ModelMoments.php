<?php
    
    class ModelMoments extends DAO
    {
        /**
         * It references to self object: ModelMoments.
         * It is used as a singleton
         * 
         * @access private
         * @since 3.0
         * @var ModelJobs
         */
        private static $instance ;

        /**
         * It creates a new ModelJobs object class ir if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since 3.0
         * @return ModelJobs
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        /**
         * Construct
         */
        function __construct()
        {
            parent::__construct();
        }
        
         /**
         * Import sql file
         * @param type $file 
         */
        public function import($file) {
            $path = osc_plugin_resource($file);
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( $this->dao->getErrorLevel().' - '.$this->dao->getErrorDesc() );
            }
        }
        
        /**
         * Return table name
         * @return string
         */
        public function getTableName()
        {
            return DB_TABLE_PREFIX.'t_moments' ;
        }
        
       
        /**
         *  Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query('DROP TABLE '. DB_TABLE_PREFIX.'t_moment_picture');
            $this->dao->query('DROP TABLE '. $this->getTableName());
            
        }
        
       
        /**
         * Insert Moment
         */
        public function insertMoment($user_id, $title, $description)
        {
            $aSet = array(
                'fk_i_user_id'   => $user_id,
                's_title' => $title,
                's_description'    => $description,
                'dt_pub_date'   => date('Y-m-d H:i:s')
            );
            
            $this->dao->insert($this->getTableName(), $aSet);
            return $this->dao->insertedId();
        }
        
        
        /**
         * Insert Moment pics
         */
        public function insertMomentPic($moment_id, $image_name)
        {
            $aSet = array(
                'fk_i_moment_id' => $moment_id,
                'pic_name' => $image_name
            );
            
            return $this->dao->insert(DB_TABLE_PREFIX.'t_moment_picture', $aSet);
        }
        
        
        /**
         * get Moments by page
         */
        public function getMoments($page)
        {
	    $items_per_page = 10;	
	    $offset = ($page - 1) * $items_per_page;

            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
	    $this->dao->limit($offset, $items_per_page);
            $this->dao->orderBy('pk_i_id', 'DESC') ;
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
			
		
        }
        /**
         * get all moments
         */
        public function allMoments()
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
			
		
        }
        
        /**
         * get all moments by user
         */
        public function allUserMoments($user_id)
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
            $this->dao->where('fk_i_user_id', $user_id);
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
			
		
        }
        
        /**
         * delete Moment
         */
        public function deleteMoment($moment_id)
        {
            $this->dao->delete( $this->getTableName(), array('pk_i_id' => $moment_id) );
            
        }
         /**
         * delete Moment images
         */
        public function deleteMomentImages($moment_id)
        {
            $this->dao->delete( DB_TABLE_PREFIX.'t_moment_picture', array('fk_i_moment_id' => $moment_id) ) ;
        }        
        
        /**
         * get Moments count
         */
        public function getTotalMoments()
        {
	    
            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
            $results = $this->dao->get();
            return count($results->result());
        }
        
        
        /**
         * get Moments by id
         */
        public function getMomentsByID($ID)
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName());
            $this->dao->where('pk_i_id', $ID);
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }
            $moment=$result->row();
            return $moment;
        }
        
        
        /**
         * get Moments by id
         */
        public function getMomentImage($ID)
        {
            $this->dao->select();
            $this->dao->from(DB_TABLE_PREFIX.'t_moment_picture');
            $this->dao->where('fk_i_moment_id', $ID);
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }
            return $results->result();
        }
        
        
      
    }
?>
