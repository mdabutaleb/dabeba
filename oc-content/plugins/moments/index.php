<?php
/*
Plugin Name: Moments
Plugin URI: http://dadebo.com/
Description: This plugin Enables Moments feature to site
Version: 1.0.0
Author: Jonajo Consulting
Author URI: http://dadebo.com/
*/
    require_once 'ModelMoments.php';
    function moments_install() {
        ModelMoments::newInstance()->import('moments/struct.sql') ;
        osc_set_preference('moments_disclaimer', 'By clicking the link below you confirm that you are 18 or older and understand "Moments" may include offensive content.', 'plugin-moments', 'STRING');
    }

    function moments_uninstall() {
        ModelMoments::newInstance()->uninstall();
        Preference::newInstance()->delete( array('s_section' => 'plugin-moments') );
    }
    
    function moments_admin() {
        osc_admin_render_plugin('moments/admin.php');
    }
    
    function moments_disclaimer() {
        $option = Params::getParam('option');

        if( Params::getParam('file') != 'moments/admin.php' ) {
            return '';
        }

        if( $option == 'set_disclaimer' ) {
            $disclaimer_text = Params::getParam('disclaimer_text');
            osc_set_preference('moments_disclaimer', $disclaimer_text, 'plugin-moments', 'STRING');

            osc_add_flash_ok_message(__('The Disclaimer text has been updated', 'moments'), 'admin');
            osc_redirect_to(osc_admin_render_plugin_url('moments/admin.php'));
        }
    }
    osc_add_hook('init_admin', 'moments_disclaimer');
    
    // function to post the moment 
    function post_moment() {
        
        if( Params::getParam('route') == 'post-moment' ) {
            $user_id = osc_logged_user_id();
            if($user_id==""){
                osc_add_flash_error_message(__('Please Login to post the Moment', 'moments'));
                osc_redirect_to(osc_user_login_url());
            }
        }
    }
    osc_add_hook('init', 'post_moment');
    
    function style_moment() { 
            echo '<link type="text/css" rel="stylesheet" href="'.osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__).'css/moment.css">';
    }
    
    function get_user_name($user_id){
        $conn = getConnection();
        $user_name = $conn->osc_dbFetchResults("SELECT s_name FROM %st_user WHERE pk_i_id='%d'", DB_TABLE_PREFIX, $user_id);
        $user_name =$user_name[0]['s_name'];
        return $user_name;
    }
    function expiry_cron(){
        $all_moments = ModelMoments::newInstance()->allMoments();
        $current_date = date('Y-m-d H:i:s');
        foreach($all_moments as $k => $moment){
            $publish_date = strtotime($moment['dt_pub_date']);
            $endDate = date('Y-m-d H:i:s', strtotime('+10 days', $publish_date));
            if($endDate < $current_date){
                ModelMoments::newInstance()->deleteMoment($moment['pk_i_id']);
                $moment_images = ModelMoments::newInstance()->getMomentImage($moment['pk_i_id']);
                if(!empty($moment_images)){
                    foreach($moment_images as $k => $image){
                            unlink(osc_content_path().'plugins/moments/images/'.$image['pic_name']);
                    }
                    ModelMoments::newInstance()->deleteMomentImages($moment['pk_i_id']);
                }
            }
        }
    }
    
    function delete_user_moments($user_id){
        $all_moments = ModelMoments::newInstance()->allUserMoments($user_id);
        foreach($all_moments as $k => $moment){
                ModelMoments::newInstance()->deleteMoment($moment['pk_i_id']);
                $moment_images = ModelMoments::newInstance()->getMomentImage($moment['pk_i_id']);
                if(!empty($moment_images)){
                    foreach($moment_images as $k => $image){
                            unlink(osc_content_path().'plugins/moments/images/'.$image['pic_name']);
                    }
                    ModelMoments::newInstance()->deleteMomentImages($moment['pk_i_id']);
                }
        }
        
    }

 
    // This is needed in order to be able to activate the plugin
    osc_register_plugin(osc_plugin_path(__FILE__), 'moments_install');
    // This is a hack to show a Uninstall link at plugins table (you could also use some other hook to show a custom option panel)
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'moments_uninstall');
    osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'moments_admin');
    osc_add_hook('header','style_moment');
    
    osc_add_route('moments-disclaimer', 'moments-disclaimer', 'moments-disclaimer', osc_plugin_folder(__FILE__).'disclaimer-page.php');
    osc_add_route('moments', 'moments/page/([0-9]+)', 'moments/page/{iPage}', osc_plugin_folder(__FILE__).'moments.php');
    osc_add_route('moment', 'moment/id/([0-9]+)', 'moment/id/{ID}', osc_plugin_folder(__FILE__).'single-moment.php');
    osc_add_route('moment-del', 'moment-delete/id/([0-9]+)', 'moment-delete/id/{ID}', osc_plugin_folder(__FILE__).'single-moment.php');
    osc_add_route('post-moment', 'post-moment', 'post-moment', osc_plugin_folder(__FILE__).'post-moment.php');
    osc_add_hook('cron_hourly', 'expiry_cron');
    osc_add_hook('delete_user', 'delete_user_moments');
?>
