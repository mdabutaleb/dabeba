<?php
    require_once 'ModelMoments.php';
    $moment_id = Params::getParam('ID');
    $moment = ModelMoments::newInstance()->getMomentsByID($moment_id);
    if( Params::getParam('route') == 'moment-del' ) {
            if(osc_logged_user_id() == $moment['fk_i_user_id']){
                ModelMoments::newInstance()->deleteMoment($moment['pk_i_id']);
                $moment_images = ModelMoments::newInstance()->getMomentImage($moment['pk_i_id']);
                if(!empty($moment_images)){
                    foreach($moment_images as $k => $image){
                            unlink(osc_content_path().'plugins/moments/images/'.$image['pic_name']);
                    }
                    ModelMoments::newInstance()->deleteMomentImages($moment['pk_i_id']);
                }
            header('Location: '.osc_route_url('moments'));
            osc_add_flash_ok_message(__('Moment Deleted successfully', 'moments'));

            }
    }

    ?>
    <div class="clear"></div>
    <div class="neighborhood-alert-row">
        <div class="user-image">
            <?php profile_picture_show($moment['fk_i_user_id']); ?>
        </div>
        <div class="neighborhood-alert-contents">
            <div class="neighborhood-alert-detail">
                <h5><a href="<?php echo osc_user_public_profile_url( $moment['fk_i_user_id']); ?>"><?php echo get_user_name($moment['fk_i_user_id']); ?></a></h5>
                <span class="moment-date"><?php echo osc_format_date($moment['dt_pub_date']); ?></span>
                <?php
                    if(osc_logged_user_id() == $moment['fk_i_user_id']){
                        echo '<span class="delete-post"><a href="'.osc_route_url('moment-del', array('ID' => $moment['pk_i_id'])).'">'; _e('Delete', 'moments'); echo '</a><span>';
                    }
                ?>
            </div>
            <div class="clear"></div>
            <p><?php echo $moment['s_title']; ?></p>
            <p class="neighborhood-alert-disc"><?php echo $moment['s_description']; ?></p>
            <div class="neighborhood-alert-imgs">
                <?php
                    $images = ModelMoments::newInstance()->getMomentImage($moment_id);
                    //echo '<pre>';print_r($images); echo '<pre>';
                    foreach($images as $k => $image){
                        echo '<div class="neighborhood-alert-image"><img src="'.osc_base_url().'oc-content/plugins/moments/images/'.$image['pic_name'].'"></div>';
                    }
                ?>
            </div>
        </div>
    </div>
