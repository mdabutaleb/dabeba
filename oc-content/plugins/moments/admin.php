<h2 class="render-title"><?php _e('Moments Setting', 'moments'); ?></h2>
<form action="<?php echo osc_admin_render_plugin_url('moments/admin.php'); ?>" method="post">
    <input type="hidden" name="option" value="set_disclaimer" />
    <fieldset>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Disclaimer Text', 'moments') ?></div>
                <div class="form-controls"><textarea name="disclaimer_text" rows="10"><?php echo osc_get_preference('moments_disclaimer', 'plugin-moments'); ?></textarea></div>
            </div>
            <div class="form-actions">
                <input type="submit" value="Save changes" class="btn btn-submit">
            </div>
        </div>
    </fieldset>
</form>