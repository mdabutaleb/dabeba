<?php
    require_once 'ModelMoments.php';
    $page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
    $moments = ModelMoments::newInstance()->getMoments($page);
    echo '<div class="moment-header"><h2>'; _e('Moments', 'moments'); echo '</h2><a href="https://dadebo.com/index.php?page=custom&route=full-calendar&date='.date('Y-m-d').'" title="Place your moment"><span class="plus_btn pull-right">
    <i class="fa fa-plus"></i></span></a></div><div class="clear"></div>';
    foreach($moments as $k => $moment){ ?>

		<div class="neighborhood-alert-row">
			<div class="user-image">
				<?php profile_picture_show($moment['fk_i_user_id']); ?>
			</div>
			<div class="neighborhood-alert-contents">
				<div class="neighborhood-alert-detail">

					<div>
						<h2 class="neighborhood_alert-title"><a href="<?php echo osc_route_url('moment', array('ID' => $moment['pk_i_id'])); ?>"><?php echo $moment['s_title']; ?></a></h2>
						<p>Posted By : <a href="<?php echo osc_user_public_profile_url( $moment['fk_i_user_id']); ?>" class="neighborhood_alert-username"><?= get_users_name($moment['fk_i_user_id']); ?></a></p>
					</div>
					<span class="neighborhood-alert-date"><?php echo $moment['dt_pub_date'] ?></span>
			</div>
				<div class="clear"></div>
			</div>
			<p class="neighborhood-alert-description">
				<?php
					$description = $moment['s_description'];
					if (strlen($description) > 280) {

						// truncate string
						$stringCut = substr($description, 0, 280);

						// make sure it ends in a word so assassinate doesn't become ass...
						$description = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.osc_route_url('moment', array('ID' => $moment['pk_i_id'])).'">Read More</a>';
					}
					echo $description;

				?>
			</p>
			<div class="neighborhood_alert-images">
				<div class="slider">
				  <ul>
					  <?php
						$images = ModelMoments::newInstance()->getMomentImage($moment['pk_i_id']);
						if(!empty($images)){
							foreach($images as $k => $image){
								echo '<li><img src="'.osc_base_url().'oc-content/plugins/moments/images/'.$image['pic_name'].'"></li>';
							}
						}

					?>
				  </ul>
				</div>
			</div>
		</div>
    <?php }


    //Pagination for moments
        $items_per_page = 10;
        $pageActual = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
        $urlActual  = osc_route_url('moments');
        $urlActual  = preg_replace('/&iPage=(\d+)?/', '', $urlActual);
        $total_moments = ModelMoments::newInstance()->getTotalMoments();
        $pageTotal  = ceil($total_moments/$items_per_page);
        $params     = array(
            'total'    => $pageTotal,
            'selected' => $pageActual - 1,
            'url'      => $urlActual . '&iPage={PAGE}',
            'sides'    => 5
        );
        echo '<div class="paginate" >';
        echo osc_pagination($params);
        echo '</div>';



?>

<script type="text/javascript">
	(function($){
		var dates = $('.neighborhood-alert-date');

		dates.each(function(i,date) {
			var currentDate = $(date).text()
			var humanRedeableDate = moment(currentDate).fromNow();
			$(date).text(humanRedeableDate);
		});

		$('.slider').unslider({
			pagination : false
		});


	}(jQuery));
</script>
