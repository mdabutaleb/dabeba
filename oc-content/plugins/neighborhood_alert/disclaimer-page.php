<h3><?php/* echo osc_get_preference('neighborhood_disclaimer', 'plugin-neighborhood');*/ ?>
<?php _e('By clicking the link below you confirm that you are 18 or older and understand "Neighborhood Alerts" may include offensive content.Post your Neighborhood Alert', 'neighborhood_alert'); ?>
</h3>
<h3><a href="<?php echo osc_route_url('neighborhood-alerts'); ?>"><?php _e('Go To Neighborhood Alerts', 'neighborhood_alert'); ?></a></h3>
<h5><?php _e('Neighborhood Alerts will only show alerts in the same zip code as yours*', 'neighborhood_alert'); ?></h5>