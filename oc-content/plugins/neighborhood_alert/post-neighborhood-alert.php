<?php
    if(isset($_POST['submit'])) 
    {
        
        require_once 'ModelNeighborhood.php';
        $user_id = osc_logged_user_id();
        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png'); // These will be the types of file that will pass the validation.
        $cout = 1;
        //validation of image file type
            while($cout<4){
                $pic = 'neighborhood_alert_pic_'.$cout;
                if($_FILES[$pic]['name'] !=""){
                    $filename = $_FILES[$pic]['name'];// Get the name of the file (including file extension).
                    $ext = strtolower(substr($filename, strpos($filename,'.'), strlen($filename)-1)); // Get the extension from the filename.
                    // Check if the filetype is allowed, if not return error.
                    if(!in_array($ext,$allowed_filetypes)){
                        $_SESSION['neighborhood_alert_title'] = $_POST['neighborhood_alert_title'];
                        $_SESSION['neighborhood_alert_message'] = $_POST['neighborhood_alert_message'];
                        header('Location: '.osc_route_url('post-neighborhood-alert'));
                        osc_add_flash_error_message(__('Image is not valid', 'neighborhood_alert'));
                        die();
                    }
                }
                $cout++;
            }
        if($user_id!=""){
            $title = $_POST['neighborhood_alert_title'];
            $description = $_POST['neighborhood_alert_message'];
            $neighborhood_alert_id = ModelNeighborhood::newInstance()->insertNeighborhoodAlert($user_id, $title, $description);
            //die('**');
            //code to process the neighborhood alert images
                $upload_path = osc_plugins_path().'neighborhood_alert/images/';
                $cout = 1;
                while($cout<4){
                    $pic = 'neighborhood_alert_pic_'.$cout;
                    if($_FILES[$pic]['name'] !=""){
                        $filename = $_FILES[$pic]['name'];// Get the name of the file (including file extension).
                        $ext = strtolower(substr($filename, strpos($filename,'.'), strlen($filename)-1)); // Get the extension from the filename.
                        $image_name = 'neighborhood_alert_'.$neighborhood_alert_id.'_'.$cout.$ext;
                        // Check if the filetype is allowed, if not return error.
                        if(!in_array($ext,$allowed_filetypes)){
                            die('The file you attempted to upload is not allowed.');
                        }
                        // Check if we can upload to the specified path, if not return error.
                        if(!is_writable($upload_path))
                        {
                            die('You cannot upload to the specified directory, please CHMOD it to 777.');
                        }
                        // Upload the file to your specified path.
                        if(move_uploaded_file($_FILES[$pic]['tmp_name'],$upload_path.$image_name)){
                            ModelNeighborhood::newInstance()->insertNeighborhoodAlertPic($neighborhood_alert_id, $image_name);
                        }
                    }
                    $cout++;
                }
            
            if($neighborhood_alert_id != ""){
                $user_zip_code = get_logged_user_zip_code($user_id);
                $users = get_users_with_same_zip($user_zip_code);
                $alert_url = osc_route_url('neighborhood-alert', array('ID' => $neighborhood_alert_id));
                foreach($users as $k => $userId){
                    if(is_neighborhood_alert_allowed($userId)){
                        send_neighborhood_alert_email($userId, $alert_url);
                    }
                }
            }
            if(isset($_SESSION['neighborhood_alert_title'])){
                $_SESSION['neighborhood_alert_title'] = "";
                $_SESSION['neighborhood_alert_message'] = "";
            }
                
            header('Location: '.osc_route_url('neighborhood-alerts'));
            osc_add_flash_ok_message(__('Neighborhood Alert posted successfully', 'neighborhood_alert'));
            
        }
        
        
    }
        
?>
<?php
    $user_id = osc_logged_user_id() ;
    $user_zip_code = get_logged_user_zip_code($user_id);
    if($user_zip_code ==""){
        echo '<div class="zip-code-message"><h5>ZipCode is required to post Neighborhood Alert.</h5>';
        echo '<a href="'.osc_user_profile_url().'"><h6>Update your ZipCode</h6></a>';
        echo '</div>';
        
    }
    else{
?>
<h2 class="render-title"><?php _e('Post your Neighborhood Alert', 'neighborhood_alert'); ?></h2>
<div class="neighborhood-alert-form form-container">
    <form action="" method="post" enctype="multipart/form-data" >
        <fieldset>
            <div class="form-horizontal">
                <div class="form-row controls">
                    <div class="form-label"><?php _e('Title', 'neighborhood_alert') ?></div>
                    <div class="form-controls"><input type="text" name="neighborhood_alert_title" value="<?php echo $_SESSION['neighborhood_alert_title']; ?>" maxlength="100"  required></div>
                </div>
                <div class="form-row controls">
                    <div class="form-label"><?php _e('Message', 'neighborhood_alert') ?></div>
                    <div class="form-controls"><textarea name="neighborhood_alert_message" rows="10" required><?php echo $_SESSION['neighborhood_alert_message']; ?></textarea></div>
                </div>
                <div class="form-row">
                    <h5><?php _e('Upload Neighborhood Alert Pics', 'neighborhood_alert'); ?></h5>
                </div>
                <div class="form-row">
                    <input type="file" name="neighborhood_alert_pic_1" id="neighborhood_alert_pic_1" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'neighborhood_alert'); ?>" data-buttonText="<?php _e('Choose file', 'neighborhood_alert'); ?>">
                </div>
               <div class="form-row">
                    <input type="file" name="neighborhood_alert_pic_2" id="neighborhood_alert_pic_2" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'neighborhood_alert'); ?>" data-buttonText="<?php _e('Choose file', 'neighborhood_alert'); ?>">
                </div>
                <div class="form-row">
                    <input type="file" name="neighborhood_alert_pic_3" id="neighborhood_alert_pic_3" class="filestyle" data-buttonBefore="true" data-placeholder="<?php _e(' no file chosen', 'neighborhood_alert'); ?>" data-buttonText="<?php _e('Choose file', 'neighborhood_alert'); ?>">
                </div>
                
                <div class="form-actions">
                    <input type="submit" value="<?php _e('Send', 'neighborhood_alert'); ?>" name="submit" class="btn btn-submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>
<?php } ?>
