<?php
/*
Plugin Name: Neighborhood alert
Plugin URI: http://dadebo.com/
Description: This plugin Enables neighborhood alert feature to site
Version: 1.0.0
Author: Jonajo Consulting
Author URI: http://dadebo.com/
*/
    require_once 'ModelNeighborhood.php';
    function neighborhood_install() {
        ModelNeighborhood::newInstance()->import('neighborhood_alert/struct.sql') ;
        ModelNeighborhood::newInstance()->insertEmailTemplates();
        osc_set_preference('neighborhood_disclaimer', 'By clicking the link below you confirm that you are 18 or older and understand "Neighborhood Alerts" may include offensive content.', 'plugin-neighborhood', 'STRING');
    }

    function neighborhood_uninstall() {
        ModelNeighborhood::newInstance()->uninstall();
        Preference::newInstance()->delete( array('s_section' => 'plugin-neighborhood') );
    }

    function neighborhood_admin() {
        osc_admin_render_plugin('neighborhood_alert/admin.php');
    }

    function neighborhood_disclaimer() {
        $option = Params::getParam('option');

        if( Params::getParam('file') != 'neighborhood_alert/admin.php' ) {
            return '';
        }

        if( $option == 'set_disclaimer' ) {
            $disclaimer_text = Params::getParam('disclaimer_text');
            osc_set_preference('neighborhood_disclaimer', $disclaimer_text, 'plugin-neighborhood', 'STRING');

            osc_add_flash_ok_message(__('The Disclaimer text has been updated', 'neighborhood'), 'admin');
            osc_redirect_to(osc_admin_render_plugin_url('neighborhood_alert/admin.php'));
        }
    }
    osc_add_hook('init_admin', 'neighborhood_disclaimer');

    // function to post the neighborhood alert
    function post_neighborhood() {
        $route = Params::getParam('route');
        if( $route == 'post-neighborhood-alert' || $route == 'neighborhood-alert-disclaimer' || $route == 'neighborhood-alerts' || $route == 'neighborhood-alert') {
            $user_id = osc_logged_user_id();
            if($user_id==""){
                osc_add_flash_error_message(__('Please login to access Neighborhood alert', 'neighborhood'));
                Session::newInstance()->_setReferer("index.php?page=custom&route=neighborhood-alert-disclaimer");
                osc_redirect_to(osc_user_login_url());
            }
        }
    }
    osc_add_hook('init', 'post_neighborhood');

    function style_neighborhood() {
            echo '<link type="text/css" rel="stylesheet" href="'.osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__).'css/neighborhood.css?v=231231">';
    }

    function get_users_name($user_id){
        $conn = getConnection();
        $user_name = $conn->osc_dbFetchResults("SELECT s_name FROM %st_user WHERE pk_i_id='%d'", DB_TABLE_PREFIX, $user_id);
        $user_name =$user_name[0]['s_name'];
        return $user_name;
    }

    function get_logged_user_zip_code($user_id){
        $conn = getConnection();
        $user_zip = $conn->osc_dbFetchResults("SELECT s_zip FROM %st_user WHERE pk_i_id='%d'", DB_TABLE_PREFIX, $user_id);
        $user_zip =$user_zip[0]['s_zip'];
        return $user_zip;
    }
    function get_users_with_same_zip($user_zip_code){
        $conn = getConnection();
        $user_ids = $conn->osc_dbFetchResults("SELECT pk_i_id FROM %st_user WHERE s_zip='%d'", DB_TABLE_PREFIX, $user_zip_code);
        foreach($user_ids as $k => $user_id){
            $users[] = $user_id['pk_i_id'];
        }
        //$users = implode(", ",$users);
        return $users;
    }


    /**
     * Send Neighborhood alert email to user
     * */

    function send_neighborhood_alert_email($user_id, $alert_url) {

        $mPages = new Page() ;
        $aPage = $mPages->findByInternalName('neighborhood_alert') ;
        $locale = osc_current_user_locale() ;
        $content = array();
        if(isset($aPage['locale'][$locale]['s_title'])) {
            $content = $aPage['locale'][$locale];
        } else {
            $content = current($aPage['locale']);
        }

        $user = User::newInstance()->findByPrimaryKey($user_id);
        $user_name = $user['s_name'];
        $user_email = $user['s_email'];

        $words   = array();
        $words[] = array('{WEB_TITLE}', '{CONTACT_NAME}', '{ALERT_URL}');
        $words[] = array(osc_page_title(), $user_name, $alert_url);

        $title = osc_mailBeauty($content['s_title'], $words);
        $body  = osc_mailBeauty($content['s_text'], $words);

		$emailParams =  array('subject'  => $title
                             ,'to'       => $user_email
                             ,'to_name'  => $user_name
                             ,'body'     => $body
                             ,'alt_body' => $body);

        osc_sendMail($emailParams);
    }

    // function to check is neighborhood alert allowed to user
    function is_neighborhood_alert_allowed($user_id){
        $conn = getConnection();
        $neighborhood_alert = $conn->osc_dbFetchResults("SELECT * FROM %st_user_field_data WHERE fk_user_id='%d' and s_field_name='Neighborhood_alert'", DB_TABLE_PREFIX, $user_id);
        $neighborhood_alert =$neighborhood_alert[0]['s_field_value'];
        if($neighborhood_alert =="Neighborhood_alert"){
            return true;
        }
        else{
            return false;
        }
    }
     function expiry_neighborhood_cron(){
        $all_neighborhood_alert = ModelNeighborhood::newInstance()->allNeighborhoodAlert();
        $current_date = date('Y-m-d H:i:s');
        foreach($all_neighborhood_alert as $k => $neighborhood_alert){
            $publish_date = strtotime($neighborhood_alert['dt_pub_date']);
            $endDate = date('Y-m-d H:i:s', strtotime('+10 days', $publish_date));
            if($endDate < $current_date){
                ModelNeighborhood::newInstance()->deleteNeighborhoodAlert($neighborhood_alert['pk_i_id']);
                $neighborhood_alert_images = ModelNeighborhood::newInstance()->getNeighborhoodAlertImage($neighborhood_alert['pk_i_id']);
                if(!empty($neighborhood_alert_images)){
                    foreach($neighborhood_alert_images as $k => $image){
                            unlink(osc_content_path().'plugins/neighborhood_alert/images/'.$image['pic_name']);
                    }
                    ModelNeighborhood::newInstance()->deleteNeighborhoodAlertImages($neighborhood_alert['pk_i_id']);
                }
            }
        }
    }


    function delete_user_neighborhood_alert($user_id){
        $all_neighborhood_alert = ModelNeighborhood::newInstance()->allUserNeighborhoodAlert($user_id);
        foreach($all_neighborhood_alert as $k => $neighborhood_alert){
                ModelNeighborhood::newInstance()->deleteNeighborhoodAlert($neighborhood_alert['pk_i_id']);
                $neighborhood_alert_images = ModelNeighborhood::newInstance()->getNeighborhoodAlertImage($neighborhood_alert['pk_i_id']);
                if(!empty($neighborhood_alert_images)){
                    foreach($neighborhood_alert_images as $k => $image){
                            unlink(osc_content_path().'plugins/neighborhood_alert/images/'.$image['pic_name']);
                    }
                    ModelNeighborhood::newInstance()->deleteNeighborhoodAlertImages($neighborhood_alert['pk_i_id']);
                }
        }
    }


    // This is needed in order to be able to activate the plugin
    osc_register_plugin(osc_plugin_path(__FILE__), 'neighborhood_install');
    // This is a hack to show a Uninstall link at plugins table (you could also use some other hook to show a custom option panel)
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'neighborhood_uninstall');
    osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'neighborhood_admin');
    osc_add_hook('header','style_neighborhood');

    osc_add_route('neighborhood-alert-disclaimer', 'neighborhood-alert-disclaimer', 'neighborhood-alert-disclaimer', osc_plugin_folder(__FILE__).'disclaimer-page.php');
    osc_add_route('neighborhood-alerts', 'neighborhood-alerts/page/([0-9]+)', 'neighborhood-alerts/page/{iPage}', osc_plugin_folder(__FILE__).'neighborhood-alerts.php');
    osc_add_route('neighborhood-alert', 'neighborhood-alert/id/([0-9]+)', 'neighborhood-alert/id/{ID}', osc_plugin_folder(__FILE__).'single-neighborhood-alert.php');
    osc_add_route('neighborhood-alert-del', 'neighborhood-alert-delete/id/([0-9]+)', 'neighborhood-alert-delete/id/{ID}', osc_plugin_folder(__FILE__).'single-neighborhood-alert.php');
    osc_add_route('post-neighborhood-alert', 'post-neighborhood-alert', 'post-neighborhood-alert', osc_plugin_folder(__FILE__).'post-neighborhood-alert.php');
    osc_add_hook('cron_hourly', 'expiry_neighborhood_cron');
    osc_add_hook('delete_user', 'delete_user_neighborhood_alert');
?>
