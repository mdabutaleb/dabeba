CREATE TABLE /*TABLE_PREFIX*/t_neighborhood_alerts (
    pk_i_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    fk_i_user_id INT UNSIGNED NOT NULL,
    s_title VARCHAR(255),
    s_description text NOT NULL,
    dt_pub_date datetime NOT NULL,
    PRIMARY KEY (pk_i_id),
    FOREIGN KEY (fk_i_user_id) REFERENCES /*TABLE_PREFIX*/t_user (pk_i_id)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';

CREATE TABLE /*TABLE_PREFIX*/t_neighborhood_alert_picture (
    pk_i_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    fk_i_neighborhood_alert_id INT UNSIGNED NOT NULL,
    pic_name VARCHAR(100),
    PRIMARY KEY (pk_i_id)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';