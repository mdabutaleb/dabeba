<?php
    $user_id = osc_logged_user_id() ;
    $user_zip_code = get_logged_user_zip_code($user_id);
    if($user_zip_code ==""){
        echo '<div class="zip-code-message"><h5>ZipCode is required to see Neighborhood Alert in your area.</h5>';
        echo '<a href="'.osc_user_profile_url().'"><h6>Update your ZipCode</h6></a>';
        echo '</div>';

    }
    else{
            require_once 'ModelNeighborhood.php';
            $page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;

			if(isset($_REQUEST['zipcode']) && $_REQUEST['zipcode'] != ""){
				$zipCode = intval($_REQUEST['zipcode']);
				$users_with_same_zip = get_users_with_same_zip($zipCode);
				$neighborhood_alerts = ModelNeighborhood::newInstance()->getNeighborhoodAlerts($page, $users_with_same_zip);
			}else{
				//$user_id = osc_logged_user_id() ;
	            //$user_zip_code = get_logged_user_zip_code($user_id);
	            //$users_with_same_zip = get_users_with_same_zip($user_zip_code);
				$users_zip_code = ModelNeighborhood::newInstance()->getUsersZipCode($page);
				/*echo '<pre>';
				print_r($users_zip_code);
				echo '</pre>';*/
	            $neighborhood_alerts = ModelNeighborhood::newInstance()->getNeighborhoodAlerts($page);
			}


            echo '<div class="neighbour_div">
            		<a href="https://dadebo.com/index.php?page=custom&route=full-calendar&date='.date('Y-m-d').'"
 title="Place your moment">
            			<span class="plus_btn pull-right"><i class="fa fa-plus"></i></span>
            		</a>
            	</div>
            	<div class="neighborhood-alert-header"><h2>'; _e('Neighborhood Alerts', 'neighborhood_alert'); echo '<span>'; _e('Zip Code', 'neighborhood_alert');  echo ': '.$user_zip_code.'</span></h2></div><div class="clear"></div>';


			//Dante Cervantes Search by Zip code
			?>
				<form class="neighborhood-search" method="post" action="<?php echo osc_base_url(true).'?page=custom&route=neighborhood-alerts'?>">
					<input type="number" placeholder="<?= __("Search",'dadebo')." ".__("Zip Code",'neighborhood_alert'); ?>" id="search" name="zipcode" min="1" />
					<button type="submit" id="submit"><?= _e("Submit",'dadebo'); ?></button>
				</form>
			<?php
			//end Dante Cervantes

            if (empty($neighborhood_alerts)) {
                echo '<p class="no-records">'; _e('There are no Neighborhood Alerts in your area.', 'neighborhood_alert'); echo '</p>';
           }
           else{
                foreach($neighborhood_alerts as $k => $neighborhood_alert){
					$userZipCode = ModelNeighborhood::newInstance()->getUserZipCode($neighborhood_alert['fk_i_user_id']);
				 ?>
                    <div class="neighborhood-alert-row">
                        <div class="user-image">
                            <?php profile_picture_show($neighborhood_alert['fk_i_user_id']); ?>
                        </div>
                        <div class="neighborhood-alert-contents">
                            <div class="neighborhood-alert-detail">

								<div>
									<h2 class="neighborhood_alert-title"><a href="<?php echo osc_route_url('neighborhood-alert', array('ID' => $neighborhood_alert['pk_i_id'])); ?>"><?php echo $neighborhood_alert['s_title']; ?></a></h2>
	                                <p>Posted By : <a href="<?php echo osc_user_public_profile_url( $neighborhood_alert['fk_i_user_id']); ?>" class="neighborhood_alert-username"><?= get_users_name($neighborhood_alert['fk_i_user_id']); ?></a>
										| <span class="neighborhood-alert-date"> <?php echo $neighborhood_alert['dt_pub_date']; ?></span>
									</p>
								</div>


								<span style="font-weight:bold">
									<?= __('Zip Code', 'neighborhood_alert') . ' : '; ?>
									<a href="index.php?page=custom&route=neighborhood-alerts&zipcode=<?= $userZipCode ?>" style="color:#22C8D9">
										<?= $userZipCode ?>
									</a>
								</span>


			    		</div>
                            <div class="clear"></div>
                        </div>
						<p class="neighborhood-alert-description">
							<?php
								$description = $neighborhood_alert['s_description'];
								if (strlen($description) > 280) {

									// truncate string
									$stringCut = substr($description, 0, 280);

									// make sure it ends in a word so assassinate doesn't become ass...
									$description = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.osc_route_url('neighborhood-alert', array('ID' => $neighborhood_alert['pk_i_id'])).'">Read More</a>';
								}
								echo $description;

							?>
						</p>
						<div class="neighborhood_alert-images">
							<div class="slider">
							  <ul>
								  <?php
  									$images = ModelNeighborhood::newInstance()->getNeighborhoodAlertImage($neighborhood_alert['pk_i_id']);
  									if(!empty($images)){
  										foreach($images as $k => $image){
  											echo '<li><img src="'.osc_base_url().'oc-content/plugins/neighborhood_alert/images/'.$image['pic_name'].'"></li>';
  										}
  									}

  								?>
							  </ul>
							</div>
						</div>
                    </div>
                <?php }


                //Pagination for neighborhood alert
					//has zipCode?
					$zipCode = (isset($_REQUEST['zipcode']) && $_REQUEST['zipcode'] != "") ? intval($_REQUEST['zipcode']) : 0;
                    $items_per_page = 10;
                    $pageActual = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
                    $urlActual  = osc_route_url('neighborhood-alerts');
                    $urlActual  = preg_replace('/&iPage=(\d+)?/', '', $urlActual);

					if($zipCode > 0){
						$urlActual = $urlActual . '&iPage={PAGE}' . '&zipcode='.$zipCode;
					}else{
					   $urlActual =	$urlActual . '&iPage={PAGE}';
					}

					if($zipCode > 0){
	                    $total_neighborhood_alerts = ModelNeighborhood::newInstance()->getTotalNeighborhoodAlerts($users_with_same_zip);
					}else {
						$total_neighborhood_alerts = ModelNeighborhood::newInstance()->getTotalNeighborhoodAlerts();
					}

                    $pageTotal  = ceil($total_neighborhood_alerts/$items_per_page);
                    $params     = array(
                        'total'    => $pageTotal,
                        'selected' => $pageActual - 1,
                        'url'      => $urlActual,
                        'sides'    => 5
                    );
                    echo '<div class="paginate" >';
                    echo osc_pagination($params);
                    echo '</div>';
           }

    }
?>

<script type="text/javascript">
	(function($){
		var dates = $('.neighborhood-alert-date');

		dates.each(function(i,date) {
			var currentDate = $(date).text()
			var humanRedeableDate = moment(currentDate).fromNow();
			$(date).text(humanRedeableDate);
		});

		$('.slider').unslider({
			pagination : false
		});

	}(jQuery));
</script>
