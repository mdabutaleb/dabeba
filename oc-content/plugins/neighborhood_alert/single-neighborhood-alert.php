<?php
    require_once 'ModelNeighborhood.php';
    $neighborhood_alert_id = Params::getParam('ID');
    $neighborhood_alert = ModelNeighborhood::newInstance()->getNeighborhoodAlertByID($neighborhood_alert_id);
    if( Params::getParam('route') == 'neighborhood-alert-del' ) {
            if(osc_logged_user_id() == $neighborhood_alert['fk_i_user_id']){
                ModelNeighborhood::newInstance()->deleteNeighborhoodAlert($neighborhood_alert['pk_i_id']);
                $neighborhood_alert_images = ModelNeighborhood::newInstance()->getNeighborhoodAlertImage($neighborhood_alert['pk_i_id']);
                if(!empty($neighborhood_alert_images)){
                    foreach($neighborhood_alert_images as $k => $image){
                            unlink(osc_content_path().'plugins/neighborhood_alert/images/'.$image['pic_name']);
                    }
                    ModelNeighborhood::newInstance()->deleteNeighborhoodAlertImages($neighborhood_alert['pk_i_id']);
                }
            header('Location: '.osc_route_url('neighborhood-alerts'));
            osc_add_flash_ok_message(__('Neighborhood Alert Deleted successfully', 'neighborhood_alert'));
                
            }
    }

    ?>
    <div class="clear"></div>
    <div class="neighborhood-alert-row">
        <div class="user-image">
            <?php profile_picture_show($neighborhood_alert['fk_i_user_id']); ?>
        </div>
        <div class="neighborhood-alert-contents">
            <div class="neighborhood-alert-detail">
                <h5><a href="<?php echo osc_user_public_profile_url( $neighborhood_alert['fk_i_user_id']); ?>"><?php echo get_users_name($neighborhood_alert['fk_i_user_id']); ?></a></h5>
                <span class="neighborhood-alert-date"><?php echo osc_format_date($neighborhood_alert['dt_pub_date']); ?></span>
                <?php
                    if(osc_logged_user_id() == $neighborhood_alert['fk_i_user_id']){
                        echo '<span class="delete-post"><a href="'.osc_route_url('neighborhood-alert-del', array('ID' => $neighborhood_alert['pk_i_id'])).'">'; _e('Delete ', 'neighborhood_alert'); echo '</a><span>';
                    }
                ?>
            </div>
            <div class="clear"></div>
            <p><?php echo $neighborhood_alert['s_title']; ?></p>
            <p class="neighborhood-alert-disc"><?php echo $neighborhood_alert['s_description']; ?></p>
            <div class="neighborhood-alert-imgs">
                <?php
                    $images = ModelNeighborhood::newInstance()->getNeighborhoodAlertImage($neighborhood_alert_id);
                    //echo '<pre>';print_r($images); echo '<pre>';
                    foreach($images as $k => $image){
                        echo '<div class="neighborhood-alert-image"><img src="'.osc_base_url().'oc-content/plugins/neighborhood_alert/images/'.$image['pic_name'].'"></div>';
                    }
                ?>
            </div>
        </div>
    </div>
