<?php

    class ModelNeighborhood extends DAO
    {
        /**
         * It references to self object: ModelNeighborhood.
         * It is used as a singleton
         *
         * @access private
         * @since 3.0
         * @var ModelJobs
         */
        private static $instance ;

        /**
         * It creates a new ModelJobs object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since 3.0
         * @return ModelJobs
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        /**
         * Construct
         */
        function __construct()
        {
            parent::__construct();
        }

         /**
         * Import sql file
         * @param type $file
         */
        public function import($file) {
            $path = osc_plugin_resource($file);
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( $this->dao->getErrorLevel().' - '.$this->dao->getErrorDesc() );
            }
        }

        /**
         * Return table name
         * @return string
         */
        public function getTableName()
        {
            return DB_TABLE_PREFIX.'t_neighborhood_alerts' ;
        }


        /**
         *  Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query('DROP TABLE '. DB_TABLE_PREFIX.'t_neighborhood_alert_picture');
            $this->dao->query('DROP TABLE '. $this->getTableName());

        }


        /**
         * Insert NeighborhoodAlert
         */
        public function insertNeighborhoodAlert($user_id, $title, $description)
        {
            $aSet = array(
                'fk_i_user_id'   => $user_id,
                's_title' => $title,
                's_description'    => $description,
                'dt_pub_date'   => date('Y-m-d H:i:s')
            );

            $this->dao->insert($this->getTableName(), $aSet);
            return $this->dao->insertedId();
        }

		public function getUserZipCode($user){
			$this->dao->select("s_zip");
			$this->dao->from(DB_TABLE_PREFIX.'t_user');
			$this->dao->where("pk_i_id",$user);
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

			$results->result();

			return $results->resultArray[0]['s_zip'];
		}


        /**
         * Insert NeighborhoodAlert pics
         */
        public function insertNeighborhoodAlertPic($neighborhood_alert_id, $image_name)
        {
            $aSet = array(
                'fk_i_neighborhood_alert_id' => $neighborhood_alert_id,
                'pic_name' => $image_name
            );

            return $this->dao->insert(DB_TABLE_PREFIX.'t_neighborhood_alert_picture', $aSet);
        }

		/**
		* get All users zip code
		*/
		public function getUsersZipCode($page){
			$items_per_page = 10;
		    $offset = ($page - 1) * $items_per_page;
			//get 10 zip codes per page
			$this->dao->select("s_zip");
			$this->dao->from(DB_TABLE_PREFIX.'t_user');
			$this->dao->where("s_zip != ''");
			$this->dao->orderBy('s_zip', 'ASC') ;
			$this->dao->groupBy('s_zip');
			$this->dao->limit($offset, $items_per_page);
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

			$results->result();

			//get posts from each zip code
			//limit 5 posts for each zip code...

			$posts = array();

			$postLimit = 5;

			foreach($results->resultArray as $user){

				$postData = array();

				$offset = ($page - 1) * $postLimit;

				$users_with_same_zip = get_users_with_same_zip($user['s_zip']);
				$this->dao->select();
	            $this->dao->from( $this->getTableName() ) ;
	            $this->dao->whereIn('fk_i_user_id', $users_with_same_zip);
		        $this->dao->limit($offset, $postLimit);
	            $this->dao->orderBy('pk_i_id', 'DESC') ;
	            $resultsPosts = $this->dao->get();

				$postData['zip'] = $user['s_zip'];
				$postData['posts'] = $resultsPosts->result();
				array_push($posts,$postData);

			}

			return $posts;
		}


        /**
         * get NeighborhoodAlerts by page
         */
        public function getNeighborhoodAlerts($page, $users_with_same_zip = 0)
        {
	    $items_per_page = 10;
	    $offset = ($page - 1) * $items_per_page;

            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
			if($users_with_same_zip){
            	$this->dao->whereIn('fk_i_user_id', $users_with_same_zip);
			}
	        $this->dao->limit($offset, $items_per_page);
            $this->dao->orderBy('pk_i_id', 'DESC') ;
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();


        }


        /**
         * get NeighborhoodAlert count
         */
        public function getTotalNeighborhoodAlerts($users_with_same_zip = 0)
        {

            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
			if($users_with_same_zip){
            	$this->dao->whereIn('fk_i_user_id', $users_with_same_zip);
			}
            $results = $this->dao->get();
            return count($results->result());
        }


        /**
         * get NeighborhoodAlert by id
         */
        public function getNeighborhoodAlertByID($ID)
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName());
            $this->dao->where('pk_i_id', $ID);
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }
            $neighborhood_alert =$result->row();
            return $neighborhood_alert;
        }


        /**
         * get NeighborhoodAlert image by id
         */
        public function getNeighborhoodAlertImage($ID)
        {
            $this->dao->select();
            $this->dao->from(DB_TABLE_PREFIX.'t_neighborhood_alert_picture');
            $this->dao->where('fk_i_neighborhood_alert_id', $ID);
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }
            return $results->result();
        }

        /**
         * Adds email templstes.
         *
         *
         */
        public function insertEmailTemplates() {
            //used for email template
            $this->dao->insert(DB_TABLE_PREFIX.'t_pages', array('s_internal_name' => 'neighborhood_alert', 'b_indelible' => 1, 'dt_pub_date' => date('Ydm')));

            $this->dao->select();
            $this->dao->from( DB_TABLE_PREFIX.'t_pages' );
            $this->dao->where('s_internal_name', 'neighborhood_alert');

            $result = $this->dao->get();
            $pageInfo = $result->row();

            foreach(osc_listLanguageCodes() as $locales) {
               $this->dao->insert(DB_TABLE_PREFIX.'t_pages_description', array('fk_i_pages_id' => $pageInfo['pk_i_id'], 'fk_c_locale_code' => $locales, 's_title' => '{WEB_TITLE} - New Neighborhood alert in your area', 's_text' => "<p>Hi {CONTACT_NAME}!</p>\r\n<p> </p>\r\n<p> There is new neighborhood alert post in your area. Please click on link to see the neighborhood alert. </p>\r\n<p> Alret link: {ALERT_URL}</p><p> </p>\r\n<p>This is an automatic email, if you have already seen this alert, please ignore this email.</p>\r\n<p> </p>\r\n<p>Thanks</p>"));
            }
        }

       /**
         * get all NeighborhoodAlert
         */
        public function allNeighborhoodAlert()
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();


        }

        /**
         * get all NeighborhoodAlert by user id
         */
        public function allUserNeighborhoodAlert($user_id)
        {
            $this->dao->select();
            $this->dao->from( $this->getTableName() ) ;
            $this->dao->where('fk_i_user_id', $user_id);
            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();


        }

        /**
         * delete NeighborhoodAlert
         */
        public function deleteNeighborhoodAlert($neighborhood_alert_id)
        {
            $this->dao->delete( $this->getTableName(), array('pk_i_id' => $neighborhood_alert_id) );
        }
         /**
         * delete NeighborhoodAlert images
         */
        public function deleteNeighborhoodAlertImages($neighborhood_alert_id)
        {
            $this->dao->delete( DB_TABLE_PREFIX.'t_neighborhood_alert_picture', array('fk_i_neighborhood_alert_id' => $neighborhood_alert_id) ) ;
        }

    }
?>
