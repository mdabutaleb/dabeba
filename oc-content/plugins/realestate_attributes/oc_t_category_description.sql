-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 26, 2016 at 02:09 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `osclass`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_t_category_description`
--

DROP TABLE IF EXISTS `oc_t_category_description`;
CREATE TABLE IF NOT EXISTS `oc_t_category_description` (
  `fk_i_category_id` int(10) unsigned NOT NULL,
  `fk_c_locale_code` char(5) NOT NULL,
  `s_name` varchar(100) DEFAULT NULL,
  `s_description` text,
  `s_slug` varchar(100) NOT NULL,
  PRIMARY KEY (`fk_i_category_id`,`fk_c_locale_code`),
  KEY `idx_s_slug` (`s_slug`),
  KEY `fk_c_locale_code` (`fk_c_locale_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_t_category_description`
--

INSERT INTO `oc_t_category_description` (`fk_i_category_id`, `fk_c_locale_code`, `s_name`, `s_description`, `s_slug`) VALUES
(1, 'en_US', 'For sale', NULL, 'for-sale'),
(2, 'en_US', 'Vehicles', NULL, 'vehicles'),
(3, 'en_US', 'Classes', NULL, 'classes'),
(4, 'en_US', 'Real estate', NULL, 'real-estate'),
(5, 'en_US', 'Services', NULL, 'services'),
(6, 'en_US', 'Community', NULL, 'community'),
(7, 'en_US', 'Personals', NULL, 'personals'),
(8, 'en_US', 'Jobs', NULL, 'jobs'),
(9, 'en_US', 'Animals', NULL, 'animals'),
(10, 'en_US', 'Art - Collectibles', NULL, 'art-collectibles'),
(11, 'en_US', 'Barter', NULL, 'barter'),
(12, 'en_US', 'Books - Magazines', NULL, 'books-magazines'),
(13, 'en_US', 'Cameras - Camera Accessories', NULL, 'cameras-camera-accessories'),
(14, 'en_US', 'CDs - Records', NULL, 'cds-records'),
(15, 'en_US', 'Cell Phones - Accessories', NULL, 'cell-phones-accessories'),
(16, 'en_US', 'Clothing', NULL, 'clothing'),
(17, 'en_US', 'Computers - Hardware', NULL, 'computers-hardware'),
(18, 'en_US', 'DVD', NULL, 'dvd'),
(19, 'en_US', 'Electronics', NULL, 'electronics'),
(20, 'en_US', 'For Babies - Infants', NULL, 'for-babies-infants'),
(21, 'en_US', 'Garage Sale', NULL, 'garage-sale'),
(22, 'en_US', 'Health - Beauty', NULL, 'health-beauty'),
(23, 'en_US', 'Home - Furniture - Garden Supplies', NULL, 'home-furniture-garden-supplies'),
(24, 'en_US', 'Jewelry - Watches', NULL, 'jewelry-watches'),
(25, 'en_US', 'Musical Instruments', NULL, 'musical-instruments'),
(26, 'en_US', 'Sporting Goods - Bicycles', NULL, 'sporting-goods-bicycles'),
(27, 'en_US', 'Tickets', NULL, 'tickets'),
(28, 'en_US', 'Toys - Games - Hobbies', NULL, 'toys-games-hobbies'),
(29, 'en_US', 'Video Games - Consoles', NULL, 'video-games-consoles'),
(30, 'en_US', 'Everything Else', NULL, 'everything-else'),
(31, 'en_US', 'Cars', NULL, 'cars'),
(32, 'en_US', 'Car Parts', NULL, 'car-parts'),
(33, 'en_US', 'Motorcycles', NULL, 'motorcycles'),
(34, 'en_US', 'Boats - Ships', NULL, 'boats-ships'),
(35, 'en_US', 'RVs - Campers - Caravans', NULL, 'rvs-campers-caravans'),
(36, 'en_US', 'Trucks - Commercial Vehicles', NULL, 'trucks-commercial-vehicles'),
(37, 'en_US', 'Other Vehicles', NULL, 'other-vehicles'),
(38, 'en_US', 'Computer - Multimedia Classes', NULL, 'computer-multimedia-classes'),
(39, 'en_US', 'Language Classes', NULL, 'language-classes'),
(40, 'en_US', 'Music - Theatre - Dance Classes', NULL, 'music-theatre-dance-classes'),
(41, 'en_US', 'Tutoring - Private Lessons', NULL, 'tutoring-private-lessons'),
(42, 'en_US', 'Other Classes', NULL, 'other-classes'),
(43, 'en_US', 'Houses - Apartments for Sale', NULL, 'houses-apartments-for-sale'),
(44, 'en_US', 'Houses - Apartments for Rent', NULL, 'houses-apartments-for-rent'),
(45, 'en_US', 'Rooms for Rent - Shared', NULL, 'rooms-for-rent-shared'),
(46, 'en_US', 'Housing Swap', NULL, 'housing-swap'),
(47, 'en_US', 'Vacation Rentals', NULL, 'vacation-rentals'),
(48, 'en_US', 'Parking Spots', NULL, 'parking-spots'),
(49, 'en_US', 'Land', NULL, 'land'),
(50, 'en_US', 'Office - Commercial Space', NULL, 'office-commercial-space'),
(51, 'en_US', 'Shops for Rent - Sale', NULL, 'shops-for-rent-sale'),
(52, 'en_US', 'Babysitter - Nanny', NULL, 'babysitter-nanny'),
(53, 'en_US', 'Casting - Auditions', NULL, 'casting-auditions'),
(54, 'en_US', 'Computer', NULL, 'computer'),
(55, 'en_US', 'Event Services', NULL, 'event-services'),
(56, 'en_US', 'Health - Beauty - Fitness', NULL, 'health-beauty-fitness'),
(57, 'en_US', 'Horoscopes - Tarot', NULL, 'horoscopes-tarot'),
(58, 'en_US', 'Household - Domestic Help', NULL, 'household-domestic-help'),
(59, 'en_US', 'Moving - Storage', NULL, 'moving-storage'),
(60, 'en_US', 'Repair', NULL, 'repair'),
(61, 'en_US', 'Writing - Editing - Translating', NULL, 'writing-editing-translating'),
(62, 'en_US', 'Other Services', NULL, 'other-services'),
(63, 'en_US', 'Carpool', NULL, 'carpool'),
(64, 'en_US', 'Community Activities', NULL, 'community-activities'),
(65, 'en_US', 'Events', NULL, 'events'),
(66, 'en_US', 'Musicians - Artists - Bands', NULL, 'musicians-artists-bands'),
(67, 'en_US', 'Volunteers', NULL, 'volunteers'),
(68, 'en_US', 'Lost And Found', NULL, 'lost-and-found'),
(69, 'en_US', 'Women looking for Men', NULL, 'women-looking-for-men'),
(70, 'en_US', 'Men looking for Women', NULL, 'men-looking-for-women'),
(71, 'en_US', 'Men looking for Men', NULL, 'men-looking-for-men'),
(72, 'en_US', 'Women looking for Women', NULL, 'women-looking-for-women'),
(73, 'en_US', 'Friendship - Activity Partners', NULL, 'friendship-activity-partners'),
(74, 'en_US', 'Missed Connections', NULL, 'missed-connections'),
(75, 'en_US', 'Accounting - Finance', NULL, 'accounting-finance'),
(76, 'en_US', 'Advertising - Public Relations', NULL, 'advertising-public-relations'),
(77, 'en_US', 'Arts - Entertainment - Publishing', NULL, 'arts-entertainment-publishing'),
(78, 'en_US', 'Clerical - Administrative', NULL, 'clerical-administrative'),
(79, 'en_US', 'Customer Service', NULL, 'customer-service'),
(80, 'en_US', 'Education - Training', NULL, 'education-training'),
(81, 'en_US', 'Engineering - Architecture', NULL, 'engineering-architecture'),
(82, 'en_US', 'Healthcare', NULL, 'healthcare'),
(83, 'en_US', 'Human Resource', NULL, 'human-resource'),
(84, 'en_US', 'Internet', NULL, 'internet'),
(85, 'en_US', 'Legal', NULL, 'legal'),
(86, 'en_US', 'Manual Labor', NULL, 'manual-labor'),
(87, 'en_US', 'Manufacturing - Operations', NULL, 'manufacturing-operations'),
(88, 'en_US', 'Marketing', NULL, 'marketing'),
(89, 'en_US', 'Non-profit - Volunteer', NULL, 'non-profit-volunteer'),
(90, 'en_US', 'Real Estate', NULL, 'real-estate_1'),
(91, 'en_US', 'Restaurant - Food Service', NULL, 'restaurant-food-service'),
(92, 'en_US', 'Retail', NULL, 'retail'),
(93, 'en_US', 'Sales', NULL, 'sales'),
(94, 'en_US', 'Technology', NULL, 'technology'),
(95, 'en_US', 'Other Jobs', NULL, 'other-jobs');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `oc_t_category_description`
--
ALTER TABLE `oc_t_category_description`
  ADD CONSTRAINT `oc_t_category_description_ibfk_1` FOREIGN KEY (`fk_i_category_id`) REFERENCES `oc_t_category` (`pk_i_id`),
  ADD CONSTRAINT `oc_t_category_description_ibfk_2` FOREIGN KEY (`fk_c_locale_code`) REFERENCES `oc_t_locale` (`pk_c_code`);
