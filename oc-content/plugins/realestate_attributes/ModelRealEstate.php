<?php
    /*
     *      OSCLass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2010 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    /**
     * Model database for RealEstate tables
     * 
     * @package OSClass
     * @subpackage Model
     * @since 3.0
     */
    class ModelRealEstate extends DAO
    {
        /**
         * It references to self object: ModelRealEstate.
         * It is used as a singleton
         * 
         * @access private
         * @since 3.0
         * @var Currency
         */
        private static $instance ;

        /**
         * It creates a new ModelRealEstate object class ir if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since 3.0
         * @return Currency
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        /**
         * Construct
         */
        function __construct()
        {
            parent::__construct();
        }
        
        /**
         * Return table name house attributes
         * @return string
         */
        public function getTable_HouseAttr()
        {
            return DB_TABLE_PREFIX.'t_item_house_attr';
        }
        
        /**
         * Import sql file
         * @param type $file 
         */
        public function import($file)
        {
            $path = osc_plugin_resource($file) ;
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( $this->dao->getErrorLevel().' - '.$this->dao->getErrorDesc() ) ;
            }
        }
        
        /**
         * Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_HouseAttr()));
        }

        /**
         * @param $item_id
         * @param $locale
         * @param $type
         * @param $numRooms
         * @param $numBathrooms
         * @param $status
         * @param $squareMeters
         * @param $year
         * @param $squareMetersTotal
         * @param $numFloors
         * @param $numGarages
         * @param $airCondition
         * @param $condition
         * @param $agency
         * @param $floorNumber
         * @param $heating
         * @param $elevator
         * @param $terrace
         * @param $parking
         * @param $furnished
         * @param $new
         * @param $by_owner
         * @param $transport
         * @param $zone
         * @return mixed
         */
        public function insertDescriptions( $item_id, $locale, $type, $numRooms, $numBathrooms, $status, $squareMeters, $year,
                                            $squareMetersTotal, $numFloors, $numGarages,$airCondition, $condition, $agency,
                                            $floorNumber, $heating, $elevator, $terrace, $parking, $furnished, $new,
                                            $by_owner, $transport, $zone) {

            $aSet = array(
                'fk_i_item_id'          => $item_id,
                'fk_c_locale_code'      => $locale,
                's_square_meters'       => $squareMeters,
                'i_num_rooms'           => $numRooms,
                'i_num_bathrooms'       => $numBathrooms,
                'e_type'                => $type,
                'e_status'              => $status,
                'i_num_floors'          => $numFloors,
                'i_num_garages'         => $numGarages,
                'b_heating'             => $heating,
                'b_air_condition'       => $airCondition,
                'b_elevator'            => $elevator,
                'b_terrace'             => $terrace,
                'b_parking'             => $parking,
                'b_furnished'           => $furnished,
                'b_new'                 => $new,
                'b_by_owner'            => $by_owner,
                's_condition'           => $condition,
                'i_year'                => $year,
                's_agency'              => $agency,
                'i_floor_number'        => $floorNumber,
                'i_plot_area'           => $squareMetersTotal,
                's_transport'           => $transport,
                's_zone'                => $zone
            );

            return $this->dao->insert($this->getTable_HouseAttr(), $aSet);

        }

        public function updateDescriptions( $item_id, $locale, $type, $numRooms, $numBathrooms, $status, $squareMeters, $year,
                                            $squareMetersTotal, $numFloors, $numGarages,$airCondition, $condition, $agency,
                                            $floorNumber, $heating, $elevator, $terrace, $parking, $furnished, $new,
                                            $by_owner, $transport, $zone) {

            $aSet = array(
                'fk_i_item_id'          => $item_id,
                'fk_c_locale_code'      => $locale,
                's_square_meters'       => $squareMeters,
                'i_num_rooms'           => $numRooms,
                'i_num_bathrooms'       => $numBathrooms,
                'e_type'                => $type,
                'e_status'              => $status,
                'i_num_floors'          => $numFloors,
                'i_num_garages'         => $numGarages,
                'b_heating'             => $heating,
                'b_air_condition'       => $airCondition,
                'b_elevator'            => $elevator,
                'b_terrace'             => $terrace,
                'b_parking'             => $parking,
                'b_furnished'           => $furnished,
                'b_new'                 => $new,
                'b_by_owner'            => $by_owner,
                's_condition'           => $condition,
                'i_year'                => $year,
                's_agency'              => $agency,
                'i_floor_number'        => $floorNumber,
                'i_plot_area'           => $squareMetersTotal,
                's_transport'           => $transport,
                's_zone'                => $zone
            );

            return $this->dao->replace($this->getTable_HouseAttr(), $aSet);

        }

        
        /**
         * Get house attributes given a item id
         * @param int $item_id
         * @return array 
         */
        public function getAttributes($item_id)
        {
            $types  = array();
            $detail = array();
            $detail['locale'] = array();
            
            $result = $this->dao->query( sprintf('SELECT * FROM %s WHERE fk_i_item_id = %d', $this->getTable_HouseAttr(), $item_id) ) ;
            $descriptions = $result->result();
            
            foreach ($descriptions as $desc) {
                $detail['locale'][$desc['fk_c_locale_code']] = $desc;
            }

            return $detail;
        }
        
        /**
         * Delete house description attributes, and house property type attributes given a locale.
         * 
         * @param type $locale 
         */
        public function deleteLocale( $locale )
        {
            return $this->dao->query( "DELETE FROM ".$this->getTable_HouseAttr()." WHERE fk_c_locale_code = '" . $locale . "'") ;
        }
        
        /**
         * Delete house attributes given a item id
         * @param type $item_id 
         */
        public function deleteItem($item_id)
        {
            return $this->dao->query("DELETE FROM ".$this->getTable_HouseAttr()." WHERE fk_i_item_id = '" . $item_id . "'") ;
        }
    }

?>