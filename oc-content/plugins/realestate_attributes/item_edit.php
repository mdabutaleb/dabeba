<h2><?php _e('Property features', 'realestate_attributes'); ?></h2>

<div class="box">
	<!-- Type
    <div class="row">
        <?php
        if( Session::newInstance()->_getForm('property_type') != '' ) {
            $detail['e_type'] = Session::newInstance()->_getForm('property_type');
        }
        ?>
        <div class="control-group">
            <label class="control-label" for="property_type">
                <?php _e('Type', 'realestate_attributes'); ?>
            </label>
            <div class="controls">
                <select name="property_type" id="property_type" style="opacity: 0;">
                    <option value="FOR RENT" <?php if(@$detail['e_type'] == 'FOR RENT') { echo "selected"; } ?>><?php _e('For rent', 'realestate_attributes'); ?></option>
                    <option value="FOR SALE" <?php if(@$detail['e_type'] == 'FOR SALE') { echo "selected"; } ?>><?php _e('For sale', 'realestate_attributes'); ?></option>
                </select>
            </div>
        </div>
    </div>
	-->
	
    <div class="row">
        <?php
            $locales = osc_get_locales();
            $counter = 0;
            $currentlocale = osc_current_user_locale();
        ?>

        <?php foreach ($locales as $locale) {  if($locale['pk_c_code'] == $currentlocale) { ?>
            <!-- <h2><?php echo $locale['s_name']; ?></h2> -->
            <?php $detail = []; ?>
            <?php $detail = $rawDetail['locale'][$locale['pk_c_code']]; ?>

                <?php
                if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numRooms') != '' ) {
                    $detail['i_num_rooms'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numRooms');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="numRooms">
                        <?php _e('Num. of rooms', 'realestate_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="numRooms_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#numRooms" id="numRooms" style="opacity: 0;">
                            <?php foreach(range(0, 10) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if($n == @$detail['i_num_rooms']) { echo "selected"; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <?php
                if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numBathrooms') != '' ) {
                    $detail['i_num_bathrooms'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numBathrooms');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="numBathrooms">
                        <?php _e('Num. of bathrooms', 'realestate_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="numBathrooms_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#numBathrooms" id="numBathrooms" style="opacity: 0;">
                            <?php foreach(range(0, 15) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if($n==@$detail['i_num_bathrooms']) { echo "selected"; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <?php
                if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'status') != '' ) {
                    $detail['e_status'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'status');
                }
                ?>
				<!-- Status
                <div class="control-group">
                    <label class="control-label" for="status">
                        <?php _e('Status', 'realestate_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="status_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#status" id="status" style="opacity: 0;">
                            <option value="NEW CONSTRUCTION" <?php if(@$detail['e_status'] == 'NEW CONSTRUCTION') { echo "selected"; } ?>><?php _e('New construction', 'realestate_attributes'); ?></option>
                            <option value="TO RENOVATE" <?php if(@$detail['e_status'] == 'TO RENOVATE') { echo "selected"; } ?>><?php _e('To renovate', 'realestate_attributes'); ?></option>
                            <option value="GOOD CONDITION" <?php if(@$detail['e_status'] == 'GOOD CONDITION') { echo "selected"; } ?>><?php _e('Good condition', 'realestate_attributes'); ?></option>
                        </select>
                    </div>
                </div>
				-->
                <div class="control-group">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'squareMeters') != '' ) {
                        $detail['s_square_meters'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'squareMeters');
                    }
                    ?>
                    <label for="squareMeters"><?php _e('Square feet', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#squareMeters" id="squareMeters" value="<?php echo @$detail['s_square_meters']; ?>" size="4" maxlength="4" />
                </div>
                <div class="control-group">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'year') != '' ) {
                        $detail['i_year'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'year');
                    }
                    ?>
                    <label for="year"><?php _e('Construction Year', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#year" id="year" value="<?php echo @$detail['i_year'];?>" size="4" maxlength="4" />
                </div>
				<!-- Square Meters (Total)
                <div class="control-group">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'squareMetersTotal') != '' ) {
                        $detail['i_plot_area'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'squareMetersTotal');
                    }
                    ?>
                    <label for="squareMetersTotal"><?php _e('Square meters (total)', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#squareMetersTotal" id="squareMetersTotal" value="<?php echo  @$detail['i_plot_area'];?>" size="4" maxlength="6" />
                </div>
				-->
				<!-- Number of Floors
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numFloors') != '' ) {
                        $detail['i_num_floors'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numFloors');
                    }
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="numFloors">
                            <?php _e('Num. of floors', 'realestate_attributes'); ?>
                        </label>
                        <div class="controls">
                            <select class="numFloors_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#numFloors" id="numFloors" style="opacity: 0;">
                                <?php foreach(range(0, 15) as $n) { ?>
                                    <option value="<?php echo $n; ?>" <?php if($n == @$detail['i_num_floors']) { echo "selected"; } ?>><?php echo $n; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
				-->
				<!-- Number of Garages
                <div class="control-group">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numGarages') != '' ) {
                        $detail['i_num_garages'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'numGarages');
                    }
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="numGarages">
                            <?php _e('Num. of garages (place for a car = one garage)', 'realestate_attributes'); ?>
                        </label>
                        <div class="controls">
                            <select class="numGarages_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#numGarages" id="numGarages" style="opacity: 0;">
                                <?php foreach(range(0, 15) as $n) { ?>
                                    <option value="<?php echo $n; ?>" <?php if($n==@$detail['i_num_garages']) { echo "selected"; } ?>><?php echo $n; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
				-->
				<!-- Condition
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'condition') != '' ) {
                        $detail['s_condition'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'condition');
                    }
                    ?>
                    <label for="condition"><?php _e('Condition', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#condition" id="condition" value="<?php echo @$detail['s_condition']; ?>" />
                </div>
				-->
				<!-- Agency
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'agency') != '' ) {
                        $detail['s_agency'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'agency');
                    }
                    ?>
                    <label for="agency"><?php _e('Agency', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#agency" id="agency" value="<?php echo @$detail['s_agency']; ?>" />
                </div>
				-->
				<!-- Floor Number
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'floorNumber') != '' ) {
                        $detail['i_floor_number'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'floorNumber');
                    }
                    ?>
                    <label for="floorNumber"><?php _e('Floor Number', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#floorNumber" id="floorNumber" value="<?php echo @$detail['i_floor_number']; ?>" />
                </div>
				-->
                <div class="row">
                    <label><?php _e('Other characteristics', 'realestate_attributes'); ?></label>
                    <div class="controls">
                        <ul class="checkbox-list">
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'heating') != '' ) {
                                $detail['b_heating'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'heating');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#heating" id="heating" value="1" <?php if(@$detail['b_heating'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="heating"><?php _e('Heating', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'airCondition') != '' ) {
                                $detail['b_air_condition'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'airCondition');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#airCondition" id="airCondition" value="1" <?php if(@$detail['b_air_condition'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="airCondition"><?php _e('Air condition', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'elevator') != '' ) {
                                $detail['b_elevator'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'elevator');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#elevator" id="elevator" value="1" <?php if(@$detail['b_elevator'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="elevator"><?php _e('Elevator', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'terrace') != '' ) {
                                $detail['b_terrace'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'terrace');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#terrace" id="terrace" value="1" <?php if(@$detail['b_terrace'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="terrace"><?php _e('Terrace', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'parking') != '' ) {
                                $detail['b_parking'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'parking');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#parking" id="parking" value="1" <?php if(@$detail['b_parking'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="parking"><?php _e('Parking', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'furnished') != '' ) {
                                $detail['b_furnished'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'furnished');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#furnished" id="furnished" value="1" <?php if(@$detail['b_furnished'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="furnished"><?php _e('Furnished', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'new') != '' ) {
                                $detail['b_new'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'new');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#new" id="new" value="1" <?php if(@$detail['b_new'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="new"><?php _e('New', 'realestate_attributes'); ?></label>
                            </li>
                            <?php
                            if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'by_owner') != '' ) {
                                $detail['b_by_owner'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'by_owner');
                            }
                            ?>
                            <li>
                                <input style="width: 20px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#by_owner" id="by_owner" value="1" <?php if(@$detail['b_by_owner'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="by_owner"><?php _e('By owner', 'realestate_attributes'); ?></label>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
				<!-- Transport
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'transport') != '' ) {
                        $detail['s_transport'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'transport');
                    }
                    ?>
                    <label for="transport"><?php _e('Transport', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#transport" id="transport" value="<?php echo @$detail['s_transport']; ?>" />
                </div>
				-->
				<!-- Zone
                <div class="row">
                    <?php
                    if( Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'zone') != '' ) {
                        $detail['s_zone'] = Session::newInstance()->_getForm('pre_' . $locale['pk_c_code'] . 'zone');
                    }
                    ?>
                    <label for="zone"><?php _e('Zone', 'realestate_attributes'); ?></label>
                    <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#zone" id="zone" value="<?php echo @$detail['s_zone']; ?>"/>
                </div>
				-->
            <?php }} ?>
    </div>
</div>

<script type="text/javascript">
        if(typeof tabberAutomatic == 'function') { 
            tabberAutomatic(); 
        }
        if(typeof themeUiHook == 'function') { 
            themeUiHook(); 
        }

        selectUi($("#property_type"));

        $('[class^="p_type_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="numRooms_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="numBathrooms_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="numFloors_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="numGarages_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="status_"]').each(function(key, value) {
            selectUi($(value));
        });

</script>