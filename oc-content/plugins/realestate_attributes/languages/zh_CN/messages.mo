��    E      D  a   l      �     �     �          '     .     <     J     Z     c  	   l     v     �     �     �  n   �     '     /  0   ?     p     u     ~     �     �     �  	   �     �     �     �     �     �     �     �                 
   0     ;     M  .   \     �     �     �     �     �     �     �     �     	     	     /	     @	     \	     x	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     
  	   

     
     #
     0
     8
  �  =
     2     9     O     b     i     p     w  	   �  	   �     �     �     �     �     �  g   �     ;     B  !   O     q     x       	   �     �     �     �     �     �     �     �     �     �  	   �     �  	   �               %  	   2  *   <     g     t  	   �     �     �     �     �     �     �     �     �          '  	   =     G     W     g  	   t     ~     �     �     �     �     �  	   �     �     �     �     �                   /             >       
                    )   *   9       ;   #       "   B             -                  D             C           %          ?   4       .   5      7   =       A                      3       <               !      (      $                     +   '   :   E   6       0         8   ,   2   @          &             1   	    Add new Add new property types After item description Agency Air Condition Air condition Bathrooms Range By Owner By owner Condition Configure plugin Construction Year Construction year Range Delete Deleting property types may end in errors. Some of those property types could be attached to some actual items Details Display options Display realestate attributes in listing page... Edit Elevator Filters Floor Number For rent For sale Furnished Garages Range Good condition Heating Hide all filters Manually New New construction Num. Bathrooms Num. Floors Num. Floors Range Num. Rooms Num. of bathrooms Num. of floors Num. of garages (place for a car = one garage) Num. of rooms Other characteristics Parking Property features Property type Property types Realestate attributes Rooms Range Select a property type Settings saved! Show all filters Show filters in search page Show only specified filters Square Meters Square Meters (total) Square Meters Range Square feet Square meters Square meters (total) Status Terrace To renovate Transport Type Undefined Update options View options Warning Zone Project-Id-Version: Real estate attributes
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-11 10:47+0100
PO-Revision-Date: 
Last-Translator: Liang <liang@theteam247.com>
Language-Team: OSClass <info@osclass.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: .
Language: zh_CN
X-Generator: Poedit 2.0.2
X-Poedit-SearchPath-0: /var/www/osclass-plugins-git/realestate_attributes
 新增 添加新物业类型 在项目描述后 中介 空调 空调 浴室范围 由业主 由业主 条件 配置插件 建筑年份 建筑年份范围 删除 删除属性类型可能会以错误结束。 其中一些属性类型可以附加到一些实际项目 详情 显示选项 在列表中显示不动产属性 编辑 电梯 过滤 楼层数 出租 出售 家具 车库范围 良好的条件 暖气 隐藏所有过滤 手动 新 新建筑 浴室数量 楼层数 楼层数范围 房间数量 浴室数量 楼层数 车库（停车的位置 = 一个车库） 房间数量 其他特点 停车位 房产特性 物业类型 物业类型 不动产属性 房间范围 选择有个物业类型 设置已保存！ 显示所有过滤 在搜索页显示过滤 只显示特定过滤 平方米 总的平方米 平方米范围 平方英尺 平方米 总平方米 状态 阳台 翻新 搬运 类型 未定义 更新选项 查看选项 警告 区 