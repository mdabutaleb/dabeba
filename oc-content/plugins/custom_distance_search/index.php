<?php
/*
Plugin Name: Custom Distance Search
Plugin URI: http://www.osclass.org/
Description: Custom search results based on the distance between cities - v1.0 Based on dienast's version of distance_search
Version: 1
Author: jonajo
Author URI: http://www.jonajo.com/
Short Name: custom_distance_search
*/

require 'customSearchDistance.php';

/**
 * Update cities table for d_coord_lat and d_coord_long values of cities
 */
function custom_ds_call_after_install() {
	customSearchDistance::newInstance()->updateTable_Cities();
}

/**
 * Show dropdown with distance kilometer values in search form (you are able to edit them)
 */
function custom_ds_search_form() {
	include("search_form.php");
}

/**
* Extend search query with foreign keys possible cities 
*/
function custom_ds_search_conditions($results) {
	if(Params::getParam('posted-today')==1){
		Search::newInstance()->addConditions(sprintf("DATE(%st_item.dt_pub_date) = '%s'", DB_TABLE_PREFIX, date('Y-m-d')));
	}
	
	$fks = customSearchDistance::newInstance()->distancefks($result);
	//Search::newInstance()->addConditions(sprintf(" 1 = 1 ".$ORs." "));	
	if ($fks) {	
		foreach ($fks as &$value) {
			$ORs = $ORs.$queryORs = " OR oc_t_item_location.fk_i_item_id = '".$value["fk_i_item_id"]."' ";				
			if ($value["s_city"] != $_GET["sCity"]) {
				Search::newInstance()->addCity(sprintf($value["s_city"]));
			}
		}
	}
}

// Hook for registering plugin 
osc_register_plugin(osc_plugin_path(__FILE__), 'custom_ds_call_after_install');

// Hook for adding new search conditions
osc_add_hook('search_conditions', 'custom_ds_search_conditions', 1);

// Hook for showing extra fields in search form
osc_add_hook('search_form', 'custom_ds_search_form', 1);
?>
