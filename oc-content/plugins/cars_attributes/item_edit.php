<script type="text/javascript">
    $(document).ready(function(){

        console.log("Car attrs ready.");

        selectUi($("#make"));
        selectUi($("#model"));

        $('[class^="car_types_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="doors"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="seats"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="num_airbags_"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="transmission"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="fuel"]').each(function(key, value) {
            selectUi($(value));
        })  ;

        $('[class^="seller"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="power_unit"]').each(function(key, value) {
            selectUi($(value));
        });

        $('[class^="gears"]').each(function(key, value) {
            selectUi($(value));
        });

        $("#make").change(function() {
            var make_id = $(this).val();
            var url = '<?php echo osc_ajax_plugin_url('cars_attributes/ajax.php') . '&makeId='; ?>' + make_id;
            var result = '';
            if(make_id != '') {
                $("#model").attr('disabled',false);
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;
                        if(length > 0) {
                            result += '<option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }
                        } else {
                            result += '<option value=""><?php _e('No results', 'cars_attributes'); ?></option>';
                        }
                        $("#model").html(result);
                    }
                });
            } else {
                result += '<option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>';
                $("#model").attr('disabled',true);
                $("#model").html(result);
            }
        });
    });


    var selectUiTest = setInterval(function() {
        console.log("interval running");
        if($("#make").length && ($("#make").parent().prop('className') != "select-box undefined" && $("#make").parent().prop('className') != "select-box undefined select-box-focus")) {
			console.log($("#make").parent().prop('className'));
			console.log($("#catId").val());

            console.log("need to wrap make");
            selectUi($("#make"));
            selectUi($("#model"));

            $('[class^="car_types_"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="doors"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="seats"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="num_airbags_"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="transmission"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="fuel"]').each(function(key, value) {
                selectUi($(value));
            })  ;

            $('[class^="seller"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="power_unit"]').each(function(key, value) {
                selectUi($(value));
            });

            $('[class^="gears"]').each(function(key, value) {
                selectUi($(value));
            });
        }
    }, 100);
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#make').rules( "add", {
			required: true,
			digits: true,
			messages: {
				required: "Please select a Make."
			}
		});
		$('#model').rules( "add", {
			required: true,
			digits: true,
			messages: {
				required: "Please select a Make."
			}
		});
	});
</script>
<h2><?php _e('Car details', 'cars_attributes') ; ?></h2>

<div>
    <!--Make-->
    <div class="row">
        <?php
        if( Session::newInstance()->_getForm('pc_make') != '' ) {
            $detail['fk_i_make_id'] = Session::newInstance()->_getForm('pc_make');
        }

        if (count($rawDetail) > 0) {
            $detail['fk_i_make_id'] = $rawDetail['locale'][osc_current_user_locale()]['fk_i_make_id'];
        }

        ?>
        <div class="control-group">
            <label class="control-label" for="make"><?php _e('Make', 'cars_attributes'); ?> *</label>
            <div class="controls">
                <select name="make" id="make" style="opacity: 0;" required>
                    <option value=""><?php _e('Select a make', 'cars_attributes'); ?></option>
                    <?php foreach($makes as $a) { ?>
                        <option value="<?php echo $a['pk_i_id']; ?>" <?php if(@$detail['fk_i_make_id'] == $a['pk_i_id']) { echo 'selected';} ?>><?php echo $a['s_name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <!--Model-->
    <div class="row">
        <?php
            if( Session::newInstance()->_getForm('pc_model') != '' ) {
                $detail['fk_i_model_id'] = Session::newInstance()->_getForm('pc_model');
            }


            if (count($rawDetail) > 0) {
                $detail['fk_i_model_id'] = $rawDetail['locale'][osc_current_user_locale()]['fk_i_model_id'];
            }
        ?>
        <div class="control-group">
            <label><?php _e('Model', 'cars_attributes'); ?> *</label>
            <select name="model" id="model" required>
                <option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>
                <?php foreach($models as $a) { ?>
                    <option value="<?php echo $a['pk_i_id']; ?>" <?php if(@$detail['fk_i_model_id'] == $a['pk_i_id']) { echo 'selected'; } ?>><?php echo $a['s_name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <!--Rest of inputs with locale-->
    <div class="row">
        <?php $counter = 0;
        $locales = osc_get_locales();
        $currentlocale = osc_current_user_locale();
        ?>
        <?php foreach ($locales as $locale) { if($locale['pk_c_code'] == $currentlocale) { ?>
            <?php $detail = $rawDetail['locale'][$locale['pk_c_code']]; ?>
            <div class="control-group">
                <!-- <h2><?php echo $locale['s_name']; ?></h2> -->
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'car_type') != '' ) {
                    $detail['fk_vehicle_type_id'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'car_type');
                }
                ?>
                <label class="control-label" for="car-type">
                    <?php _e('Car type', 'cars_attributes'); ?>
                </label>
                <div class="controls">
                    <select class="car_types_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#car_type" id="car_type" style="opacity: 0;">
                        <option value="" selected><?php _e('Select a car type', 'cars_attributes'); ?></option>
                        <?php foreach($car_types[$locale['pk_c_code']] as $k => $v) { ?>
                            <option value="<?php echo  $k; ?>" <?php if(@$detail['fk_vehicle_type_id'] == $k) { echo 'selected'; } ?>><?php echo @$v; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row _200">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'year') != '' ) {
                    $detail['i_year'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'year');
                }
                ?>
                <label><?php _e('Year', 'cars_attributes'); ?></label>
                <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#year" id="year" value="<?php echo @$detail['i_year']; ?>" size=4 />
            </div>
			<!-- Doors 
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'doors') != '' ) {
                    $detail['i_doors'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'doors');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="doors">
                        <?php _e('Doors', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="doors_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#doors" id="doors" style="opacity: 0;">
                            <option value=""><?php _e('Select num. of doors', 'cars_attributes'); ?></option>
                            <?php foreach(range(3, 5) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if(@$detail['i_doors'] == $n) { echo 'selected'; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
			-->
			
            <!-- Seats 
			
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'seats') != '' ) {
                    $detail['i_seats'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'seats');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="seats">
                        <?php _e('Seats', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="seats_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#seats" id="seats" style="opacity: 0;">
                            <option value=""><?php _e('Select num. of seats', 'cars_attributes'); ?></option>
                            <?php foreach(range(1, 17) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if(@$detail['i_seats'] == $n) { echo 'selected'; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
			-->
            <!-- End Seats -->
			
            <div class="row _200">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'mileage') != '' ) {
                    $detail['i_mileage'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'mileage');
                }
                ?>
                <label><?php _e('Mileage', 'cars_attributes'); ?></label>
                <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#mileage" id="mileage" value="<?php echo @$detail['i_mileage']; ?>" />
            </div>
			<!-- Engine Size 
            <div class="row _200">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'engine_size') != '' ) {
                    $detail['i_engine_size'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'engine_size');
                }
                ?>
                <label><?php _e('Engine size (cc)', 'cars_attributes'); ?></label>
                <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#engine_size" id="engine_size" value="<?php echo @$detail['i_engine_size']; ?>" />
            </div>
			-->
            <!-- Airbags
			
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'num_airbags') != '' ) {
                    $detail['i_num_airbags'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'num_airbags');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="airbags">
                        <?php _e('Num. Airbags', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="num_airbags_<?php echo $counter++; ?>" name="<?php echo @$locale['pk_c_code']; ?>#num_airbags" id="num_airbags" style="opacity: 0;">
                            <option value=""><?php _e('Select num. of airbags', 'cars_attributes'); ?></option>
                            <?php foreach(range(1, 8) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if(@$detail['i_num_airbags'] == $n) { echo 'selected'; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
			-->
            <!-- End Airbags -->

            <!-- Transmission -->
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'transmission') != '' ) {
                    $detail['e_transmission'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'transmission');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="transmission">
                        <?php _e('Transmission', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="transmission_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#transmission" id="transmission" style="opacity: 0;">
                            <option value=""><?php _e('Select a transmission', 'cars_attributes') ?></option>
                            <option value="MANUAL" <?php if(@$detail['e_transmission'] == 'MANUAL') { echo 'selected'; } ?>><?php _e('Manual', 'cars_attributes'); ?></option>
                            <option value="AUTO" <?php if(@$detail['e_transmission'] == 'AUTO') { echo 'selected'; } ?>><?php _e('Auto', 'cars_attributes'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <!-- End Transmission -->

            <!-- Fuel -->
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'fuel') != '' ) {
                    $detail['e_fuel'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'fuel');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="fuel">
                        <?php _e('Fuel', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="fuel_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#fuel" id="fuel" style="opacity: 0;">
                            <option value=""><?php _e('Select a fuel', 'cars_attributes') ?></option>
                            <option value="DIESEL" <?php if(@$detail['e_fuel'] == 'DIESEL') { echo 'selected'; } ?>><?php _e('Diesel', 'cars_attributes'); ?></option>
                            <option value="GASOLINE" <?php if(@$detail['e_fuel'] == 'GASOLINE') { echo 'selected'; } ?>><?php _e('Gasoline', 'cars_attributes'); ?></option>
                            <option value="ELECTRIC-HIBRID" <?php if(@$detail['e_fuel'] == 'ELECTRIC-HIBRID') { echo 'selected'; } ?>><?php _e('Electric-hibrid', 'cars_attributes'); ?></option>
                            <option value="OTHER" <?php if(@$detail['e_fuel'] == 'OTHER') { echo 'selected'; } ?>><?php _e('Other', 'cars_attributes'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <!-- End Fuel -->

            <!-- Seller -->
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'seller') != '' ) {
                    $detail['e_seller'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'seller');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="seller">
                        <?php _e('Seller', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="seller_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#seller" id="seller" style="opacity: 0;">
                            <option value=""><?php _e('Select a seller', 'cars_attributes') ?></option>
                            <option value="DEALER" <?php if(@$detail['e_seller'] == 'DEALER') { echo 'selected'; } ?>><?php _e('Dealer', 'cars_attributes'); ?></option>
                            <option value="OWNER" <?php if(@$detail['e_seller'] == 'OWNER') { echo 'selected'; } ?>><?php _e('Owner', 'cars_attributes'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <!-- End Seller -->

            <div class="row _20" style="display:none">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'warranty') != '' ) {
                    $detail['b_warranty'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'warranty');
                }
                ?>
                <input style="margin-top: 8px;" type="checkbox" name="<?php echo @$locale['pk_c_code']; ?>#warranty" id="warranty" value="1" <?php if(@$detail['b_warranty'] == 1) { echo 'checked="yes"'; } ?> /> <label><?php _e('Warranty', 'cars_attributes'); ?></label>
            </div>
            <div class="row _20" style="margin-bottom:20px">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'new') != '' ) {
                    $detail['b_new'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'new');
                }
                ?>
                <label style="padding-top:0"><?php _e('Condition', 'cars_attributes') ?></label>

                <input type="radio" style="width:auto !important" name="<?php echo @$locale['pk_c_code']; ?>#new" id="new" value="1" <?php if(@$detail['b_new'] == 1) { echo 'checked="yes"'; } ?> /> 
                <label for="new" style="float:none;display:inline"><?php _e('New', 'cars_attributes'); ?></label>

                <input type="radio" style="width:auto !important" name="<?php echo @$locale['pk_c_code']; ?>#new" id="new_1" value="0" <?php if(@$detail['b_new'] == 0) { echo 'checked="yes"'; } ?> /> 
                <label for="new_1" style="float:none;display:inline"><?php _e('Used', 'cars_attributes'); ?></label>
            </div>

            <!-- Power
			
            <div class="row">
                <?php
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'power_unit') != '' ) {
                    $detail['e_power_unit'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'power_unit');
                }
                if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'power') != '' ) {
                    $detail['i_power'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'power');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="power_unit">
                        <?php _e('Power', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <input type="text" name="<?php echo @$locale['pk_c_code']; ?>#power" id="power" value="<?php echo @$detail['i_power']; ?>" />
                        <select class="power_unit_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#power_unit" id="power_unit" style="opacity: 0;">
                            <option value="KW" <?php if(@$detail['e_power_unit'] == 'KW') { echo 'selected'; } ?>><?php _e('Kw', 'cars_attributes'); ?></option>
                            <option value="CV" <?php if(@$detail['e_power_unit'] == 'CV') { echo 'selected'; } ?>><?php _e('Cv', 'cars_attributes'); ?></option>
                            <option value="CH" <?php if(@$detail['e_power_unit'] == 'CH') { echo 'selected'; } ?>><?php _e('Ch', 'cars_attributes'); ?></option>
                            <option value="KM" <?php if(@$detail['e_power_unit'] == 'KM') { echo 'selected'; } ?>><?php _e('Km', 'cars_attributes'); ?></option>
                            <option value="HP" <?php if(@$detail['e_power_unit'] == 'HP') { echo 'selected'; } ?>><?php _e('Hp', 'cars_attributes'); ?></option>
                            <option value="PS" <?php if(@$detail['e_power_unit'] == 'PS') { echo 'selected'; } ?>><?php _e('Ps', 'cars_attributes'); ?></option>
                            <option value="PK" <?php if(@$detail['e_power_unit'] == 'PK') { echo 'selected'; } ?>><?php _e('Pk', 'cars_attributes'); ?></option>
                            <option value="CP" <?php if(@$detail['e_power_unit'] == 'CP') { echo 'selected'; } ?>><?php _e('Cp', 'cars_attributes'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
			-->
            <!-- End Power -->

            <!-- Gears
			
            <div class="row">
                <?php if( Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'gears') != '' ) {
                    $detail['i_gears'] = Session::newInstance()->_getForm('pc_' . $locale['pk_c_code'] . 'gears');
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="gears">
                        <?php _e('Gears', 'cars_attributes'); ?>
                    </label>
                    <div class="controls">
                        <select class="gears_<?php echo $counter; ?>" name="<?php echo @$locale['pk_c_code']; ?>#gears" id="gears" style="opacity: 0;">
                            <option value=""><?php _e('Select num. of gears', 'cars_attributes'); ?></option>
                            <?php foreach(range(1, 8) as $n) { ?>
                                <option value="<?php echo $n; ?>" <?php if(@$detail['i_gears'] == $n) { echo 'selected'; } ?>><?php echo $n; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
			-->
            <!-- End Gears -->
            <?php $counter++;
                $detail = [];
            ?>
        <?php }} ?>
    </div>
</div>
