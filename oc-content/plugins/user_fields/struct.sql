CREATE TABLE /*TABLE_PREFIX*/t_user_fields (
    pk_i_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    s_field_type VARCHAR(255),
    s_field_options VARCHAR(2048),
    s_field_name VARCHAR(255),
    s_field_label VARCHAR(255),
    s_field_status TINYINT(1) NOT NULL,
    max_length INT(20),
    required TINYINT(1),
    PRIMARY KEY (pk_i_id)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';

CREATE TABLE /*TABLE_PREFIX*/t_user_field_data (
    pk_i_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    fk_user_id INT UNSIGNED NOT NULL,
    fk_field_id INT UNSIGNED NOT NULL,
    s_field_type VARCHAR(255),
    s_field_name VARCHAR(255),
    s_field_label VARCHAR(255),
    s_field_value VARCHAR(255),
    PRIMARY KEY (pk_i_id),
    FOREIGN KEY (fk_user_id) REFERENCES /*TABLE_PREFIX*/t_user (pk_i_id),
    FOREIGN KEY (fk_field_id) REFERENCES /*TABLE_PREFIX*/t_user_fields (pk_i_id)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';