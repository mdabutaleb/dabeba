<?php if ( ! defined('OC_ADMIN')) exit('Direct access is not allowed.');

$doaction=Params::getParam('doaction');
switch ($doaction)
{

case('add'):  
                   $field_type=Params::getParam('fieldtype');
                   $field_name=Params::getParam('fieldname');
                   $field_label=Params::getParam('fieldlabel');
                   $field_options=Params::getParam('options');
                   $field_max_length=Params::getParam('maxlength');
                   $field_required=Params::getParam('required');
                   $field_status=1;
                   ModelUserfield::newInstance()->insertUserFiled($field_type, $field_options, $field_name, $field_label, $field_status, $field_max_length, $field_required );
                   header('Location: '.osc_admin_render_plugin_url('user_fields/admin/custom_fields.php')); 
                   osc_add_flash_ok_message(__("Field added sucessfully", "user_fields"), 'admin');

break;


case('edit'):  
                    $field_id=Params::getParam('fieldid');
                    $field_type=Params::getParam('fieldtype');
                    $field_name=Params::getParam('fieldname');
                    $field_label=Params::getParam('fieldlabel');
                    $field_options=Params::getParam('options');
                    $field_max_length=Params::getParam('maxlength');
                    $field_required=Params::getParam('required');
                    $field_status=1;
                    ModelUserfield::newInstance()->updateUserFiled($field_id, $field_type, $field_options, $field_name, $field_label, $field_status, $field_max_length, $field_required);
                    header('Location: '.osc_admin_render_plugin_url('user_fields/admin/custom_fields.php')); 
                    osc_add_flash_ok_message(__("Field Updated sucessfully", "user_fields"), 'admin');
break;

case('delete'):
    
                    $field_id=Params::getParam('field_id');
                    ModelUserfield::newInstance()->deleteFiled($field_id);
                    ModelUserfield::newInstance()->deleteFiledUserData($field_id);
                    header('Location: '.osc_admin_render_plugin_url('user_fields/admin/custom_fields.php')); 
                    osc_add_flash_ok_message(__("Field Deleted sucessfully", "user_fields"), 'admin');
break;

case('activate'):  
                    $field_id=Params::getParam('field_id');
                    ModelUserfield::newInstance()->activateFiled($field_id);
                    header('Location: '.osc_admin_render_plugin_url('user_fields/admin/custom_fields.php')); 
                    osc_add_flash_ok_message(__("Field Activated sucessfully", "user_fields"), 'admin');
break;
case('deactivate'):  
                    $field_id=Params::getParam('field_id');
                    ModelUserfield::newInstance()->deactivateFiled($field_id);
                    header('Location: '.osc_admin_render_plugin_url('user_fields/admin/custom_fields.php')); 
                    osc_add_flash_ok_message(__("Field Deactivated sucessfully", "user_fields"), 'admin');
break;
case('registration_fields'):
                    $reg_fields=array();
                    $reg_req_fields=array();
                    $usertype=Params::getParam('usertype');
                    $cellphone=Params::getParam('cellphone');
                    $phone=Params::getParam('phone');
                    $country=Params::getParam('country');
                    $region=Params::getParam('region');
                    $city=Params::getParam('city');
                    $cityarea=Params::getParam('cityarea');
                    $address=Params::getParam('address');
                    $website=Params::getParam('website');
                    $location_dropdown=Params::getParam('location-dropdown');
                    if($usertype=="selected"){
                        $reg_fields[]="usertype";
                    }
                    if($cellphone=="selected"){
                        $reg_fields[]="cellphone";
                    }
                    if($phone=="selected"){
                        $reg_fields[]="phone";
                    }
                    if($country=="selected"){
                        $reg_fields[]="country";
                    }
                    if($region=="selected"){
                        $reg_fields[]="region";
                    }
                    if($city=="selected"){
                        $reg_fields[]="city";
                    }
                    if($cityarea=="selected"){
                        $reg_fields[]="cityarea";
                    }
                    if($address=="selected"){
                        $reg_fields[]="address";
                    }
                    if($website=="selected"){
                        $reg_fields[]="website";
                    }
                    if($location_dropdown=="selected"){
                        $reg_fields[]="location-dropdown";
                    }
                    $r_usertype=Params::getParam('r_usertype');
                    $r_cellphone=Params::getParam('r_cellphone');
                    $r_phone=Params::getParam('r_phone');
                    $r_country=Params::getParam('r_country');
                    $r_region=Params::getParam('r_region');
                    $r_city=Params::getParam('r_city');
                    $r_cityarea=Params::getParam('r_cityarea');
                    $r_address=Params::getParam('r_address');
                    $r_website=Params::getParam('r_website');
                    
                    if($r_usertype=="selected"){
                        $reg_req_fields[]="r_usertype";
                    }
                    if($r_cellphone=="selected"){
                        $reg_req_fields[]="r_cellphone";
                    }
                    if($r_phone=="selected"){
                        $reg_req_fields[]="r_phone";
                    }
                    if($r_country=="selected"){
                        $reg_req_fields[]="r_country";
                    }
                    if($r_region=="selected"){
                        $reg_req_fields[]="r_region";
                    }
                    if($r_city=="selected"){
                        $reg_req_fields[]="r_city";
                    }
                    if($r_cityarea=="selected"){
                        $reg_req_fields[]="r_cityarea";
                    }
                    if($r_address=="selected"){
                        $reg_req_fields[]="r_address";
                    }
                    if($r_website=="selected"){
                        $reg_req_fields[]="r_website";
                    }
                    
                    $fields_req_string=implode(", ",$reg_req_fields);
                    $fields_string=implode(", ",$reg_fields);
                    osc_set_preference('reg_fields', $fields_string, 'user_reg_meta_field');
                    osc_set_preference('reg_req_fields', $fields_req_string, 'user_reg_meta_field');
                    header('Location: '.osc_admin_render_plugin_url('user_fields/admin/registration_fields.php')); 
                    osc_add_flash_ok_message(__("Registration Fields updated sucessfully", "user_fields"), 'admin');
break;
default:

}

?>