<?php if ( ! defined('OC_ADMIN')) exit('Direct access is not allowed.');
?>
<script type="text/javascript">

    function nospaces(t){
    
        if(t.value.match(/\s/g)){
        
        alert('Sorry, spaces not allowed in Field Name');
        alert('<?php echo osc_esc_js(__('Sorry, spaces not allowed in Field Name', 'user_fields')); ?>'); 
        t.value=t.value.replace(/\s/g,'');
        
        }
    
    }
</script>

<h2><?php _e('Add New User Field', 'user_fields'); ?></h2>
<form action="<?php echo osc_admin_base_url(true); ?>" method="post">
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="doaction" value="add" />
    <input type="hidden" name="file" value="user_fields/admin/field_options.php" />
    <div>
        <label for="fieldtype" style="display: block;"><?php _e('Select User field Type', 'user_fields'); ?></label>
        <select name="fieldtype" id="user-meta-field-selector">
            <option value="text"><?php _e('Text', 'user_fields'); ?></option>
            <option value="textarea"><?php _e('TextArea', 'user_fields'); ?></option>
            <option value="dropdown"><?php _e('Drpodown', 'user_fields'); ?></option>
            <option value="radio"><?php _e('Radio', 'user_fields'); ?></option>
            <option value="checkbox"><?php _e('Checkbox', 'user_fields'); ?></option>
            <option value="url"><?php _e('URL', 'user_fields'); ?></option>
            <option value="date"><?php _e('Date', 'user_fields'); ?></option>
        </select>
    </div>
    <div id="field-options">
        <label for="options" style="display: block;"><?php _e('Options', 'user_fields'); ?></label>
        <input type="text" value="" name="options" id="options">
            <p>(<?php _e('Separate options with commas', 'user_fields'); ?>)</p>
    </div>
    <div>
        <label for="fieldname" style="display: block;"><?php _e('Enter User field Name', 'user_fields'); ?></label>
        <input type="text" value="" name="fieldname" id="fieldname" onkeyup="nospaces(this)">
    </div>
    <div>
        <label for="fieldlabel" style="display: block;"><?php _e('Enter User field Label', 'user_fields'); ?></label>
        <input type="text" value="" name="fieldlabel" id="fieldlabel">
    </div>
    <div>
        <label for="maxlength" style="display: block;"><?php _e('Max Length', 'user_fields'); ?></label>
        <input type="text" value="" name="maxlength" id="maxlength">
    </div>
    <div>
        <input type="checkbox" name="required" value="1">
        <label for="required"><?php _e('Required', 'user_fields'); ?></label>
    </div>
    </br>
    <div>
        <input type="submit" value="<?php _e('Save', 'user_fields'); ?>" class="btn btn-submit" />
    </div>    
</form>