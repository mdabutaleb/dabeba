<?php if ( ! defined('OC_ADMIN')) exit('Direct access is not allowed.');
$active_fields=osc_get_preference('reg_fields', 'user_reg_meta_field');
$active_fields=explode(", ",$active_fields);
$required_fields=osc_get_preference('reg_req_fields', 'user_reg_meta_field');
$required_fields=explode(", ",$required_fields);
?>
<h2> <?php _e('Select Registration Field To Show', 'user_fields'); ?></h2>
<form action="<?php echo osc_admin_base_url(true); ?>" method="post">
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="doaction" value="registration_fields" />
    <input type="hidden" name="file" value="user_fields/admin/field_options.php" />
    <div class="reg-field-table">
        <table>
            <tr>
                <th><?php _e('Fields', 'user_fields'); ?></th>
                <th><?php _e('Required', 'user_fields'); ?></th>
            </tr>
            <tr>
                <td><input type="checkbox" name="usertype" value="selected" <?php if(in_array("usertype",$active_fields)){ echo "checked";} ?>><label for="usertype"><?php _e('User type', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_usertype" value="selected" <?php if(in_array("r_usertype",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="cellphone" value="selected" <?php if(in_array("cellphone",$active_fields)){ echo "checked";} ?>><label for="cellphone"><?php _e('Cell phone', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_cellphone" value="selected" <?php if(in_array("r_cellphone",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="phone" value="selected" <?php if(in_array("phone",$active_fields)){ echo "checked";} ?>><label for="phone"><?php _e('Phone', 'user_fields'); ?></label><br></td>
                <td><input type="checkbox" name="r_phone" value="selected" <?php if(in_array("r_phone",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="country" value="selected" <?php if(in_array("country",$active_fields)){ echo "checked";} ?>><label for="country"><?php _e('Country', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_country" value="selected" <?php if(in_array("r_country",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="region" value="selected" <?php if(in_array("region",$active_fields)){ echo "checked";} ?>><label for="region"><?php _e('Region', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_region" value="selected" <?php if(in_array("r_region",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="city" value="selected" <?php if(in_array("city",$active_fields)){ echo "checked";} ?>><label for="city"><?php _e('City', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_city" value="selected" <?php if(in_array("r_city",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="cityarea" value="selected" <?php if(in_array("cityarea",$active_fields)){ echo "checked";} ?>><label for="cityarea"><?php _e('City area', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_cityarea" value="selected" <?php if(in_array("r_cityarea",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="address" value="selected" <?php if(in_array("address",$active_fields)){ echo "checked";} ?>><label for="address"><?php _e('Address', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_address" value="selected" <?php if(in_array("r_address",$required_fields)){ echo "checked";} ?>></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="website" value="selected" <?php if(in_array("website",$active_fields)){ echo "checked";} ?>><label for="website"><?php _e('Website', 'user_fields'); ?></label></td>
                <td><input type="checkbox" name="r_website" value="selected" <?php if(in_array("r_website",$required_fields)){ echo "checked";} ?>></td>
            </tr>
         
        </table>
        <p><input type="checkbox" name="location-dropdown" value="selected" <?php if(in_array("location-dropdown",$active_fields)){ echo "checked";} ?>><label for="location-dropdown"><?php _e('Activate Location Dropdown', 'user_fields'); ?></label></p>
    </div>
    </br>
    <div>
        <input type="submit" value="<?php _e('Save', 'user_fields'); ?>" class="btn btn-submit" />
    </div>    
</form>