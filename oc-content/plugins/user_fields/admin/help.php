<?php if ( ! defined('OC_ADMIN')) exit('Direct access is not allowed.'); ?>

<h2><?php _e('User Fields and Registration Fields', 'user_fields'); ?></h2>
<h4 style="color: #259fa5;"><?php _e('Description', 'user_fields'); ?></h4>
<p><?php _e('User Custom Field Plugin allow admin to add extra fields to user profile. With User Field plugin we can add fields to users account and we can show them on Users Profile', 'user_fields'); ?></p>

<h4 style="color: #259fa5;"><?php _e('To Create user fields', 'user_fields'); ?></h4>
<p><?php _e('Admin can create different user fields from Custom Field option from plugin menu.', 'user_fields'); ?></p>
<p><?php _e('click on Add new button to add user custom field.', 'user_fields'); ?></p>
<p><?php _e('Admin can edit, delete, activate each field.', 'user_fields'); ?></p>
<h4 style="color: #259fa5;"><?php _e('To show user Field', 'user_fields'); ?></h4>
<p><?php _e('Plugin Provides Three functions to show User field on users profile', 'user_fields'); ?></p>
<p>1) <b><?php echo"&lt;?php get_user_field_split_name('Field_name'); ?&gt;";?></b> and <b><?php echo"&lt;?php get_user_field_split_value('Field_name'); ?&gt;";?></b> ( <?php _e('To show individual field name and value', 'user_fields'); ?> ) </p> 
<p>2) <b><?php echo"&lt;?php get_user_field('Field_name'); ?&gt;";?></b>  (<?php _e('To show singal Field name and value.', 'user_fields'); ?> )</p>  
<p>3) <b><?php echo"&lt;?php get_user_meta_fields(); ?&gt;";?></b> (<?php _e('To show all Fields and its values.', 'user_fields'); ?> )</p>
<p><?php _e('To show User Custom Fields on User profile Use any one of the above function . Using frist function User can print field lable and field value seperatly . Using second function user can print single fields label and its value. Using third function user can print all custom fields name and values.', 'user_fields'); ?> <p>

<h4 style="color: #259fa5;"><?php _e('Enable Registration Fields', 'user_fields'); ?></h4>
<p><?php _e('This Feature is similar to osclass', 'user_fields'); ?> <a target="_blank" href="http://market.osclass.org/plugins/miscellaneous/required-fields_30">"<?php _e('Required fields', 'user_fields'); ?>" </a> <?php _e('plugin and improved version.', 'user_fields'); ?> </p>
<p><?php _e('This Feature is with easy user interface to show registration fields on Registration field. No need edit any file. Fixed bug for location Dropdown.', 'user_fields'); ?></p>