<?php if ( ! defined('OC_ADMIN')) exit('Direct access is not allowed.'); ?>

<h2><?php _e('User Custom fields', 'user_fields'); ?>
<a class="btn add-button" style="float: none; margin-left: 20px;" href="<?php echo osc_admin_render_plugin_url('user_fields/admin/add_user_field.php?doaction=add'); ?>"><?php _e('Add new', 'user_fields'); ?></a>
</h2>
<?php
$user_fields=array();
$user_fields= ModelUserfield::newInstance()->getUserFileds();

?>
<table style="width: 100%" class="user-fields-table">
    <tr>
        <th><?php _e('Field Label', 'user_fields'); ?></th>
        <th><?php _e('Field Name', 'user_fields'); ?></th>
        <th><?php _e('Field Type', 'user_fields'); ?></th>
        <th><?php _e('Field Options', 'user_fields'); ?></th>
        <th><?php _e('Settings', 'user_fields'); ?></th>
    </tr>
    <?php foreach ($user_fields as $field) {
        if($field['s_field_status']==1){
            $field_status="Deactivate";
            $status_action="deactivate";
        }
        else{
            $field_status="Activate";
             $status_action="activate";
        }
        $options_lenght= strlen($field['s_field_options']);
        if($options_lenght>50){
            $field_options= substr($field['s_field_options'], 0, 50). "...";
        }
        else{
            $field_options= $field['s_field_options'];
        }
        echo '<tr class="'.$field_status.'" style="width: 100%"><td> <span class="field-label">'.$field['s_field_label'].'</span></td><td><span class="">'.$field['s_field_name'].'</span></td> ' ;
        echo '<td><span class="">'.$field['s_field_type'].'</span></td>';
        echo '<td><span class="">'.$field_options.'</span></td>';
        echo '<td><span class="field-options"><a href="'.osc_admin_render_plugin_url('user_fields/admin/field_options.php?doaction='.$status_action.'&field_id='.$field['pk_i_id']).'">'.$field_status.'</a><a href="'.osc_admin_render_plugin_url('user_fields/admin/edit_user_field.php?doaction=edit&field_id='.$field['pk_i_id']).'">'. __('Edit').'</a><a href="'.osc_admin_render_plugin_url('user_fields/admin/field_options.php?doaction=delete&field_id='.$field['pk_i_id']).'">'.__('Delete').'</a></span></td></tr>';
    } ?>
</table>