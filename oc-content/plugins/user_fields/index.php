<?php
/*
Plugin Name: User Fields
Plugin URI: 
Description: Extra user Fields and registration fields.
Version: 1.0.0
Author: Rahul Patil
Author URI: 
Short Name: user_fileds
Plugin update URI:
*/

    require_once 'ModelUserfield.php';
    function user_menu_install() {
        ModelUserfield::newInstance()->import('user_fields/struct.sql') ;
       
    }

    function user_menu_uninstall() {
        ModelUserfield::newInstance()->uninstall();
        Preference::newInstance()->delete( array('s_section' => 'user_reg_meta_field') );
    }
    
    function user_extra_fields(){
        $user_id= osc_logged_user_id();
        $field_data=ModelUserfield::newInstance()->getUserActiveFiledValues($user_id);
        $update=0;
        $active_fields=ModelUserfield::newInstance()->getUserActiveFiled();
       
        if($field_data==null){
            $active_fields=$active_fields;
        }
        else{
            foreach($active_fields as $k => $field){
                if($field_data[$k]['s_field_value'] !=""){
                    $active_fields[$k]['s_field_value'] = $field_data[$k]['s_field_value'];
                }
            }
            $update=1;
            echo '<input type="hidden" name="update-action" value="update">';
        }
        echo '<div class="user-meta-fields">';
        foreach ($active_fields as $fields) {
            
            if($update==1){
                $field_value=$fields['s_field_value'];
            }else{
                $field_value="";
            }
            if($fields['s_field_type']=="text"){
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><input type="text" name="'.$fields['s_field_name'].'" value="'.osc_esc_html($field_value).'" '.(($fields['max_length'] != 0)?' maxlength="'.$fields['max_length'].'"':""). '></div>';
                echo '</div>';
            }
            elseif($fields['s_field_type']=="url"){
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><input type="url" name="'.$fields['s_field_name'].'" value="'.osc_esc_html($field_value).'" '.(($fields['max_length'] != 0)?' maxlength="'.$fields['max_length'].'"':""). '></div>';
                echo '</div>';
            }
            elseif($fields['s_field_type']=="date"){
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><input type="date" name="'.$fields['s_field_name'].'" value="'.osc_esc_html($field_value).'"></div>';
                echo '</div>';
            }
            elseif($fields['s_field_type']=="textarea"){
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><textarea type="text" name="'.$fields['s_field_name'].'"rows="10" '.(($fields['max_length'] != 0)?' maxlength="'.$fields['max_length'].'"':""). '>'.osc_esc_html($field_value).'</textarea></div>';
                echo '</div>';
            }
            elseif($fields['s_field_type']=="radio"){
               $value= $fields['s_field_options'];
               $values=array();
                $values=explode(",",$value);
                $i=0;
                $count=count($values);
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                while($count>$i){
                    $selected="";
                    if($values[$i]==$field_value){
                        $selected="checked";
                    }
                    echo '<div class="controls"><input type="radio" name="'.$fields['s_field_name'].'" value="'.osc_esc_html($values[$i]).'" '.$selected.'>'.$values[$i].'</div>';
                    $i++;
                }
                echo '</div>';
            }
            elseif($fields['s_field_type']=="checkbox"){
                $selected="";
                if($fields['s_field_name']==$field_value){
                    $selected="checked";
                }
                echo '<div class="control-group">';
                //echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><input type="checkbox" name="'.$fields['s_field_name'].'" value="'.osc_esc_html($fields['s_field_name']).'" '.$selected.'> '.$fields['s_field_label'].'</div>';
                echo '</div>';
            }
            elseif($fields['s_field_type']=="dropdown"){
                echo '<div class="control-group">';
                echo '<label for="meta_'.$fields['s_field_name'].'">'.$fields['s_field_label'].'</label>';
                echo '<div class="controls"><select name="'.$fields['s_field_name'].'">';
                $value= $fields['s_field_options'];
               $values=array();
                $values=explode(",",$value);
                $i=0;
                $count=count($values);
                 while($count>$i){
                    $selected="";
                    if($values[$i]==$field_value){
                        $selected="selected";
                    }
                    echo '<option value="'.osc_esc_html($values[$i]).'" '.$selected.'>'.$values[$i].'</option>';
                    $i++;
                }
                echo '</select></div>';
                echo '</div>';
            }
         }
      echo '</div>';
    }
     function user_add_fields(){
        $action=Params::getParam('update-action');
        $active_fields=ModelUserfield::newInstance()->getUserActiveFiled();
        foreach ($active_fields as $fields) {
            $user_id= osc_logged_user_id();
            $fieldid=$fields['pk_i_id'];
            $fieldname=$fields['s_field_name'];
            $fieldtype=$fields['s_field_type'];
            $fieldlabel=$fields['s_field_label'];
            $fieldvalue=Params::getParam($fieldname);
            if($action=="update"){
                $field_exist=ModelUserfield::newInstance()->checkFieldForUser($user_id, $fieldid);
                if($field_exist==null){
                    ModelUserfield::newInstance()->insertUserdata($user_id, $fieldid, $fieldtype, $fieldname, $fieldlabel, $fieldvalue);
                }
                else{
                    ModelUserfield::newInstance()->updateUserdata($user_id, $fieldid, $fieldtype, $fieldname, $fieldlabel, $fieldvalue);
                }
                
            }else{
                ModelUserfield::newInstance()->insertUserdata($user_id, $fieldid, $fieldtype, $fieldname, $fieldlabel, $fieldvalue);
            }
        }
        
    }
    function validation_error(){
         $error=array();
        $error[] = 10;
        return $error;
    }
    function required_field_validation(){
        $required_fields=ModelUserfield::newInstance()->getRequiredFields();
        foreach($required_fields as $field){
            if( Params::getParam($field['s_field_name'], false, false) == '' ) {
                $msg = "' ". $field['s_field_label']. " '". ' field is required';
                osc_add_flash_error_message( $msg);
                osc_redirect_to(osc_user_profile_url());
                return $flash_error;
            }
        }
        
    }
    
    function user_fields_setting(){
        
            echo '<h3><a href="#">User Custom Fields</a></h3>
            <ul>
                <li><a href="'.osc_admin_render_plugin_url("user_fields/admin/custom_fields.php").'"> ' . __('Custom Fields', 'user_fields') . '</a></li>
                <li><a href="'.osc_admin_render_plugin_url("user_fields/admin/registration_fields.php").'">' . __('Registration Fields', 'user_fields') . '</a></li>
                <li><a href="'.osc_admin_render_plugin_url("user_fields/admin/help.php").'"> ' . __('Help', 'user_fields') . '</a></li>
            </ul>';
    }
    function get_user_field($field_name){
        $user_id= osc_user_id();
        $user_field=ModelUserfield::newInstance()->getUserFiledByFiledName($user_id, $field_name);
        if($user_field!=null){
            if($user_field['s_field_type']=="url"){
                echo '<p class="meta_'.$user_field['s_field_name'].'"><a href="'.$user_field['s_field_value'].'">'. $user_field['s_field_label'] .'</a></p>';
            }
            else{
                echo '<p class="meta_'.$user_field['s_field_name'].'"><strong>'. $user_field['s_field_label'] .':</strong><span> '. $user_field['s_field_value'].'</span></p>';
            }
        
        }
    }
    function get_user_meta_fields(){
        $user_id= osc_user_id();
        $user_fields=ModelUserfield::newInstance()->getUserFiledAllFileds($user_id);
        if($user_fields!=null){
            foreach ($user_fields as $field) {
                if($field['s_field_type']=="url"){
                    echo '<p class="meta_'.$field['s_field_name'].'"><a href="'.$field['s_field_value'].'">'. $field['s_field_label'] .'</a></p>';
                }
                else{
                    echo '<p class="meta_'.$field['s_field_name'].'"><strong>'. $field['s_field_label'] .':</strong> <span>'. $field['s_field_value'] .'</span></p>';
                }
            }
        }
    }
    function get_user_field_split_name($field_name){
        $user_id= osc_user_id();
        $user_field=ModelUserfield::newInstance()->getUserFiledByFiledName($user_id, $field_name);
        if($user_field!=null){
        echo $user_field['s_field_label'];
        }
        
    }
    function get_user_field_split_value($field_name){
        $user_id= osc_user_id();
        $user_field=ModelUserfield::newInstance()->getUserFiledByFiledName($user_id, $field_name);
        if($user_field!=null){
            if($user_field['s_field_type']=="url"){
                    echo '<a class="meta_'.$user_field['s_field_name'].'" href="'.$user_field['s_field_value'].'">'. $user_field['s_field_value'].'</a>';
                }
                else{
                    return $user_field['s_field_value'];
                }
        
        }
    }
    
    function registration_fields(){
        $active_fields=osc_get_preference('reg_fields', 'user_reg_meta_field');
        $active_fields=explode(", ",$active_fields);
        if(in_array("location-dropdown",$active_fields)){
            UserForm::location_javascript();
        }    
        if(in_array("usertype",$active_fields)){ ?>
            <div class="control-group">
            <label for="user_type"><?php _e('User type', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::is_company_select() ; ?>
            </div>
            </div>
        <?php }
        if(in_array("cellphone",$active_fields)){ ?>
            <div class="control-group">
            <label for="phoneMobile"><?php _e('Cell phone', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::mobile_text() ; ?>
            </div>
            </div>
        <?php }
         if(in_array("phone",$active_fields)){ ?>
            <div class="control-group">
            <label for="phoneLand"><?php _e('Phone', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::phone_land_text() ; ?>
            </div>
            </div>
        <?php }
         if(in_array("country",$active_fields)){ ?>
            <div class="control-group">
            <label for="country"><?php _e('Country', 'user_fields') ; ?> </label>
            <div class="controls">
                <?php
                    if(in_array("location-dropdown",$active_fields)){
                       UserForm::country_select(osc_get_countries(), osc_user());
                    }
                    else{
                        UserForm::country_select(osc_get_countries()) ;
                    }
                ?>
            </div>
            </div>
        <?php }
         if(in_array("region",$active_fields)){ ?>
            <div class="control-group">
            <label for="region"><?php _e('Region', 'user_fields') ; ?> </label>
            <div class="controls">
                 <?php
                    if(in_array("location-dropdown",$active_fields)){
                       UserForm::region_select(osc_get_regions(), osc_user());
                    }
                    else{
                       UserForm::region_select(array()) ;
                    }
                ?>
            </div>
            </div>
        <?php }
         if(in_array("city",$active_fields)){ ?>
            <div class="control-group">
            <label for="city"><?php _e('City', 'user_fields') ; ?> </label>
            <div class="controls">
                 <?php
                    if(in_array("location-dropdown",$active_fields)){
                       UserForm::city_select(osc_get_cities(), osc_user());
                    }
                    else{
                       UserForm::city_select(array()) ;
                    }
                ?>
            </div>
            </div>
        <?php }
         if(in_array("cityarea",$active_fields)){ ?>
            <div class="control-group">
            <label for="city_area"><?php _e('City area', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::city_area_text() ; ?>
            </div>
            </div>
        <?php }
         if(in_array("address",$active_fields)){ ?>
            <div class="control-group">
            <label for="address"><?php _e('Address', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::address_text() ; ?>
            </div>
            </div>
        <?php }
        if(in_array("website",$active_fields)){ ?>
            <div class="control-group">
            <label for="webSite"><?php _e('Website', 'user_fields') ; ?></label>
            <div class="controls">
            <?php UserForm::website_text() ; ?>
            </div>
            </div>
        <?php }
    }
    
    function registration_req_fields(){
        $req_fields=osc_get_preference('reg_req_fields', 'user_reg_meta_field');
        $req_fields=explode(", ",$req_fields);
        ?>
        <script>
            $(document).ready(function(){
                <?php if(in_array("r_cellphone",$req_fields)){ ?>
                $("#s_phone_mobile").rules("add", {required: true, messages: { required: "Mobile phone is required" }});
                <?php } if(in_array("r_phone",$req_fields)){ ?>
                $("#s_phone_land").rules("add", {required: true, messages: { required: "Land phone is required" }});
                <?php } if(in_array("r_country",$req_fields)){ ?>
                $("#countryId").rules("add", {required: true, messages: { required: "Country is required" }});
                <?php } if(in_array("r_region",$req_fields)){ ?>
                $("#region").rules("add", {required: true, messages: { required: "Region is required" }});
                <?php } if(in_array("r_city",$req_fields)){  ?>
                $("#city").rules("add", {required: true, messages: { required: "City is required" }});
                <?php } if(in_array("r_cityarea",$req_fields)){ ?>
                $("#cityArea").rules("add", {required: true, messages: { required: "City area is required" }});
                <?php } if(in_array("r_address",$req_fields)){ ?>
                $("#address").rules("add", {required: true, messages: { required: "Address is required" }});
                <?php } if(in_array("r_website",$req_fields)){ ?>
                $("#s_website").rules("add", {required: true, messages: { required: "Website is required" }});
                <?php } ?>
            }); 
        </Script>
  <?php  }
    
    
    
    function register_user_field_script(){ ?>
        <script>
            $('#field-options').hide();
            $('#user-meta-field-selector').on('change', function() {
                $seleted_field = $(this).val();
                if ($seleted_field=='dropdown') {
                    $('#field-options').show();
                }
                else if ($seleted_field=='radio') {
                    $('#field-options').show();
                }
                else{
                     $('#field-options').hide();
                }
                
              });
            $field_selected=$('select[name=fieldtype]').val();
            if ($field_selected=='dropdown') {
                    $('#field-options').show();
                }
                else if ($field_selected=='radio') {
                    $('#field-options').show();
                }
                else{
                     $('#field-options').hide();
                }
        </script>
   <?php }
    function register_user_field_style(){ ?>
        <style>
    
            .user-fields-table td{
                background-color: #E2E2E2;
                border: 1px solid #ddd;
                padding: 5px;
            }
            .user-fields-table th{
                border: 1px solid #ddd;
                padding: 5px;
            }
            .user-fields-table .field-label{
                font-weight: bold;
                text-transform: capitalize;
            }
            .user-fields-table .field-options{
                float: right;
            }
            .user-fields-table .field-options a{
                padding-right: 10px;
            }
            .Deactivate td{
                background-color: #F7F7F7 !important;
            }
            .Activate td{
                 background-color: #FDE7E7 !important;
            }
            .reg-field-table td, .reg-field-table th{
                border: 1px solid #ddd;
                padding: 5px;
            }
            .user-meta-fields .controls{
                 margin-left: 170px;
            }
            .user-meta-fields .control-group label{
                float: left;
                text-align: right !important;
                width: 150px;
                color: #616161;
                font-size: 13px;
            }
            .user-meta-fields .control-group{
                margin-bottom: 15px;
            }
        </style>
    <?php }
    
    function registration_field_add($user_id){
        $fieldvalue =Params::getParam('Neighborhood_alert');
        if($fieldvalue == "Neighborhood_alert"){
            ModelUserfield::newInstance()->insertUserdata($user_id, 1, 'checkbox', 'Neighborhood_alert', 'Allow Neighborhood alert', $fieldvalue);
        }
    }
    
    /**
     * ADD HOOKS
     */
    osc_register_plugin(osc_plugin_path(__FILE__), 'user_menu_install');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'user_menu_uninstall');
    osc_add_hook('user_profile_form', 'user_extra_fields');
    osc_add_hook('user_edit_completed', 'user_add_fields');
    osc_add_hook('admin_menu', 'user_fields_setting');
    osc_add_hook('admin_footer', 'register_user_field_script');
    osc_add_hook('admin_header', 'register_user_field_style');
    osc_add_hook('user_register_form', 'registration_fields');
    osc_add_hook('footer', 'registration_req_fields');
    osc_add_hook('pre_user_post', 'required_field_validation');
    osc_add_hook('user_register_completed', 'registration_field_add');    
?>
