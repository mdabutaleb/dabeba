<?php
    /*
     *      OSCLass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2010 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    /**
     * Model database for User Fields Table
     * 
     * @package OSClass
     * @subpackage Model
     * @since 3.0
     */
    class ModelUserfield extends DAO
    {
        /**
         * It references to self object: ModelUserfield.
         * It is used as a singleton
         * 
         * @access private
         * @since 3.0
         * @var ModelJobs
         */
        private static $instance ;

        /**
         * It creates a new ModelJobs object class ir if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since 3.0
         * @return ModelJobs
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        /**
         * Construct
         */
        function __construct()
        {
            parent::__construct();
        }
        
         /**
         * Import sql file
         * @param type $file 
         */
        public function import($file) {
            $path = osc_plugin_resource($file);
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( $this->dao->getErrorLevel().' - '.$this->dao->getErrorDesc() );
            }
        }
        
        /**
         * Return table name jobs attributes
         * @return string
         */
        public function getTableName()
        {
            return DB_TABLE_PREFIX.'t_user_fields' ;
        }
        
       
        /**
         *  Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query('DROP TABLE '. DB_TABLE_PREFIX.'t_user_field_data');
            $this->dao->query('DROP TABLE '. $this->getTableName());
            
        }
        
       
        /**
         * Insert User Fields
         */
        public function insertUserFiled($fieldtype, $field_options, $fieldname, $fieldlable, $field_status, $field_max_length, $field_required)
        {
            $aSet = array(
                's_field_type'   => $fieldtype,
                's_field_options' => $field_options,
                's_field_name'    => $fieldname,
                's_field_label'   => $fieldlable,
                's_field_status'    => $field_status,
                'max_length'    => $field_max_length,
                'required'    => $field_required
            );
            
            return $this->dao->insert($this->getTableName(), $aSet);
        }
        
        /**
         * Update user field.
         */
          public function updateUserFiled($field_id, $field_type, $field_options, $field_name, $field_label, $field_status, $field_max_length, $field_required) {
            
            return $this->dao->update($this->getTableName(), array("s_field_type" => $field_type, "s_field_options" => $field_options, "s_field_name" =>$field_name, "s_field_label" => $field_label, "s_field_status" => $field_status, "max_length" => $field_max_length, "required" => $field_required), array('pk_i_id' => $field_id));
           
        }
        /**
         * Insert User data
         */
        public function insertUserdata($user_id, $fieldid, $fieldtype, $fieldname, $fieldlabel, $fieldvalue)
        {
            $aSet = array(
                'fk_user_id'   => $user_id,
                'fk_field_id' => $fieldid,
                's_field_type'    => $fieldtype,
                's_field_name'    => $fieldname,
                's_field_label'    => $fieldlabel,
                's_field_value'   => $fieldvalue
            );
            
            return $this->dao->insert(DB_TABLE_PREFIX.'t_user_field_data', $aSet);
        }
        /**
         * update User data
         */
        public function updateUserdata($user_id, $fieldid, $fieldtype, $fieldname, $fieldlabel, $fieldvalue)
        {
            return $this->dao->update(DB_TABLE_PREFIX.'t_user_field_data', array('s_field_type' => $fieldtype, 's_field_name' => $fieldname, 's_field_label' => $fieldlabel, 's_field_value' => $fieldvalue), array('fk_user_id' => $user_id, 'fk_field_id' => $fieldid));
        }
         /**
         * Get User Fields
         */
        public function getUserFileds()
        {
            $this->dao->select('*');
            $this->dao->from($this->getTableName());
            $result = $this->dao->get();
            return $result->result();
        }
        /**
         * get user field by id.
         */
         public function getUserFiled($field_id) {
            $this->dao->select('*');
            $this->dao->from($this->getTableName());
            $this->dao->where('pk_i_id', $field_id);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }

            $user_field=$result->row();
            return $user_field;
        }
        /**
         * get user field by fieldname.
         */
         public function getUserFiledByFiledName($user_id, $field_name) {
            $this->dao->select('*');
            $this->dao->from(DB_TABLE_PREFIX.'t_user_field_data');
            $this->dao->where('fk_user_id', $user_id);
            $this->dao->where('s_field_name', $field_name);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }

            $user_field=$result->row();
            return $user_field;
        }
        
         /**
         * check user field exist.
         */
         public function checkFieldForUser($user_id, $fieldid) {
            $this->dao->select('*');
            $this->dao->from(DB_TABLE_PREFIX.'t_user_field_data');
            $this->dao->where('fk_user_id', $user_id);
            $this->dao->where('fk_field_id', $fieldid);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }

            $field=$result->row();
            return $field;
        }
        /**
         * get user all field .
         */
         public function getUserFiledAllFileds($user_id) {
            $this->dao->select('*');
            $this->dao->from(DB_TABLE_PREFIX.'t_user_field_data');
            $this->dao->where('fk_user_id', $user_id);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }
            return $result->result();
        }
         /**
         * get user fileds data .
         */
         public function getUserActiveFiledValues($user_id) {
            $result = $this->dao->query(sprintf("SELECT i.*, l.s_field_value FROM %st_user_fields i LEFT JOIN %st_user_field_data l ON i.pk_i_id=l.fk_field_id where (l.fk_user_id IS NULL)OR(l.fk_user_id= %d) AND i.s_field_status= 1", DB_TABLE_PREFIX, DB_TABLE_PREFIX, $user_id));
            if( $result->numRows == 0 ) {
                return array();
            }
            return $result->result();
        }
        /**
         * get user active fields.
         */
         public function getUserActiveFiled() {
            $this->dao->select('*');
            $this->dao->from($this->getTableName());
            $this->dao->where('s_field_status', 1);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }
            return $result->result();
        }
        
         /**
         * get required fields.
         */
         public function getRequiredFields() {
            $this->dao->select('*');
            $this->dao->from($this->getTableName());
            $this->dao->where('s_field_status', 1);
            $this->dao->where('required', 1);
            $result = $this->dao->get();

            if( $result->numRows == 0 ) {
                return array();
            }
            return $result->result();
        }
        
        /**
         * Delete User Field
         */
        public function deleteFiled($field_id)
        {
            return $this->dao->delete($this->getTableName(), "pk_i_id = $field_id");

        }
         /**
         * Delete User Field data
         */
        public function deleteFiledUserData($field_id)
        {
            return $this->dao->delete(DB_TABLE_PREFIX.'t_user_field_data', "fk_field_id = $field_id");

        }
        /**
         * Activate User Field
         */
         public function activateFiled($field_id)
        {
                 return $this->dao->update($this->getTableName(), array('s_field_status' => 1), array('pk_i_id' => $field_id));
        }
        /**
         * Deactivate User Field
         */
         public function deactivateFiled($field_id)
        {
                 return $this->dao->update($this->getTableName(), array('s_field_status' => 0), array('pk_i_id' => $field_id));
        }
      
    }
?>