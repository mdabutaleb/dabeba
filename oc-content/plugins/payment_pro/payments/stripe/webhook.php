<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

ob_get_clean();

$stripe = StripePayment::getEnvironment();
\Stripe\Stripe::setApiKey($stripe['secret_key']);
$body = @file_get_contents('php://input');
$event_json = json_decode($body);

if(!isset($event_json->id)) {
    echo "WRONG EVENT";die;
    //payment_pro_do_404();
}

$event_id = $event_json->id;
$event = \Stripe\Event::retrieve($event_id);

$customer_email = '';
if(isset($event->data) && isset($event->data->object) && isset($event->data->object->customer)) {
    $customer_id = $event->data->object->customer;
    $customer_email = $customer_id;
    $customer = \Stripe\Customer::retrieve($customer_id);
    if(isset($customer->email)) {
        $customer_email = $customer->email;
    } else {
        $customer_email = @$customer->__get('cards')->__get('data')[0]->__get('name');
    }
} else {
    // CUSTOMER ID MISSING ?
    print_r("CUSTOMER ID");die;
    //payment_pro_do_404();
}

osc_run_hook('payment_pro_stripe_webhook', $event);

$type = $event->type;

switch($type) {
    case 'customer.subscription.created':
        $request = Params::getParamsAsArray();
        osc_run_hook('payment_pro_signup_subscription', $event, $request);
        break;
    case 'customer.subscription.deleted':
        $request = Params::getParamsAsArray();
        osc_run_hook('payment_pro_cancel_subscription', $event, $request);
        break;
    //case 'charge.succeeded':
    case 'invoice.payment_succeeded':

        $exists = ModelPaymentPro::newInstance()->getPaymentByCode($event->data->object->id, 'STRIPE', PAYMENT_PRO_COMPLETED);

        // DEBUG
        if (isset($exists['pk_i_id'])) {
            echo "PAYMENT ALREADY EXISTS " . $event->data->object->id . "/" . $exists['pk_i_id']; die;
            //payment_pro_do_404();
            //return PAYMENT_PRO_ALREADY_PAID;
        }

        // /!\  /!\  /!\ WARNING  /!\  /!\  /!\
        // WE SHOULD CHECK IF THE AMOUNTS ARE CORRECT
        // but we retrieve the event from the Stripe server, so it should be safe

        if(isset($event->data) && isset($event->data->object) && isset($event->data->object->subscription)) {
            $sub_id = $event->data->object->subscription;
            // SAVE TRANSACTION LOG
            $productsdb = ModelPaymentPro::newInstance()->subscriptionBySourceCode($sub_id);

            // Prepare items from the DB
            $items = array();
            $amount = 0;
            $amount_tax = 0;
            $amount_total = 0;
            foreach($productsdb as $p) {
                $amount += $p['i_quantity']*$p['i_amount']/1000000;
                $amount_tax += $p['i_amount_tax']/1000000;
                $amount_total += $p['i_amount_total']/1000000;
                $extra = json_decode($p['s_extra'], true);
                $extra['customer_id'] = $customer_id;
                $items[] = array(
                    'id' => $p['i_product_type'],
                    'description' => $p['s_concept'],
                    'amount' => $p['i_amount']/1000000,
                    'tax' => $p['i_tax']/100,
                    'amount_total' => $p['i_amount_total']/1000000,
                    'quantity' => $p['i_quantity'],
                    'currency' => $p['s_currency_code'],
                    'extra' => $extra,
                    'sub_id' => $p['s_code'],
                    'update_item_sub_id' => $p['pk_i_id']
                );
                ModelPaymentPro::newInstance()->updateSubscriptionItemData($p['pk_i_id'], array('s_extra' => json_encode($extra)));
            }

            // RECREATE EXTRA DATA
            $data = array(
                'items' => $items,
                'amount' => $event->data->object->total,
                'email' => $customer_email
            );

            $invoiceId = ModelPaymentPro::newInstance()->saveInvoice(
                $event->data->object->id,
                $amount,
                $amount_tax,
                $event->data->object->total/100,
                PAYMENT_PRO_COMPLETED,
                strtoupper($event->data->object->currency),
                $customer_email,
                null,
                'STRIPE',
                $items
            );

            ModelPaymentPro::newInstance()->updateSubscriptionStatusBySourceCode($sub_id, PAYMENT_PRO_COMPLETED);
            $status = PAYMENT_PRO_COMPLETED;
            if ($status == PAYMENT_PRO_COMPLETED) {
                foreach($items as $item) {
                    $itemid = explode("-", $item['id']);
                    $item['item_id'] = $itemid[count($itemid)-1];
                    if (substr($item['id'], 0, 4) == 'SUB-') {
                        $tmp = explode("/", str_replace("SUB-", "", $item['id']));
                        foreach($tmp as $t) {
                            $itemid = explode("-", $t);
                            $tmp_item = $item;
                            $tmp_item['id'] = $t;
                            $tmp_item['item_id'] = $itemid[count($itemid)-1];
                            osc_run_hook('payment_pro_item_paid', $tmp_item, $data, $invoiceId);
                        }
                    } else {
                        osc_run_hook('payment_pro_item_paid', $item, $data, $invoiceId);
                    }
                }
            }
        } else {
            echo "NO SUBSCRIPTION";die;
            //payment_pro_do_404();
            // SUB ID DOES NOT EXISTS
        }

        break;
    default:
        break;
}

echo "COMPLETED"; DIE;

