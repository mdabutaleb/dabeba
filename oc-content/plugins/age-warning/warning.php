<div>
    <p>
        <?php _e('By clicking the link below you confirm that you are 18 or older and understand personals may include adult content', 'age-warning'); ?>
    </p>
</div>
<div class="agree-disagree">
    <a href="<?php echo osc_render_file_url(osc_plugin_folder(__FILE__) . 'confirm.php&backto='.Session::newInstance()->_get('agew_backto'));?>">
    <?php _e('I agree', 'age-warning');?></a> <?php _e('or', 'age-warning');?> <a href="/"><?php _e('Take me out!', 'age-warning');?></a>
</div>

<div class="safer-sex">
    <a href="https://www.google.com/search?q=safer+sex&amp;oq=safer+sex"><?php _e('Safer sex', 'age-warning');?></a> <?php _e('reduces the risk of sexually transmitted diseases ( eg .: HIV). Report suspected exploitation of minors.', 'age-warning');?>
</div>
