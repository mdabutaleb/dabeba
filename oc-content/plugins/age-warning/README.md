# Custom Age Warning Message for +18 Categories

With this Plugin you may select on which categories you desire to display a custom warning page.

How to use this plugin?

1. Just install the plugin.
2. After you have installed the plugin, click on configure.
3. Select the categories where you desire to display the warning message.
4. That's it!

###### Enjoy! -By devGarage