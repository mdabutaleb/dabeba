<?php
/*
Plugin Name: Custom Age warning
Plugin URI: http://www.jonajo.com/
Description: Display a warning message about adult content
Version: 1.0.0
Author: Jonajo
Author URI: https://jonajo.com/
Short Name: agewarning
*/


    function agewarning_install() {
    }

    function agewarning_uninstall() {
    }

    function age_warning_config() {
        osc_plugin_configure_view(osc_plugin_path(__FILE__));
    }

    function agewarning_splash() {
        $category1 = Params::getParam('sCategory');
        $list        = array();
        $aCategories = Category::newInstance()->toRootTree($category1);

        if(count($aCategories) > 0) {
            $deep = 1;
            foreach ($aCategories as $single) {
                $list[] =  $single['pk_i_id'];
                $deep++;
            }
        }

        if(osc_is_search_page()) {

            if(osc_is_this_category('agewarning', osc_item_category_id()) || osc_is_this_category('agewarning', $list[0]) ) {

                error_log('agewarning_splash');

                if (Session::newInstance()->_get("agewarning_accepted") != '1' &&
                    Cookie::newInstance()->get_value('agewarning_accepted') != '1' &&
                    !osc_is_web_user_logged_in() &&
                    Params::getParam('action') != 'renderplugin') {

                    error_log(Params::getParam('file'));

                    if(Params::getParam('file') == osc_plugin_folder(__FILE__) . 'confirm.php') {
                        $url = Session::newInstance()->_get('agew_backto');
                        Session::newInstance()->_set("agewarning_accepted", "1");
                        Cookie::newInstance()->push("agewarning_accepted", "1");
                        Cookie::newInstance()->set();
                        Session::newInstance()->_drop('agew_backto');
                        header("Location: ".$url);
                        exit;
                    }

                }

                if(Params::getParam('file') != osc_plugin_folder(__FILE__) . 'warning.php'
                   && Params::getParam('file') != osc_plugin_folder(__FILE__) . 'confirm.php') {
                    Session::newInstance()->_set('agew_backto', $_SERVER['REQUEST_URI']);
                    header("Location: ".osc_render_file_url(osc_plugin_folder(__FILE__) . 'warning.php'));
                    exit;
                }

            }

        }
    }


    /**
     * ADD HOOKS
     */
    osc_register_plugin(osc_plugin_path(__FILE__), 'agewarning_install');
    osc_add_hook(__FILE__ . "_configure", 'age_warning_config');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'agewarning_uninstall');
    osc_add_hook('before_html', 'agewarning_splash');

?>
