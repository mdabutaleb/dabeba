<script type="text/html" id="calendar-day-info-template">
<div class="chinese-calendar-info {{css_class}}">
  <div class="heading">
    <div class="date">{{formated_date}}</div>
    <div class="lunar-date">农历: {{lunarmonth}}{{lunarday}}, {{ganzhi}}年</div>
    <div class="animal-image">
      <img src="/oc-content/plugins/chinese-calendar/img/{{animal_img}}" />
    </div>
    <div class="chong-sha">
      {{huangli.chong}} | {{huangli.sha}}
    </div>
  </div>

  <div>
    <h4>宜</h4>
    {{#each huangli.yi}}
      <span class="label">{{this}}</span>
    {{/each}}
  </div>
  <div>
    <h4>忌</h4>
    {{#each huangli.ji}}
      <span class="label">{{this}}</span>
    {{/each}}
  </div>
  {{#if is_sidebar_calendar}}
    <div class="view-more">
      <a href="/index.php?page=custom&route=full-calendar&date={{iso_date}}">查看详情</a>
    </div>
  {{/if}}
</div>
</script>

<div class="chinese-calendar" id="chinese-calendar"></div>

<script>
$(function () {
  var defaultDate = '<?php echo $_GET["date"] ?>';
  var isFullCalendar = !!window.isFullCalendar;
  var template = Handlebars.compile($('#calendar-day-info-template').html());
  var animalMaps = {
    '鼠': 'shu',
    '牛': 'niu',
    '虎': 'hu',
    '兔': 'tu',
    '龙': 'long',
    '蛇': 'she',
    '马': 'ma',
    '羊': 'yang',
    '猴': 'hou',
    '鸡': 'ji',
    '狗': 'gou',
    '猪': 'zhu'
  };
  var options = {
    inline: true,
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    changeYear: true,
    changeMonth: true,
    onSelect: function (date) {
      loadCalendarDayInfo(date);
    }
  };

  if (defaultDate) {
    options.defaultDate = defaultDate;
  }

  if (isFullCalendar) {
    options.dayNamesMin = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  }

  $('#chinese-calendar').datepicker(options);

  if (isFullCalendar) {
    loadCalendarDayInfo(defaultDate || moment().format('YYYY-MM-DD'));
  }

  function loadCalendarDayInfo(date) {
    $.get('/index.php?page=ajax&action=runhook&hook=chinese_calendar_get&date=' + date)
     .then(function (info) {
        if (!info) {
          return;
        }

        info.iso_date = date;
        info.formated_date = moment(date).format('ddd MMM D, YYYY');
        info.is_full_calendar = !!window.isFullCalendar;
        info.is_sidebar_calendar = !info.is_full_calendar;

        info.css_class = info.is_full_calendar ? '' : ' chinese-calendar-info-popup';

        info.animal_img = animalMaps[info.shengxiao] + '.jpg';

        var html = template(info);

        if (info.is_full_calendar) {
          var $container = $('#chinese-calendar .ui-datepicker');
          var $panel = $container.find('.calendar-info-panel');
          if ($panel.length === 0) {
            $panel = '<div class="calendar-info-panel">' + html + '</div>';
            $container.append($panel);
          } else {
            $panel.html(html);
          }
        } else {
          $.fancybox({
            content: html,
            margin: 0,
            padding: 0
          });
        }
      });
  }
});
</script>