<h1>Chinese Calendar</h1>
<?php
$language = osc_current_user_locale();
  if($language=="zh_CN"){
    $language_text = "黄历是中国人的传统天文历法，带有许多表示当天吉凶的一种日历";
  }
  else{
    $language_text = "The Chinese Almanac has been traditionally used by the Chinese people in order to get cosmic insight and advice on auspicious and inauspicious days of the calendar.";
  }
 ?>
<p>
  <?php echo $language_text ; ?>
</p>

<script>
var isFullCalendar = true;
</script>

<div class="full-calendar">
  <?php include_once 'calendar.php' ?>
</div>
