<?php
/*
Plugin Name: Chinese Calendar
Plugin URI: http://dadebo.com/
Description: This plugin Enables Chinese Calendar feature to site
Version: 1.0.0
Author: Team247
Author URI: http://dadebo.com/
*/

function chinese_calendar_install() {
}

function chinese_calendar_uninstall() {
}

function chinese_calendar_register_assets() {
  osc_register_script('handlebars', 'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min.js');
  osc_enqueue_script('handlebars');
  osc_enqueue_style('chinse-calendar-css', osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__).'css/calendar.css');
}

function chinese_calendar_include_calendar_chart() {
  include_once 'calendar-chart.php';
}

function chinese_calendar_include_calendar() {
  include_once 'calendar.php';
}

function chinese_calendar_text(){
  echo "<p>黄历</p>";
}

function ajax_chinese_calendar_get() {
  header('Content-type:application/json;charset=utf-8');

  $date = $_GET['date'];
  
  if (empty($date)) {
    die('Missing date');
  }

  $result = chinese_calendar_fetch_calendar_data($date);

  echo json_encode($result);
}

function chinese_calendar_fetch_calendar_data($date) {
  $cache_dir = osc_base_path() . 'cache/';
  $cache_path = $cache_dir . 'calendar_' . $date . '.json';
  if (file_exists($cache_path)) {
    return json_decode(file_get_contents($cache_path));
  }

  if (!file_exists($cache_dir)) {
    mkdir($cache_dir);
  }

  $result = chinese_calendar_fetch_calendar_data_no_cache($date);
  if (!empty($result)) {
    file_put_contents($cache_path, json_encode($result));
  }

  return $result;
}

function chinese_calendar_fetch_calendar_data_no_cache($date) {
    $host = "http://jisuwnl.market.alicloudapi.com";
    $path = "/calendar/query";
    $method = "GET";
    $appcode = "a54266d4eb6c47f1a7fa55aa54024e08";
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "date=" . $date;
    $bodys = "";
    $url = $host . $path . "?" . $querys;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);

    $result = json_decode($output, true);

    return empty($result['result']) ? null : $result['result'];
}

/* HOOKS */
osc_register_plugin(osc_plugin_path(__FILE__), 'chinese_calendar_install');
osc_add_hook(osc_plugin_path(__FILE__) . "_uninstall", 'chinese_calendar_uninstall');

osc_add_hook('header', 'chinese_calendar_register_assets');
osc_add_hook('add_to_left_sidebar_second', 'chinese_calendar_include_calendar_chart');
osc_add_hook('add_to_left_sidebar_second', 'chinese_calendar_text');
osc_add_hook('add_to_left_sidebar_second', 'chinese_calendar_include_calendar');

osc_add_hook('ajax_chinese_calendar_get', 'ajax_chinese_calendar_get');

osc_add_route('full-calendar', 'full-calendar', 'full-calendar', osc_plugin_folder(__FILE__).'full-calendar.php');