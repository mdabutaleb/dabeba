<?php
  // Create menu
  $title = __('Map Settings', 'rad_search');
  radius_menu($title);

  
  // GET & UPDATE PARAMETERS
  // General
  $map_zoom = radius_param_update( 'map_zoom', 'plugin_action' );
  $max_items = radius_param_update( 'max_items', 'plugin_action' );
  $map_cluster = radius_param_update( 'map_cluster', 'plugin_action', 'check' );
  $map_roads = radius_param_update( 'map_roads', 'plugin_action', 'check' );
  $def_lat = radius_param_update( 'def_lat', 'plugin_action' );
  $def_long = radius_param_update( 'def_long', 'plugin_action' );
  
  // Home page
  $map_type_home = radius_param_update( 'map_type_home', 'plugin_action' );
  $map_width_home = radius_param_update( 'map_width_home', 'plugin_action' );
  $map_height_home = radius_param_update( 'map_height_home', 'plugin_action' );
  $only_premium_home = radius_param_update( 'only_premium_home', 'plugin_action', 'check' );
  
  // Search page
  $map_type_search = radius_param_update( 'map_type_search', 'plugin_action' );
  $map_width_search = radius_param_update( 'map_width_search', 'plugin_action' );
  $map_height_search = radius_param_update( 'map_height_search', 'plugin_action' );
  $only_premium_search = radius_param_update( 'only_premium_search', 'plugin_action', 'check' );
  $map_hook = radius_param_update( 'map_hook', 'plugin_action', 'check' );
  $same_cat = radius_param_update( 'same_cat', 'plugin_action', 'check' );
  $map_loc = radius_param_update( 'map_loc', 'plugin_action' );
  
  // Listing page
  $map_type_item = radius_param_update( 'map_type_item', 'plugin_action' );
  $map_width_item = radius_param_update( 'map_width_item', 'plugin_action' );
  $map_height_item = radius_param_update( 'map_height_item', 'plugin_action' );
  $only_premium_item = radius_param_update( 'only_premium_item', 'plugin_action', 'check' );
  
  // Publish page
  $map_type_publish = radius_param_update( 'map_type_publish', 'plugin_action' );
  $map_width_publish = radius_param_update( 'map_width_publish', 'plugin_action' );
  $map_height_publish = radius_param_update( 'map_height_publish', 'plugin_action' );
  $map_zoom_publish = radius_param_update( 'map_zoom_publish', 'plugin_action' );
  $map_lat_publish = radius_param_update( 'map_lat_publish', 'plugin_action' );
  $map_lng_publish = radius_param_update( 'map_lng_publish', 'plugin_action' );


  if(Params::getParam('plugin_action') == 'done') {
    message_ok( __('Map settings were successfully saved', 'rad_search') );
  }
?>

<div class="mb-body">

  <!-- PROMOTE FUNCTIONALITY -->
  <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>map_settings.php" />
    <input type="hidden" name="plugin_action" value="done" />
    
    <!-- GLOBAL MAP SETTINGS -->
    <div class="mb-box">
      <div class="mb-head"><i class="fa fa-cog"></i> <?php _e('Global map settings', 'rad_search'); ?></div>

      <div class="mb-inside">
        <div class="mb-row">
          <label for="map_cluster" class="h1"><span><?php _e('Enable Cluster on Map', 'rad_search'); ?></span></label> 
          <input name="map_cluster" id="map_cluster" class="element-slide" type="checkbox" <?php echo ($map_cluster == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Listings will be grouped on map by distance, this will make map more clean.', 'rad_search'); ?></div>
        </div>

       <div class="mb-row">
          <label for="map_roads" class="h2"><span><?php _e('Add Traffic Layer to Map', 'rad_search'); ?></span></label> 
          <input name="map_roads" id="map_roads" class="element-slide" type="checkbox" <?php echo ($map_roads == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Advanced description of roads for cars will be visible on map.', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="map_zoom" class="h3"><span><?php _e('Default Map Zoom', 'rad_search'); ?></span></label> 
          <select name="map_zoom" id="map_zoom">
            <option value="0" <?php if($map_zoom == '0') { ?>selected="selected"<?php } ?>><?php _e('Auto-adjust zoom', 'rad_search'); ?></option>
            <option value="2" <?php if($map_zoom == '2') { ?>selected="selected"<?php } ?>><?php _e('2', 'rad_search'); ?></option>
            <option value="3" <?php if($map_zoom == '3') { ?>selected="selected"<?php } ?>><?php _e('3', 'rad_search'); ?></option>
            <option value="4" <?php if($map_zoom == '4') { ?>selected="selected"<?php } ?>><?php _e('4', 'rad_search'); ?></option>
            <option value="5" <?php if($map_zoom == '5') { ?>selected="selected"<?php } ?>><?php _e('5', 'rad_search'); ?></option>
            <option value="6" <?php if($map_zoom == '6') { ?>selected="selected"<?php } ?>><?php _e('6', 'rad_search'); ?></option>
            <option value="7" <?php if($map_zoom == '7') { ?>selected="selected"<?php } ?>><?php _e('7', 'rad_search'); ?></option>
            <option value="8" <?php if($map_zoom == '8') { ?>selected="selected"<?php } ?>><?php _e('8', 'rad_search'); ?></option>
            <option value="9" <?php if($map_zoom == '9') { ?>selected="selected"<?php } ?>><?php _e('9', 'rad_search'); ?></option>
            <option value="10" <?php if($map_zoom == '10') { ?>selected="selected"<?php } ?>><?php _e('10', 'rad_search'); ?></option>
            <option value="11" <?php if($map_zoom == '11') { ?>selected="selected"<?php } ?>><?php _e('11', 'rad_search'); ?></option>
            <option value="12" <?php if($map_zoom == '12') { ?>selected="selected"<?php } ?>><?php _e('12', 'rad_search'); ?></option>
            <option value="13" <?php if($map_zoom == '13') { ?>selected="selected"<?php } ?>><?php _e('13', 'rad_search'); ?></option>
            <option value="14" <?php if($map_zoom == '14') { ?>selected="selected"<?php } ?>><?php _e('14', 'rad_search'); ?></option>
            <option value="15" <?php if($map_zoom == '15') { ?>selected="selected"<?php } ?>><?php _e('15', 'rad_search'); ?></option>
            <option value="16" <?php if($map_zoom == '16') { ?>selected="selected"<?php } ?>><?php _e('16', 'rad_search'); ?></option>
          </select>
          
          <div class="mb-explain"><?php _e('Higher value means more detail zoom.', 'rad_search'); ?></div>
        </div>

        <div class="mb-row">
          <label for="def_lat" class="h4"><span><?php _e('Default Latitude', 'rad_search'); ?></span></label> 
          <input size="12" name="def_lat" id="def_lat" class="mb-short" type="text" value="<?php echo $def_lat; ?>" />
        </div>

        <div class="mb-row">
          <label for="def_long" class="h4"><span><?php _e('Default Longitude', 'rad_search'); ?></span></label> 
          <input size="12" name="def_long" id="def_long" class="mb-short" type="text" value="<?php echo $def_long; ?>" />
        </div>        

        <div class="mb-row">
          <label for="max_items" class="h6"><span><?php _e('Maximum Items Shown on Map', 'rad_search'); ?></span></label> 
          <input size="10" name="max_items" id="max_items" class="mb-short" type="text" value="<?php echo $max_items; ?>" />
          <div class="mb-input-desc"><?php _e('items', 'rad_search'); ?></div>
        </div>    
        
      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </div>
    
    
    
    <!-- HOMEPAGE MAP SETTINGS -->
    <div class="mb-box">
      <div class="mb-head"><i class="fa fa-home"></i> <?php _e('Home page map settings', 'rad_search'); ?></div>

      <div class="mb-inside">

        <div class="mb-row">
          <label for="only_premium_home" class="h7"><span><?php _e('Show Only Premiums', 'rad_search'); ?></span></label> 
          <input name="only_premium_home" id="only_premium_home" class="element-slide" type="checkbox" <?php echo ($only_premium_home == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Only premium listings will be shown on home page map.', 'rad_search'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_type_home" class="h8"><span><?php _e('Map Type', 'rad_search'); ?></span></label> 
          <select name="map_type_home" id="map_type_home">
            <option value="ROADMAP" <?php if($map_type_home == 'ROADMAP') { ?>selected="selected"<?php } ?>><?php _e('Roadmap', 'rad_search'); ?></option>
            <option value="SATELLITE" <?php if($map_type_home == 'SATELLITE') { ?>selected="selected"<?php } ?>><?php _e('Satellite', 'rad_search'); ?></option>
            <option value="HYBRID" <?php if($map_type_home == 'HYBRID') { ?>selected="selected"<?php } ?>><?php _e('Hybrid', 'rad_search'); ?></option>
            <option value="TERRAIN" <?php if($map_type_home == 'TERRAIN') { ?>selected="selected"<?php } ?>><?php _e('Terrain', 'rad_search'); ?></option>
          </select>
        </div>

        <div class="mb-row">
          <label for="map_width_home" class="h10"><span><?php _e('Map Width', 'rad_search'); ?></span></label> 
          <input size="10" name="map_width_home" id="map_width_home" class="mb-short" type="text" value="<?php echo $map_width_home; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'rad_search'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_height_home" class="h10"><span><?php _e('Map Height', 'rad_search'); ?></span></label> 
          <input size="10" name="map_height_home" id="map_height_home" class="mb-short" type="text" value="<?php echo $map_height_home; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </div>
    
    
    
    <!-- SEARCH MAP SETTINGS -->
    <div class="mb-box">
      <div class="mb-head"><i class="fa fa-search"></i> <?php _e('Search/Category map settings', 'rad_search'); ?></div>

      <div class="mb-inside">
         
        <div class="mb-row">
          <label for="only_premium_search" class="h7"><span><?php _e('Show Only Premiums', 'rad_search'); ?></span></label> 
          <input name="only_premium_search" id="only_premium_search" class="element-slide" type="checkbox" <?php echo ($only_premium_search == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Only premium listings will be shown on search page map.', 'rad_search'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_type_search" class="h8"><span><?php _e('Map Type', 'rad_search'); ?></span></label> 
          <select name="map_type_search" id="map_type_search">
            <option value="ROADMAP" <?php if($map_type_search == 'ROADMAP') { ?>selected="selected"<?php } ?>><?php _e('Roadmap', 'rad_search'); ?></option>
            <option value="SATELLITE" <?php if($map_type_search == 'SATELLITE') { ?>selected="selected"<?php } ?>><?php _e('Satellite', 'rad_search'); ?></option>
            <option value="HYBRID" <?php if($map_type_search == 'HYBRID') { ?>selected="selected"<?php } ?>><?php _e('Hybrid', 'rad_search'); ?></option>
            <option value="TERRAIN" <?php if($map_type_search == 'TERRAIN') { ?>selected="selected"<?php } ?>><?php _e('Terrain', 'rad_search'); ?></option>
          </select>
        </div>

        <div class="mb-row">
          <label for="map_width_search" class="h10"><span><?php _e('Map Width', 'rad_search'); ?></span></label> 
          <input size="10" name="map_width_search" id="map_width_search" class="mb-short" type="text" value="<?php echo $map_width_search; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'rad_search'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_height_search" class="h10"><span><?php _e('Map Height', 'rad_search'); ?></span></label> 
          <input size="10" name="map_height_search" id="map_height_search" class="mb-short" type="text" value="<?php echo $map_height_search; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </div>
    
    
    
    <!-- LISTING MAP SETTINGS -->
    <div class="mb-box">
      <div class="mb-head"><i class="fa fa-flag"></i> <?php _e('Item map settings', 'rad_search'); ?></div>

      <div class="mb-inside">
         
      
        <div class="mb-row">
          <label for="only_premium_item" class="h7"><span><?php _e('Show Only Premiums', 'radius_item'); ?></span></label> 
          <input name="only_premium_item" id="only_premium_item" class="element-slide" type="checkbox" <?php echo ($only_premium_item == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Only premium listings will be shown on listing page map.', 'radius_item'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="same_cat" class="h16"><span><?php _e('Show Only Listings from Same Category', 'radius_item'); ?></span></label> 
          <input name="same_cat" id="same_cat" class="element-slide" type="checkbox" <?php echo ($same_cat == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Only listings that belongs to same category as browsed listing will be shown on map - related listings.', 'radius_item'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_hook" class="h17"><span><?php _e('Auto-Hook Map to Listing Page', 'radius_item'); ?></span></label> 
          <input name="map_hook" id="map_hook" class="element-slide" type="checkbox" <?php echo ($map_hook == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Map will be shown on listing page automatically without need to modify theme files.', 'radius_item'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="map_loc" class="h18"><span><?php _e('Items by Location', 'radius_item'); ?></span></label> 
          <select name="map_loc" id="map_loc">
            <option value="0" <?php if($map_loc == 0) { ?>selected="selected"<?php } ?>><?php _e('Show all items', 'rad_search'); ?></option>
            <option value="1" <?php if($map_loc == 1) { ?>selected="selected"<?php } ?>><?php _e('Items in same country', 'rad_search'); ?></option>
            <option value="2" <?php if($map_loc == 2) { ?>selected="selected"<?php } ?>><?php _e('Items in same region', 'rad_search'); ?></option>
            <option value="3" <?php if($map_loc == 3) { ?>selected="selected"<?php } ?>><?php _e('Items in same city', 'rad_search'); ?></option>
          </select>
          
          <div class="mb-explain"><?php _e('Select if you want to include to map items from same Country/Region/City as browsed listing.', 'radius_item'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="map_type_item" class="h8"><span><?php _e('Map Type', 'radius_item'); ?></span></label> 
          <select name="map_type_item" id="map_type_item">
            <option value="ROADMAP" <?php if($map_type_item == 'ROADMAP') { ?>selected="selected"<?php } ?>><?php _e('Roadmap', 'radius_item'); ?></option>
            <option value="SATELLITE" <?php if($map_type_item == 'SATELLITE') { ?>selected="selected"<?php } ?>><?php _e('Satellite', 'radius_item'); ?></option>
            <option value="HYBRID" <?php if($map_type_item == 'HYBRID') { ?>selected="selected"<?php } ?>><?php _e('Hybrid', 'radius_item'); ?></option>
            <option value="TERRAIN" <?php if($map_type_item == 'TERRAIN') { ?>selected="selected"<?php } ?>><?php _e('Terrain', 'radius_item'); ?></option>
          </select>
        </div>

        <div class="mb-row">
          <label for="map_width_item" class="h10"><span><?php _e('Map Width', 'radius_item'); ?></span></label> 
          <input size="10" name="map_width_item" id="map_width_item" class="mb-short" type="text" value="<?php echo $map_width_item; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'radius_item'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_height_item" class="h10"><span><?php _e('Map Height', 'radius_item'); ?></span></label> 
          <input size="10" name="map_height_item" id="map_height_item" class="mb-short" type="text" value="<?php echo $map_height_item; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'radius_item'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </div>
    
    
    
    <!-- PUBLISH MAP SETTINGS -->
    <div class="mb-box">
      <div class="mb-head"><i class="fa fa-plus-circle"></i> <?php _e('Publish map settings', 'rad_search'); ?></div>

      <div class="mb-inside">
         
        <div class="mb-row">
          <label for="map_type_publish" class="h8"><span><?php _e('Map Type', 'radius_publish'); ?></span></label> 
          <select name="map_type_publish" id="map_type_publish">
            <option value="ROADMAP" <?php if($map_type_publish == 'ROADMAP') { ?>selected="selected"<?php } ?>><?php _e('Roadmap', 'radius_publish'); ?></option>
            <option value="SATELLITE" <?php if($map_type_publish == 'SATELLITE') { ?>selected="selected"<?php } ?>><?php _e('Satellite', 'radius_publish'); ?></option>
            <option value="HYBRID" <?php if($map_type_publish == 'HYBRID') { ?>selected="selected"<?php } ?>><?php _e('Hybrid', 'radius_publish'); ?></option>
            <option value="TERRAIN" <?php if($map_type_publish == 'TERRAIN') { ?>selected="selected"<?php } ?>><?php _e('Terrain', 'radius_publish'); ?></option>
          </select>
        </div>

        <div class="mb-row">
          <label for="map_width_publish" class="h10"><span><?php _e('Map Width', 'radius_publish'); ?></span></label> 
          <input size="10" name="map_width_publish" id="map_width_publish" class="mb-short" type="text" value="<?php echo $map_width_publish; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'radius_publish'); ?></div>
        </div>

        <div class="mb-row">
          <label for="map_height_publish" class="h10"><span><?php _e('Map Height', 'radius_publish'); ?></span></label> 
          <input size="10" name="map_height_publish" id="map_height_publish" class="mb-short" type="text" value="<?php echo $map_height_publish; ?>" />
          <div class="mb-input-desc"><?php _e('Enter value with unit only! i.e. 100% or 300px', 'radius_publish'); ?></div>
        </div>
       
        <div class="mb-row">
          <label for="map_zoom_publish" class="h25"><span><?php _e('Default Map Zoom', 'rad_search'); ?></span></label> 
          <select name="map_zoom_publish" id="map_zoom_publish">
            <option value="2" <?php if($map_zoom_publish == '2') { ?>selected="selected"<?php } ?>><?php _e('2', 'rad_search'); ?></option>
            <option value="3" <?php if($map_zoom_publish == '3') { ?>selected="selected"<?php } ?>><?php _e('3', 'rad_search'); ?></option>
            <option value="4" <?php if($map_zoom_publish == '4') { ?>selected="selected"<?php } ?>><?php _e('4', 'rad_search'); ?></option>
            <option value="5" <?php if($map_zoom_publish == '5') { ?>selected="selected"<?php } ?>><?php _e('5', 'rad_search'); ?></option>
            <option value="6" <?php if($map_zoom_publish == '6') { ?>selected="selected"<?php } ?>><?php _e('6', 'rad_search'); ?></option>
            <option value="7" <?php if($map_zoom_publish == '7') { ?>selected="selected"<?php } ?>><?php _e('7', 'rad_search'); ?></option>
            <option value="8" <?php if($map_zoom_publish == '8') { ?>selected="selected"<?php } ?>><?php _e('8', 'rad_search'); ?></option>
            <option value="9" <?php if($map_zoom_publish == '9') { ?>selected="selected"<?php } ?>><?php _e('9', 'rad_search'); ?></option>
            <option value="10" <?php if($map_zoom_publish == '10') { ?>selected="selected"<?php } ?>><?php _e('10', 'rad_search'); ?></option>
            <option value="11" <?php if($map_zoom_publish == '11') { ?>selected="selected"<?php } ?>><?php _e('11', 'rad_search'); ?></option>
            <option value="12" <?php if($map_zoom_publish == '12') { ?>selected="selected"<?php } ?>><?php _e('12', 'rad_search'); ?></option>
            <option value="13" <?php if($map_zoom_publish == '13') { ?>selected="selected"<?php } ?>><?php _e('13', 'rad_search'); ?></option>
            <option value="14" <?php if($map_zoom_publish == '14') { ?>selected="selected"<?php } ?>><?php _e('14', 'rad_search'); ?></option>
            <option value="15" <?php if($map_zoom_publish == '15') { ?>selected="selected"<?php } ?>><?php _e('15', 'rad_search'); ?></option>
            <option value="16" <?php if($map_zoom_publish == '16') { ?>selected="selected"<?php } ?>><?php _e('16', 'rad_search'); ?></option>
          </select>
          
          <div class="mb-explain"><?php _e('Higher value means more detail zoom.', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="map_lat_publish" class="h4"><span><?php _e('Default Latitude', 'rad_search'); ?></span></label> 
          <input size="12" name="map_lat_publish" id="map_lat_publish" class="mb-short" type="text" value="<?php echo $map_lat_publish; ?>" />
        </div>

        <div class="mb-row">
          <label for="map_lng_publish" class="h4"><span><?php _e('Default Longitude', 'rad_search'); ?></span></label> 
          <input size="12" name="map_lng_publish" id="map_lng_publish" class="mb-short" type="text" value="<?php echo $map_lng_publish; ?>" />
        </div> 
        
      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </div>
  </form>

  

  <!-- PLUGIN INTEGRATION -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-wrench"></i> <?php _e('Map Integration', 'rad_search'); ?></div>

    <div class="mb-inside">

      <div class="mb-row">
        <div class="mb-line"><?php _e('To show map with listings anywhere you want (home/search/item page), place following code to your theme:', 'rad_search'); ?></div>
        <div class="mb-row">&nbsp;</div>
        <span class="mb-code">&lt;?php if(function_exists('radius_map_items')) { radius_map_items(); } ?&gt;</span>
        
        <div class="mb-row">&nbsp;</div>
        <div class="mb-row">&nbsp;</div>

        <div class="mb-line"><?php _e('To add map on publish page, where user can choose location of item from map:', 'rad_search'); ?></div>
        <div class="mb-row">&nbsp;</div>
        <span class="mb-code">&lt;?php if(function_exists('radius_map_publish')) { echo radius_map_publish(); } ?&gt;</span>
      </div>
    </div>
  </div>



  <!-- HELP TOPICS -->
  <div class="mb-box" id="mb-help">
    <div class="mb-head"><i class="fa fa-question-circle"></i> <?php _e('Help', 'rad_search'); ?></div>

    <div class="mb-inside">
      <div class="mb-row mb-help"><span class="sup">(1)</span> <div class="h1"><?php _e('When enabled, listings on map will be grouped by distance. This remove mess when there is too many listings in same or very same location and are replaced by group mark. When user zoom into this area, listings are explored.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(2)</span> <div class="h2"><?php _e('Define if you want to add traffic layer to map. This will add advanced level/view of traffic routes for cars.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(3)</span> <div class="h3"><?php _e('When auto-adjust zoom is enabled, map is centered and zoomed in way that all listings are shown to user (this setting is recommended). When you choose specific value, zoom is static and centered to defined coordinates. Note that bigger zoom number means closer zoom on map.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(4)</span> <div class="h4"><?php _e('If auto-zoom feature is set to manual value (not auto-adjust), you can define where will be map centered by default - latitude and longitude of this place. These fields are required as map would be centered to 0, 0 coordinates when empty. When auto-zoom is set to auto-adjust, default latitude & longitude are not taken into account', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(6)</span> <div class="h6"><?php _e('Limit number of listings that can be shown on map. Note that this is maximum count of listings and usually less listings is shown. Note that also when there are 2 listings with same coordinates, just 1 is shown, but in maximum count of listings it is counted as 2 listings. (Example: You have max. count set to 100, there is 100 listings with same coordinates, finally just 1 listing is shown on map).', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(7)</span> <div class="h7"><?php _e('Restrict listings to show only premiums in map on home/search/item page.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(8)</span> <div class="h8"><?php _e('Set map type that should be used. There are 4 options, you can find details on following URL:', 'rad_search'); ?> <a href="https://developers.google.com/maps/documentation/javascript/maptypes#MapTypes" target="_blank"><?php _e('Google API map types', 'rad_search'); ?></a></div></div>
      <div class="mb-row mb-help"><span class="sup">(10)</span> <div class="h10"><?php _e('Set map width & height. Note that it is required to enter also unit (px, %, ...). Example: 100% or 50% or 500px or 300px ...', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(16)</span> <div class="h16"><?php _e('When enabled, on map in item page, only listings that are from same category as browsed listing are shown. I.e. if browsed item is from Cars category, only listings from same category will be shown on map.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(17)</span> <div class="h17"><?php _e('Use to place map into item page without modifications of theme code. Hook with name item_detail is used. Map will be shown on same place as Google Map plugin.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(18)</span> <div class="h18"><?php _e('Restrict listings shown on map in item page by it\'s location. Only listings with same Country/Region/City as browsed item has will be shown. I.e. if browsed listing if located in Alabama and "Listings in same region" is choosen, only listings from Alabama will be shown on map.', 'rad_search'); ?></div></div>
    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
