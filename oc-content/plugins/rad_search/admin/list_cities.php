<?php
  // Create menu
  $title = __('Listings Coordinates', 'rad_search');
  radius_menu($title);
  
  
  $per_page = osc_get_preference('per_page', 'plugin-rad_search') <> '' ? osc_get_preference('per_page', 'plugin-rad_search') : 100;

  $cities_total = ModelRadius::newInstance()->countCities();
  $num_all = $cities_total['total_count'];
  $num_list = $per_page;

  //Start position for pagination
  if (Params::getParam('start')) {
    $start = intval(Params::getParam('start'));
    if ($start > 0) {
      $start--;
    }
  } else {
    $start = 0;
  }

  $f_country = Params::getParam('country-filter');
  $f_region = Params::getParam('region-filter');
  $f_city = Params::getParam('city-filter');
  $f_only_null = Params::getParam('only_null') == 'on' ? 1 : 0;
?>



<div class="mb-body">
  
  <!-- CITY COORDINATES -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-building-o"></i> <?php _e('List of coordinates for all cities', 'rad_search'); ?></div>

    <div class="mb-inside">
      <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>list_cities.php" />
        <input type="hidden" name="plugin_action" value="city-refresh" />
        
        <div class="mb-row mb-entry-filter">

          <div class="mb-col-2_1_2">
            <label for="city-filter"><span><?php _e('City', 'rad_search'); ?></span></label>
            <input size="15" type="text" name="city-filter" value="<?php echo Params::getParam('city-filter'); ?>" />
          </div>

          <div class="mb-col-2_1_2">
            <label for="region-filter"><span><?php _e('Region', 'rad_search'); ?></span></label>
            <input size="15" type="text" name="region-filter" value="<?php echo Params::getParam('region-filter'); ?>" />
          </div>

          <div class="mb-col-3_1_2">
            <label for="country-filter"><span><?php _e('Country', 'rad_search'); ?></span></label>
            
            <?php $country_stats = ModelRadius::newInstance()->countCitiesByCountry(); ?>
            <select name="country-filter">
              <option value="" <?php echo Params::getParam('country-filter') == '' ? 'selected="selected"' : ''; ?>><?php _e('All countries', 'rad_search'); ?></option>

              <?php foreach($country_stats as $c) { ?>
                <option value="<?php echo strtolower($c['pk_c_code']); ?>" <?php echo Params::getParam('country-filter') == strtolower($c['pk_c_code']) ? 'selected="selected"' : ''; ?>><?php echo $c['s_name']; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="mb-col-2_1_2">
            <input name="only_null" id="only_null" class="element-slide" type="checkbox" <?php echo (Params::getParam('only_null') == 'on' ? 'checked' : ''); ?> />
            <label for="only_null" class="only_null"><?php _e('Cities without coords', 'rad_search'); ?></label>
          </div>

          <div class="mb-col-1">
            <button type="submit" class="mb-button-white"><i class="fa fa-filter"></i> <?php _e('Filter', 'rad_search');?></button>
          </div>
        </div>
      </form>
      
      
      <div class="mb-table radius_list">
        <div class="mb-table-head">
          <div class="mb-col-2 mb-align-left"><?php _e('Country', 'rad_search');?></div>
          <div class="mb-col-2 mb-align-left"><?php _e('Region', 'rad_search');?></div>
          <div class="mb-col-2 mb-align-left"><?php _e('City', 'rad_search');?></div>
          <div class="mb-col-1_2">&nbsp;</div>
          <div class="mb-col-1_1_2"><?php _e('Latitude', 'rad_search');?></div>
          <div class="mb-col-1_1_2"><?php _e('Longitude', 'rad_search');?></div>
          <div class="mb-col-2_1_2 mb-align-left"><?php _e('Status', 'rad_search');?></div>
        </div>

        <?php
          if($f_country <> '' or $f_region <> '' or $f_city <> '' or $f_only_premium <> '' or $f_only_null) {
            $cities_list = ModelRadius::newInstance()->getCitiesSearch($num_list, $f_city, $f_region, $f_country, $f_only_null);
          } else {
            $cities_list = ModelRadius::newInstance()->getCities($start, $num_list);
          }
        ?>
        
        <?php
          foreach($cities_list as $city ) {
            $city_full = City::newInstance()->findByPrimaryKey($city['city_id']);
            $region_full = Region::newInstance()->findByPrimaryKey($city_full['fk_i_region_id']);
            $country_full = Country::newInstance()->findByPrimaryKey($city_full['fk_c_country_code']);

            $empty = '<div class="mb-i mb-gray">' . __('- empty -', 'rad_search') . '</div>';

            $map_img = '';
            if(($city['latitude'] == '' or $city['latitude'] == 0) and ($city['longitude'] == '' or $city['longitude'] == 0)) {
              if($city['status'] == '') {
                $city['status'] = __('Coordinate were not uploaded yet', 'rad_search');
              }

              $status = '<img src="' . osc_base_url() . 'oc-content/plugins/rad_search/images/deny.png" title="' . $city['status'] . '"/><span>[' . $city['status'] . ']</span>';
              $map = 0;
            } else {
              $status = '<img src="' . osc_base_url() . 'oc-content/plugins/rad_search/images/accept.png" title="' . $city['status'] . '"/>';
              $map = 1;
              $map_img = 'rel="http://maps.googleapis.com/maps/api/staticmap?zoom=12&size=400x300&center=' . $city['latitude'] . ',' . $city['longitude'] . '&markers=color:red%7C' . $city['latitude'] . ',' . $city['longitude'] . '"';
            }

            if($city['latitude'] == '' or $city['latitude'] == 0) { 
              $city['latitude'] = $empty;
            }

            if($city['longitude'] == '' or $city['longitude'] == 0) { 
              $city['longitude'] = $empty;
            }

            echo '<div class="mb-table-row">';
            echo '<div class="mb-col-2 mb-align-left"><a href="' . osc_base_url() . 'index.php?page=search&city=' . $city_full['pk_i_id'] . '" target="_blank">' . $city_full['s_name'] . ' (id: ' . $city_full['pk_i_id'] . ')</a></div>';
            echo '<div class="mb-col-2 mb-align-left">' . $region_full['s_name'] . ' (id: ' . $region_full['pk_i_id'] . ')</div>';
            echo '<div class="mb-col-2 mb-align-left">' . $country_full['s_name'] . ' (code: ' . $country_full['pk_c_code'] . ')</div>';
            echo '<div class="mb-col-1_2 ' . ($map == 1 ? 'map' : '') . '" ' . ($map == 1 ? $map_img : '') . '><i class="fa fa-search-plus"></i></div>';
            echo '<div class="mb-col-1_1_2">' . $city['latitude'] . '</div>';
            echo '<div class="mb-col-1_1_2">' . $city['longitude'] . '</div>';
            echo '<div class="mb-col-2_1_2 mb-entry-status mb-align-left"  title="' . $city['status'] . '">' . $status . '</div>';
            echo '</div>'; 
          }

          if($num_all == 0) { 
            echo '<div class="mb-notes">';
            echo '<div class="zero-city">' . __('You need to finish', 'rad_search') . ' <a href="" target="_blank">' . __('Manual coordinates upload steps', 'rad_search') . '</a> ' . __('first. Currently there are no cities available in radius search plugin.', 'rad_search') . '</div>';
            echo '</div>';
          }

          if($num_all > 0 and count($cities_list) == 0) { 
            echo '<div class="mb-notes">';
            echo '<div class="zero-city">' . __('No cities match search criteria', 'rad_search') . '</div>';
            echo '</div>';
          }
        ?>
      </div>

      
      <?php if($f_country <> '' or $f_region <> '' or $f_city <> '' or $f_only_premium <> '') { ?>
        <?php if(count($cities_list) >= $num_list) { ?>
          <div class="mb-notes">
            <div class="zero-city"><?php echo __('There are more results, only first', 'rad_search') . ' ' . $num_list . ' ' . __('results are shown.', 'rad_search'); ?></div>
          </div>
        <?php } ?>
      <?php } else { ?>
        <?php if($num_all > $num_list) { ?>
          <div id="mb-pagination">
            <div class="mb-pagination-wrap">
              <div><?php _e('Page', 'rad_search'); ?>:</div> <?php echo add_pagination($num_all, $num_list, 'rad_search/admin/list_cities.php'); ?>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
      
      <div id="radius-admin-map"><img /></div>

    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
