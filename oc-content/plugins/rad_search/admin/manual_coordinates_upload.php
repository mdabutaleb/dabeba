<?php
  // Create menu
  $title = __('Manual Coordinates Upload', 'rad_search');
  radius_menu($title);

  
  

  // STEP 1 - COORDINATES UPLOAD FROM FILE TO DATABASE
  if(Params::getParam('plugin_action')=='upload') {
    // UPLOAD FILE WITH COORDINATES INTO SERVER
    if ($_FILES['file']['error'] > 0) {
      if($_FILES['file']['error'] == 4) { 
        message_error('Error: No file selected'); 
      } else {
        message_error('Error: ' . $_FILES['file']['error']);
      }
    } else {
      move_uploaded_file($_FILES['file']['tmp_name'], osc_plugins_path() . 'rad_search/upload/cords.txt');

      message_ok(
       'Upload: ' . $_FILES['file']['name'] . ' > renamed to cords.txt<br>' .
       'Type: ' . $_FILES['file']['type'] . '<br>' .
       'Size: ' . round($_FILES['file']['size']/1024, 2) . 'kB<br>' .
       'Stored in: ' . osc_base_url() . 'oc-content/plugins/rad_search/upload/cords.txt<br />' .
       'Path: ' . osc_plugins_path() . 'rad_search/upload/cords.txt'
      );
    }

    ModelRadius::newInstance()->deleteCordFromHelp();

    $file = fopen(osc_plugins_path() . 'rad_search/upload/cords.txt', 'r') or exit("Unable to open file!");

    // ADD EACH ROW FROM FILE TO DATABASE
    while(!feof($file)) {
      $row = strtolower(fgets($file));
      $row_e = explode("\t", $row);
      $country_code = $row_e['0'];
      $city_name = $row_e['2'];
      $region_name = $row_e['3'];
      $lat = $row_e['9'];
      $long = $row_e['10'];

      ModelRadius::newInstance()->insertCordRaw( $country_code, $region_name, $city_name, $long, $lat );
    }
    fclose($file);

    // REMOVE DUPLICATES AND PLACE COORDINATES TO FINAL HELP TABLE
    ModelRadius::newInstance()->validateData();

    // ATTACH COORDINATES TO CITIES FROM DATABASE
    ModelRadius::newInstance()->copyCitiesToRadius();
    message_ok(__('STEP 1 - COMPLETE: Data were successfully uploaded to database', 'rad_search'));
  }



  // STEP 2 - CREATE TABLE WITH COORDINATES ATTACHED TO EACH CITY
  if(Params::getParam('plugin_action')=='finish') {
    $start_time = microtime(true);
    $max_time = ini_get('max_execution_time');

    $i = 1;
    $stop_position = Params::getParam('stop_city_id');
    $count_complete = 0;
    $count_incomplete = 0;
    $city_load_limit = osc_get_preference('city_load_limit','plugin-rad_search') <> '' ? osc_get_preference('city_load_limit','plugin-rad_search') : 10000;
    $fill_country = Params::getParam('fill-country');
    $finished_step2 = false;
    $cities_list = ModelRadius::newInstance()->getCitiesRegionsListNull($stop_position, $city_load_limit + 1, $fill_country);

    foreach($cities_list as $city) {
      if(microtime(true) - $start_time > $max_time - 10) {
        $cities_left = ModelRadius::newInstance()->countCitiesLeft($fill_country);
        message_ok('Script has almost reached maximum execution time set on your server. To avoid error message script was stopped. <br />Execution time of your server is: ' . $max_time . ' seconds. You can increase it in admin panel.<br />Cities to that were successfully attached coordinates in this step: ' . $count_complete . ' cities<br />Cities to that were not found coordinates in this step: ' . $count_incomplete . ' cities<br />Cities left to fill: ' . $cities_left['total_count'] . ' cities<br /><br />Please click on <strong>Continue filling table with cities coordinates</strong> again in STEP 2 and script will continue filling coordinates from place it was stopped.');
        $stop_city_id = $city['city_id'];
        break;
      } else if ($i == count($cities_list)) {
        $cities_left = ModelRadius::newInstance()->countCitiesLeft($fill_country);
        message_ok($i-1 . ' cities were processed, repeat this step again.<br />Total city count to that were successfully attached coordinates in this step: ' . $count_complete . ' cities<br />Total city count to that were not found coordinates in this step: ' . $count_incomplete . ' cities<br />Cities left to fill: ' . $cities_left['total_count'] . ' cities<br /><br />Please click on <strong>Continue filling table with cities coordinates</strong> again in STEP 2 and script will continue filling coordinates from place it was stopped.');
        $stop_city_id = $city['city_id'];
        break;
      } else {
        $cord = ModelRadius::newInstance()->getCordByCityRegionRaw( strtolower($city['city_name']), strtolower($city['region_name']), strtolower($city['country_code']) );

        if(!is_null($cord['longitude']) and !is_null($cord['latitude'])) {
          ModelRadius::newInstance()->updateCityCord( $city['city_id'], $cord['longitude'], $cord['latitude'] );
          ModelRadius::newInstance()->updateCityStatus( $city['city_id'], 'action successful in STEP 2');
          $count_complete = $count_complete + 1;
        } else {
          $count_incomplete = $count_incomplete + 1;
          ModelRadius::newInstance()->updateCityCord( $city['city_id'], 0, 0 );
          ModelRadius::newInstance()->updateCityStatus( $city['city_id'], 'action unsuccessful in STEP 2 - coordinates for city not found');
        }

        $stop_city_id = '';
      }

      $i++;
    }

    if(microtime(true) - $start_time < $max_time - 10 && $stop_city_id == '') {
      message_ok(__('STEP 2 - COMPLETE: You have successfully attached all coordinates to cities. If there are cities they do not have attached coordinates, please use Google Script to complete them.', 'rad_search'));
      $finished_step2 = true;
    }
  }



  // STEP 3 - FILL EMPTY COORDINATES IN _CITY_RADIUS TABLE USING GOOGLE SCRIPT
  if(Params::getParam('plugin_action') == 'finish_google_api') {
    $i = 1;
    $stop_position = Params::getParam('stop_city_id');
    $start_time = microtime(true);
    $max_time = ini_get('max_execution_time');
    $count_complete_g = 0;
    $city_load_limit = osc_get_preference('city_load_limit','plugin-rad_search') <> '' ? osc_get_preference('city_load_limit','plugin-rad_search') : 10000;
    $fill_country = Params::getParam('fill-country');
    $cities_list = ModelRadius::newInstance()->getCitiesRegionsListNullZero($stop_position, $city_load_limit + 1, $fill_country);

    foreach($cities_list as $city ) { 

      if(microtime(true) - $start_time > $max_time - 10) {
        $cities_left = ModelRadius::newInstance()->countCitiesLeftZero($fill_country);
        message_ok('Script has almost reached maximum execution time set on your server. To avoid error message script was stopped. <br />Execution time of your server is: ' . $max_time . ' seconds. You can increase it in admin panel.<br />Total city count to that were successfully attached coordinates in this step: ' . $count_complete_g . ' cities<br />Cities left to fill: ' . $cities_left['total_count'] . ' cities<br /><br />Please click on button <strong>Attach coordinates using Google</strong> again in 4. step and script will continue fulling coordinates from place it was stopped.');
        $stop_city_id = $city['city_id'];
        break;
      } else if ($i == count($cities_list)) {
        $cities_left = ModelRadius::newInstance()->countCitiesLeft($fill_country);
        message_ok($i-1 . ' ' . __('cities were processed and successfully got coordinates via Google API, repeat this step again', 'rad_search'));
        $stop_city_id = $city['city_id'];
        break;
      } else {
        $address = $city['city_name'] . ' ' . $city['region_name'] . ' ' . $city['country_code'];

        if(trim($address) <> '') {
          $count_complete_g = $count_complete_g + 1;
          $cord = getCordByAddress( $address ); 
          usleep(100000);  // after using Google API, script is stopped for 0.1 second to avoid ban from Google

          ModelRadius::newInstance()->updateCityCord( $city['city_id'], $cord[1], $cord[0] );
          ModelRadius::newInstance()->updateCityStatus( $city['city_id'], 'action successful in STEP 3');
        }

        $stop_city_id = '';
      }

      $i++;
    }

    $city_load_limit = osc_get_preference('city_load_limit','plugin-rad_search') <> '' ? osc_get_preference('city_load_limit','plugin-rad_search') : 10000;

    $finished_step3 = false;
    if(microtime(true) - $start_time < $max_time - 10 and $city_load_limit > count($cities_list)) {
      message_ok(__('STEP 3 - COMPLETE: You have successfully attached coordinates to cities using Google API', 'rad_search'));
      $finished_step3 = true;
    }
  }



  // STEP 4 - ATTACH COORDINATES TO LISTINGS
  if(Params::getParam('plugin_action')=='cord') {
    $count_in = 0;
    $count_out = 0;
    $count_add = 0;
    $start_time = microtime(true);
    $max_time = ini_get('max_execution_time');
    $only_google_api = osc_get_preference('only_google_api', 'plugin-rad_search') <> '' ? osc_get_preference('only_google_api', 'plugin-rad_search') : 0;
    $item_load_limit = osc_get_preference('item_load_limit','plugin-rad_search') <> '' ? osc_get_preference('item_load_limit','plugin-rad_search') : 200;
    $item_list = ModelRadius::newInstance()->getItemsForCords($item_load_limit);

    foreach($item_list as $item ) { 
      if(microtime(true) - $start_time > $max_time - 10) {
        message_ok('Script has almost reached maximum execution time set on your server. To avoid error message script was stopped. <br />Execution time of your server is: ' . $max_time . ' seconds. You can increase it in admin panel.<br />Number of items that got coordinates from database: ' . $count_in . ' items.<br /> Number of items that got coordinates using Google: ' . $count_out . ' items<br />Number of items without address: ' . $count_add . ' items<br /><br />Please click again on <strong>Run now!</strong> and script will continue fulling coordinates from place it was stopped.');
        break;
      } else {
        if($only_google_api == 0) {
          $detail = ModelRadius::newInstance()->getCordByItemId( $item['pk_i_id'] );
          $loc = ModelRadius::newInstance()->getItemLoc( $item['pk_i_id'] );
          $address = $loc['s_city'] . ' ' . $loc['s_region'] . ' ' . $loc['s_country'];

          if(trim($address) == '') { $has_address = false; } else { $has_address = true; }

          if($detail['latitude'] == 0 or $detail['latitude'] == '' or $detail['longitude'] == 0 or $detail['longitude'] == '') { 
            $run = true; 
          } else { 
            $run = false; 
          }

          $has_city = false;  
          $try = ModelRadius::newInstance()->getCordByCity($loc['fk_i_city_id']);

          if(isset($try['city_id'])) {
            $lat = $try['latitude'];
            $long = $try['longitude'];
            $has_city = true;
            $count_in = $count_in + 1;
            ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action successful - coordinates for city found in table');
          } else {
            if($has_address) {
              $count_out = $count_out + 1;
            } else {
              $count_add = $count_add + 1;
            }
          }
        }

        if( $run or $only_google_api == 1 ) {
          if(!$has_city and $has_address or $only_google_api == 1) {
            $cord = getCordByAddress( $address );
            $lat = $cord[0];
            $long = $cord[1];
            usleep(100000);  // after using Google API, script is stopped for 0.5 second to avoid ban from Google
          }

          if($long <> '' and $long <> 0 and $lat <> '' and $lat <> 0) {
            // FIRST FIND ITEM CITY AND IF NO COORDINATES HERE, UPDATE IT
            $c_city = $loc['fk_i_city_id'] <> '' ? $loc['fk_i_city_id'] : $loc['s_city'];
            $c_region = $loc['fk_i_region_id'] <> '' ? $loc['fk_i_region_id'] : $loc['s_region'];
            $c_country = $loc['fk_c_country_code'] <> '' ? $loc['fk_c_country_code'] : $loc['s_country'];

            $city_id = radius_get_city_id($c_city, $c_region, $c_country);

            $city_cord = ModelRadius::newInstance()->getCordByCity($city_id);

            if(($city_cord['latitude'] == '' or $city_cord['latitude'] == 0) and ($city_cord['longitude'] == '' or $city_cord['longitude'] == '')) {
              ModelRadius::newInstance()->updateCityCord( $city_id, $long, $lat );
              ModelRadius::newInstance()->updateCityStatus( $city_id, 'action successful - in STEP 4 via Google API');
            }

            if(isset($detail['item_id']) and $has_address) {
              ModelRadius::newInstance()->updateCord( $item['pk_i_id'], $long, $lat ); 
            } else {
              ModelRadius::newInstance()->insertCord( $item['pk_i_id'], $long, $lat ); 
            }

            ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action successful - coordinates for city found via Google API');
          } else {
            ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action unsuccessful - coordinates for city NOT found via Google API');
          }
        }
      }
    }

    if(microtime(true) - $start_time < $max_time - 10) {
      message_ok('Coordinates were successfully attached to items.<br />Number of items that got coordinates from database: ' . $count_in . ' items.<br /> Number of items that got coordinates using Google: ' . $count_out . ' items<br /> Number of items without address: ' . $count_add . ' items');
    }
  }
?>


<div class="mb-body">
  <div class="mb-row mb-notes" style="margin-bottom:30px;">
    <div class="mb-line"><?php _e('Do not jump over steps, go straight 1 > 2 > 3 > 4', 'rad_search'); ?></div>
    <div class="mb-line"><?php _e('Finish all steps!', 'rad_search'); ?></div>
    <div class="mb-line">
      <?php if(radius_check_google_api()) { ?>
        <?php _e('Google script status', 'rad_search'); ?> - <strong style="color:green"><?php _e('OK', 'rad_search'); ?></strong> - <?php _e('you did not reach daily quota - 2500 requests per day', 'rad_search'); ?>
      <?php } else { ?>
        <?php _e('Google script status', 'rad_search'); ?> - <strong style="color:red"><?php _e('ERROR', 'rad_search'); ?></strong> - <?php _e('you reached daily quota - 2500 requests per day, you will be able to use Google API again tomorrow', 'rad_search'); ?>
      <?php } ?>
    </div>
  </div>


  <!-- 1. STEP - FILE UPLOAD -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-cloud-upload"></i><strong> 1. <?php _e('STEP', 'rad_search'); ?></strong> - <?php _e('Upload file with coordinates', 'rad_search'); ?></div>

    <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="page" value="plugins" />
      <input type="hidden" name="action" value="renderplugin" />
      <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>manual_coordinates_upload.php" />
      <input type="hidden" name="plugin_action" value="upload" />

      <div class="mb-inside">

        <div class="mb-row">
          <div class="mb-line"><?php _e('In this step you download file with coordinates, upload it to your server and then copy coordinates into help table in database', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('Download file with coordinates of cities for your country on', 'rad_search'); ?> <a href="http://download.geonames.org/export/zip/" target="_blank">http://download.geonames.org/export/zip/</a></div>
        </div>
        
        <div class="mb-row mb-add-lines">
          <label for="file"><span><?php _e('Select file to upload', 'rad_search'); ?></span></label>
          <input type="file" name="file" id="file">   
          <div class="mb-explain"><?php _e('File extension must be .txt', 'rad_search'); ?></div>          
        </div>
        
        <div class="mb-row mb-notes">
          <div class="mb-line"><?php _e('Select .txt file that\'s name is in format: COUNTRY_CODE.txt (example: US.txt, DE.txt, ES.txt, CZ.txt, ...)', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('Note that following operations are very resource consuming and requires good server.', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('More cities you have, more time it takes to finish.', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('Recommended maximum allowed memory size set in your PHP installation', 'rad_search'); ?>: <strong>128M</strong>. <?php _e('Your current setting', 'rad_search'); ?>: <strong><?php echo ini_get('memory_limit'); ?></strong></div>
          <div class="mb-line"><?php _e('Recommended maximum execution time set in your PHP installation', 'rad_search'); ?>: <strong>180s</strong>. <?php _e('Your current setting', 'rad_search'); ?>: <strong><?php echo ini_get('max_execution_time'); ?>s</strong></div>
          <div class="mb-line"><?php _e('To increase maximum allowed memory size & maximum execution time, contact your hosting provider. You do not need permanent increase, just temporary till you finish all steps.', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Upload', 'rad_search');?></button>
      </div>
      
    </form>
  </div>


  
  
  
  <!-- 2. STEP - CREATE TABLE -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-table"></i><strong> 2. <?php _e('STEP', 'rad_search'); ?></strong> - <?php _e('Create new table for cities with their coordinates', 'rad_search'); ?></div>

    <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="page" value="plugins" />
      <input type="hidden" name="action" value="renderplugin" />
      <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>manual_coordinates_upload.php" />
      <input type="hidden" name="plugin_action" value="finish" />
      <input type="hidden" name="stop_city_id" value="<?php echo $stop_city_id;?>" />

      <div class="mb-inside">

        <?php $city_load_limit = osc_get_preference('city_load_limit','plugin-rad_search') <> '' ? osc_get_preference('city_load_limit','plugin-rad_search') : 10000; ?>

        <div class="mb-row">
          <div class="mb-line"><?php _e('Choose country you have uploaded coordinates in 1. step', 'rad_search'); ?></div>
        </div>
        
        
        <div class="mb-row fill-country-select mb-add-lines">
          <label class="h1" for="fill-country"><span><?php _e('Country to proceed', 'rad_search'); ?></span></label>

          <?php $country_stats = ModelRadius::newInstance()->countCitiesByCountry(); ?>
          <select id="fill-country" name="fill-country">
            <option value="" <?php echo Params::getParam('fill-country') == '' ? 'selected="selected"' : ''; ?>><?php _e('All countries', 'rad_search'); ?></option>

            <?php foreach($country_stats as $c) { ?>
              <option value="<?php echo strtolower($c['pk_c_code']); ?>" <?php echo Params::getParam('fill-country') == strtolower($c['pk_c_code']) ? 'selected="selected"' : ''; ?>><?php echo $c['s_name']; ?></option>
            <?php } ?>
          </select>
        </div>
        

        <div class="mb-table" style="margin-bottom:20px;">
          <?php $country_stats = ModelRadius::newInstance()->countCitiesByCountry(); ?>
          
          <div class="mb-table-head">
            <div class="mb-col-4 mb-align-left"><?php _e('Country', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('All cities', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Completed', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Not found', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Not filled', 'rad_search'); ?></div>
          </div>

          <?php foreach($country_stats as $c) { ?>
            <div class="mb-table-row">
              <div class="mb-col-4 mb-align-left"><?php echo $c['s_name']; ?></div>
              <div class="mb-col-2"><?php echo $c['city_total']; ?></div>
              <div class="mb-col-2"><?php echo $c['city_complete']; ?></div>
              <div class="mb-col-2"><?php echo $c['city_notfound']; ?></div>
              <div class="mb-col-2"><?php echo $c['city_notfilled']; ?></div>
            </div>
          <?php } ?>
        </div>

       
        <div class="mb-row mb-notes" style="margin-bottom:0px;">
          <div class="mb-line"><?php _e('In this step, city coordinates from help table are paired with cities in your osclass installation and are stored in table', 'rad_search'); ?> <strong><?php echo ModelRadius::newInstance()->getTable_CityRadius(); ?></strong></div>
          <div class="mb-line"><?php _e('In case this step is not successful enough (too many cities without coordinates), that does not mean plugin does not work for you. Coordinates can still be got from Google API. Only issue is that function search in radius will be limited just to listings located in cities that have coordinates.', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('This step must be repeated multiple times as it is not possible to attach coordinates to all cities in one step. Script is then restored on place where it was stopped. Currently you have set in', 'rad_search'); ?> <a href="<?php echo osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/general_settings.php'; ?>" target="_blank"><?php _e('General settings', 'rad_search'); ?></a> <?php _e('to proceed', 'rad_search'); ?> <strong><?php echo $city_load_limit; ?> <?php _e('cities', 'rad_search'); ?></strong> <?php _e('in one step. Too many cities to proceed in one step may result in PHP error (maximum allowed memory size exceeded or maximum allowed execution time exceeded)', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row mb-notes">
          <div class="mb-line"><?php _e('Script is stopped when it reach maximum execution time set on your server or complete all cities', 'rad_search'); ?></div>
        </div>
      </div>

      <div class="mb-foot">
        <?php if(Params::getParam('plugin_action') == 'finish' and !$finished_step2) { ?>
          <button type="submit" class="mb-button"><?php _e('Continue (this step has not finished yet)', 'rad_search'); ?></button>
        <?php } else { ?>
          <button type="submit" class="mb-button"><?php _e('Start', 'radius_serch');?></button>
        <?php } ?>
      </div>
      
    </form>
  </div>


  
  <!-- 3. STEP - USE GOOGLE API -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-google"></i><strong> 3. <?php _e('STEP', 'rad_search'); ?></strong> - <?php _e('Use Google API for missing coordinates for cities', 'rad_search'); ?></div>

    <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="page" value="plugins" />
      <input type="hidden" name="action" value="renderplugin" />
      <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>manual_coordinates_upload.php" />
      <input type="hidden" name="plugin_action" value="finish_google_api" />
      <input type="hidden" name="stop_city_id" value="<?php echo $stop_city_id;?>" />

      <div class="mb-inside">

        <div class="mb-row">
          <div class="mb-line"><?php _e('Choose country you have uploaded coordinates in 1. step', 'rad_search'); ?></div>
        </div>
        
        
        <div class="mb-row fill-country-select mb-add-lines">
          <label class="h1" for="fill-country"><span><?php _e('Country to proceed', 'rad_search'); ?></span></label>

          <?php $country_stats = ModelRadius::newInstance()->countCitiesByCountry(); ?>
          <select name="fill-country" id="fill-country">
            <option value="" <?php echo Params::getParam('fill-country') == '' ? 'selected="selected"' : ''; ?>><?php _e('All countries', 'rad_search'); ?></option>

            <?php foreach($country_stats as $c) { ?>
              <option value="<?php echo strtolower($c['pk_c_code']); ?>" <?php echo Params::getParam('fill-country') == strtolower($c['pk_c_code']) ? 'selected="selected"' : ''; ?>><?php echo $c['s_name']; ?></option>
            <?php } ?>
          </select>
        </div>
        
       
        <div class="mb-row mb-notes">
          <div class="mb-line"><?php _e('Cities that has not got attached coordinates after <strong>STEP 2</strong> will be sent to Google API to find correct latitude and longitude', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('Daily quota for Google API is <u>2500 request per day</u>, therefore if you have more cities without coordinates, you may need to repeat this step for more days', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('There is waiting time after each coordinate is got for 0.1 second to not get blocked on Google API', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <?php if(Params::getParam('plugin_action')=='finish_google_api' and !$finished_step3) { ?>
          <button type="submit" class="mb-button"><?php _e('Continue (this step has not finished yet)', 'rad_search'); ?></button>
        <?php } else { ?>
          <button type="submit" class="mb-button"><?php _e('Start', 'radius_serch');?></button>
        <?php } ?>
      </div>
      
    </form>
  </div>
  
  
  
  <!-- 4. STEP - FILL ITEM COORDINATES -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-random"></i><strong> 4. <?php _e('STEP', 'rad_search'); ?></strong> - <?php _e('Attach coordinates to listings', 'rad_search'); ?></div>

    <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="page" value="plugins" />
      <input type="hidden" name="action" value="renderplugin" />
      <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>manual_coordinates_upload.php" />
      <input type="hidden" name="plugin_action" value="finish_google_api" />
      <input type="hidden" name="plugin_action" value="cord" />

      <div class="mb-inside">

        <div class="mb-row">
          <div class="mb-line"><?php _e('Only listings that are active, non-spam & enabled with zero or null coordinates are proceed in this step', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('When it is not possible to find coordinate for listing in database, it is checked via Google API', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('For better accuracy you can set Use only Google API to get coordinates for listings in', 'rad_search'); ?> <a href="<?php echo osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/general_settings.php'; ?>" target="_blank"><?php _e('Global Settings', 'rad_search'); ?></a></div>
          <div class="mb-line">&nbsp;&nbsp;- <?php _e('when enabled, coordinates for items are taken only using Google API - country, region, city, city area and address are used ', 'rad_search'); ?></div>
          <div class="mb-line">&nbsp;&nbsp;- <?php _e('when disabled, coordinates for items are first checked in database (country, region and city are used). When no match found, coordinates are checked on Google API', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('When coordinates to item are found using Google API, script is stopped for 0.1 second to avoid ban from Google API.', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Start', 'radius_serch');?></button>
      </div>
    </form>
  </div>


  <!-- HELP TOPICS -->
  <div class="mb-box" id="mb-help">
    <div class="mb-head"><i class="fa fa-question-circle"></i> <?php _e('Help', 'rad_search'); ?></div>

    <div class="mb-inside">
      <div class="mb-row mb-help"><span class="sup">(1)</span> <div class="h1"><?php _e('Select country that\'s cities will be filled. If you do not select country, script will go through all cities from all countries and it will take rapidly more time.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><div><?php _e('Note that if you do not upload any coordinates, it does not have any influence on functionality of plugin. Reason of manual coordinates upload is to lower trafic required to Google API.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><div>
        <?php _e('Tables used by plugin', 'rad_search'); ?>: <br />
        <strong><?php echo ModelRadius::newInstance()->getTable_CordsValidate(); ?></strong> - <?php _e('store raw data from 1. step copied from text file', 'rad_search'); ?><br />
        <strong><?php echo ModelRadius::newInstance()->getTable_Cords(); ?></strong> - <?php _e('store raw data copied from validate table cleaned from duplicates, created in 1. step', 'rad_search'); ?><br />
        <strong><?php echo ModelRadius::newInstance()->getTable_CityRadius(); ?></strong> - <?php _e('store data with coordinates paired to cities from osclass installation, created & used in 2. - 3. step', 'rad_search'); ?><br />
        <strong><?php echo ModelRadius::newInstance()->getTable_Radius(); ?></strong> - <?php _e('store data with coordinates paired to listings, created in 4. step', 'rad_search'); ?><br />
      </div></div>
    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
