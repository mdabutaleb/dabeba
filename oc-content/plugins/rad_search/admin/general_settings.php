<?php
  // Create menu
  $title = __('Global Settings', 'rad_search');
  radius_menu($title);

  
  // GET & UPDATE PARAMETERS
  $measure = radius_param_update( 'measure', 'plugin_action' );
  $hook = radius_param_update( 'hook', 'plugin_action', 'check' );
  $search_select = radius_param_update( 'search_select', 'plugin_action' );
  $only_google_api = radius_param_update( 'only_google_api', 'plugin_action', 'check' );
  $per_page = radius_param_update( 'per_page', 'plugin_action' );
  $city_load_limit = radius_param_update( 'city_load_limit', 'plugin_action' );
  $item_load_limit = radius_param_update( 'item_load_limit', 'plugin_action' );



  if(Params::getParam('plugin_action') == 'done') {
    message_ok( __('Global settings were successfully saved', 'rad_search') );
  }
?>

<div class="mb-body">

  <!-- GLOBAL SETTINGS -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-cog"></i> <?php _e('Global settings', 'rad_search'); ?></div>

    <div class="mb-inside">
      <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>general_settings.php" />
        <input type="hidden" name="plugin_action" value="done" />

        
        <div class="mb-row">
          <label for="hook" class="h1"><span><?php _e('Hook Distance Box to Search Sidebar', 'rad_search'); ?></span></label> 
          <input name="hook" id="hook" class="element-slide" type="checkbox" <?php echo ($hook == 1 ? 'checked' : ''); ?> />
        </div>
               
        <div class="mb-row">
          <label for="only_google_api" class="h2"><span><?php _e('Use only Google API to get Coordinates', 'rad_search'); ?></span></label> 
          <input name="only_google_api" id="only_google_api" class="element-slide" type="checkbox" <?php echo ($only_google_api == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('When checked, more accurate coordinates are provided. Do not manually upload coordinates when you want to use this.', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="measure" class="h3"><span><?php _e('Measure in Distance Field', 'rad_search'); ?></span></label> 
          <select name="measure" id="measure">
            <option value="km" <?php if($measure == 'km') { ?>selected="selected"<?php } ?>><?php _e('Kilometer (km)', 'rad_search'); ?></option>
            <option value="mi" <?php if($measure == 'mi') { ?>selected="selected"<?php } ?>><?php _e('Mile (mile)', 'rad_search'); ?></option>
          </select>
        </div>
        
        <div class="mb-row">
          <label for="search_select" class="h4"><span><?php _e('Measure in Distance Field', 'rad_search'); ?></span></label> 
          <select name="search_select" id="search_select">
            <option value="1" <?php if($search_select == 1) { ?>selected="selected"<?php } ?>><?php _e('Select box', 'rad_search'); ?></option>
            <option value="0" <?php if($search_select == 0) { ?>selected="selected"<?php } ?>><?php _e('Input box', 'rad_search'); ?></option>
          </select>
        </div>
        
        <div class="mb-row">
          <label for="per_page" class="h5"><span><?php _e('Items/Cities per Page', 'rad_search'); ?></span></label> 
          <input size="6" name="per_page" id="per_page" class="mb-short" type="text" value="<?php echo $per_page; ?>" />
          <div class="mb-input-desc"><?php _e('entries', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="city_load_limit" class="h6"><span><?php _e('City Load Limit', 'rad_search'); ?></span></label> 
          <input size="6" name="city_load_limit" id="city_load_limit" class="mb-short" type="text" value="<?php echo $city_load_limit; ?>" />
          <div class="mb-input-desc"><?php _e('cities', 'rad_search'); ?></div>
          
          <div class="mb-explain"><?php _e('How many cities can be loaded in 1 coordinates upload step.', 'rad_search'); ?></div>
        </div>
        
        <div class="mb-row">
          <label for="item_load_limit" class="h6"><span><?php _e('Items Load Limit', 'rad_search'); ?></span></label> 
          <input size="6" name="item_load_limit" id="item_load_limit" class="mb-short" type="text" value="<?php echo $item_load_limit; ?>" />
          <div class="mb-input-desc"><?php _e('items', 'rad_search'); ?></div>
          
          <div class="mb-explain"><?php _e('How many items can be loaded in 1 coordinates upload step.', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </form>
  </div>



  <!-- PLUGIN INTEGRATION -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-wrench"></i> <?php _e('Distance Box Manual Setup', 'rad_search'); ?></div>

    <div class="mb-inside">

      <div class="mb-row">
        <div class="mb-line"><?php _e('Use only if you have disabled to hook distance search box into search sidebar. Otherwise box will be placed to front automatically.', 'rad_search'); ?></div>
        <div class="mb-line"><?php _e('Place following code into sidebar search form on search page.', 'rad_search'); ?></div>
        <div class="mb-line"><?php _e('This file is located in theme folder and called search.php', 'rad_search'); ?></div>
        <div class="mb-line"><?php _e('Code must (!) be placed between', 'rad_search'); ?> &lt;form&gt; <?php _e('and', 'rad_search'); ?> &lt;/form&gt;</div>

        <div class="mb-row">&nbsp;</div>

        <span class="mb-code">&lt;?php if(function_exists('radius_distance_box')) { echo radius_distance_box( ); } ?&gt;</span>
      </div>
    </div>
  </div>



  <!-- HELP TOPICS -->
  <div class="mb-box" id="mb-help">
    <div class="mb-head"><i class="fa fa-question-circle"></i> <?php _e('Help', 'rad_search'); ?></div>

    <div class="mb-inside">
      <div class="mb-row mb-help"><span class="sup">(1)</span> <div class="h1"><?php _e('Set to YES to automatically add distance box into sidebar search form. You do not need to edit any theme files. This field will enable to users to search listings not only in selected country/region/city, but also in radius around country/region/city. If you set to NO and you want to use this field on different place, follow <strong>Distance Box Manual Setup</strong> guide.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(2)</span> <div class="h2"><?php _e('When enabled, there is no need to manually upload coordinates for cities. This setting is recommended only if you have few listings. When disabled and new listing is published, plugin will check country/region/city of listing in database and if coordinates are found, use these. If not, Google API is used. When this functionality is enabled, plugin will everytime check for coordinates on Google API using Country, Region, City but also Address field. This leads to more accurate data, especially if your classifieds are for specific area/city only.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(3)</span> <div class="h3"><?php _e('Choose which measure you want to use - kilometers or miles. Based on measure selected, also calculation in radius is modified to apply this change.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(4)</span> <div class="h4"><?php _e('Choose if you want to use Select box or simple Input box for distance field.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(5)</span> <div class="h5"><?php _e('Enter how many cities/items you want to list in plugin settings. Simply - pagination number. This has no effect on front-office. Higher number require more resource from server/hosting.', 'rad_search'); ?></div></div>
      <div class="mb-row mb-help"><span class="sup">(6)</span> <div class="h6"><?php _e('When coordinates for cities and existing items are loaded, usually it is not possible to do it in 1 step, but this process is divided into multiple steps based on hosting performance. Set how many items/cities can be processed in one step. Note that higher number require more performance on server side.', 'rad_search'); ?></div></div>
    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
