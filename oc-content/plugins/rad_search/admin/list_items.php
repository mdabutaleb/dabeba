<?php
  // Create menu
  $title = __('Listings Coordinates', 'rad_search');
  radius_menu($title);
  
  
  $per_page = osc_get_preference('per_page', 'plugin-rad_search') <> '' ? osc_get_preference('per_page', 'plugin-rad_search') : 100;

  $items_total = ModelRadius::newInstance()->countItems();
  $num_all = $items_total['total_count'];
  $num_list = $per_page;


  // Figure out what rows we want, and SELECT them
  if (Params::getParam('start')) {
    $start = intval(Params::getParam('start'));
    if ($start > 0) {
      $start--;
    }
  } else {
    $start = 0;
  }

  $f_item = Params::getParam('item-filter');
  $f_country = Params::getParam('country-filter');
  $f_region = Params::getParam('region-filter');
  $f_city = Params::getParam('city-filter');
  $f_only_null = Params::getParam('only_null') == 'on' ? 1 : 0;
?>



<div class="mb-body">
  <div class="mb-row mb-notes" style="margin-bottom:30px;">
    <div class="mb-line"><?php _e('Only to listings, that are activated, enabled and not marked as spam are attached coordinates when you upload coordinates via <strong>Manual Coordinates Upload</strong> section', 'rad_search'); ?></div>
    <div class="mb-line"><?php _e('Only listings, that are activated, enabled and not marked as spam are shown', 'rad_search'); ?></div>
  </div>
  
  
  <!-- ITEM COORDINATES -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-list"></i> <?php _e('List of coordinates for all items', 'rad_search'); ?></div>

    <div class="mb-inside">
      <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>list_items.php" />
        <input type="hidden" name="plugin_action" value="item-refresh" />
        
        <div class="mb-row mb-entry-filter">
          <div class="mb-col-2">
            <label for="item-filter"><span><?php _e('Item ID', 'rad_search'); ?></span></label>
            <input size="7" type="text" name="item-filter" value="<?php echo Params::getParam('item-filter'); ?>" />
          </div>

          <div class="mb-col-2">
            <label for="city-filter"><span><?php _e('City', 'rad_search'); ?></span></label>
            <input size="15" type="text" name="city-filter" value="<?php echo Params::getParam('city-filter'); ?>" />
          </div>

          <div class="mb-col-2">
            <label for="region-filter"><span><?php _e('Region', 'rad_search'); ?></span></label>
            <input size="15" type="text" name="region-filter" value="<?php echo Params::getParam('region-filter'); ?>" />
          </div>

          <div class="mb-col-2_1_2">
            <label for="country-filter"><span><?php _e('Country', 'rad_search'); ?></span></label>
            
            <?php $country_stats = ModelRadius::newInstance()->countCitiesByCountry(); ?>
            <select name="country-filter">
              <option value="" <?php echo Params::getParam('country-filter') == '' ? 'selected="selected"' : ''; ?>><?php _e('All countries', 'rad_search'); ?></option>

              <?php foreach($country_stats as $c) { ?>
                <option value="<?php echo strtolower($c['pk_c_code']); ?>" <?php echo Params::getParam('country-filter') == strtolower($c['pk_c_code']) ? 'selected="selected"' : ''; ?>><?php echo $c['s_name']; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="mb-col-2_1_2">
            <input name="only_null" id="only_null" class="element-slide" type="checkbox" <?php echo (Params::getParam('only_null') == 'on' ? 'checked' : ''); ?> />
            <label for="only_null" class="only_null"><?php _e('Items without coords', 'rad_search'); ?></label>
          </div>

          <div class="mb-col-1">
            <button type="submit" class="mb-button-white"><i class="fa fa-filter"></i> <?php _e('Filter', 'rad_search');?></button>
          </div>
        </div>
      </form>
      
      
      <div class="mb-table radius_list">
        <div class="mb-table-head">
          <div class="mb-col-3 mb-align-left"><?php _e('Listing', 'rad_search');?></div>
          <div class="mb-col-1_1_2 mb-align-left"><?php _e('Country', 'rad_search');?></div>
          <div class="mb-col-1_1_2 mb-align-left"><?php _e('Region', 'rad_search');?></div>
          <div class="mb-col-1_1_2 mb-align-left"><?php _e('City', 'rad_search');?></div>
          <div class="mb-col-1_2">&nbsp;</div>
          <div class="mb-col-1"><?php _e('Latitude', 'rad_search');?></div>
          <div class="mb-col-1"><?php _e('Longitude', 'rad_search');?></div>
          <div class="mb-col-2 mb-align-left"><?php _e('Status', 'rad_search');?></div>
        </div>

        <?php
          if($f_item <> '' or $f_country <> '' or $f_region <> '' or $f_city <> '' or $f_only_null <> '') {
            $items_list = ModelRadius::newInstance()->getItemsSearch($num_list, osc_current_user_locale(), $f_item, $f_city, $f_region, $f_country, $f_only_null);
          } else {
            $items_list = ModelRadius::newInstance()->getItems($start, $num_list, osc_current_user_locale());
          }
        ?>
        <?php
          foreach($items_list as $item ) { 
            $detail = ModelRadius::newInstance()->getCordByItemId( $item['pk_i_id'] );
            $loc = ModelRadius::newInstance()->getItemLoc( $item['pk_i_id'] );

            $empty = '<div class="mb-i mb-gray">' . __('- empty -', 'rad_search') . '</div>';

            $map_img = '';
            if(($detail['latitude'] == '' or $detail['latitude'] == 0) and ($detail['longitude'] == '' or $detail['longitude'] == 0)) {
              if($detail['status'] == '') {
                $detail['status'] = __('Coordinate were not uploaded yet', 'rad_search');
              }

              $status = '<img src="' . osc_base_url() . 'oc-content/plugins/rad_search/images/deny.png" title="' . $detail['status'] . '"/><span>[' . $detail['status'] . ']</span>';
              $map = 0;
            } else {
              $status = '<img src="' . osc_base_url() . 'oc-content/plugins/rad_search/images/accept.png" title="' . $detail['status'] . '"/>';
              $map = 1;
              $map_img = 'rel="http://maps.googleapis.com/maps/api/staticmap?zoom=12&size=400x300&center=' . $detail['latitude'] . ',' . $detail['longitude'] . '&markers=color:red%7C' . $detail['latitude'] . ',' . $detail['longitude'] . '"';
            }

            if($detail['latitude'] == '' or $detail['latitude'] == 0) { 
              $detail['latitude'] = $empty;
            }

            if($detail['longitude'] == '' or $detail['longitude'] == 0) { 
              $detail['longitude'] = $empty;
            }

            echo '<div class="mb-table-row">';
            echo '<div class="mb-col-3 mb-align-left"><a target="_blank" href="' . osc_base_url() . 'oc-admin/index.php?page=items&action=item_edit&id=' . $item['pk_i_id'] . '" title="' . $item['s_title'] . ' (id: ' . $item['pk_i_id'] . ')">' . $item['s_title'] . ' (id: ' . $item['pk_i_id'] . ')</a></div>';
            echo '<div class="mb-col-1_1_2 mb-align-left">' . ($loc['s_country'] <> '' ? $loc['s_country'] : $empty) . '</div>';
            echo '<div class="mb-col-1_1_2 mb-align-left">' . ($loc['s_region'] <> '' ? $loc['s_region'] : $empty) . '</div>';
            echo '<div class="mb-col-1_1_2 mb-align-left">' . ($loc['s_city'] <> '' ? $loc['s_city'] : $empty) . '</div>';
            echo '<div class="mb-col-1_2 ' . ($map == 1 ? 'map' : '') . '" ' . ($map == 1 ? $map_img : '') . '><i class="fa fa-search-plus"></i></div>';
            echo '<div class="mb-col-1">' . $detail['latitude'] . '</div>';
            echo '<div class="mb-col-1">' . $detail['longitude'] . '</div>';
            echo '<div class="mb-col-2 mb-entry-status mb-align-left"  title="' . $detail['status'] . '">' . $status . '</div>';
            echo '</div>'; 
          }

         
          if($num_all == 0) { 
            echo '<div class="mb-notes">';
            echo '<div class="zero-city">' . __('No listings found in your osclass installation.', 'rad_search') . '</div>';
            echo '</div>';
          }

          if($num_all > 0 and count($items_list) == 0) { 
            echo '<div class="mb-notes">';
            echo '<div class="zero-city">' . __('No listings match search criteria', 'rad_search') . '</div>';
            echo '</div>';
          }
        ?>
      </div>

      <?php if($f_item <> '' or $f_country <> '' or $f_region <> '' or $f_city <> '' or $f_only_null <> '') { ?>
        <?php if(count($items_list) >= $num_list) { ?>
          <div class="mb-notes">
            <div class="zero-city"><?php echo __('There are more results, only first', 'rad_search') . ' ' . $num_list . ' ' . __('results are shown.', 'rad_search'); ?></div>
          </div>
        <?php } ?>
      <?php } else { ?>
        <?php if($num_all > $num_list) { ?>
          <div id="mb-pagination">
            <div class="mb-pagination-wrap">
              <div><?php _e('Page', 'rad_search'); ?>:</div> <?php echo add_pagination($num_all, $num_list, 'rad_search/admin/list_items.php'); ?>
            </div>
          </div>
        <?php } ?>
      <?php } ?>


      <div id="radius-admin-map"><img /></div>

    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
	