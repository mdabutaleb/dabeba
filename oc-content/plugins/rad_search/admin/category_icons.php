<?php
  // Create menu
  $title = __('Category Icons', 'rad_search');
  radius_menu($title);
  
  
  // GET & UPDATE PARAMETERS
  $default_icon = radius_param_update( 'default_icon', 'plugin_action', 'check' );
  
  if(Params::getParam('plugin_action') == 'done') {
    message_ok( __('Settings for Category Icons were successfully saved', 'rad_search') );
  }
  
    
  
  // ICONS UPLOAD
  if(Params::getParam('plugin_action')=='images') {
    $upload_icon_dir = osc_base_path() . 'oc-content/plugins/rad_search/icons/';

    if (!file_exists($upload_icon_dir)) { mkdir($upload_icon_dir, 0777, true); }

    $count_real = 0;
    for ($i=1; $i<=1000; $i++) {
      if(isset($_FILES['icon' .$i]) and $_FILES['icon' .$i]['name'] <> ''){

        $file_ext   = strtolower(end(explode('.', $_FILES['icon' .$i]['name'])));
        $file_name  = $i . '.' . $file_ext;
        $file_tmp   = $_FILES['icon' .$i]['tmp_name'];
        $file_type  = $_FILES['icon' .$i]['type'];   
        $extensions = array("png");

        if(in_array($file_ext,$extensions )=== false) {
          $errors = __('extension not allowed, only allowed extension is .png!','rad_search');
        } 
          
        if(empty($errors)==true){
          move_uploaded_file($file_tmp, $upload_icon_dir.$file_name);
          message_ok(__('Icon image #','rad_search') . $i . __(' uploaded successfully.','rad_search'));
          $count_real++;
        } else {
          message_error(__('There was error when uploading icon image #','rad_search') . $i . ': ' .$errors);
        }
      }
    }
  }


  
  // REMOVE ICON
  if(Params::getParam('remove_cat') <> '') {
    $upload_icon_dir = osc_base_path() . 'oc-content/plugins/rad_search/icons/';
    $cat_id = Params::getParam('remove_cat');

    if(unlink($upload_icon_dir . $cat_id . '.png')) {
      message_ok(__('Icon for category #','rad_search') . $cat_id . ' ' . __('removed successfully.','rad_search'));
    } else {
      message_error(__('Warning: Icon for category #','rad_search') . $cat_id . ' ' . __('was not removed.','rad_search'));
    }
  }
?>



<div class="mb-body">
  
  <!-- CATEGORY ICONS SETTINGS -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-cog"></i> <?php _e('Category icons settings', 'rad_search'); ?></div>

    <div class="mb-inside">
      <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>category_icons.php" />
        <input type="hidden" name="plugin_action" value="done" />

        
        <div class="mb-row">
          <label for="default_icon" class="h1"><span><?php _e('Use Default Category Icon', 'rad_search'); ?></span></label> 
          <input name="default_icon" id="default_icon" class="element-slide" type="checkbox" <?php echo ($default_icon == 1 ? 'checked' : ''); ?> />
          
          <div class="mb-explain"><?php _e('Default icon will be used for all categories, no matter what icons were uploaded.', 'rad_search'); ?></div>
        </div>

      </div>

      <div class="mb-foot">
        <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
      </div>
    </form>
  </div>
  
  
  
  
  <!-- CATEGORY ICONS UPLOAD -->
  <div class="mb-box">
    <div class="mb-head"><i class="fa fa-upload"></i> <?php _e('Upload category icons', 'rad_search'); ?></div>

    <div class="mb-inside">
      <form name="promo_form" id="promo_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>category_icons.php" />
        <input type="hidden" name="plugin_action" value="images" />
        
        <div class="mb-notes" style="margin-top:5px;margin-bottom:30px;">
          <div class="mb-line"><?php _e('Recommended icon size is 32x32 px or smaller. Do not upload bigger icons', 'rad_search'); ?></div>
          <div class="mb-line"><?php _e('Only <strong>png</strong> images/icons are allowed.', 'rad_search'); ?></div>
        </div>

        <div class="mb-table" style="margin-bottom:40px;">
          <div class="mb-table-head">
            <div class="mb-col-1"><?php _e('Cat ID', 'rad_search'); ?></div>
            <div class="mb-col-3 mb-align-left"><?php _e('Name', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Has Icon', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Uploaded Icon', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Add', 'rad_search'); ?></div>
            <div class="mb-col-2"><?php _e('Remove', 'rad_search'); ?></div>
          </div>
        
          <?php radius_has_subcategories(Category::newInstance()->toTree(),  0); ?> 
        </div>

       
        <div class="mb-foot">
          <button type="submit" class="mb-button"><?php _e('Save', 'rad_search');?></button>
        </div>
      </form>
    </div>
  </div>
  
  
  <!-- HELP TOPICS -->
  <div class="mb-box" id="mb-help">
    <div class="mb-head"><i class="fa fa-question-circle"></i> <?php _e('Help', 'rad_search'); ?></div>

    <div class="mb-inside">
      <div class="mb-row mb-help"><span class="sup">(1)</span> <div class="h1"><?php _e('Default icon for all categories will be used, no matter what icons were uploaded. When set to YES your icons are disabled. Use this if you are just uploading icons and you do not have all of them.', 'rad_search'); ?></div></div>
    </div>
  </div>
</div>

<?php echo radius_footer(); ?>
