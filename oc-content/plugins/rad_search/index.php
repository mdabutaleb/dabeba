<?php
/*
  Plugin Name: Radius Search Plugin
  Plugin URI: http://www.mb-themes.com
  Description: Allows to use advanced search capabilities and add map with listings to front
  Version: 2.2.7
  Author: MB Themes
  Author URI: http://www.mb-themes.com
  Author Email: info@mb-themes.com
  Short Name: rad_search
  Plugin update URI: radius-search-plugin
  Support URI: http://forums.mb-themes.com/radius-search-plugin/
*/



require_once 'model/ModelRadius.php';
require_once 'admin/pagination.php';



// INSTALL FUNCTION - DEFINE VARIABLES
function radius_call_after_install() {
  ModelRadius::newInstance()->import('rad_search/model/struct.sql');
  
  // General settings
  osc_set_preference('measure', 'km', 'plugin-rad_search', 'STRING');
  osc_set_preference('hook', 1, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('per_page', 100, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_hook', 1, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_zoom', 0, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_loc', 1, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('same_cat', 0, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('max_items', 200, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('city_load_limit', 10000, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('item_load_limit', 100, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('only_google_api', 0, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_cluster', 1, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_roads', 0, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('def_lat', '', 'plugin-rad_search', 'STRING');
  osc_set_preference('def_long', '', 'plugin-rad_search', 'STRING');
  osc_set_preference('search_select', '', 'plugin-rad_search', 'INTEGER');
  osc_set_preference('default_icon', 1, 'plugin-rad_search', 'INTEGER');

  // Home page map
  osc_set_preference('map_type_home', 'ROADMAP', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_width_home', '100%', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_height_home', '300px', 'plugin-rad_search', 'STRING');
  osc_set_preference('only_premium_home', 0, 'plugin-rad_search', 'INTEGER');

  // Search page map
  osc_set_preference('map_type_search', 'ROADMAP', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_width_search', '100%', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_height_search', '300px', 'plugin-rad_search', 'STRING');
  osc_set_preference('only_premium_search', 0, 'plugin-rad_search', 'INTEGER');

  // Listing page map
  osc_set_preference('map_type_item', 'ROADMAP', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_width_item', '100%', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_height_item', '300px', 'plugin-rad_search', 'STRING');
  osc_set_preference('only_premium_item', 0, 'plugin-rad_search', 'INTEGER');

  // Publish page map
  osc_set_preference('map_type_publish', 'ROADMAP', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_width_publish', '100%', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_height_publish', '300px', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_zoom_publish', 12, 'plugin-rad_search', 'INTEGER');
  osc_set_preference('map_lat_publish', '40.7033127', 'plugin-rad_search', 'STRING');
  osc_set_preference('map_lng_publish', '-73.979681', 'plugin-rad_search', 'STRING');
}

function radius_call_after_uninstall() {
  ModelRadius::newInstance()->uninstall();
  osc_delete_preference('measure', 'plugin-rad_search');
  osc_delete_preference('hook', 'plugin-rad_search');
  osc_delete_preference('per_page', 'plugin-rad_search');
  osc_delete_preference('map_hook', 'plugin-rad_search');
  osc_delete_preference('map_zoom', 'plugin-rad_search');
  osc_delete_preference('map_loc', 'plugin-rad_search');
  osc_delete_preference('same_cat', 'plugin-rad_search');
  osc_delete_preference('max_items', 'plugin-rad_search');
  osc_delete_preference('city_load_limit', 'plugin-rad_search');
  osc_delete_preference('item_load_limit', 'plugin-rad_search');
  osc_delete_preference('only_google_api', 'plugin-rad_search');
  osc_delete_preference('map_cluster', 'plugin-rad_search');
  osc_delete_preference('map_roads', 'plugin-rad_search');
  osc_delete_preference('def_lat', 'plugin-rad_search');
  osc_delete_preference('def_long', 'plugin-rad_search');
  osc_delete_preference('search_select', 'plugin-rad_search');
  osc_delete_preference('default_icon', 'plugin-rad_search');

  osc_delete_preference('map_type_home', 'plugin-rad_search');
  osc_delete_preference('map_width_home', 'plugin-rad_search');
  osc_delete_preference('map_height_home', 'plugin-rad_search');
  osc_delete_preference('only_premium_home', 'plugin-rad_search');

  osc_delete_preference('map_type_search', 'plugin-rad_search');
  osc_delete_preference('map_width_search', 'plugin-rad_search');
  osc_delete_preference('map_height_search', 'plugin-rad_search');
  osc_delete_preference('only_premium_search', 'plugin-rad_search');

  osc_delete_preference('map_type_item', 'plugin-rad_search');
  osc_delete_preference('map_width_item', 'plugin-rad_search');
  osc_delete_preference('map_height_item', 'plugin-rad_search');
  osc_delete_preference('only_premium_item', 'plugin-rad_search');

  osc_delete_preference('map_type_publish', 'plugin-rad_search');
  osc_delete_preference('map_width_publish', 'plugin-rad_search');
  osc_delete_preference('map_height_publish', 'plugin-rad_search');
  osc_delete_preference('map_zoom_publish', 'plugin-rad_search');
  osc_delete_preference('map_lat_publish', 'plugin-rad_search');
  osc_delete_preference('map_lng_publish', 'plugin-rad_search');
}



// HELP FUNCTIONS
if(!function_exists('radius_param_update')) {
  function radius_param_update( $param_name, $update_param_name, $type = NULL ) {
  
    $val = '';
    if( $type == 'check') {

      // Checkbox input
      if( Params::getParam( $param_name ) == 'on' ) {
        $val = 1;
      } else {
        if( Params::getParam( $update_param_name ) == 'done' ) {
          $val = 0;
        } else {
          $val = ( osc_get_preference( $param_name, 'plugin-rad_search' ) != '' ) ? osc_get_preference( $param_name, 'plugin-rad_search' ) : '';
        }
      }
    } else {

      // Other inputs (text, password, ...)
      if( Params::getParam( $update_param_name ) == 'done' && Params::existParam($param_name)) {
        $val = Params::getParam( $param_name );
      } else {
        $val = ( osc_get_preference( $param_name, 'plugin-rad_search') != '' ) ? osc_get_preference( $param_name, 'plugin-rad_search' ) : '';
      }
    }


    // If save button was pressed, update param
    if( Params::getParam( $update_param_name ) == 'done' ) {

      if(osc_get_preference( $param_name ) == '') {
        osc_set_preference( $param_name, $val, 'plugin-rad_search', 'STRING');  
      } else {
        $dao_preference = new Preference();
        $dao_preference->update( array( "s_value" => $val ), array( "s_section" => "plugin-rad_search", "s_name" => $param_name ));
        osc_reset_preferences();
        unset($dao_preference);
      }
    }

    return $val;
  }
}


if(!function_exists('message_ok')) {
  function message_ok( $text ) {
    $final  = '<div class="flashmessage flashmessage-ok flashmessage-inline">';
    $final .= $text;
    $final .= '</div>';
    echo $final;
  }
}

if(!function_exists('message_error')) {
  function message_error( $text ) {
    $final  = '<div class="flashmessage flashmessage-error flashmessage-inline">';
    $final .= $text;
    $final .= '</div>';
    echo $final;
  }
}



// ADMIN MENU
function radius_menu($title = NULL) {
  echo '<link href="' . osc_base_url() . 'oc-content/plugins/rad_search/css/admin.css" rel="stylesheet" type="text/css" />';
  echo '<link href="' . osc_base_url() . 'oc-content/plugins/rad_search/css/bootstrap-switch.css" rel="stylesheet" type="text/css" />';
  echo '<link href="' . osc_base_url() . 'oc-content/plugins/rad_search/css/tipped.css" rel="stylesheet" type="text/css" />';
  echo '<link href="//fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />';
  echo '<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />';
  echo '<script src="' . osc_base_url() . 'oc-content/plugins/rad_search/js/admin.js"></script>';
  echo '<script src="' . osc_base_url() . 'oc-content/plugins/rad_search/js/tipped.js"></script>';
  echo '<script src="' . osc_base_url() . 'oc-content/plugins/rad_search/js/bootstrap-switch.js"></script>';

  if( $title == '') { $title = __('Configure', 'rad_search'); }

  $text  = '<div class="mb-head">';
  $text .= '<div class="mb-head-left">';
  $text .= '<h1>' . $title . '</h1>';
  $text .= '<h2>Radius Search Plugin</h2>';
  $text .= '</div>';
  $text .= '<div class="mb-head-right">';
  $text .= '<ul class="mb-menu">';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/general_settings.php"><i class="fa fa-wrench"></i><span>' . __('Global Settings', 'rad_search') . '</span></a></li>';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/map_settings.php"><i class="fa fa-map-o"></i><span>' . __('Map Settings', 'rad_search') . '</span></a></li>';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/manual_coordinates_upload.php"><i class="fa fa-cloud-upload"></i><span>' . __('Coords Upload', 'rad_search') . '</span></a></li>';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/list_items.php"><i class="fa fa-list"></i><span>' . __('Item Coords', 'rad_search') . '</span></a></li>';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/list_cities.php"><i class="fa fa-building-o"></i><span>' . __('City Coords', 'rad_search') . '</span></a></li>';
  $text .= '<li><a href="' . osc_base_url() . 'oc-admin/index.php?page=plugins&action=renderplugin&file=rad_search/admin/category_icons.php"><i class="fa fa-folder-o"></i><span>' . __('Category Icons', 'rad_search') . '</span></a></li>';
  $text .= '</ul>';
  $text .= '</div>';
  $text .= '</div>';

  echo $text;
}



// ADMIN FOOTER
function radius_footer() {
  $pluginInfo = osc_plugin_get_info('rad_search/index.php');
  $text  = '<div class="mb-footer">';
  $text .= '<a target="_blank" class="mb-developer" href="http://mb-themes.com"><img src="http://mb-themes.com/favicon.ico" alt="MB Themes" /> MB-Themes.com</a>';
  $text .= '<a target="_blank" href="' . $pluginInfo['support_uri'] . '"><i class="fa fa-bug"></i> ' . __('Report Bug', 'rad_search') . '</a>';
  $text .= '<a target="_blank" href="http://forums.mb-themes.com/"><i class="fa fa-comments"></i> ' . __('Support Forums', 'rad_search') . '</a>';
  $text .= '<a target="_blank" class="mb-last" href="mailto:info@mb-themes.com"><i class="fa fa-envelope"></i> ' . __('Contact Us', 'rad_search') . '</a>';
  $text .= '<span class="mb-version">v' . $pluginInfo['version'] . '</span>';
  $text .= '</div>';

  return $text;
}



// ADD MENU LINK TO PLUGIN LIST
function radius_admin_menu() {
echo '<h3><a href="#">Radius Search</a></h3>
<ul> 
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/general_settings.php') . '">&raquo; ' . __('Global Settings', 'rad_search') . '</a></li>
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/map_settings.php') . '">&raquo; ' . __('Map Settings', 'rad_search') . '</a></li>
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/manual_coordinates_upload.php') . '">&raquo; ' . __('Coords Upload', 'rad_search') . '</a></li>
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/list_items.php') . '">&raquo; ' . __('Item Coords', 'rad_search') . '</a></li>
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/list_cities.php') . '">&raquo; ' . __('City Coords', 'rad_search') . '</a></li>
  <li><a style="color:#2eacce;" href="' . osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/category_icons.php') . '">&raquo; ' . __('Category Icons', 'rad_search') . '</a></li>
</ul>';
}



// GET COORDINATES BY ENTERED ADDRESS FROM GOOGLE API
function getCordByAddress( $address ) {
  $address = str_replace(" ", "+", $address);
  $url = 'https://maps.google.com/maps/api/geocode/json?address=' . $address . '&sensor=false';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $response = curl_exec($ch);
  curl_close($ch);
  $response_a = json_decode($response);

  $lat = $response_a->results[0]->geometry->location->lat;
  $long = $response_a->results[0]->geometry->location->lng;

  if($lat == '') { $lat = 0; }
  if($long == '') { $long = 0; }

  return array( $lat, $long );
}



// GET CURRENT MEASURE USED IN PLUIGN
function getMeasure(){
  if(osc_get_preference('measure','plugin-rad_search') == '' ){
   return 'km';   // kilometers is default measure
  } else {
    return osc_get_preference('measure','plugin-rad_search');
  }
}



// FUNCTION TO CREATE RADIUS DISTANCE BOX
osc_enqueue_style('rad_search_form', osc_base_url() . 'oc-content/plugins/rad_search/css/distance_box.css');

function radius_distance_box() {
  $use_select = osc_get_preference('search_select', 'plugin-rad_search');

  $text  = '<div class="radius_clear"></div>';
  $text  = '<div class="rad_search_box" title="' . __('You can search in distance of city you have entered in search', 'rad_search') . '">';
  $text .= '<label for="radius_size" class="radius_text">' . __('Distance', 'rad_search') . '</label>';

  if($use_select == 1) {
    $text .= '<select name="radius_size" id="radius_size">';
    $text .= '<option value=""' . (Params::getParam('radius_size') == '' ? 'selected="selected"' : '') . '>-</option>';
    $text .= '<option value="1"' . (Params::getParam('radius_size') == 1 ? 'selected="selected"' : '') . '>1 ' . getMeasure() . '</option>';
    $text .= '<option value="5"' . (Params::getParam('radius_size') == 5 ? 'selected="selected"' : '') . '>5 ' . getMeasure() . '</option>';
    $text .= '<option value="10"' . (Params::getParam('radius_size') == 10 ? 'selected="selected"' : '') . '>10 ' . getMeasure() . '</option>';
    $text .= '<option value="20"' . (Params::getParam('radius_size') == 20 ? 'selected="selected"' : '') . '>20 ' . getMeasure() . '</option>';
    $text .= '<option value="50"' . (Params::getParam('radius_size') == 50 ? 'selected="selected"' : '') . '>50 ' . getMeasure() . '</option>';
    $text .= '<option value="100"' . (Params::getParam('radius_size') == 100 ? 'selected="selected"' : '') . '>100 ' . getMeasure() . '</option>';
    $text .= '<option value="200"' . (Params::getParam('radius_size') == 200 ? 'selected="selected"' : '') . '>200 ' . getMeasure() . '</option>';
    $text .= '<option value="500"' . (Params::getParam('radius_size') == 500 ? 'selected="selected"' : '') . '>500 ' . getMeasure() . '</option>';
    $text .= '<option value="1000"' . (Params::getParam('radius_size') == 1000 ? 'selected="selected"' : '') . '>1000 ' . getMeasure() . '</option>';
    $text .= '<option value="5000"' . (Params::getParam('radius_size') == 5000 ? 'selected="selected"' : '') . '>5000 ' . getMeasure() . '</option>';
    $text .= '</select>';
  } else {
    $text .= '<input value="' . Params::getParam('radius_size') . '" type="number" min="0" step="1" id="radius_size" name="radius_size" placeholder="-" />';
    $text .= '<span class="radius_desc">' . __(getMeasure(), 'rad_search') . '</span>';
  }

  $text .= '</div>';
  return $text;
}



// ADD RADIUS DISTANCE INPUT TO SEARCH
function rad_search_form() {
  if(osc_get_preference('hook', 'plugin-rad_search') == 1) {
    echo radius_distance_box();
  }
}

osc_add_hook('search_form', 'rad_search_form');



// GET LATITUDE AND LONGITUDE OF CITY IN CURRENT SEARCH PARAMS
function getLatLongOfSearch($params) {
  // GET REGION, FIND NAME & ID
  $reg_id = trim( Params::getParam('region') );

  if($reg_id == '') {
    $reg_id = trim( strtolower(Params::getParam('sRegion')) );
  }

  if(is_numeric($reg_id) and $reg_id > 0) {
    $reg = Region::newInstance()->findByPrimaryKey($reg_id);
    $reg = strtolower($reg['s_name']);
  } else {
    $reg = $reg_id;
  }

  // GET CITY, FIND NAME & ID
  $city_id = trim( Params::getParam('city') );

  if($city_id == '') {
    $city_id = trim( strtolower(Params::getParam('sCity')) );
  }

  if(is_numeric($city_id) and $city_id > 0) {
    $city = City::newInstance()->findByPrimaryKey($city_id);
    $city = strtolower($city['s_name']); 
  } else {
    $city = $city_id;
  }

  // TRY TO FIND CITY
  if(is_numeric($city_id) and $city_id > 0) {
    $try = ModelRadius::newInstance()->getCordByCity($city_id);
  } else {
    if($city <> '' and $reg <> '') {
      $try = ModelRadius::newInstance()->getCordByCityRegion($city, $reg);
      ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action successful - coordinates found using region & city name');
    } else {
      if($city <> '') {
        $try = ModelRadius::newInstance()->getCordByCityName($city);
        ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action successful - coordinates found using city name');
      } else {
        $try = false;
        ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action unsuccessful - not possible to find coordinates in database');
      }
    }
  }

  if(isset($try['city_id'])) {

    // CITY FOUND IN DATABASE, GET COORDINATES FROM DB
    $lat = $try['latitude'];
    $long = $try['longitude'];
  } else {

    // CITY NOT FOUND IN DB, USE GOOGLE API
    $add = $reg . ' ' . $city . ' ' . Params::getParam('city_area');
    $loc = getCordByAddress( trim($add) );
    $lat = $loc[0];
    $long = $loc[1];
    ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], 'action successful - coordinates found using Google API');
  }

  return array('lat' => $lat, 'long' => $long);
}



// ADD CITIES INTO SEARCH THAT ARE IN RADIUS OF SEARCHED ONE
function rad_search_conditions($params) {
  $s_city = Params::getParam('city') <> '' ? Params::getParam('city') : Params::getParam('sCity');
  $s_region = Params::getParam('region') <> '' ? Params::getParam('region') : Params::getParam('sRegion');
  $s_country = Params::getParam('country') <> '' ? Params::getParam('country') : Params::getParam('sCountry');
  
  $city_id = radius_get_city_id($s_city, $s_region, $s_country);
  $cords = ModelRadius::newInstance()->getCordByCity($city_id);

  $lat = isset($cords['latitude']) ? $cords['latitude'] : '';
  $long = isset($cords['longitude']) ? $cords['longitude'] : '';

  $multiplicator = 1;
  if(getMeasure() == 'mi') { 
    $multiplicator = 1.609344000000865;  // 1 km = 1.6 mile
  } else { 
    $multiplicator = 1; // 1 km = 1 km
  }

  $distance = Params::getParam('radius_size');
  $distance = $distance * $multiplicator;
  $point = 1/360*40075.16;
  $rad = $distance/$point;

  $cities_list_clean = array();
  $regions_list_clean = array();
  $countries_list_clean = array();

  if( Params::getParam('radius_size') <> '' and Params::getParam('radius_size') > 0) {

    // FIND CITIES IN RADIUS
    $location_list = ModelRadius::newInstance()->getCitiesInRadius($lat, $long, $rad);

    foreach($location_list as $c) {
      $cities_list_clean[] = $c['city_id'];
      $regions_list_clean[] = $c['region_id'];
      $countries_list_clean[] = $c['country_code'];
    }

    // ADD CITIES, REGIONS & COUNTRIES TO SEARCH
    Search::newInstance()->addCity($cities_list_clean);
    Search::newInstance()->addRegion($regions_list_clean);
    Search::newInstance()->addCountry($countries_list_clean);
  }

  return $cities_list_clean;
}



// FIND CITY ID IN DATABASE BY COUNTRY, REGION & CITY NAME
function radius_get_city_id($city, $region = null, $country = null) {
  if(is_numeric($city) and $city > 0) {
    return $city;
  }

  if(is_numeric($region) and $region > 0) {
    $result = City::newInstance()->findByName($city, $region);
    return $result['pk_i_id'];
  }

  $result = ModelRadius::newInstance()->getCityId($city, $region, $country);
  return $result;
}



// FIND AND UPDATE COORDINATES OF LISTING ON ADD/EDIT
function radius_item_add_edit_post($item) {
  $only_google_api = osc_get_preference('only_google_api', 'plugin-rad_search') <> '' ? osc_get_preference('only_google_api', 'plugin-rad_search') : 0;
  $has_city = false;

  // CHECK IF WE GOT COORDINATES FROM PUBLISH PAGE
  $posted_lat = Params::getParam('rad_lat');
  $posted_long = Params::getParam('rad_lng');

  if($posted_lat <> '' and $posted_long <> '') {
    $posted = true;
  } else {
    $posted = false;
  }

  if($item['fk_i_category_id'] != null) {
    // CHECK IF COORDINATES ARE IN DATABASE
    $detail = ModelRadius::newInstance()->getCordByItemId( $item['pk_i_id'] );

    $address = $item['s_country'] . ' ' . $item['s_city'] . ' ' . $item['s_region'];

    if(!is_numeric($item['s_city_area'] && $item['s_city_area'] <> '')) {
      $address .= ' ' . $item['s_city_area'];
    }

    if(!is_numeric($item['s_address']) && $item['s_address'] <> '') {
      $address .= ' ' . $item['s_address'];
    }

    if($detail['latitude'] == 0 or $detail['latitude'] == '' or $detail['longitude'] == 0 or $detail['longitude'] == '') { 
      $run = true; 
    } else { 
      $run = false; 
    }


    if($only_google_api == 0 && !$posted) {
      $reg = trim(strtolower( $item['s_region'] ));
      $city = trim(strtolower( $item['s_city'] ));
    
      $try = ModelRadius::newInstance()->getCordByCityRegion($city, $reg);

      if(!isset($try['city_id'])) {
        $try = ModelRadius::newInstance()->getCordByCityName($city);
      } 

      if(isset($try['city_id']) and $try['latitude'] <> '' and $try['latitude'] <> 0 and $try['longitude'] <> '' and $try['longitude'] <> 0) {
        $lat = $try['latitude'];
        $long = $try['longitude'];
        $has_city = true;
        $status = 'action successful - cords found in database';
      }
    }

    if( $run) {
      if($posted) {
        $lat = $posted_lat;
        $long = $posted_long;
        $status = 'action successful - coordinates from map';
      } else {
        if(!$has_city) {
          $cord = getCordByAddress( $address );
          $lat = $cord[0];
          $long = $cord[1];
          $status = 'action successful - listing publish via Google API';
        }
      }

      // UPDATE LISTING CORDS
      if(isset($detail['item_id'])) {
        ModelRadius::newInstance()->updateCord( $item['pk_i_id'], $long, $lat ); 
      } else {
        ModelRadius::newInstance()->insertCord( $item['pk_i_id'], $long, $lat ); 
      }

      // FIND ITEM CITY AND IF NO COORDINATES HERE, UPDATE IT
      $c_city = $item['fk_i_city_id'] <> '' ? $item['fk_i_city_id'] : $item['s_city'];
      $c_region = $item['fk_i_region_id'] <> '' ? $item['fk_i_region_id'] : $item['s_region'];
      $c_country = $item['fk_c_country_code'] <> '' ? $item['fk_c_country_code'] : $item['s_country'];

      $city_id = radius_get_city_id($c_city, $c_region, $c_country);

      $city_cord = ModelRadius::newInstance()->getCordByCity($city_id);

      if($city_cord['latitude'] == '' or $city_cord['latitude'] == 0 or $city_cord['longitude'] == '' or $city_cord['longitude'] == '') {
        ModelRadius::newInstance()->updateCityCord( $city_id, $long, $lat );
        ModelRadius::newInstance()->updateCityStatus( $city_id, 'action successful - in new listing publish via Google API');
      }
    }
  }

  ModelRadius::newInstance()->updateItemStatus( $item['pk_i_id'], $status);

}



// CATEGORY LIST FOR CATEGORY ICONS UPLOAD
function radius_has_subcategories($categories, $deep = 0) {
  foreach($categories as $c) {
    echo '<div class="mb-table-row' . ($deep == 0 ? ' parent' : '') . '">';
    echo '<div class="mb-col-1">' . $c['pk_i_id'] . '</div>';
    echo '<div class="mb-col-3 mb-align-left sub' . $deep . '">' . $c['s_name'] . '</div>';

    if (file_exists(osc_base_path() . 'oc-content/plugins/rad_search/icons/' . $c['pk_i_id'] . '.png')) { 
      echo '<div class="mb-col-2"><img src="' . osc_base_url() . 'oc-content/plugins/rad_search/icons/img_yes.png" alt="Has icon" /></div>';  
    } else {
      echo '<div class="mb-col-2"><img src="' . osc_base_url() . 'oc-content/plugins/rad_search/icons/img_no.png" alt="Has not icon"/></div>';  
    }

    if (file_exists(osc_base_path() . 'oc-content/plugins/rad_search/icons/' . $c['pk_i_id'] . '.png')) { 
      echo '<div class="mb-col-2 uploaded-icon"><img src="' . osc_base_url() . 'oc-content/plugins/rad_search/icons/' . $c['pk_i_id'] . '.png" /></div>';  
    } else {
      echo '<div class="mb-col-2">&nbsp;</div>';  
    }

    echo '<div class="mb-col-2"><a class="add_img" id="icon' . $c['pk_i_id'] . '" href="#">' . __('Add icon', 'rad_search') . '</a></div>';
    echo '<div class="mb-col-2"><a class="remove" href="' . osc_admin_base_url(true) . '?page=plugins&action=renderplugin&file=rad_search/admin/category_icons.php&remove_cat=' . $c['pk_i_id'] . '">' . __('Remove', 'rad_search') . '</a></div>';
    echo '</div>';

    // ALLOW ICONS FOR FIRST-LEVEL CATEGORIES ONLY (MAIN)
    // if(isset($c['categories']) && is_array($c['categories']) && !empty($c['categories'])) {
    //   osc_has_subcategories_special($c['categories'], $deep+1);
    // }   
  }
}



// TEST IF GOOGLE API IS NOT OVERLOADED
function radius_check_google_api() {
  $address = 'London';
  $json = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.rawurlencode($address).'&sensor=false');

  if (strpos($json,'error') == false) { 
    return true;
  } else {
    return false;
  }
}


// FIND ROOT CATEGORY
function radius_root_category($cat_id) {
  $cat = Category::newInstance()->findRootCategory($cat_id);
  return $cat['pk_i_id'];
}



// HOOK TO GET COORDINATES OF LISTING WHILE ADD/EDIT
osc_add_hook('posted_item', 'radius_item_add_edit_post');
osc_add_hook('edited_item', 'radius_item_add_edit_post');



// CREATE MAP WITH LISTINGS
osc_enqueue_style('radius_map', osc_base_url() . 'oc-content/plugins/rad_search/css/listing_map.css');

function radius_map_items() {
  $global_items = View::newInstance()->_get('items');
  $global_item = View::newInstance()->_get('item');

  include_once 'listings_map.php';

  View::newInstance()->_exportVariableToView('items', $global_items);
  View::newInstance()->_exportVariableToView('item', $global_item);
}



// ADD MAP WITH LISTINGS TO ITEM PAGE
if(osc_get_preference('map_hook', 'plugin-rad_search') == 1) {
  osc_add_hook('item_detail', 'radius_map_items');
}



// ADD MAP TO PUBLISH PAGE
osc_enqueue_style('radius_map_publish', osc_base_url() . 'oc-content/plugins/rad_search/css/publish_map.css');

function radius_map_publish() {
  include_once 'publish_map.php';
}

function radius_map_publish_js() {
  if(osc_is_publish_page()) {
    osc_register_script('radius_map_publish_js', 'https://maps.googleapis.com/maps/api/js?v=3');
    osc_enqueue_script('radius_map_publish_js');
  }
}

osc_add_hook('header', 'radius_map_publish_js');



// ADD MENU TO PLUGINS MENU LIST
osc_add_hook('admin_menu','radius_admin_menu', 1);



// DISPLAY CONFIGURE LINK IN LIST OF PLUGINS
function radius_conf() {
  osc_admin_render_plugin( osc_plugin_path( dirname(__FILE__) ) . '/admin/general_settings.php' );
}

osc_add_hook( osc_plugin_path( __FILE__ ) . '_configure', 'radius_conf' );	


// CALL WHEN PLUGIN IS ACTIVATED - INSTALLED
osc_register_plugin(osc_plugin_path(__FILE__), 'radius_call_after_install');

// SHOW UNINSTALL LINK
osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', 'radius_call_after_uninstall');

// ADD NEW SEARCH CONDITIONS
osc_add_hook('search_conditions', 'rad_search_conditions');
?>