$(document).ready(function(){

  // HELP TOPICS
  $('#mb-help > .mb-inside > .mb-row.mb-help > div').each(function(){
    var cl = $(this).attr('class');
    $('label.' + cl + ' span').addClass('mb-has-tooltip').prop('title', $(this).text());
  });

  $('.mb-row label').click(function() {
    var cl = $(this).attr('class');
    var pos = $('#mb-help > .mb-inside > .mb-row.mb-help > div.' + cl).offset().top - $('.navbar').outerHeight() - 12;;
    $('html, body').animate({
      scrollTop: pos
    }, 1400, function(){
      $('#mb-help > .mb-inside > .mb-row.mb-help > div.' + cl).addClass('mb-help-highlight');
    });

    return false;
  });


  // ON-CLICK ANY ELEMENT REMOVE HIGHLIGHT
  $('body, body *').click(function(){
    $('.mb-help-highlight').removeClass('mb-help-highlight');
  });


  // GENERATE TOOLTIPS
  Tipped.create('.mb-has-tooltip', { maxWidth: 200, radius: false });


  // CHECKBOX & RADIO SWITCH
  $.fn.bootstrapSwitch.defaults.size = 'small';
  $.fn.bootstrapSwitch.defaults.labelWidth = '0px';
  $.fn.bootstrapSwitch.defaults.handleWidth = '50px';

  $(".element-slide").bootstrapSwitch();



  // MAP SETTINGS - HIDE SOME BLOCKS ON SELECT
  if($('select[name="map_zoom"]').val() != 0) {
    $('input[name="def_lat"], input[name="def_long"]').attr('disabled', false);
    $('label[for="def_lat"], label[for="def_long"]').removeClass('disable');
  } else {
    $('input[name="def_lat"], input[name="def_long"]').attr('disabled', true);
    $('label[for="def_lat"], label[for="def_long"]').addClass('disable');
  }

  $('select[name="map_zoom"]').change(function(){
    if($('select[name="map_zoom"]').val() != 0) {
      $('input[name="def_lat"], input[name="def_long"]').attr('disabled', false);
      $('label[for="def_lat"], label[for="def_long"]').removeClass('disable');
    } else {
      $('input[name="def_lat"], input[name="def_long"]').attr('disabled', true);
      $('label[for="def_lat"], label[for="def_long"]').addClass('disable');
    }
  });



  // SHOW POSITION OF CITY/LISTING ON HOVER IN OC-ADMIN
  $('.radius_list .map').hover(function(){
    $('#radius-admin-map').fadeIn(200);
    $('#radius-admin-map').css('left', $(this).position().left - 415 + 'px');
    $('#radius-admin-map').css('top', $(this).position().top - 140 + 'px');
    $('#radius-admin-map').find('img').prop('src', $(this).attr('rel'));
  }, function(){
    $('#radius-admin-map').stop(true, true).fadeOut(200);
  });



  // CATEGORY IMAGES UPLOAD
  $('.add_img').click(function() {
    var id = $(this).attr('id');
    $(this).parent().html('<input type="file" name="' + id + '" />');
    return false;
  });

});