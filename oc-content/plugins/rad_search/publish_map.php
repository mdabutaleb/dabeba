<?php 
  $publish_map_type = osc_get_preference('map_type_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_type_publish', 'plugin-rad_search') : 'ROADMAP';
  $publish_map_width = osc_get_preference('map_width_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_width_publish', 'plugin-rad_search') : '100%';
  $publish_map_height = osc_get_preference('map_height_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_height_publish', 'plugin-rad_search') : '300px';
  $publish_map_zoom = osc_get_preference('map_zoom_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_zoom_publish', 'plugin-rad_search') : 12;
  $publish_map_lat = osc_get_preference('map_lat_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_lat_publish', 'plugin-rad_search') : '40.7033127';
  $publish_map_lng = osc_get_preference('map_lng_publish', 'plugin-rad_search') <> '' ? osc_get_preference('map_lng_publish', 'plugin-rad_search') : '-73.979681';

  // MAKE SURE UNIT IS ENTERED
  if (strpos($publish_map_width, '%') === false && strpos($publish_map_width, 'px') === false) {
    $publish_map_width = $publish_map_width . 'px';
  }

  if (strpos($publish_map_height, '%') === false && strpos($publish_map_height, 'px') === false) {
    $publish_map_height = $publish_map_height . 'px';
  }
?>

<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3"></script>-->

<div id="publish-map-top">
  <input type="text" id="selected_loc" size="30" value="<?php echo osc_esc_html(__('Click on map to mark location of your item', 'rad_search')); ?>">
  <span onclick="PublishMyPosition();" id="detection-result" class="radius_publish_locate"><?php _e('Show my position', 'rad_search'); ?></span>
</div>

<input type="hidden" id="lat" name="rad_lat" size="10">
<input type="hidden" id="lng" name="rad_lng" size="10">
<input type="hidden" id="rad_country" name="country">
<input type="hidden" id="rad_region" name="region">
<input type="hidden" id="rad_city" name="city">
<input type="hidden" id="rad_address" name="address">
<input type="hidden" id="rad_zip" name="s_zip">

<div style="width:<?php echo $publish_map_width; ?>;height:<?php echo $publish_map_height; ?>" id="map_publish_canvas"></div>



<script type="text/javascript">
  // DETECT USER POSITION AND MOVE MAP TO IT
  function PublishMyPosition() {
    var output = document.getElementById("detection-result");

    if (!navigator.geolocation){
      output.innerHTML = "<?php echo osc_esc_html(__('Geolocation is not supported by your browser', 'rad_search')); ?>";
      return;
    }

    function success(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;

      output.innerHTML = "<?php echo osc_esc_html(__('Done!', 'rad_search')); ?>";

      $('#map_publish_canvas').fadeOut(200);
      initialize(latitude, longitude, 1);
      $('#map_publish_canvas').fadeIn(200);

      setTimeout(function(){ 
        $(output).fadeOut(150, function() { 
          output.innerHTML = "<?php echo osc_esc_html(__('Show my position', 'rad_search')); ?>"; 
          $(output).fadeIn(150); 
        }) 
      }, 5000);
    };

    function error() {
      output.innerHTML = "<?php echo osc_esc_html(__('Unable to retrieve your location', 'rad_search')); ?>";
      $('#detection-result').addClass('error');
    };

    output.innerHTML = "<?php echo osc_esc_html(__('Locating...', 'rad_search')); ?>";
    
    navigator.geolocation.getCurrentPosition(success, error);
  }


  // CHECK FOR EXISTING LOCATION FIELDS
  $(document).ready(function(){
    if($('input[name="country"]:not(#rad_country)').length) {
      $('#rad_country').remove();
    }

    if($('input[name="region"]:not(#rad_region)').length) {
      $('#rad_region').remove();
    }

    if($('input[name="city"]:not(#rad_city)').length) {
      $('#rad_city').remove();
    }

    if($('input[name="address"]:not(#rad_address)').length) {
      $('#rad_address').remove();
    }

    if($('input[name="s_zip"]:not(#rad_zip)').length) {
      $('#rad_zip').remove();
    }
  });

  var map;
  var geocoder;
  var myOptions;

  function initialize(def_lat, def_long, extra) {
    if(def_lat != 0 && def_lat != '' && typeof def_lat !== 'undefined' && def_lat.isNumeric) {
      var center_lat = def_lat;
    } else {
      var center_lat = <?php echo $publish_map_lat; ?>;
    }

    if(def_long != 0 && def_long != '' && typeof def_long !== 'undefined' && def_long.isNumeric) {
      var center_long = def_long;
    } else {
      var center_long = <?php echo $publish_map_lng; ?>;
    }



    var myOptions = {
      center: new google.maps.LatLng(center_lat, center_long ),
      zoom: <?php echo $publish_map_zoom; ?>,
      mapTypeId: google.maps.MapTypeId.<?php echo $publish_map_type; ?>
    };

    geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById("map_publish_canvas"), myOptions);
    google.maps.event.addListener(map, 'click', function(event) {
      placeMarker(event.latLng);
    });

    var marker;
    function placeMarker(location) {
      if(marker){
        marker.setPosition(location);
      } else {
        marker = new google.maps.Marker({
          position: location, 
          map: map
        });
      }
      
      document.getElementById('lat').value=location.lat();
      document.getElementById('lng').value=location.lng();
      getAddress(location);
    }


    if(extra == 1) {
      var marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(center_lat, center_long),
        title: '<?php echo osc_esc_js(__('Your current position', 'rad_search')); ?>',
        icon: '<?php echo osc_base_url() . 'oc-content/plugins/rad_search/images/marker-red.png'; ?>'
      });
    }

    function getAddress(latLng) {
      geocoder.geocode( {'latLng': latLng}, function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          if(results[0]) {
            document.getElementById("selected_loc").value = results[0].formatted_address;

            var rad_address = results[0].formatted_address;
            
            var indice=0;
            for (var j=0; j<results.length; j++) {
              if (results[j].types[0]=='locality') {
                  indice=j;
                  break;
              }
            }


            // GET REGION
            if(typeof results[j] !== 'undefined') {
              if(typeof results[j].address_components !== 'undefined') {
                for (var i=0; i<results[j].address_components.length; i++) {
                  if (results[j].address_components[i].types[0] == "administrative_area_level_1") {
                    var rad_region = results[j].address_components[i].long_name;
                  }
                }
              }
            }


            // GET COUNTRY, CITY, STREET, STREET NUMBER
            if(typeof results[0] !== undefined) {
              if(typeof results[0].address_components !== undefined) {
                for (var i=0; i<results[0].address_components.length; i++) {
                  if (results[0].address_components[i].types[0] == "locality") {
                    var rad_city = results[0].address_components[i].long_name;
                  }
              
                  if (results[0].address_components[i].types[0] == "country") {
                    var rad_country = results[0].address_components[i].long_name;
                    var rad_country_short = results[0].address_components[i].short_name;
                  }

                  if (results[0].address_components[i].types[0] == "route") {
                    var rad_street = results[0].address_components[i].long_name;
                  }

                  if (results[0].address_components[i].types[0] == "street_number") {
                    var rad_street_number = results[0].address_components[i].long_name;
                  }

                  if (results[0].address_components[i].types[0] == "postal_code") {
                    var rad_zip = results[0].address_components[i].long_name;
                  }
                }
              }
            }

            if(typeof rad_country === 'undefined') { rad_country = ''; }
            if(typeof rad_region === 'undefined') { rad_region = ''; }
            if(typeof rad_city === 'undefined') { rad_city = ''; }
            if(typeof rad_street === 'undefined') { rad_street = ''; }
            if(typeof rad_street_number === 'undefined') { rad_street_number = ''; }
            if(typeof rad_zip === 'undefined') { rad_zip = ''; }


            // SET LOCATION VALUES TO FIELDS
            if($('select#countryId').length) {
              $('select#countryId option[value=' + rad_country_short + ']').attr('selected','selected');
              $('select#countryId').parent().find('span').text($('select#countryId').find('option:selected').text());
            }

            $('input[name="country"]').val(rad_country);
            $('input[name="region"]').val(rad_region);
            $('input[name="city"]').val(rad_city);
            $('input[name="address"]').val(rad_street + ' ' + rad_street_number);
            $('input[name="s_zip"]').val(rad_zip);

          } else {
            document.getElementById("selected_loc").value = "<?php echo osc_esc_html(__('No results', 'rad_search')); ?>";
          }
        } else {
          if(status == 'OVER_QUERY_LIMIT') {
            status = "<?php echo osc_esc_html(__('You click too much!', 'rad_search')); ?>";
          }

          document.getElementById("selected_loc").value = status;
        }
      });
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);
</script>