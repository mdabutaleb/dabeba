<?php

class ModelRadius extends DAO {
private static $instance;

public static function newInstance() {
  if( !self::$instance instanceof self ) {
    self::$instance = new self ;
  }
  return self::$instance ;
}

function __construct() {
  parent::__construct();
}

public function import($file) {
  $path = osc_plugin_resource($file);
  $sql = file_get_contents($path);
  if(!$this->dao->importSQL($sql)){ throw new Exception("Error importSQL::ModelRadius<br>".$file.'<br>'.$path.'<br><br>Please check your database for tables t_item_radius, t_cord_radius, t_cord_validate_radius and t_city_radius. <br>If any of those tables exists in your database, drop them!');} 
  $this->dao->query('ALTER TABLE ' . $this->getTable_CityRadius() . ' ADD COLUMN rownum_id INT AUTO_INCREMENT UNIQUE FIRST');
}
 
public function uninstall() {
  $this->dao->query('DROP TABLE '. $this->getTable_CityRadius());
  $this->dao->query('DROP TABLE '. $this->getTable_Radius());
  $this->dao->query('DROP TABLE '. $this->getTable_Cords());
  $this->dao->query('DROP TABLE '. $this->getTable_CordsValidate());
}

public function getTable_Radius() {
  return DB_TABLE_PREFIX.'t_item_radius' ;
}

public function getTable_CityRadius() {
  return DB_TABLE_PREFIX.'t_city_radius' ;
}

public function getTable_Cords() {
  return DB_TABLE_PREFIX.'t_cords_radius' ;
}

public function getTable_CordsValidate() {
  return DB_TABLE_PREFIX.'t_cords_validate_radius' ;
}

public function insertCordRaw( $country_code, $region_name, $city_name, $long, $lat ) {
  $aSet = array(
    'longitude' => trim($long),
    'latitude' => trim($lat),
    'country_code' => trim($country_code),
    'region_name' => trim($region_name),
    'city_name' => trim($city_name)
  );

  return $this->dao->insert( $this->getTable_CordsValidate(), $aSet);
}

public function validateData() {
  $this->dao->query('DELETE FROM ' . $this->getTable_Cords()); 
  $this->dao->query('INSERT INTO ' . $this->getTable_Cords() . ' (country_code, region_name, city_name, latitude, longitude) SELECT DISTINCT * FROM ' . $this->getTable_CordsValidate() . ' GROUP BY country_code, region_name, city_name'); 
  return $this->dao->query('DELETE FROM ' . $this->getTable_CordsValidate());  
}

public function deleteCordFromHelp() {
  return $this->dao->query('DELETE FROM '. $this->getTable_CordsValidate());  
}

public function getCordByItemId( $item_id ) {
  $this->dao->select();
  $this->dao->from( $this->getTable_Radius() );
  $this->dao->where('item_id', $item_id );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function getCordByCity( $city_id) {
  $this->dao->select();
  $this->dao->from( $this->getTable_CityRadius() );
  $this->dao->where('city_id', $city_id );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function copyCitiesToRadius() {
  return $this->dao->query('INSERT INTO ' . $this->getTable_CityRadius() . ' (city_id) SELECT c.pk_i_id FROM ' . DB_TABLE_PREFIX . 't_city c');
}

public function getCordByCityRegionRaw( $city_name, $region_name, $country_code ) {
  $this->dao->select();
  $this->dao->from( $this->getTable_Cords() );
  $this->dao->where('city_name', $city_name );
  $this->dao->where('region_name', $region_name );
  $this->dao->where('country_code', $country_code );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function getCordByCityRegion( $city_name, $region_name ) {
  $this->dao->select('d.city_id, d.latitude, d.longitude');
  $this->dao->from( $this->getTable_CityRadius() . ' as d, ' . DB_TABLE_PREFIX.'t_city' . ' as c, ' . DB_TABLE_PREFIX.'t_region' . ' as r' );
  $this->dao->where('d.city_id = c.pk_i_id and c.fk_i_region_id = r.pk_i_id' );
  $this->dao->where('c.s_name', $city_name );
  $this->dao->where('r.s_name', $region_name );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function getCordByCityName( $city_name ) {
  $this->dao->select('d.city_id, d.latitude, d.longitude');
  $this->dao->from( $this->getTable_CityRadius() . ' as d, ' . DB_TABLE_PREFIX.'t_city' . ' as c' );
  $this->dao->where('d.city_id = c.pk_i_id' );
  $this->dao->where('c.s_name', $city_name );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function countCitiesAll($country_code = NULL) {
  $this->dao->select('COUNT(*) total_count');
  $this->dao->from( $this->getTable_CityRadius() . ' r, ' . DB_TABLE_PREFIX . 't_city c');

  if($country_code <> '') {
    $this->dao->where('c.fk_c_country_code', $country_code);
  }

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function countCitiesLeft($country = null) {
  if($country <> '') {
    $result = $this->dao->query('SELECT count(*) total_count FROM ' . DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX . 't_city_radius r where c.pk_i_id = r.city_id and c.fk_c_country_code = "' . $country . '" and (r.latitude is null or r.longitude is null)');
  } else {
    $result = $this->dao->query('SELECT count(*) total_count FROM ' . DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX . 't_city_radius r where c.pk_i_id = r.city_id and (r.latitude is null or r.longitude is null)');
  }

  if( !$result ) { return array(); }
  return $result->row();
}

public function countCitiesLeftZero($country = null) {
  if($country <> '') {
    $result = $this->dao->query('SELECT count(*) total_count FROM ' . DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX . 't_city_radius r where c.pk_i_id = r.city_id and c.fk_c_country_code = "' . $country . '" and (coalesce(r.latitude, 0) = 0 or coalesce(r.longitude, 0) = 0)');
  } else {
    $result = $this->dao->query('SELECT count(*) total_count FROM ' . DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX . 't_city_radius r where c.pk_i_id = r.city_id and (coalesce(r.latitude, 0) = 0 or coalesce(r.longitude, 0) = 0)');
  }

  if( !$result ) { return array(); }
  return $result->row();
}
public function countCities( ) {
  $this->dao->select('COUNT(*) total_count');
  $this->dao->from( $this->getTable_CityRadius() );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function countItems( ) {
  $this->dao->select('COUNT(*) total_count');
  $this->dao->from( DB_TABLE_PREFIX.'t_item' );

  $this->dao->where('b_active', 1);
  $this->dao->where('b_enabled', 1);
  $this->dao->where('b_spam', 0);

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function insertCord( $item_id, $long, $lat ) {
  $aSet = array(
    'longitude' => $long,
    'latitude' => $lat,
    'item_id' => $item_id
  );

  return $this->dao->insert( $this->getTable_Radius(), $aSet);
}

public function insertCityCord( $city_id, $long, $lat ) {
  $aSet = array(
    'longitude' => $long,
    'latitude' => $lat,
    'city_id' => $city_id
  );

  return $this->dao->insert( $this->getTable_CityRadius(), $aSet);
}

public function updateCord( $item_id, $long, $lat) {
  $aSet = array(
    'longitude' => $long,
    'latitude' => $lat,
    'item_id' => $item_id
  );

  $aWhere = array( 'item_id' => $item_id);

  return $this->_update($this->getTable_Radius(), $aSet, $aWhere);
}

public function updateCityCord( $city_id, $long, $lat) {
  $aSet = array(
    'longitude' => $long,
    'latitude' => $lat,
    'city_id' => $city_id
  );

  $aWhere = array( 'city_id' => $city_id);

  return $this->_update($this->getTable_CityRadius(), $aSet, $aWhere);
}

public function updateCityStatus( $city_id, $message) {
  $aSet = array('status' => $message);
  $aWhere = array('city_id' => $city_id);

  return $this->_update($this->getTable_CityRadius(), $aSet, $aWhere);
}

public function updateItemStatus( $item_id, $message) {
  $aSet = array('status' => $message);
  $aWhere = array('item_id' => $item_id);

  return $this->_update($this->getTable_Radius(), $aSet, $aWhere);
}

public function _update($table, $values, $where)   {
  $this->dao->from($table) ;
  $this->dao->set($values) ;
  $this->dao->where($where) ;
  return $this->dao->update() ;
}

public function deleteCord( $item_id ) {
  return $this->dao->delete($this->getTable_Radius(), array('item_id' => $item_id) ) ;
}

public function deleteCityCord( $city_id ) {
  return $this->dao->delete($this->getTable_CityRadius(), array('city_id' => $city_id) ) ;
}

public function getAllItemCord() {
  $this->dao->select();
  $this->dao->from( $this->getTable_Radius() );
  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();

  return $prepare;
}

public function getAllCityCord() {
  $this->dao->select();
  $this->dao->from( $this->getTable_CityRadius() );
  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();

  return $prepare;
}

public function getItems( $start = NULL, $num_list = NULL, $locale = null ) {
  $this->dao->select('i.*, d.*, @curRow:=@curRow+1 AS rownum');
  $this->dao->from( DB_TABLE_PREFIX.'t_item i, ' . DB_TABLE_PREFIX.'t_item_description d, (SELECT @curRow := 0) r' );


  if($start == '') { $start = 1; }
  if($num_list == '') { $num_list = 100; }
  $end = $start + $num_list*2 - 1;

  $this->dao->where('i.pk_i_id = d.fk_i_item_id');
  $this->dao->where('b_active', 1);
  $this->dao->where('b_enabled', 1);
  $this->dao->where('b_spam', 0);

  if(!is_null($locale)) {
    $this->dao->where('d.fk_c_locale_code', $locale);
  }

  $this->dao->having('rownum between ' . $start . ' and ' . $end);

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getItemsSearch( $limit = NULL, $locale = NULL, $item_id = NULL, $city = NULL, $region = NULL, $country = NULL, $only_null = NULL ) {
  $this->dao->select('i.*, d.*');
  $this->dao->from( DB_TABLE_PREFIX.'t_item i, ' . DB_TABLE_PREFIX.'t_item_description d, ' . DB_TABLE_PREFIX.'t_item_location t');

  if($limit == '') { $limit = 100; }

  $this->dao->where('i.pk_i_id = d.fk_i_item_id AND i.pk_i_id = t.fk_i_item_id');
  $this->dao->where('b_active', 1);
  $this->dao->where('b_enabled', 1);
  $this->dao->where('b_spam', 0);

  $this->dao->join(DB_TABLE_PREFIX . 't_item_radius r', 'i.pk_i_id = r.item_id', 'LEFT OUTER');

  if(!is_null($locale)) {
    $this->dao->where('d.fk_c_locale_code', $locale);
  }

  if(is_numeric($item_id) and $item_id <> '' and $item_id > 0) {
    $this->dao->where('i.pk_i_id', $item_id);
  }

  if($city <> '') {
    if(is_numeric($city) and $city > 0) {
      $this->dao->where('t.fk_i_city_id)', $city );
    } else {
      $this->dao->where('lower(t.s_city)', strtolower($city) );
    }
  }

  if($region <> '') {
    if(is_numeric($region) and $region > 0) {
      $this->dao->where('t.fk_i_region_id)', $region );
    } else {
      $this->dao->where('lower(t.s_region)', strtolower($region) );
    }
  }

  if($country <> '') {
    if(strlen($country) == 2) {
      $this->dao->where('lower(t.fk_c_country_code)', strtolower($country) );
    } else {
      $this->dao->where('lower(t.s_country)', strtolower($country) );
    }
  }

  if($only_null == 1) {
    $this->dao->where('coalesce(r.latitude, 0) = 0');
    $this->dao->where('coalesce(r.longitude, 0) = 0');
  }

  $this->dao->limit($limit);

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getItemsForCords($limit = NULL) {
  $this->dao->select('i.pk_i_id');
  $this->dao->from( DB_TABLE_PREFIX . 't_item i');
  $this->dao->join($this->getTable_Radius() . ' r', 'i.pk_i_id = r.item_id', 'LEFT OUTER');
  $this->dao->where('r.latitude is null or r.longitude is null or r.latitude = 0 or r.longitude = 0');

  $this->dao->where('b_active', 1);
  $this->dao->where('b_enabled', 1);
  $this->dao->where('b_spam', 0);

  if($limit <> '' && $limit > 0) {
    $this->dao->limit($limit);
  }

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCities( $start = NULL, $num_list = NULL ) {
  $this->dao->select();
  $this->dao->from( DB_TABLE_PREFIX.'t_city_radius' );

  if($start == '') { $start = 1; }
  if($num_list == '') { $num_list = 100; }
  $end = $start + $num_list - 1;

  $this->dao->where('rownum_id <= ' . $end );
  $this->dao->where('rownum_id >= ' . $start );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCitiesSearch( $limit = NULL, $city = NULL, $region = NULL, $country_code = NULL, $only_null = NULL ) {
  $this->dao->select('rad.*');
  //$this->dao->from( DB_TABLE_PREFIX.'t_city_radius as rad, ' . DB_TABLE_PREFIX.'t_city as c, ' . DB_TABLE_PREFIX.'t_region as r');
  $this->dao->from( DB_TABLE_PREFIX.'t_city as c, ' . DB_TABLE_PREFIX.'t_region as r');
  $this->dao->where('c.fk_i_region_id = r.pk_i_id');

  $this->dao->join(DB_TABLE_PREFIX . 't_city_radius as rad', 'c.pk_i_id = rad.city_id', 'LEFT OUTER');

  if($only_null == 1) {
    $this->dao->where('coalesce(rad.latitude, 0) = 0');
    $this->dao->where('coalesce(rad.latitude, 0) = 0');
  }

  if($country_code <> '') {
    $this->dao->where('c.fk_c_country_code', $country_code);
  }

  if($region <> '') {
    if(is_numeric($region) and $region > 0) {
      $this->dao->where('c.fk_i_region_id', $region);
    } else {
      $this->dao->where('r.s_name', $region);
    }
  }

  if($city <> '') {
    if(is_numeric($city) and $city > 0) {
      $this->dao->where('c.pk_i_id', $city);
    } else {
      $this->dao->where('c.s_name', $city);
    }
  }

  if($limit == '') { $limit = 100; }
  $this->dao->limit($limit);

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCitiesRegionsListNull( $start = NULL, $limit = NULL, $country_code = NULL ) {
  $this->dao->select('d.city_id city_id, c.s_name city_name, r.s_name region_name, c.fk_c_country_code country_code');
  $this->dao->from( DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX.'t_region r, ' . DB_TABLE_PREFIX . 't_city_radius d' );
  $this->dao->where('d.city_id = c.pk_i_id');
  $this->dao->where('c.fk_i_region_id = r.pk_i_id');
  $this->dao->where('coalesce(d.latitude, 999) = 999 and coalesce(d.longitude, 999) = 999');
  
  if($start <> '' && $start > 0) {
    $this->dao->where('d.city_id >= ' . $start);
  }

  if($country_code <> '') {
    $this->dao->where('c.fk_c_country_code', $country_code);
  }

  if($limit <> '' && $limit > 0) {
    $this->dao->limit($limit);
  }

  $this->dao->orderby('d.city_id ASC');

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCitiesRegionsListNullZero( $start = NULL, $limit = NULL, $country_code = NULL ) {
  $this->dao->select('d.city_id city_id, c.s_name city_name, r.s_name region_name, c.fk_c_country_code country_code, d.latitude, d.longitude');
  $this->dao->from( DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX.'t_region r, ' . DB_TABLE_PREFIX . 't_city_radius d' );
  $this->dao->where('d.city_id = c.pk_i_id');
  $this->dao->where('c.fk_i_region_id = r.pk_i_id');
  $this->dao->where('coalesce(d.latitude, 0) = 0 and coalesce(d.longitude, 0) = 0');

  if($start <> '') {
    $this->dao->where('d.city_id >= ' . $start);
  }

  if($country_code <> '') {
    $this->dao->where('c.fk_c_country_code', $country_code);
  }

  if($limit <> '' && $limit > 0) {
    $this->dao->limit($limit);
  }

  $this->dao->orderby('d.city_id ASC');
  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCitiesRegionsListFull( $start = NULL ) {
  $this->dao->select('c.pk_i_id city_id, c.s_name city_name, r.s_name region_name, c.fk_c_country_code country_code');
  $this->dao->from( DB_TABLE_PREFIX . 't_city c, ' . DB_TABLE_PREFIX.'t_region r' );
  $this->dao->where('c.fk_i_region_id = r.pk_i_id');
  
  if($start <> '') {
    $this->dao->where('c.pk_i_id >= ' . $start);
  }

  if($limit <> '' && $limit > 0) {
    $this->dao->limit($limit);
  }

  $this->dao->orderby('c.pk_i_id ASC');
  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCitiesInRadius( $lat, $long, $radius ) {
  $this->dao->select('distinct city_id as city_id, t.fk_i_region_id as region_id, t.fk_c_country_code as country_code');
  $this->dao->from( DB_TABLE_PREFIX . 't_city_radius r, ' . DB_TABLE_PREFIX . 't_item_location t' );
  $this->dao->where('r.city_id = t.fk_i_city_id');
  $this->dao->where('(latitude - ' . $lat . ') * (latitude - ' . $lat . ') + (longitude - ' . $long . ') * (longitude - ' . $long . ') <= ' . $radius * $radius);

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getItemLoc( $item_id ) {
  $this->dao->select('lo.*');
  $this->dao->from( DB_TABLE_PREFIX.'t_item_location as lo' );
  $this->dao->where('lo.fk_i_item_id', $item_id );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function getCityRegion( $region_id ) {
  $this->dao->select();
  $this->dao->from( DB_TABLE_PREFIX.'t_region' );
  $this->dao->where('pk_i_id', $region_id );

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  return $result->row();
}

public function countCitiesByCountry() {
  $this->dao->select('co.pk_c_code, co.s_name, count(c.pk_i_id) as city_total, count(rc.city_id) as city_complete, count(rb.city_id) as city_notfound, count(ra.city_id) as city_notfilled');
  $this->dao->from( DB_TABLE_PREFIX . 't_country as co, ' . DB_TABLE_PREFIX . 't_region as r, ' . DB_TABLE_PREFIX . 't_city as c');
  $this->dao->join(DB_TABLE_PREFIX . 't_city_radius as rc', 'c.pk_i_id = rc.city_id AND coalesce(rc.latitude, 0) <> 0', 'LEFT OUTER');
  $this->dao->join(DB_TABLE_PREFIX . 't_city_radius as rb', 'c.pk_i_id = rb.city_id AND rb.latitude = 0', 'LEFT OUTER');
  $this->dao->join(DB_TABLE_PREFIX . 't_city_radius as ra', 'c.pk_i_id = ra.city_id AND ra.latitude IS NULL', 'LEFT OUTER');
  $this->dao->where('co.pk_c_code = r.fk_c_country_code AND r.pk_i_id = c.fk_i_region_id');
  $this->dao->groupBy('co.s_name');


  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $prepare = $result->result();
  return $prepare;
}

public function getCityId( $city, $region = NULL, $country = NULL ) {
  $this->dao->select('c.pk_i_id as city_id');
  $this->dao->from( DB_TABLE_PREFIX.'t_city' . ' c, ' . DB_TABLE_PREFIX.'t_region' . ' as r, ' . DB_TABLE_PREFIX.'t_country' . ' as o' );
  $this->dao->where('c.fk_i_region_id = r.pk_i_id and c.fk_c_country_code = o.pk_c_code' );

  $this->dao->where('lower(c.s_name)', strtolower($city) );

  if($region <> '') {
    $this->dao->where('lower(r.s_name)', strtolower($region) );
  }

  if($country <> '') {
    if(strlen($country) == 2) {
      $this->dao->where('lower(c.fk_c_country_code)', strtolower($country) );
    } else {
      $this->dao->where('lower(o.s_name)', strtolower($country) );
    }
  }

  $result = $this->dao->get();
  if( !$result ) { return array(); }
  $data = $result->row();
  return isset($data['city_id']) ? $data['city_id'] : '';
}

// End of DAO Class
}
?>