<?php 
  $auto_zoom = osc_get_preference('map_zoom', 'plugin-rad_search') <> '' ? osc_get_preference('map_zoom', 'plugin-rad_search') : 0;
  $map_max_items = osc_get_preference('max_items', 'plugin-rad_search') <> '' ? osc_get_preference('max_items', 'plugin-rad_search') : 200;
  $map_cluster = osc_get_preference('map_cluster', 'plugin-rad_search') <> '' ? osc_get_preference('map_cluster', 'plugin-rad_search') : 1;
  $map_roads = osc_get_preference('map_roads', 'plugin-rad_search') <> '' ? osc_get_preference('map_roads', 'plugin-rad_search') : 0;


  // MAP FOR HOME PAGE SETTINGS
  $home_map_type = osc_get_preference('map_type_home', 'plugin-rad_search') <> '' ? osc_get_preference('map_type_home', 'plugin-rad_search') : 'ROADMAP';
  $home_map_width = osc_get_preference('map_width_home', 'plugin-rad_search') <> '' ? osc_get_preference('map_width_home', 'plugin-rad_search') : '100%';
  $home_map_height = osc_get_preference('map_height_home', 'plugin-rad_search') <> '' ? osc_get_preference('map_height_home', 'plugin-rad_search') : '300px';
  $home_only_premiums = osc_get_preference('only_premium_home', 'plugin-rad_search') <> '' ? osc_get_preference('only_premium_home', 'plugin-rad_search') : 0;


  // MAP FOR SEARCH PAGE SETTINGS
  $search_map_type = osc_get_preference('map_type_search', 'plugin-rad_search') <> '' ? osc_get_preference('map_type_search', 'plugin-rad_search') : 'ROADMAP';
  $search_map_width = osc_get_preference('map_width_search', 'plugin-rad_search') <> '' ? osc_get_preference('map_width_search', 'plugin-rad_search') : '100%';
  $search_map_height = osc_get_preference('map_height_search', 'plugin-rad_search') <> '' ? osc_get_preference('map_height_search', 'plugin-rad_search') : '300px';
  $search_only_premiums = osc_get_preference('only_premium_search', 'plugin-rad_search') <> '' ? osc_get_preference('only_premium_search', 'plugin-rad_search') : 0;


  // MAP FOR LISTING PAGE SETTINGS
  $item_map_type = osc_get_preference('map_type_item', 'plugin-rad_search') <> '' ? osc_get_preference('map_type_item', 'plugin-rad_search') : 'ROADMAP';
  $item_map_width = osc_get_preference('map_width_item', 'plugin-rad_search') <> '' ? osc_get_preference('map_width_item', 'plugin-rad_search') : '100%';
  $item_map_height = osc_get_preference('map_height_item', 'plugin-rad_search') <> '' ? osc_get_preference('map_height_item', 'plugin-rad_search') : '300px';
  $item_only_premiums = osc_get_preference('only_premium_item', 'plugin-rad_search') <> '' ? osc_get_preference('only_premium_item', 'plugin-rad_search') : 0;
  $item_same_cat = osc_get_preference('same_cat', 'plugin-rad_search') <> '' ? osc_get_preference('same_cat', 'plugin-rad_search') : 0;
  $item_map_hook = osc_get_preference('map_hook', 'plugin-rad_search') <> '' ? osc_get_preference('map_hook', 'plugin-rad_search') : 1;
  $item_listings_location = osc_get_preference('map_loc', 'plugin-rad_search') <> '' ? osc_get_preference('map_loc', 'plugin-rad_search') : 0;  // 0 == all, 1 == country, 2 == region, 3 == city

  $def_lat = osc_get_preference('def_lat', 'plugin-rad_search') <> '' ? osc_get_preference('def_lat', 'plugin-rad_search') : '';
  $def_long = osc_get_preference('def_long', 'plugin-rad_search') <> '' ? osc_get_preference('def_long', 'plugin-rad_search') : '';

  $default_icon = osc_get_preference('default_icon', 'plugin-rad_search') <> '' ? osc_get_preference('default_icon', 'plugin-rad_search') : '';


  // IF MAP IS NOT PLACED ON LISTING PAGE, GET CENTER COORDINATES FOR MAP
  $this_id = osc_item_id();
  if($this_id == '' or $this_id == 0) {
    $detail = array('latitude' => $def_lat, 'longitude' => $def_long);
  }


  // SEARCH FOR ITEMS
  $map_width = '100%';
  $map_height = '300px';
  $map_type = 'ROADMAP';

  $radiusSearch = new Search();

  // ADD PARAMETERS FOR HOME PAGE
  if(osc_is_home_page()) {
    $radiusSearch->onlyPremium($home_only_premiums);

    $map_width = $home_map_width <> '' ? $home_map_width : '100%';
    $map_height = $home_map_height <> '' ? $home_map_height : '300px';
    $map_type = $home_map_type <> '' ? $home_map_type : 'ROADMAP';
  }

  // ADD PARAMETERS FOR SEARCH PAGE
  if(osc_is_search_page()) {
    $map_width = $search_map_width <> '' ? $search_map_width : '100%';
    $map_height = $search_map_height <> '' ? $search_map_height : '300px';
    $map_type = $search_map_type <> '' ? $search_map_type : 'ROADMAP';

    $cat = osc_search_category_id();
    $country = osc_search_country();
    $reg = osc_search_region();
    $city = osc_search_city();
    $price_min = osc_search_price_min();
    $price_max = osc_search_price_max();
    $with_pic = osc_search_has_pic();
    $pattern = osc_search_pattern();

    if(isset($cat[0]) && $cat[0] <> '' && $cat[0] <> 0) { $radiusSearch->addCategory($cat[0]); }
    if($country <> '') { $radiusSearch->addCountry($country); }
    if($reg <> '') { $radiusSearch->addRegion($reg); }
    if($city <> '') { $radiusSearch->addCity($city); }
    if($price_min <> '' && $price_min > 0) { $radiusSearch->priceMin($price_min); }
    if($price_max <> '' && $price_max > 0) { $radiusSearch->priceMax($price_max); }
    if($with_pic <> '') { $radiusSearch->withPicture($with_pic); }
    if($pattern <> '') { $radiusSearch->addPattern($pattern); }

    $radiusSearch->onlyPremium($search_only_premiums);

    // GET CITIES FROM RADIUS
    $rad_cities = rad_search_conditions( isset($params) ? $params : '' );
    if(is_array($rad_cities) && !empty($rad_cities)) {
      $radiusSearch->addCity($rad_cities);
    }
  }

  // ADD PARAMETERS FOR LISTING PAGE
  if(osc_is_ad_page()) {
    $map_width = $item_map_width <> '' ? $item_map_width : '100%';
    $map_height = $item_map_height <> '' ? $item_map_height : '300px';
    $map_type = $item_map_type <> '' ? $item_map_type : 'ROADMAP';

    $radiusSearch->onlyPremium($item_only_premiums);

    if($item_same_cat == 1) {
      $radiusSearch->addCategory(osc_item_category_id());
    }

    if($item_listings_location == 1) {
      $item_country = osc_item_country_code() <> '' ? osc_item_country_code() : osc_item_country();

      if($item_country <> '') {
        $radiusSearch->addCountry($item_country);
      }
    } else if ($item_listings_location == 2) {
      $item_region = osc_item_region_id() <> '' ? osc_item_region_id() : osc_item_region();

      if($item_region <> '') {
        $radiusSearch->addRegion($item_region);
      }
    } else if ($item_listings_location == 3) {
      $item_city = osc_item_city_id() <> '' ? osc_item_city_id() : osc_item_city();

      if($item_city <> '') {
        $radiusSearch->addCity($item_city);
      }
    }
  }


  // MAKE SURE UNIT IS ENTERED
  if (strpos($map_width, '%') === false && strpos($map_width, 'px') === false) {
    $map_width = $map_width . 'px'; 
  }

  if (strpos($map_height, '%') === false && strpos($map_height, 'px') === false) {
    $map_height = $map_height . 'px';
  }


  $radiusSearch->limit(0, $map_max_items);
  $result = $radiusSearch->doSearch();

  View::newInstance()-> _exportVariableToView('items', $result );
?>



<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3"></script>

<?php if($map_cluster == 1) { ?>
  <script type="text/javascript" src="<?php echo osc_base_url(); ?>oc-content/plugins/rad_search/js/markerclusterer.js"></script>
<?php } ?>

<script type="text/javascript">
  var infowindow = null;

  $(document).ready(function () {
    initialize();
  });


  // DETECT USER POSITION AND MOVE MAP TO IT
  function DetectMyPosition() {
    var output = document.getElementById("detection-result");

    if (!navigator.geolocation){
      output.innerHTML = "<?php echo osc_esc_html(__('Geolocation is not supported by your browser', 'rad_search')); ?>";
      return;
    }

    function success(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;

      output.innerHTML = "<?php echo osc_esc_html(__('Done!', 'rad_search')); ?>";

      $('#map_radius_canvas').fadeOut(200);
      initialize(latitude, longitude, 1);
      $('#map_radius_canvas').fadeIn(200);

      setTimeout(function(){ 
        $(output).fadeOut(150, function() { 
          output.innerHTML = "<?php echo osc_esc_html(__('Show listings in my area', 'rad_search')); ?>"; 
          $(output).fadeIn(150); 
        }) 
      }, 5000);
    };

    function error(error) {
      output.innerHTML = "<?php echo osc_esc_html(__('Unable to retrieve your location', 'rad_search')); ?>" + " (" + error.code + ")";
      $('#detection-result').addClass('error');
    };


    // ERROR CODES
    // 0: unknown error
    // 1: permission denied
    // 2: position unavailable (error response from location provider)
    // 3: timed out


    output.innerHTML = "<?php echo osc_esc_html(__('Locating...', 'rad_search')); ?>";
    
    navigator.geolocation.getCurrentPosition(success, error);
  }


  // SHOW LISTINGS NEAR USER
  function ShowNearListings() {
    var output = document.getElementById("radius-show-now");

    if (!navigator.geolocation){
      output.innerHTML = "<?php echo osc_esc_js(__('Geolocation is not supported by your browser', 'rad_search')); ?>";
      return;
    }

    function success(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;
      var rad = $('#radius-show-value').val();

      if(rad == 0 || rad == '') {
        var rad = 100000;
      }

      //output.innerHTML = "<?php echo osc_esc_html(__('Done!', 'rad_search')); ?>";

      $('#map_radius_canvas').fadeOut(200);
      initialize('', '', 0, rad, latitude, longitude);
      $('#map_radius_canvas').fadeIn(200);
    };

    function error(error) {
      output.innerHTML = "<?php echo osc_esc_html(__('Unable to retrieve your location', 'rad_search')); ?>" + " (" + error.code + ")";
      $('#radius-show-now').addClass('error');
    };


    // ERROR CODES
    // 0: unknown error
    // 1: permission denied
    // 2: position unavailable (error response from location provider)
    // 3: timed out


    output.innerHTML = "<?php echo osc_esc_html(__('Locating...', 'rad_search')); ?>";
    
    navigator.geolocation.getCurrentPosition(success, error);
  }

  $(document).ready(function(){
    // WHEN SHOW LISTINGS IN MY RADIUS IS ACTIVATED
    $('.radius-show-now').click(function(){

    });
  });


  // CALCULATE DISTANCE BETWEEN 2 COORDINATES
  function GetDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var mes = "<?php echo getMeasure(); ?>";
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km

    if(mes == 'mile') {
      d = d*0.621371;
    }

    return d;
  }

  function deg2rad(deg) {
    return deg * (Math.PI/180)
  }


  // GET DISTANCES OF LISTINGS FROM USER POSITION
  function GetDistance() {
    var output = document.getElementById("distance-result");

    if (!navigator.geolocation){
      output.innerHTML = "<?php echo osc_esc_js(__('Geolocation is not supported by your browser', 'rad_search')); ?>";
      return;
    }

    function success(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;

      output.innerHTML = "<?php echo osc_esc_js(__('Calculated!', 'rad_search')); ?>";
      var mes = "<?php echo getMeasure(); ?>";

      // CALCULATE AND ADD DISTANCE TO OPENED INFOBOX
      js_cord.forEach(function(entry) {
        if($('#' + entry[0]).length) {
          $('#' + entry[0]).text(GetDistanceFromLatLonInKm(latitude, longitude, entry[1], entry[2]).toFixed(2) + mes );
          $('#' + entry[0]).attr('onclick', '');
          $('#' + entry[0]).removeClass('init');
        }
      });

      setTimeout(function(){ 
        $(output).fadeOut(150, function() { 
          output.innerHTML = "<?php echo osc_esc_html(__('How far is it?', 'rad_search')); ?>"; 
          $(output).fadeIn(150); 
        }); 
      }, 5000);
    };

    function error(error) {
      output.innerHTML = "<?php echo osc_esc_html(__('Unable to retrieve your location', 'rad_search')); ?>" + " (" + error.code + ")";
      $('#distance-result').addClass('error');
    };


    // ERROR CODES
    // 0: unknown error
    // 1: permission denied
    // 2: position unavailable (error response from location provider)
    // 3: timed out


    output.innerHTML = "<?php echo osc_esc_html(__('Locating...', 'rad_search')); ?>";
    
    navigator.geolocation.getCurrentPosition(success, error);
  }


  // MAP INITIALIZATION FUNCTION
  var sites = [];

  function initialize(def_lat, def_long, extra, rad, rad_lat, rad_long ) {
    var auto_zoom =  <?php echo $auto_zoom; ?>;

    <?php 
      if($this_id <> '' and $this_id <> 0) {
        $detail = ModelRadius::newInstance()->getCordByItemId( $this_id ); 
      }

      $detail['latitude'] = isset($detail['latitude']) ? $detail['latitude'] : '';
      $detail['longitude'] = isset($detail['longitude']) ? $detail['longitude'] : '';
    ?>

    if(def_lat != 0 && def_lat != '' && typeof def_lat !== 'undefined') {
      var center_lat = def_lat;
    } else {
      var center_lat = <?php echo $detail['latitude'] == '' ? 0 : $detail['latitude']; ?>
    }

    if(def_long != 0 && def_long != '' && typeof def_long !== 'undefined') {
      var center_long = def_long;
    } else {
      var center_long = <?php echo $detail['longitude'] == '' ? 0 : $detail['longitude']; ?>
    }

    if(typeof extra == 'undefined') {
      var extra = -1;
    }

    if(rad != 0 && rad != '' && typeof rad !== 'undefined') {
    } else {
      var rad = 0;
    }

    if(rad_lat != 0 && rad_lat != '' && typeof rad_lat !== 'undefined') {
    } else {
      var rad_lat = 0;
    }

    if(rad_long != 0 && rad_long != '' && typeof rad_long !== 'undefined') {
    } else {
      var rad_long = 0;
    }

    // SETTINGS FOR MAP
    var myOptions = {
      minZoom: 2,
      maxZoom: 16,
      center: { lat: center_lat, lng: center_long},
      mapTypeId: google.maps.MapTypeId.<?php echo $map_type; ?>
    }

    // CREATE MAP OBJECT
    var map = new google.maps.Map(document.getElementById("map_radius_canvas"), myOptions);

    <?php if($auto_zoom <> '' and $auto_zoom > 0) { ?>
      if(extra == 1) {
        map.setZoom(13);
      } else {
       map.setZoom(<?php echo $auto_zoom; ?>);
      }
    <?php } else { ?>
      if(extra == 1) {
        map.setZoom(13);
      }
    <?php } ?>

    <?php if($map_cluster == 1) { ?>
      // CLUSTER MARKERS
      var markerCluster = new MarkerClusterer(map);
    <?php } ?>


    // CREATE MAP MARKERS
    var sites = [
      <?php 
      $count_me = 1;
      osc_reset_items();
      $already = array();

      while(osc_has_items()) {
        $detail = ModelRadius::newInstance()->getCordByItemId( osc_item_id() );
        $detail['latitude'] = isset($detail['latitude']) ? $detail['latitude'] : '';
        $detail['longitude'] = isset($detail['longitude']) ? $detail['longitude'] : '';

        if(!in_array($detail['latitude'] . '-' . $detail['longitude'], $already)) {
          if($count_me <> 1 and $detail['latitude'] <> '' and $detail['longitude'] <> '' and $detail['latitude'] <> 0 and $detail['longitude'] <> 0) { echo ', '; } 

          if($detail['latitude'] <> '' and $detail['longitude'] <> '' and $detail['latitude'] <> 0 and $detail['longitude'] <> 0) {
            $count_me++;
            $already[] = $detail['latitude'] . '-' . $detail['longitude'];

            if(osc_count_item_resources()) {
              $img_src = osc_resource_thumbnail_url();
            } else {
              $img_src = osc_base_url() . 'oc-content/plugins/rad_search/images/no-image.png';
            }
            
            $text  = '<div id="radius-list-wrap">';
            $text .= '<a class="radius-list-img-wrap" href="' . osc_item_url() . '"><img src="' . $img_src . '"/><div class="radius-price">' . osc_format_price(osc_item_price()) . '</div></a>';
            $text .= '<a class="radius-list-title" href="'. osc_item_url() .'">'. addslashes(osc_item_title()) . '</a>';
            $text .= '<div id="distance' . osc_item_id() . '" class="distance-value init" onclick="GetDistance()">' . __('how far is it?', 'rad_search') . '</div>';
            $text .= '</div>';

            $cat_id = osc_item_category_id();
            $cat_id = radius_root_category($cat_id);

            if (file_exists(osc_base_path() . 'oc-content/plugins/rad_search/icons/' . $cat_id . '.png') && $default_icon == 0) {
              $marker_url = osc_base_url() . 'oc-content/plugins/rad_search/icons/' . $cat_id . '.png';
            } else {
              if(osc_item_id() == $this_id) { 
                $marker_url = osc_base_url() . 'oc-content/plugins/rad_search/images/marker-blue.png';
              } else {
                $marker_url = osc_base_url() . 'oc-content/plugins/rad_search/images/marker-red.png';
              }
            }
            ?>

            ['<?php echo addslashes(osc_item_title()); ?>', <?php echo $detail['latitude']; ?>, <?php echo $detail['longitude']; ?>, '<?php echo $text; ?>', '<?php echo $marker_url; ?>']
          <?php } 
          }          
      } ?>
    ];


    // WHEN USER SELECT TO SEE LISTINGS IN DISTANCE FROM HIM, FIND SUCH MARKERS
    if(rad != 0 && rad_lat != 0 && rad_long != 0) { 
      var indexes = new Array();
      var j = 0;
      var pass = 0;

      for (var i = 0; i < sites.length; i++) {
        var entry = sites[i];

        if(GetDistanceFromLatLonInKm(entry[1], entry[2], rad_lat, rad_long) > rad) {
          indexes[j] = i;
          j++
        } else {
          pass++;
        }
      }


      // IF AT LEAST 1 LISTING FOUND IN USER DISTANCE, REMOVE NOT MATCHING FROM ARRAY.
      if(pass > 0) {
        for (var i = 0; i < indexes.length; i++) {
          sites[indexes[i]] = '';
        }
      }
    }


    // CREATE MAP MARKERS OBJECT
    setMarkers(map, sites <?php if($map_cluster == 1) { ?>, markerCluster<?php } ?>);
    infowindow = new google.maps.InfoWindow({
      content: "loading..."
    });


    // CREATE LISTING COORDINATES FIELD FOR AUTO-CENTER MAP (only when auto-zoom is set to automatic)
    var LatLngList = new Array (
      <?php
        osc_reset_items();
        $map_cords = '';
        $count_me = 1;
        $already = array();
        $already_js = array();

        while(osc_has_items()) {
          $detail = ModelRadius::newInstance()->getCordByItemId( osc_item_id() );
          $detail['latitude'] = isset($detail['latitude']) ? $detail['latitude'] : '';
          $detail['longitude'] = isset($detail['longitude']) ? $detail['longitude'] : '';

          if(!in_array($detail['latitude'] . '-' . $detail['longitude'], $already)) {
            if($detail['latitude'] <> '' and $detail['longitude'] <> '' and $detail['latitude'] <> 0 and $detail['longitude'] <> 0) {
              if($count_me <> 1 && $map_cords <> '') { 
                $map_cords .= ', '; 
              }
  
              $map_cords .= 'new google.maps.LatLng(' . $detail['latitude'] . ', ' . $detail['longitude'] . ')';
              $already[] = $detail['latitude'] . '-' . $detail['longitude'];
              $already_js[] = array(osc_item_id(), $detail['latitude'],  $detail['longitude']);
              $count_me++;
            }
          }   
        }

        echo $map_cords; 
      ?>
    );


    // CALCULATE CENTER BY ADDING BOUNDARIES TO MAP
    if((auto_zoom == 0 && extra != 1) || (rad != 0 && rad_lat != 0 && rad_long != 0) || (auto_zoom > 0 && extra == 0)) {
      var bounds = new google.maps.LatLngBounds ();
      var counter = 0;
      for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
        if(rad != 0 && rad_lat != 0 && rad_long != 0) { 
          if(GetDistanceFromLatLonInKm(LatLngList[i].lat(), LatLngList[i].lng(), rad_lat, rad_long) < rad) {
            bounds.extend (LatLngList[i]);
            counter++;
          }
        } else {
          bounds.extend (LatLngList[i]);
        }
      }


      // IN CASE NO COORDINATES FOUND IN RADIUS, SHOW ALL LISTINGS
      if(counter == 0 && rad != 0 && rad_lat != 0 && rad_long != 0) {
        for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
          bounds.extend (LatLngList[i]);
        }

        $('#radius-show-now').html('<?php echo osc_esc_js(__('No listings found in entered distance', 'rad_search')); ?>');
        $('#radius-show-now').addClass('error');
        setTimeout(function(){ 
          $('#radius-show-now').fadeOut(150, function() { 
            $('#radius-show-now').html('<?php echo osc_esc_js(__('Show listings', 'rad_search')); ?>'); 
            $('#radius-show-now').removeClass('error'); 
            $('#radius-show-now').fadeIn(150);
            $('#radius-show-value').val('');
          }); 
        }, 3000);
      }

      map.fitBounds (bounds);


      // WHEN USER SEARCH LISTINGS NEAR TO HIM, DRAW CIRCLE
      if(counter > 0 && rad != 0 && rad_lat != 0 && rad_long != 0) {
        var marker = new google.maps.Marker({
          map: map,
          position: new google.maps.LatLng(rad_lat, rad_long),
          title: '<?php echo osc_esc_js(__('Your current position', 'rad_search')); ?>',
          icon: '<?php echo osc_base_url() . 'oc-content/plugins/rad_search/images/marker-green.png'; ?>'
        });

        // ADD CIRCLE AROUND MARKER
        if(rad < 100000) {
          var circle = new google.maps.Circle({
            map: map,
            radius: rad*1000,    // metres * 1000
            strokeColor: '#6DAE3F',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FFFFFF',
            fillOpacity: 0.22
          });

          circle.bindTo('center', marker, 'position');
        }

        $('#radius-show-now').html(counter + ' <?php echo osc_esc_js(__('listings found!', 'rad_search')); ?>');
        setTimeout(function(){ 
          $('#radius-show-now').fadeOut(150, function() { 
            $('#radius-show-now').html('<?php echo osc_esc_js(__('Show listings', 'rad_search')); ?>'); 
            $('#radius-show-now').fadeIn(150);
          }); 
        }, 5000);
      }
    }

    // WHEN USER SELECT TO MOVE TO ITS POSITION, ADD GREEN MARKER WHERE IS USER LOCATED
    if(extra == 1) {
      var marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(center_lat, center_long),
        title: '<?php echo osc_esc_js(__('Your current position', 'rad_search')); ?>',
        icon: '<?php echo osc_base_url() . 'oc-content/plugins/rad_search/images/marker-green.png'; ?>'
      });

      marker.setMap(map);
    }


    // ADD TRAFIC LAYER TO MAP
    <?php if($map_roads == 1) { ?>
      var trafficLayer = new google.maps.TrafficLayer();
      trafficLayer.setMap(map);
    <?php } ?>

  } // end of initialize function



  // GROUP TITLE, ICON, POSITION OF LISTING INTO MARKER
  function setMarkers(map, markers <?php if($map_cluster == 1) { ?>, markerCluster<?php } ?>) {
    for (var i = 0; i < markers.length; i++) {
      var site_m = markers[i];
      var siteLatLng = new google.maps.LatLng(site_m[1], site_m[2]);
      var marker = new google.maps.Marker({
        position: siteLatLng,
        map: map,
        title: site_m[0],
        html: site_m[3],
        icon: site_m[4]
      });

      <?php if($map_cluster == 1) { ?>
        markerCluster.addMarker(marker);
      <?php } ?>

      var contentString = "<?php echo osc_esc_html(__('Related items', 'radius')); ?>";

      google.maps.event.addListener(marker, "click", function () {
        infowindow.setContent(this.html);
        infowindow.open(map, this);
      });
    }
  }


  // COORDINATES OF EXISTING MARKERS/LISTINGS FOR FUNCTION GET DISTANCE
  var js_cord = new Array();
  <?php if(is_array($already_js) && !empty($already_js)) { ?>
    <?php foreach($already_js as $a) { ?>
      js_cord[<?php echo $a[0]; ?>] = new Array("distance<?php echo $a[0]; ?>", <?php echo $a[1]; ?>, <?php echo $a[2]; ?>);
    <?php } ?>
  <?php } ?>

</script>

<h2 class="radius_h2">
  <?php _e('Related listings in your area', 'rad_search'); ?> <!-- <span onclick="GetDistance();" id="distance-result" class="radius_locate">
  <?php _e('How far is it?', 'rad_search'); ?></span> --> <span onclick="DetectMyPosition();" id="detection-result" class="radius_locate">
  <?php _e('Show listings in my area', 'rad_search'); ?></span>
</h2>
<div id="map_radius_canvas" style="z-index:3;width:<?php echo $map_width; ?>;height:<?php echo $map_height; ?>;"></div>
<!--
<div id="map_radius_here">
  <span class="radius-show-before"><?php _e('Show only listings that are', 'rad_search'); ?></span>
  <input id="radius-show-value" type="number" min="0" step="1" />
  <span class="radius-show-desc"><?php echo getMeasure(); ?> <?php _e('distanced', 'rad_search'); ?></span>
  <span id="radius-show-now" class="radius-show-now radius_locate" onclick="ShowNearListings();"><?php _e('Show listings', 'rad_search'); ?></span>
</div>
-->