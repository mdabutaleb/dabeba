<?php
/* Set the default timezone */
date_default_timezone_set("America/Los_Angeles");

$time =  strtotime('next Monday -1 week', strtotime('this sunday'));
$date = new DateTime();
$date->setTimestamp($time);

// For the homepage, we need to search 
if (osc_category()['pk_i_id'] == 1) {
	$category = [
		'65',
		'40'
	];

	$url = osc_base_url() . "index.php?page=search&sCategory=65+40&date=";

} else {
	$category = osc_category()['pk_i_id'];
	$url = osc_base_url() . "index.php?page=search&sCategory=" . $category . "&date=";
}


/* Total days we need  (7 * 4) */
$totalDaysNeeded = 28;
$headers = ["M", "T", "W", "T", "F", "S", "S"];
$today = new DateTime();
?>
<div id="calendar">
	<fieldset>
		<?php if(osc_current_user_locale() == 'zh_TW') { ?>
            <h3 class="calendar-title">日曆</h3>
        <?php } else { ?>
            <h3 class="calendar-title">Calendar</h3>
        <?php } ?>
		<h4 style="line-height: 0;" class="calendar-subtitle">
			<?php _e($date->format('F'), 'custom_calendar') ?> <?php echo $date->format('Y') ?>
		</h4>
		<table id="custom_calendar_table" border="1" style="border:1px solid black; border-collapse:collapse;">
			<!-- Headers of the Calendar. -->
			<thead>
			<tr>
				<?php foreach ($headers as $header) { ?>
					<td>
						<?php echo $header ?>
					</td>
				<?php } ?>
			</tr>
			</thead>
				<tr>
					<?php $counter = 0; ?>
					<?php $printDate = 0; ?>
					<?php $hasURL = FALSE; ?>
					<?php for ($i = 0; $i < 28; $i++) { ?>
						<?php if ($counter == 7) { ?>
							<?php $counter = 0; ?>
							<?php echo '</tr>'; ?>
						<?php } ?>
					<td>
						<?php if ($i == 0) { ?>
							<?php $printDate = $date->modify('+' . $i . ' days'); ?>
						<?php } else { ?>
							<?php $printDate = $date->modify('+1 days'); ?>
						<?php } ?>

						<?php if($printDate->format('Y/m/d') > $today->format('Y/m/d')) {  ?>
							<?php $hasURL = TRUE; ?>
						<?php } ?>


						<?php if($hasURL) { ?>
							<a href="<?php echo $url. $printDate->format('Y/m/d'); ?>">
								<?php if ($printDate->format('Y/m/d') == $today->format('Y/m/d')) { ?>
									<b><?php echo $printDate->format('j'); ?></b>
								<?php } else { ?>
									<?php echo $printDate->format('j'); ?>
								<?php } ?>
							</a>
						<?php } else { ?>
							<?php if ($printDate->format('Y/m/d') == $today->format('Y/m/d')) { ?>
								<a href="<?php echo $url . $printDate->format('Y/m/d') ?>">
									<b><?php echo $printDate->format('j'); ?></b>
								</a>
							<?php } else { ?>
								<?php echo $printDate->format('j'); ?>
							<?php } ?>
						<?php } ?>

						<?php $counter++; ?>
					</td>
			<?php } ?>
			</tr>
		</table>
	</fieldset>
</div>
