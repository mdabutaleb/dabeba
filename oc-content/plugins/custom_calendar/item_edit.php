<script type="text/javascript">
    $(document).ready(function() {
        $("#date").datepicker(
            { 
                minDate: 0, 
                maxDate: "+1Y" ,
                dateFormat: 'yy-mm-dd'
            } 
        );
    });
</script>

<h2>
    <?php _e('Calendar details', 'custom_calendar') ; ?>
</h2>

<div>    
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('c_date') != '' ) {
                $detail['i_event_date'] = Session::newInstance()->_getForm('c_date');
            }
        ?>
        <label>
            <?php _e('Date', 'custom_calendar'); ?>
        </label>
        <input type="text" id="date" name="date" class="required" value="<?php echo @$detail['i_event_date']; ?>"/>
    </div>
</div>
