<h2>
    <?php _e('Time of event', 'custom_calendar');?>
</h2>

<table style="margin-left: 20px;">
    <?php if( !empty($detail['i_event_date']) ) { ?>
        <tr>
            <td width="150px">
                <label>
                    <?php _e('Date', 'custom_calendar'); ?>
                </label>
            </td>
            <td width="150px">
                <?php echo @$detail['i_event_date']; ?>
            </td>
        </tr>
    <?php } ?>
</table>