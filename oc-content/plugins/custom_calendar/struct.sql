CREATE TABLE `oc_t_item_calendar_attr` (
  `fk_i_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `i_event_date` date NOT NULL,
  PRIMARY KEY (`fk_i_item_id`),
  CONSTRAINT `oc_t_item_calendar_attr_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `oc_t_item` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;