<?php
/*
Plugin Name: Custom Calendar
Plugin URI: https://jonajo.com/
Description: Calendar functionality for events.
Version: 1.0
Author: Jonajo
Author URI: https://jonajo.com/
Short Name: calendar
*/

require_once 'ModelCalendar.php';


function custom_calendar_call_after_install() {
	ModelCalendar::newInstance()->import('custom_calendar/struct.sql');
}


function custom_calendar_call_after_uninstall() {
	ModelCalendar::newInstance()->uninstall();
}


function custom_calendar_search_form($catID = null) {

	if($catID == null) {
        return false;
    }
    
    // we check if the category is the same as our plugin
    foreach($catID as $id) {
        if( osc_is_this_category('calendar', $id) ) {
            include_once 'calendar.php';
            break;
        }
    }
}

function custom_make_calendar() {
    include_once 'calendar.php';
}


function calendar_config() {
    osc_plugin_configure_view(osc_plugin_path(__FILE__));
}


function calendar_form($catID = '') {
    
    // We received the categoryID
    if ($catID == '') {
        return false;
    }
    
    // check if the category is the same as our plugin
    if (osc_is_this_category('calendar', $catID)) { 
        require_once 'item_edit.php';
    }
}


function calendar_form_post($item) {
    $catID = isset($item['fk_i_category_id'])?$item['fk_i_category_id']:null;
    $itemID = isset($item['pk_i_id'])?$item['pk_i_id']:null;
    
    // we received the categoryID and the Item ID
    if($catID == null) {
        return false;
    }
    
    // We check if the category is the same as our plugin
    if( osc_is_this_category('calendar', $catID) && $itemID != null ) {
        $arrayInsert = _getCalendarParameters();
        ModelCalendar::newInstance()->insertCalendarAttr($arrayInsert, $itemID);
    }
}


function _getCalendarParameters() {
    $date = (Params::getParam("date") == '') ? null : Params::getParam("date");

    $array = array(
        'date'	=> $date,
    );

    return $array;
}


function calendar_pre_item_post() {
    Session::newInstance()->_setForm('c_date', Params::getParam("date"));
    Session::newInstance()->_keepForm('c_date');
}


// Needs tuning since we need this on the calendar itself as anchor tags.
function calendar_search_conditions($params = null) {

	if (is_null($params)) {
		return;
	}

	if (is_null($params['date']) || $params['date'] == '') {
		return;
	}
		
	$subquery = "SELECT fk_i_item_id FROM oc_t_item_calendar_attr WHERE i_event_date = '" . $params['date'] . "'";		
	Search::newInstance()->addConditions("pk_i_id IN (" . $subquery. ")");
}


// What appears on the listing page once you open an ad.
function calendar_item_detail() {
    
    if (osc_is_this_category('calendar', osc_item_category_id())) {
        
        $detail   = ModelCalendar::newInstance()->getCalendarAttr(osc_item_id()) ;
        
        if (count($detail) == 0) {
            return;
        }

        require_once 'item_detail.php' ;
    }
}

// Edit of the published ad..
function calendar_item_edit($catID = null, $itemID = null) {
    
    if (osc_is_this_category('calendar', $catID)) {        
        $detail = ModelCalendar::newInstance()->getCalendarAttr($itemID);
        require_once 'item_edit.php';
    }
}

// Item edited post.
function calendar_item_edit_post($item) {
    $catID = isset($item['fk_i_category_id']) ? $item['fk_i_category_id'] : null;
    $itemID = isset($item['pk_i_id']) ? $item['pk_i_id'] : null;
    
    // We received the categoryID and the Item ID
    if ($catID == null) {
        return false;
    }

    // We check if the category is the same as our plugin
    if( osc_is_this_category('calendar', $catID) ) {
        $arrayUpdate = _getCalendarParameters();
        ModelCalendar::newInstance()->updateCalendarAttr($arrayUpdate, $itemID);
    }
}

// Delete the calendar event item.
function calendar_delete_item($item_id) {
	ModelCalendar::newInstance()->deleteCalendarAttr($item_id);
}


/* HOOKS */
osc_register_plugin(osc_plugin_path(__FILE__), 'custom_calendar_call_after_install');
osc_add_hook(__FILE__ . "_configure", 'calendar_config');
osc_add_hook(osc_plugin_path(__FILE__) . "_uninstall", 'custom_calendar_call_after_uninstall');
osc_add_hook('item_form', 'calendar_form');
osc_add_hook('posted_item', 'calendar_form_post');
osc_add_hook('search_form', 'custom_calendar_search_form', 1);
osc_add_hook('search_conditions', 'calendar_search_conditions');
osc_add_hook('item_detail', 'calendar_item_detail');
osc_add_hook('item_edit', 'calendar_item_edit');
osc_add_hook('edited_item', 'calendar_item_edit_post');
osc_add_hook('delete_item', 'calendar_delete_item');
osc_add_hook('pre_item_post', 'calendar_pre_item_post');
?>
