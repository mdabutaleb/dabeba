<?php

    /**
     * Model database for Calendar Attributes table
     * 
     * @package CustomCalendar
     * @subpackage Model
     * @since 3.0
     */

    class ModelCalendar extends DAO {

        /**
         * It references to self object: ModelCalendar.
         * It is used as a singleton
         * 
         * @access private
         * @since 1.0
         * @var ModelCalendar
         */
        private static $instance;


        /**
         * It creates a new ModelCalendar object class ir if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since 1.0
         * @return ModelCalendar
         */
        public static function newInstance() {
            
            if(!self::$instance instanceof self) {
                self::$instance = new self ;
            }

            return self::$instance ;
        }


        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
        }
        

        /**
         * Return table name calendar attributes
         * @return string
         */
        public function getTable_CalendarAttr() {
            return DB_TABLE_PREFIX.'t_item_calendar_attr';
        }
        
        
        /**
         * Import sql file
         * @param type $file 
         */
        public function import($file) {
            $path = osc_plugin_resource($file) ;
            $sql = file_get_contents($path);

            if (!$this->dao->importSQL($sql)) {
                throw new Exception( "Error importSQL::ModelCalendar<br>".$file ) ;
            }
        }
        

        /**
         * Remove data and tables related to the plugin.
         */
        public function uninstall() {
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_CalendarAttr())) ;
        }
        

        /**
         * Get Calendar attributes given a item id
         *
         * @param int $itemId
         * @return array 
         */
        public function getCalendarAttr($itemId) {
            $this->dao->select();
            $this->dao->from($this->getTable_CalendarAttr());
            $this->dao->where('fk_i_item_id', $itemId);

            $result = $this->dao->get();

            if (!$result ) {
                return array() ;
            }

            return $result->row();
        }
        
        
        /**
         * Insert Calendar attributes 
         * 
         * @param array $arrayInsert 
         */
        public function insertCalendarAttr($arrayInsert, $itemId) {
            $aSet = $this->toArrayInsert($arrayInsert);
            $aSet['fk_i_item_id'] = $itemId;
            
            return $this->dao->insert($this->getTable_CalendarAttr(), $aSet);
        }
        
        
        /**
         * Update Car attributes given a item id
         * 
         * @param type $arrayUpdate 
         */
        public function updateCalendarAttr($arrayUpdate, $itemId) {
            $aUpdate = $this->toArrayInsert($arrayUpdate) ;
            return $this->_update( $this->getTable_CalendarAttr(), $aUpdate, array('fk_i_item_id' => $itemId));
        }
        
        
        /**
         * Delete Car attributes given a item id
         * 
         * @param int $itemId 
         */
        public function deleteCalendarAttr($itemId) {
            return $this->dao->delete( $this->getTable_CalendarAttr(), array('fk_i_item_id' => $itemId));
        }
        

        /**
         * Return an array, associates field name in database with the value
         * @param type $arrayInsert
         * @return type 
         */
        private function toArrayInsert($arrayInsert) {
            $array = array(
                'i_event_date'  =>  $arrayInsert['date'],
            );

            return $array;
        }
        

        function _update($table, $values, $where) {
            $this->dao->from($table) ;
            $this->dao->set($values) ;
            $this->dao->where($where) ;
            return $this->dao->update() ;
        }
    }
?>
